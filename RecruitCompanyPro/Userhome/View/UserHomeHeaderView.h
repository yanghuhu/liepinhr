//
//  UserHomeHeaderView.h
//  HSBCTempPro
//
//  Created by Michael on 2018/2/13.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  UserHomeHeaderViewDelegate
// 跳转用户信息编辑页面
- (void)toUserInfoEditVC;
@end

@interface UserHomeHeaderView : UIView

@property (nonatomic , weak) id<UserHomeHeaderViewDelegate>delegate;

//  更新用户信息展示
- (void)updateInfoAppear;

@end

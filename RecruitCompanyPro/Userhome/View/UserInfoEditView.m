//
//  UserInfoEditView.m
//  RecruitCompanyPro
//
//  Created by Michael on 2018/2/13.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "UserInfoEditView.h"

@interface UserInfoEditView(){
    
    UITextField * nickTextField; // 昵称tf
}
@end

@implementation UserInfoEditView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createSubViews];
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self createSubViews];
    }
    return self;
}

- (void)didMoveToSuperview{
    if([self superview]){
        NSString * nickName = [HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel.nickName;
        nickTextField.text = nickName?nickName:[HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel.cellphone;
    }
}

- (void)createSubViews{
    
    UILabel * nickNameLb = [[UILabel alloc] init];
    nickNameLb.font = [UIFont systemFontOfSize:15];
    nickNameLb.text = @"昵称:";
    [self addSubview:nickNameLb];
    [nickNameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).mas_offset(HorPxFit(30));
        make.top.equalTo(self).mas_offset(VerPxFit(30));
        make.size.mas_equalTo(CGSizeMake(50, VerPxFit(60)));
    }];
    
    nickTextField = [self tf];
    [nickTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(nickNameLb.mas_right);
        make.centerY.mas_equalTo(nickNameLb.mas_centerY).mas_offset(VerPxFit(-3));
        make.right.equalTo(self).mas_offset(-HorPxFit(20));
        make.height.mas_equalTo(VerPxFit(30));
    }];
}

- (NSString *)nickName{
    return nickTextField.text;
}

- (UITextField *)tf{
    UITextField * tf = [[UITextField alloc] init];
    tf.font = [UIFont systemFontOfSize:15];
    UIView * leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 0)];
    tf.leftView = leftView;
    tf.leftViewMode = UITextFieldViewModeAlways;
    [self addSubview:tf];
    
    UIView * bottomLine = [[UIView alloc] init];
    bottomLine.backgroundColor = COLOR(220, 200, 200, 1);
    [self addSubview:bottomLine];
    [bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(tf);
        make.top.mas_equalTo(tf.mas_bottom);
        make.height.mas_equalTo(1);
    }];
    return tf;
}

@end


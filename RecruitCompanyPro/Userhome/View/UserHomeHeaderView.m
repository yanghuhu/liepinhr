//
//  UserHomeHeaderView.m
//  HSBCTempPro
//
//  Created by Michael on 2018/2/13.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "UserHomeHeaderView.h"
#import "UIImage+YYAdd.h"
#import "UIImageView+YYWebImage.h"

@interface UserHomeHeaderView(){
    
    UIImageView * avatarImgV;  // 头像imageView
    UILabel * zhanghaoLb;      // 账号
    UILabel * nickLb;          // 昵称
    UILabel * sourceLb;        // 评分
}
@end

@implementation UserHomeHeaderView

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    
    UIImage * img = [UIImage imageNamed:@"avatar_placeholder"];
    UIImage * img_ = [img imageByRoundCornerRadius:HorPxFit(240)/2.0];
    avatarImgV = [[UIImageView alloc] initWithImage:img_];
    avatarImgV.backgroundColor = [UIColor clearColor];
    avatarImgV.clipsToBounds = YES;
    avatarImgV.userInteractionEnabled = YES;
    [self addSubview:avatarImgV];
    [avatarImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.mas_centerY);
        make.left.equalTo(self).mas_offset(HorPxFit(30));
        make.size.mas_equalTo(CGSizeMake(HorPxFit(95), VerPxFit(95)));
    }];
    avatarImgV.layer.cornerRadius = HorPxFit(95)/2.0;
    
    UIView  * avatarBkView = [[UIView alloc] init];
    avatarBkView.layer.cornerRadius = VerPxFit(100)/2;
    avatarBkView.backgroundColor = [UIColor whiteColor];
    [self insertSubview:avatarBkView belowSubview:avatarImgV];
    [avatarBkView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(avatarImgV.mas_centerX);
        make.centerY.mas_equalTo(avatarImgV.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(100), VerPxFit(100)));
    }];
    UITapGestureRecognizer * userInfoEditGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userInfoEdit)];
    [avatarImgV addGestureRecognizer:userInfoEditGesture];
    
    CGFloat titleW = 40;
  
    UILabel * zhanghaoTitleLb = [self lb:@"账号:"];
    if([[HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel.type isEqualToString:@"HR"]){
        [zhanghaoTitleLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(avatarBkView.mas_centerY);
            make.left.equalTo(avatarBkView.mas_right).mas_offset(VerPxFit(20));
            make.size.mas_equalTo(CGSizeMake(titleW, 25));
        }];
        UILabel * pingfenTitleLb = [self lb:@"评分:"];
        [pingfenTitleLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(zhanghaoTitleLb);
            make.top.mas_equalTo(zhanghaoTitleLb.mas_bottom).mas_offset(VerPxFit(10));
            make.size.mas_equalTo(CGSizeMake(titleW, 25));
        }];
        sourceLb = [self lb:nil];
        [sourceLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(pingfenTitleLb.mas_right).mas_offset(VerPxFit(10));
            make.centerY.mas_equalTo(pingfenTitleLb.mas_centerY);
            make.width.mas_equalTo(30);
            make.height.equalTo(pingfenTitleLb);
        }];
        UIImageView * startFlag = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"star"]];
        startFlag.contentMode = UIViewContentModeScaleAspectFit;
        [self addSubview:startFlag];
        [startFlag mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(sourceLb.mas_centerY);
            make.left.mas_equalTo(sourceLb.mas_right).mas_offset(HorPxFit(0));
            make.size.mas_equalTo(CGSizeMake(HorPxFit(40), HorPxFit(40)));
        }];
    }else{
        [zhanghaoTitleLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(avatarBkView.mas_centerY).mas_offset(VerPxFit(20));
            make.left.equalTo(avatarBkView.mas_right).mas_offset(VerPxFit(20));
            make.size.mas_equalTo(CGSizeMake(titleW, 25));
        }];
    }
   
    UILabel * nickTitleLb = [self lb:@"昵称:"];
    [nickTitleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(zhanghaoTitleLb);
        make.bottom.mas_equalTo(zhanghaoTitleLb.mas_top).offset(-VerPxFit(10));
        make.size.mas_equalTo(CGSizeMake(titleW, 25));
    }];
    
    nickLb = [self lb:nil];
    [nickLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(nickTitleLb.mas_right).mas_offset(VerPxFit(10));
        make.centerY.mas_equalTo(nickTitleLb.mas_centerY);
        make.right.equalTo(self).mas_offset(VerPxFit(10));
        make.height.equalTo(nickTitleLb);
    }];
    
    zhanghaoLb = [self lb:nil];
    [zhanghaoLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(zhanghaoTitleLb.mas_right).mas_offset(VerPxFit(10));
        make.centerY.mas_equalTo(zhanghaoTitleLb.mas_centerY);
        make.right.equalTo(self).mas_offset(VerPxFit(10));
        make.height.equalTo(zhanghaoTitleLb);
    }];
}

- (UILabel *)lb:(NSString *)text{
    UILabel * lb = [[UILabel alloc] init];
    if (text) {
        lb.text = text;
    }
    lb.font = [UIFont systemFontOfSize:15];
    lb.textColor = [UIColor whiteColor];
    [self addSubview:lb];
    return lb;
}

- (void)userInfoEdit{
    [self.delegate toUserInfoEditVC];
}

- (void)updateInfoAppear{
     [avatarImgV setImageWithURL:[NSURL URLWithString:[HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel.headPic] placeholder:[UIImage imageNamed:@"avatar_placeholder"]];
    if([[HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel.type isEqualToString:@"HR"]){
        sourceLb.text = [NSString stringWithFormat:@"%.1f",[HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel.score];
    }
    zhanghaoLb.text = [HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel.cellphone;
    NSString * nickName = [HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel.nickName;
    if(nickName){
        nickLb.text = nickName;
    }else{
        nickLb.text = [HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel.cellphone;
    }
}

@end

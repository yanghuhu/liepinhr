//
//  UserCenterViewController.m
//  RecruitCompanyPro
//
//  Created by Michael on 2018/2/9.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "UserCenterViewController.h"
#import "UIImage+YYAdd.h"
#import "UserHomeHeaderView.h"
#import "UserModule.h"
#import "SettingViewController.h"
#import "UserInfoEditViewController.h"
#import "LangeSetViewController.h"
#import "LoginViewController.h"
#import "BaseNavigationController.h"
#import "AppDelegate.h"
#import "ResetPwdViewController.h"
#import "VersionViewController.h"
#import "UserModule.h"

#define KEYUITableViewCell @"KEYUITableViewCell"

@interface UserCenterViewController ()<UITableViewDelegate,UITableViewDataSource,UserHomeHeaderViewDelegate,UserInfoEditViewControllerDelegate>{
    
    UserHomeHeaderView * headerView;   // 顶部view
}

@property (nonatomic, strong) UITableView * tableView_;
@property (nonatomic , strong) NSArray * titleArray;  // 标题数组
@property (nonatomic , strong) NSArray * iconArray;   // 标题对应的icon数组

@end

@implementation UserCenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"个人中心";
  
    UIBarButtonItem * item = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"back"] imageByTintColor:[UIColor whiteColor]] style:UIBarButtonItemStylePlain target:self action:@selector(backAction)];
    self.navigationItem.leftBarButtonItem  = item;
    
    self.view.backgroundColor = ViewControllerBkColor;
    self.titleArray = @[@[@"我的收藏",@"我的记录"],
                        @[@"语言设置"],
                        @[@"修改密码"],
                        @[@"意见反馈",@"关于猎聘",@"版本检测"]];
    self.iconArray= @[@"like",@"set",@"record"];
    
    [self createSubViews];
    [self getUserInfo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.navigationController.navigationBar.hidden = NO;
}

- (void)backAction{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)getUserInfo{
    [BaseHelper showProgressLoadingInView:self.view];
    @weakify(self)
    [UserModule getUserInfoSuccess:^(HRModel * usermodel){
        @strongify(self)
        [BaseHelper hideProgressHudInView:self.view];
        [HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel = usermodel;
        [headerView updateInfoAppear];
    } failure:^(NSString *error, ResponseType responseType) {
        @strongify(self)
        [BaseHelper hideProgressHudInView:self.view];
        if (error) {
            [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        }
    }];
}

- (void)createSubViews{
    
    UIImageView * bkImgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"userHomeBk"]];
    bkImgV.userInteractionEnabled = YES;
    [self.view addSubview:bkImgV];
    @weakify(self)
    [bkImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.and.right.equalTo(self.view);
        make.top.equalTo(self.view).mas_offset(NavigationBarHeight);
        make.bottom.equalTo(self.view);
    }];
    
    headerView = [[UserHomeHeaderView alloc] init];
    headerView.delegate = self;
    [self.view addSubview:headerView];
    [headerView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.and.right.equalTo(self.view);
        make.top.equalTo(self.view).mas_offset(NavigationBarHeight+VerPxFit(10));
        make.height.mas_equalTo(VerPxFit(170));
    }];
    
    [headerView updateInfoAppear];
    
    self.tableView_ = [[UITableView alloc] init];
    self.tableView_.delegate = self;
    self.tableView_.dataSource = self;
    self.tableView_.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView_.showsVerticalScrollIndicator = NO;
    self.tableView_.backgroundColor = [UIColor clearColor];
    [self.tableView_ registerClass:[UITableViewCell class] forCellReuseIdentifier:KEYUITableViewCell];
    [self.view addSubview:self.tableView_];
    [self.tableView_ mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.mas_equalTo(headerView.mas_bottom).mas_offset(VerPxFit(50));
        make.left.and.right.equalTo(self.view);
        make.bottom.equalTo(self.view).mas_offset(-VerPxFit(20));
    }];
    
    [BaseHelper setExtraCellLineHidden:self.tableView_];
    [headerView updateInfoAppear];
}

#pragma mark -- UserHomeHeaderViewDelegate
- (void)toUserInfoEditVC{
    UserInfoEditViewController * vc = [[UserInfoEditViewController alloc] init];
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark -- UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.titleArray.count+1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return section==self.titleArray.count ? 1 : [self.titleArray[section] count];

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    if (indexPath.section!=self.titleArray.count) {
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:KEYUITableViewCell forIndexPath:indexPath];
        cell.separatorInset = UIEdgeInsetsZero;
        cell.textLabel.text = self.titleArray[indexPath.section][indexPath.row];
        cell.textLabel.font = [UIFont systemFontOfSize:15];
        return cell;
        
    }else{
        static NSString * CellIdentifier = @"CellIdentifier_";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        cell.textLabel.font = [UIFont systemFontOfSize:15];
        UIButton * bt = [UIButton buttonWithType:UIButtonTypeCustom];
        [bt addTarget:self action:@selector(logoutAction) forControlEvents:UIControlEventTouchUpInside];
        bt.frame = CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 44);
        [bt setTitle:@"退出账号" forState:UIControlStateNormal];
        bt.titleLabel.font = [UIFont systemFontOfSize:15];
        [bt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [cell addSubview:bt];
        return cell;
    }

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 0){
        return 0;
    }else{
        return VerPxFit(20);
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 1){
        UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.frame), VerPxFit(50))];
        view.backgroundColor = [UIColor clearColor];
        return view;
    }else{
        return nil;
    }
}

#pragma mark -- UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    switch (indexPath.section) {
        case 0:
        {
            
        }
            break;
        case 1:
        {
            LangeSetViewController * vc = [[LangeSetViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 2:
        {
            ResetPwdViewController * vc = [[ResetPwdViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
            break;
        case 3:
        {
            if (indexPath.row == 0) {
                
            }else if (indexPath.row == 1){
                
            }else if (indexPath.row == 2){
                VersionViewController * vc = [[VersionViewController alloc] init];
                [self.navigationController pushViewController:vc animated:YES];
            }
        }
            break;
            
        default:
            break;
    }
}

#pragma mark -- UserInfoEditViewControllerDelegate
- (void)infoUpdateSuccess{
    [self getUserInfo];
}

#pragma mark -- PersonHomeHeaderDelegate
- (void)toUserSetPage{
    SettingViewController * vc = [[SettingViewController alloc] init];
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)logoutAction{
    [BaseHelper showProgressLoadingInView:self.view];
    [UserModule logoutSuccess:^{
        [BaseHelper hideProgressHudInView:self.view];
        [HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel = nil;
        LoginViewController * vc = [[LoginViewController alloc] init];
        BaseNavigationController * nav = [[BaseNavigationController alloc] initWithRootViewController:vc];
        AppDelegate * appdelete = (AppDelegate *)[UIApplication sharedApplication].delegate;
        appdelete.window.rootViewController = nav;
        [DataDefault removeWithKey:DataDefaultUserInfoKey isPrivate:NO];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        if (error) {
            [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        }
    }];
}

@end


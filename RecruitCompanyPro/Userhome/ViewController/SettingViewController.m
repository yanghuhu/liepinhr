//
//  SettingViewController.m
//  RecruitCompanyPro
//
//  Created by Michael on 2018/2/13.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "SettingViewController.h"
#import "LangeSetViewController.h"
#import "LoginViewController.h"
#import "BaseNavigationController.h"
#import "AppDelegate.h"
#import "ResetPwdViewController.h"
#import "VersionViewController.h"
#import "UserModule.h"

@interface SettingViewController ()<UITableViewDelegate,UITableViewDataSource>{

    UITableView * tableView;
}
@end

@implementation SettingViewController

- (void)viewDidLoad {
    self.view.backgroundColor = COLOR(244, 244, 244, 1);
    self.navigationItem.title = @"设置";
    
    tableView = [[UITableView alloc] init];
    tableView.delegate = self;
    tableView.backgroundColor = [UIColor clearColor];
    tableView.dataSource = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).mas_offset(VerPxFit(64));
        make.left.and.right.equalTo(self.view);
        make.bottom.equalTo(self.view);
    }];
    [BaseHelper setExtraCellLineHidden:tableView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)logoutAction{
    [BaseHelper showProgressLoadingInView:self.view];
    @weakify(self)
    [UserModule logoutSuccess:^{
        @strongify(self)
        [BaseHelper hideProgressHudInView:self.view];
        [HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel = nil;
        LoginViewController * vc = [[LoginViewController alloc] init];
        BaseNavigationController * nav = [[BaseNavigationController alloc] initWithRootViewController:vc];
        AppDelegate * appdelete = (AppDelegate *)[UIApplication sharedApplication].delegate;
        appdelete.window.rootViewController = nav;
        [DataDefault removeWithKey:DataDefaultUserInfoKey isPrivate:NO];
    } failure:^(NSString *error, ResponseType responseType) {
        @strongify(self)
        [BaseHelper hideProgressHudInView:self.view];
        if (error) {
            [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        }
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 2;
    }else if (section == 1){
        return 1;
    }
    else if (section == 2){
        return 3;
    }else if (section == 3){
        return 1;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 || indexPath.section == 1 || indexPath.section == 2) {
        static NSString * CellIdentifier = @"CellIdentifier";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.textLabel.font = [UIFont systemFontOfSize:15];
            UIView * bottomLine = [[UIView alloc] init];
            bottomLine.backgroundColor = COLOR(235, 235, 235, 1);
            [cell addSubview:bottomLine];
            [bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.and.right.equalTo(cell);
                make.bottom.mas_equalTo(cell.mas_bottom);
                make.height.mas_equalTo(1);
            }];
        }
        if (indexPath.section == 0) {
            if (indexPath.row == 0) {
                cell.textLabel.text = @"新消息设置";
            }else if (indexPath.row == 1){
                cell.textLabel.text = @"语言设置";
            }
        }else if (indexPath.section == 1){
            cell.textLabel.text = @"修改密码";
        }else if (indexPath.section == 2){
            if (indexPath.row == 0) {
                cell.textLabel.text = @"意见反馈";
            }else if (indexPath.row == 1){
                cell.textLabel.text = @"关于猎聘";
            }else if (indexPath.row == 2){
                cell.textLabel.text = @"版本检测";
            }
        }
        return cell;
    }else if (indexPath.section == 3){
        static NSString * CellIdentifier = @"CellIdentifier_";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        UIButton * bt = [UIButton buttonWithType:UIButtonTypeCustom];
        [bt addTarget:self action:@selector(logoutAction) forControlEvents:UIControlEventTouchUpInside];
        bt.frame = CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 44);
        [bt setTitle:@"退出账号" forState:UIControlStateNormal];
        bt.titleLabel.font = [UIFont systemFontOfSize:15];
        [bt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [cell addSubview:bt];
        return cell;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return nil;
    }
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(tableView.frame), 40)];
    
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(section == 0){
        return 0;
    }
    return VerPxFit(30);
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            
        }else if (indexPath.row == 1){
            LangeSetViewController * vc = [[LangeSetViewController alloc] init];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }else if (indexPath.section == 1){
        ResetPwdViewController * vc = [[ResetPwdViewController alloc] init];
        [self.navigationController pushViewController:vc animated:YES];
    }
    else if (indexPath.section == 2){
        if (indexPath.row == 0) {
            
        }else if (indexPath.row == 1){
            
        }else if (indexPath.row == 2){
            if (indexPath.row == 2) {
                VersionViewController * vc = [[VersionViewController alloc] init];
                [self.navigationController pushViewController:vc animated:YES];
            }
        }
    }
}

@end

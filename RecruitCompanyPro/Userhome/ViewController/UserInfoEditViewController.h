//
//  UserInfoEditViewController.h
//  RecruitCompanyPro
//
//  Created by Michael on 2018/2/13.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  UserInfoEditViewControllerDelegate
// 用户信息编辑成功
- (void)infoUpdateSuccess;
@end

@interface UserInfoEditViewController : UIViewController

@property (nonatomic , weak) id<UserInfoEditViewControllerDelegate>delegate;

@end

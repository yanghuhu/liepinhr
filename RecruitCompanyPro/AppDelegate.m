//
//  AppDelegate.m
//  RecruitCompanyPro
//
//  Created by Michael on 2017/11/28.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginViewController.h"
#import "JobAddViewController.h"
#import "HRModel.h"
#import "JobCenterViewController.h"
#import "HomeContainViewController.h"
#import <AlipaySDK/AlipaySDK.h>


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
    [IQKeyboardManager sharedManager].enable = YES;
    
    [HSBCGlobalInstance sharedHSBCGlobalInstance].isShowLibrary = NO;
    
    BOOL isloginAuto = [[DataDefault objectForKey:DataDefaultAutologinKey isPrivate:NO] boolValue];
    UIViewController * vc = nil;
    if (isloginAuto) {
        HRModel * model = [BaseHelper getUserFromDataDefault];
        if (model) {
            [HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel = model;
            [HSBCGlobalInstance sharedHSBCGlobalInstance].netWorkToken = model.netToken;
            if ([[HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel.type isEqualToString:@"HR"]) {
                vc = [[JobCenterViewController alloc] init];
            }else{
                vc = [[HomeContainViewController alloc] init];
            }
        }else{
            vc = [[LoginViewController alloc] init];
        }
    }else{
        vc = [[LoginViewController alloc] init];
    }
    UINavigationController * nav = [[UINavigationController alloc] initWithRootViewController:vc];
//    JobAddViewController * vc = [[JobAddViewController alloc] init];
    self.window.rootViewController = nav;
    [self.window makeKeyAndVisible];
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    if ([HSBCGlobalInstance sharedHSBCGlobalInstance].isShowLibrary)
        return UIInterfaceOrientationMaskAll;
    else
        return UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
}


- (BOOL)application:(UIApplication *)application openURL:(nonnull NSURL *)url options:(nonnull NSDictionary<NSString *,id> *)options {
    if (options) {
       if ([url.host isEqualToString:@"safepay"]) {
           
            // 支付跳转支付宝钱包进行支付，处理支付结果
            [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:PayResultDictionaryNotification object:resultDic];
            }];
        }
    }
    return YES;
}

// 9.0 之前走旧接口
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if ([url.host isEqualToString:@"safepay"]) {
        
        // 支付跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            [[NSNotificationCenter defaultCenter] postNotificationName:PayResultDictionaryNotification object:resultDic];
        }];
    }
    return YES;
}

@end

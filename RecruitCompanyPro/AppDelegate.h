//
//  AppDelegate.h
//  RecruitCompanyPro
//
//  Created by Michael on 2017/11/28.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end


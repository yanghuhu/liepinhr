//
//  ManageModule.m
//  RecruitCompanyPro
//
//  Created by Michael on 2018/1/9.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "ManageModule.h"
#import "NetworkHandle.h"
#import "JDAuditeModel.h"
#import "HRAuditeModel.h"
#import "RewardModel.h"

@implementation ManageModule

+ (void)jobAuditWithArray:(NSDictionary *)array
                  success:(RequestSuccessBlock)succBlock
                  failure:(RequestFailureBlock)failBlock{
    
    [NetworkHandle postRequestForApi:@"jobDetail/batchAudit" mockObj:nil params:array  handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        NSArray * content = response_[@"content"];
        if (content) {
            NSArray * models = [NSArray modelArrayWithClass:[JDAuditeModel class] json:[content modelToJSONObject]];
            return @[@YES,models];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)HRAuditWithArray:(NSDictionary *)array
                  success:(RequestSuccessBlock)succBlock
                  failure:(RequestFailureBlock)failBlock{
    [NetworkHandle postRequestForApi:@"user/auditHr" mockObj:nil params:array  handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        NSArray * content = response_[@"content"];
        if (content) {
            NSArray * models = [NSArray modelArrayWithClass:[JDAuditeModel class] json:[content modelToJSONObject]];
            return @[@YES,models];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)getJobAuditWaitWithPage:(NSInteger)page
                        success:(RequestSuccessBlock)succBlock
                        failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * dic = @{@"page":@(page),@"size":@(10)};
    
    [NetworkHandle getRequestForApi:@"jobDetail/getAuditJobDetails" mockObj:nil params:dic handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        NSArray * content = response_[@"content"];
        if (content) {
            NSArray * models = [NSArray modelArrayWithClass:[JDAuditeModel class] json:[content modelToJSONObject]];
            return @[@YES,models];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)getJobAuditFinishedWithPage:(NSInteger)page
                            success:(RequestSuccessBlock)succBlock
                            failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * dic = @{@"page":@(page),@"size":@(10)};
    
    [NetworkHandle getRequestForApi:@"jobDetail/getAuditedJobDetails" mockObj:nil params:dic handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        NSArray * content = response_[@"content"];
        if (content) {
            NSArray * models = [NSArray modelArrayWithClass:[JDAuditeModel class] json:[content modelToJSONObject]];
            return @[@YES,models];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)getHRAuditWaitWithPage:(NSInteger)page
                       success:(RequestSuccessBlock)succBlock
                       failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * dic = @{@"page":@(page),@"size":@(10)};
    
    [NetworkHandle getRequestForApi:@"user/getAuditHrs" mockObj:nil params:dic handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        NSArray * content = response_[@"content"];
        if (content) {
            NSArray * models = [NSArray modelArrayWithClass:[HRAuditeModel class] json:[content modelToJSONObject]];
            return @[@YES,models];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)getHRAuditFinishedWithPage:(NSInteger)page
                           success:(RequestSuccessBlock)succBlock
                           failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * dic = @{@"page":@(page),@"size":@(10)};
    
    [NetworkHandle getRequestForApi:@"user/getAuditedHrs" mockObj:nil params:dic handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        NSArray * content = response_[@"content"];
        if (content) {
            NSArray * models = [NSArray modelArrayWithClass:[HRAuditeModel class] json:[content modelToJSONObject]];
            return @[@YES,models];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
    
}
+ (void)getCompanyBalanceSuccess:(RequestSuccessBlock)succBlock
                         failure:(RequestFailureBlock)failBlock{
    
    [NetworkHandle getRequestForApi:@"account/companyAccount" mockObj:nil params:nil handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        NSNumber * balance = response_[@"balance"];
        if (balance) {
            NSInteger  ba = [balance integerValue]/100;
            return @[@YES,@(ba)];
        }else{
            return @[@YES];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)getCompanyRewardDetailWithStartTime:(NSTimeInterval)startTime
                                    endTime:(NSTimeInterval)endTime
                                       page:(NSInteger)page
                                 changeType:(NSString *)chargeType
                                    success:(RequestSuccessBlock)succBlock
                                    failure:(RequestFailureBlock)failBlock{
 
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    if (startTime != 0) {
        [dic setObject:@(startTime) forKey:@"startTime"];
    }
    if (endTime != 0) {
        [dic setObject:@(endTime) forKey:@"endTime"];
    }
    [dic setObject:@(page) forKey:@"page"];
    if (chargeType) {
        [dic setObject:chargeType forKey:@"changeType"];
    }
    [NetworkHandle getRequestForApi:@"account/getCompanyAccountChangeLogs" mockObj:nil params:dic handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        NSArray * dataArray = response_[@"content"];
        if (dataArray) {
            NSArray * models = [NSArray modelArrayWithClass:[RewardModel class] json:[dataArray modelToJSONObject]];
            return @[@YES,models];
        }else{
            return @[@YES];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

@end

//
//  ManageModule.h
//  RecruitCompanyPro
//
//  Created by Michael on 2018/1/9.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ManageModule : NSObject

/*
 * 职位审核
 * @param
 *   id_  职位id
 *   auditResult  审核结果
 */
+ (void)jobAuditWithArray:(NSDictionary *)array
                success:(RequestSuccessBlock)succBlock
                failure:(RequestFailureBlock)failBlock;

/*
 * HR审核
 * @param
 *   id_  职位id
 *   auditResult  审核结果
 */
+ (void)HRAuditWithArray:(NSDictionary *)array
                 success:(RequestSuccessBlock)succBlock
                 failure:(RequestFailureBlock)failBlock;


/*
 *  获取待审核职位列表
 *  @param
 *      page  页码
 *
 */
+ (void)getJobAuditWaitWithPage:(NSInteger)page
                        success:(RequestSuccessBlock)succBlock
                        failure:(RequestFailureBlock)failBlock;

/*
 *  获取已审核职位列表
 *  @param
 *      page  页码
 *
 */
+ (void)getJobAuditFinishedWithPage:(NSInteger)page
                        success:(RequestSuccessBlock)succBlock
                        failure:(RequestFailureBlock)failBlock;

/*
 *  获取待审核HR列表
 *  @param
 *      page  页码
 *
 */
+ (void)getHRAuditWaitWithPage:(NSInteger)page
                            success:(RequestSuccessBlock)succBlock
                            failure:(RequestFailureBlock)failBlock;

/*
 *  获取已审核HR列表
 *  @param
 *      page  页码
 *
 */
+ (void)getHRAuditFinishedWithPage:(NSInteger)page
                           success:(RequestSuccessBlock)succBlock
                           failure:(RequestFailureBlock)failBlock;

/*
 *  获取账户信息
 */
+ (void)getCompanyBalanceSuccess:(RequestSuccessBlock)succBlock
                        failure:(RequestFailureBlock)failBlock;

/*
 *  获取公司收支明细
 *      startTime  起始时间
 *      endTime   截止时间
 *      page    页码
 *      chargeType  数据类型   RECHARGE(充值)，ORDER_PAYMENT(订单支付)，ORDER_REFUND(订单退款)
 */
+ (void)getCompanyRewardDetailWithStartTime:(NSTimeInterval)startTime
                                    endTime:(NSTimeInterval)endTime
                                       page:(NSInteger)page
                                 changeType:(NSString *)chargeType
                                    success:(RequestSuccessBlock)succBlock
                                    failure:(RequestFailureBlock)failBlock;
@end

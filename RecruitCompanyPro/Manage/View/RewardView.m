//
//  RewardView.m
//  RecruitCompanyPro
//
//  Created by Michael on 2018/1/26.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "RewardView.h"
#import "RewardHeaderView.h"
#import "RewardListCell.h"
#import "RewardListTitleView.h"
#import "RewardModel.h"

@interface RewardView()<UITableViewDelegate,UITableViewDataSource,RewardHeaderViewDelegate>{
    
    RewardHeaderView * rewardHeaderView;
    UITableView * tableView;
    NSInteger pageNum;   //  页码
}

@property (nonatomic , strong) NSDate * startTime;     // 起始时间
@property (nonatomic , strong) NSDate * endTime;       // 终止时间
@property (nonatomic , strong) NSString  * dataType;   // 数据类型，入帐/出账

@end

@implementation RewardView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    CGFloat selfW = ScreenWidth - HorPxFit(245);
    rewardHeaderView = [[RewardHeaderView alloc] initWithFrame:CGRectMake(0, VerPxFit(20), selfW, VerPxFit(290))];
    rewardHeaderView.delegate = self;
    [self addSubview:rewardHeaderView];
    
    UILabel * lb = [[UILabel alloc] init];
    lb.text = @"赏金明细";
    lb.textColor = [UIColor whiteColor];
    lb.font = [UIFont systemFontOfSize:16];
    [self addSubview:lb];
    [lb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).mas_equalTo(HorPxFit(40));
        make.top.mas_equalTo(rewardHeaderView.mas_bottom).mas_offset(VerPxFit(0));
        make.right.equalTo(self).mas_offset(-HorPxFit(40));
        make.height.mas_equalTo(25);
    }];
    
    RewardListTitleView * titleView = [[RewardListTitleView alloc] init];
    [self addSubview:titleView];
    [titleView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).mas_offset(HorPxFit(40));
        make.top.mas_equalTo(lb.mas_bottom).mas_offset(VerPxFit(16));
        make.right.equalTo(self).mas_offset(-HorPxFit(40));
        make.height.mas_equalTo(VerPxFit(40));
    }];
    
    pageNum = 0;
    
    tableView = [[UITableView alloc] init];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.backgroundColor = [UIColor clearColor];
    [self addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).mas_offset(HorPxFit(40));
        make.top.mas_equalTo(titleView.mas_bottom);
        make.right.equalTo(self).mas_offset(-HorPxFit(40));
        make.bottom.equalTo(self).mas_offset(-VerPxFit(15));
    }];
    @weakify(self)
    tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        @strongify(self)
        [self changeDataSourceWithState:TableViewDataSourceChangeForRefresh];
    }];
    tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        @strongify(self)
        [self changeDataSourceWithState:TableViewDataSourceChangeForGetMore];
    }];
}

- (void)didMoveToSuperview{
    [super didMoveToSuperview];
    if (!self.dataSource) {
        [self changeDataSourceWithState:TableViewDataSourceChangeForRefresh];
    }
}

- (void)updateBalanceAppear:(NSInteger)balance{
    rewardHeaderView.balance = balance;
}

- (void)changeDataSourceWithState:(TableViewDataSourceChange) changeType{
    if (changeType == TableViewDataSourceChangeForGetMore) {
        //  加载更多
        pageNum ++;
    }else{
        // 下拉刷新
        pageNum = 0;
    }
    [self.delegate getRewardDataWithStartTime:self.startTime endTiem:self.endTime dataType:self.dataType withPage:pageNum];
}

- (void)setDataSource:(NSMutableArray *)dataSource{
    if (!_dataSource) {
        _dataSource = [NSMutableArray array];
    }
    if (pageNum == 0) {
        [_dataSource removeAllObjects];
    }
    [_dataSource addObjectsFromArray:dataSource];
    [tableView.mj_header endRefreshing];
    [tableView.mj_footer endRefreshing];
    [tableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return _dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * CellIdentifier = @"CellIdentifier";
    RewardListCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        cell = [[RewardListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    RewardModel * model = _dataSource[indexPath.row];
    cell.rewardModel = model;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark --  RewardHeaderViewDelegate
- (void)getRewardDataWithStartTime:(NSDate *)startDate endDate:(NSDate *)endDate withType:(NSString *)type{
    pageNum = 0;
    self.startTime = startDate;
    self.endTime = endDate;
    self.dataType = type;
    [self.delegate getRewardDataWithStartTime:startDate endTiem:endDate dataType:type withPage:pageNum];
}

@end

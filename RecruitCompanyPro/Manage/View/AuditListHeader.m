//
//  AuditListHeader.m
//  RecruitCompanyPro
//
//  Created by Michael on 2018/1/10.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "AuditListHeader.h"

@interface AuditListHeader(){
    
    UIButton * passAllBt;   // 通过的一键全选按钮
    UIButton * failAllBt;    // 不通过的一键全选按钮
}
@end

@implementation AuditListHeader

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self createSubViews];
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    @weakify(self)
    UILabel * jobNumLb = [self lb:@"工号"];
    [jobNumLb mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.and.top.equalTo(self);
        make.width.mas_equalTo(HorPxFit(110));
        make.height.mas_equalTo(HRAuditListHeaderH);
    }];
    
    UILabel * nameLb = [self lb:@"用户名"];
    [nameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.equalTo(self);
        make.left.mas_equalTo(jobNumLb.mas_right);
        make.width.mas_equalTo(HorPxFit(110));
        make.height.mas_equalTo(HRAuditListHeaderH);
    }];
    
    UILabel * idCardLb = [self lb:@"身份证号"];
    [idCardLb mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.equalTo(self);
        make.left.mas_equalTo(nameLb.mas_right);
        make.width.mas_equalTo(HorPxFit(170));
        make.height.mas_equalTo(HRAuditListHeaderH);
    }];
    
    UILabel * positionLb = [self lb:@"部门"];
    [positionLb mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.equalTo(self);
        make.left.mas_equalTo(idCardLb.mas_right);
        make.width.mas_equalTo(HorPxFit(120));
        make.height.mas_equalTo(HRAuditListHeaderH);
    }];
    
    UILabel * dealLb = [self lb:@"处理"];
    [dealLb mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.equalTo(self);
        make.left.mas_equalTo(positionLb.mas_right);
        make.right.equalTo(self);
        make.height.mas_equalTo(HRAuditListHeaderH);
    }];
    
    passAllBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [passAllBt addTarget:self action:@selector(passAllBtAction) forControlEvents:UIControlEventTouchUpInside];
    passAllBt.titleLabel.font = [UIFont systemFontOfSize:12];
    passAllBt.backgroundColor = [UIColor clearColor];
    passAllBt.selected = NO;
    [passAllBt setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [passAllBt setTitle:@"通过全选" forState:UIControlStateNormal];
    [self addSubview:passAllBt];
    [passAllBt mas_updateConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(dealLb.mas_bottom);
        make.right.equalTo(self);
        make.height.mas_equalTo(HRAuditListHeaderHWithActionBt-HRAuditListHeaderH);
        make.width.mas_equalTo(HorPxFit(70));
    }];
    
    failAllBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [failAllBt addTarget:self action:@selector(failAllBtAction) forControlEvents:UIControlEventTouchUpInside];
    [failAllBt setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    failAllBt.backgroundColor = [UIColor clearColor];
    [failAllBt setTitle:@"不通过全选" forState:UIControlStateNormal];
    failAllBt.selected = NO;
    failAllBt.titleLabel.font = [UIFont systemFontOfSize:12];
    [self addSubview:failAllBt];
    [failAllBt mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(passAllBt.mas_left);
        make.top.mas_equalTo(dealLb.mas_bottom);
        make.width.mas_equalTo(HorPxFit(70));
        make.height.mas_equalTo(HRAuditListHeaderHWithActionBt-HRAuditListHeaderH);
    }];
}

- (void)actionBtAppearShift:(BOOL)appearType{
    if (appearType) {
        failAllBt.hidden = NO;
        passAllBt.hidden = NO;
    }else{
        failAllBt.hidden = YES;
        passAllBt.hidden = YES;
    }
}

- (void)failAllBtAction{
    failAllBt.selected = !failAllBt.selected;
    if (failAllBt.selected) {
        [failAllBt setTitleColor:COLOR(195, 4, 0, 1) forState:UIControlStateNormal];
        [passAllBt setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        passAllBt.selected = NO;
    }else{
        [failAllBt setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    }
    [self.delegate hrFailAllSelected:failAllBt.selected];
}

- (void)passAllBtAction{
    passAllBt.selected = !passAllBt.selected;
    if (passAllBt.selected) {
        [passAllBt setTitleColor:COLOR(40, 255, 32, 1) forState:UIControlStateNormal];
        [failAllBt setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        failAllBt.selected = NO;
    }else{
        [passAllBt setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    }
    [self.delegate hrPassAllSelected:passAllBt.selected];
}

- (UILabel *)lb:(NSString *)text{
    UILabel * lb = [[UILabel alloc] init];
    lb.textAlignment = NSTextAlignmentCenter;
    lb.font = [UIFont systemFontOfSize:15];
    lb.textColor = [UIColor whiteColor];
    lb.text = text;
    lb.backgroundColor = [UIColor clearColor];
    [self addSubview:lb];
    return lb;
}

@end

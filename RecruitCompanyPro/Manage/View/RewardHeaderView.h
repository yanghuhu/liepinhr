//
//  RewardHeaderView.h
//  RecruitCompanyPro
//
//  Created by Michael on 2018/1/25.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RewardHeaderViewDelegate
//  根据选择条件获取明细数据
- (void)getRewardDataWithStartTime:(NSDate *)date  endDate:(NSDate *)date withType:(NSString *)type;
@end

@interface RewardHeaderView : UIView

@property (nonatomic , strong) NSDate * startDate;     // 起始时间
@property (nonatomic , strong) NSDate * endDate;        // 截止时间
@property (nonatomic , assign) NSInteger balance;   // 总金额

@property (nonatomic , weak) id<RewardHeaderViewDelegate>delegate;

@end

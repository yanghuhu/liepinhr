//
//  AuditView.m
//  RecruitCompanyPro
//
//  Created by Michael on 2018/1/26.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "AuditView.h"
#import "AuditListHeader.h"
#import "AuditJDListHeader.h"
#import "UIView+YYAdd.h"
#import "HRTableViewCell.h"
#import "JDTableViewCell.h"

#define ListActionBtUnselectColor COLOR(70, 71, 70, 1)      // 一键全选按钮不选中颜色
#define ListActionBtSelectColor  [UIColor whiteColor]       // 一键全选按钮选中颜色

@interface AuditView()<AuditListHeaderDelegate,AuditJDListHeaderDelegate,UITableViewDelegate,UITableViewDataSource,JDTableViewCellDelegate>{
    
    UITableView * tableView;
    UIButton * dealWaitBt;     // 列表切换，待审核按钮
    UIButton * dealFinishBt;   // 列表切换，已审核按钮
    UIButton * submitBt;     // 审核数据提交按钮
    UIView * headerView;    //   列表头容器view
    
    NSInteger hrWaitPage;       //  hr待审核列表页码
    NSInteger hrFinishedPage;    //  hr 已审核列表页码
    NSInteger JDWaitPage;       //  职位待审核列表页码
    NSInteger JDFinishedPage;   //  职位待审核列表页码
}

@property (nonatomic , assign) AuditListAppearType appearType;   // 显示类型，待处理/已处理

@property (nonatomic , strong) AuditListHeader * hrListHeader;     // hr列表头view
@property (nonatomic , strong) AuditJDListHeader * jdListHeader;   // jd列表头view

@property (nonatomic , strong) JDAuditeModel * jdAuditeModelShowNote;    // 当前展示note的JDModel

@property (nonatomic , strong) NSMutableArray * hrWaitDataSource;           // hr待审核列表数据源
@property (nonatomic , strong) NSMutableArray * hrFinishedDataSource;       //  hr已审核列表数据源
@property (nonatomic , strong) NSMutableArray * jdWaitDataSource;           //  职位待审核列表数据源
@property (nonatomic , strong) NSMutableArray * jdFinishDataSource;         //  职位已审核列表数据源
@end

@implementation AuditView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        hrWaitPage = 0;
        hrFinishedPage = 0;
        JDWaitPage = 0;
        JDFinishedPage = 0;
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    @weakify(self)

    
    dealWaitBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [dealWaitBt setTitle:@"未处理" forState:UIControlStateNormal];
    [dealWaitBt setBackgroundImage:[UIImage imageNamed:@"listActionBt"] forState:UIControlStateNormal];
    dealWaitBt.titleLabel.font = [UIFont systemFontOfSize:15];
    [dealWaitBt setTitleColor:ListActionBtSelectColor forState:UIControlStateNormal];
    [dealWaitBt addTarget:self action:@selector(listShiftAction:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:dealWaitBt];
    [dealWaitBt mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.right.equalTo(self).mas_offset(-HorPxFit(40));
        make.size.mas_equalTo(CGSizeMake(HorPxFit(80), VerPxFit(30)));
        make.top.equalTo(self).mas_offset(40);
    }];
    
    dealFinishBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [dealFinishBt setTitle:@"已处理" forState:UIControlStateNormal];
    [dealFinishBt setTitleColor:ListActionBtUnselectColor forState:UIControlStateNormal];
    [dealFinishBt addTarget:self action:@selector(listShiftAction:) forControlEvents:UIControlEventTouchUpInside];
    dealFinishBt.titleLabel.font = [UIFont systemFontOfSize:15];
    [self addSubview:dealFinishBt];
    [dealFinishBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(dealWaitBt.mas_left).mas_offset(-HorPxFit(10));
        make.size.mas_equalTo(CGSizeMake(HorPxFit(80), VerPxFit(30)));
        make.centerY.mas_equalTo(dealWaitBt.mas_centerY);
    }];
    
    headerView = [[UIView alloc] initWithFrame:CGRectMake(HorPxFit(60), VerPxFit(40)+VerPxFit(35)+VerPxFit(20), CGRectGetWidth(self.frame)-HorPxFit(60)*2, HRAuditListHeaderHWithActionBt)];
    headerView.clipsToBounds = YES;
    headerView.backgroundColor = [UIColor clearColor];
    [self addSubview:headerView];
    
    tableView = [[UITableView alloc] init];
    tableView.backgroundColor = [UIColor clearColor];
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self addSubview:tableView];
    [tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.mas_equalTo(headerView.mas_bottom);
        make.left.and.right.equalTo(headerView);
        make.bottom.equalTo(self).mas_offset(-VerPxFit(80));
    }];
    
    tableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        if (_listType == ListTypeJD) {
            if (_appearType == AuditListAppearTypeWaitDeal) {
                JDWaitPage = 0;
                [self.delegate getJDWaitauditWithPage:JDWaitPage];
            }else{
                JDFinishedPage = 0;
                [self.delegate getJDAuditFinishWithPage:JDFinishedPage];
            }
        }else if(_listType == ListTypeHR){
            if (_appearType == AuditListAppearTypeWaitDeal) {
                hrWaitPage = 0;
                [self.delegate getHRWaitauditWithPage:hrWaitPage];
            }else{
                hrFinishedPage = 0;
                [self.delegate getHRAuditFinishWithPage:hrFinishedPage];
            }
        }
    }];
    tableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        if (_listType == ListTypeJD) {
            if (_appearType == AuditListAppearTypeWaitDeal) {
                JDWaitPage +=1;
                [self.delegate getJDWaitauditWithPage:JDWaitPage];
            }else{
                JDFinishedPage +=1;
                [self.delegate getJDAuditFinishWithPage:JDFinishedPage];
            }
        }else if(_listType == ListTypeHR){
            if (_appearType == AuditListAppearTypeWaitDeal) {
                hrWaitPage +=1;
                [self.delegate getHRWaitauditWithPage:hrWaitPage];
            }else{
                hrFinishedPage +=1;
                [self.delegate getHRAuditFinishWithPage:hrFinishedPage];
            }
        }
    }];
    
    submitBt = [UIButton buttonWithType:UIButtonTypeCustom];
    submitBt.backgroundColor = [UIColor clearColor];
    [submitBt setTitle:@"提交" forState:UIControlStateNormal];
    [submitBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [submitBt addTarget:self action:@selector(auditSubmitAction) forControlEvents:UIControlEventTouchUpInside];
    [submitBt setBackgroundImage:[UIImage imageNamed:@"submitBtbk"] forState:UIControlStateNormal];
    submitBt.titleLabel.font = [UIFont systemFontOfSize:15];
    [self insertSubview:submitBt atIndex:0];
    [submitBt mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.centerX.mas_equalTo(tableView.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(280), VerPxFit(68)));
        make.bottom.mas_equalTo(self).mas_offset(-VerPxFit(10));
    }];
}

- (void)auditSubmitAction{
    if (_listType == ListTypeHR) {
        NSMutableArray * tempArray = [NSMutableArray array];
        for (HRAuditeModel * model in _hrWaitDataSource) {
            if (model.auditString) {
                [tempArray addObject:model];
            }
        }
        [self.delegate hrAudtionActionWithData:tempArray];
    }else{
        NSMutableArray * tempArray = [NSMutableArray array];
        for (JDAuditeModel * model in _jdWaitDataSource) {
            if (model.auditString) {
                [tempArray addObject:model];
            }
        }
        [self.delegate jdAudtionActionWithData:tempArray];
    }
}

#pragma mark publich methods
- (void)setListType:(ListType)listType{
    _listType = listType;
    if (listType == ListTypeHR) {
        if (self.jdListHeader) {
            [self.jdListHeader removeFromSuperview];
            [headerView removeAllSubviews];
        }
        self.hrListHeader = [[AuditListHeader alloc] init];
        _hrListHeader.delegate = self;
        [headerView addSubview:_hrListHeader];
        [_hrListHeader mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.and.right.and.bottom.and.top.equalTo(headerView);
        }];
        [tableView reloadData];
        if (_appearType == AuditListAppearTypeWaitDeal) {
            if (!_hrWaitDataSource) {
                [BaseHelper showProgressLoading];
                [self.delegate getHRWaitauditWithPage:hrWaitPage];
            }
        }else{
            if (!_hrFinishedDataSource) {
                [BaseHelper showProgressLoading];
                [self.delegate getHRAuditFinishWithPage:hrFinishedPage];
            }
        }
    }else if (listType == ListTypeJD){
        if (_hrListHeader) {
            [_hrListHeader removeFromSuperview];
        }
        self.jdListHeader = [[AuditJDListHeader alloc] init];
        _jdListHeader.delegate = self;
        [headerView addSubview:_jdListHeader];
        [_jdListHeader mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.and.right.and.bottom.and.top.equalTo(headerView);
        }];
        
        [tableView reloadData];
        if (_appearType == AuditListAppearTypeWaitDeal) {
            if (!_jdWaitDataSource) {
                [BaseHelper showProgressLoading];
                [self.delegate getJDWaitauditWithPage:JDWaitPage];
            }
        }else{
            if (!_jdFinishDataSource) {
                [BaseHelper showProgressLoading];
                [self.delegate getJDAuditFinishWithPage:JDFinishedPage];
            }
        }
    }
}

- (void)refreshJDWaitauditFinish:(BOOL)isSuccess withArray:(NSArray<JDAuditeModel*>*)array{
    [BaseHelper hideProgressHud];
    if(!_jdWaitDataSource){
        _jdWaitDataSource = [NSMutableArray array];
    }
    if (JDWaitPage == 0) {
        [_jdWaitDataSource removeAllObjects];
    }
    [_jdWaitDataSource addObjectsFromArray:array];
    [tableView reloadData];
    
    if (_listType == ListTypeJD && _appearType == AuditListAppearTypeWaitDeal) {
        [tableView.mj_footer endRefreshing];
        [tableView.mj_header endRefreshing];
    }
}

- (void)refreshJDAuditFinishFinish:(BOOL)isSuccess withArray:(NSArray<JDAuditeModel*>*)array{
    [BaseHelper hideProgressHud];
    if(!_jdFinishDataSource){
        _jdFinishDataSource = [NSMutableArray array];
    }
    if (JDFinishedPage == 0) {
        [_jdFinishDataSource removeAllObjects];
    }
    [_jdFinishDataSource addObjectsFromArray:array];
    [tableView reloadData];
    
    if (_listType == ListTypeJD && _appearType == AuditListAppearTypeFinishDeal) {
        [tableView.mj_footer endRefreshing];
        [tableView.mj_header endRefreshing];
    }
}

- (void)refreshHRWaitauditFinish:(BOOL)isSuccess withArray:(NSArray<HRAuditeModel*>*)array{
    [BaseHelper hideProgressHud];
    if(!_hrWaitDataSource){
        _hrWaitDataSource = [NSMutableArray array];
    }
    if (hrWaitPage == 0) {
        [_hrWaitDataSource removeAllObjects];
    }
    [_hrWaitDataSource addObjectsFromArray:array];
    [tableView reloadData];
    
    if (_listType == ListTypeHR && _appearType == AuditListAppearTypeWaitDeal) {
        [tableView.mj_footer endRefreshing];
        [tableView.mj_header endRefreshing];
    }
}

- (void)refreshHRAuditFinishFinish:(BOOL)isSuccess withArray:(NSArray<HRAuditeModel*>*)array{
    [BaseHelper hideProgressHud];
    if(!_hrFinishedDataSource){
        _hrFinishedDataSource = [NSMutableArray array];
    }
    if (hrFinishedPage == 0) {
        [_hrFinishedDataSource removeAllObjects];
    }
    [_hrFinishedDataSource addObjectsFromArray:array];
    [tableView reloadData];
    if (_listType == ListTypeHR && _appearType == AuditListAppearTypeFinishDeal) {
        [tableView.mj_footer endRefreshing];
        [tableView.mj_header endRefreshing];
    }
}

- (void)hrAuditActionFinishWithData:(NSArray *)array{
    for (NSDictionary * dic in array) {
        HRAuditeModel * modelNew;
        NSString * idStr = [NSString stringWithFormat:@"%@",dic[@"id"]];
        for (HRAuditeModel * model_ in _hrWaitDataSource) {
            NSString * modelId = [NSString stringWithFormat:@"%ld",model_.id_];
            if ([idStr isEqualToString:modelId]){
                modelNew = model_;
                break;
            }
        }
        if (modelNew) {
            [_hrWaitDataSource removeObject:modelNew];
        }
    }
    [tableView reloadData];
}

- (void)jdAuditActionFinishWithData:(NSArray *)array{
    for (NSDictionary * dic in array) {
        JDAuditeModel * modelNew;
        NSString * idStr = [NSString stringWithFormat:@"%@",dic[@"id"]];
        for (JDAuditeModel * model_ in _jdWaitDataSource) {
            NSString * modelId = [NSString stringWithFormat:@"%ld",model_.id_];
            if ([idStr isEqualToString:modelId]){
                modelNew = model_;
                break;
            }
        }
        if (modelNew) {
            [_jdWaitDataSource removeObject:modelNew];
        }
    }
    [tableView reloadData];
}

- (void)listShiftAction:(UIButton *)bt{
    if (bt == dealFinishBt && _appearType == AuditListAppearTypeWaitDeal) {     //  列表切换到已处理状态
        [dealFinishBt setBackgroundImage:[UIImage imageNamed:@"listActionBt"] forState:UIControlStateNormal];
        [dealFinishBt setTitleColor:ListActionBtSelectColor forState:UIControlStateNormal];
        
        [dealWaitBt setBackgroundImage:nil forState:UIControlStateNormal];
        [dealWaitBt setTitleColor:ListActionBtUnselectColor forState:UIControlStateNormal];
        
        _appearType = AuditListAppearTypeFinishDeal;
        [tableView reloadData];
        
        submitBt.hidden = YES;
        [tableView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self).mas_offset(-VerPxFit(20));
        }];
        
        headerView.height = HRAuditListHeaderH;
        if (_listType == ListTypeHR) {
            if (!_hrFinishedDataSource) {
                [BaseHelper showProgressLoading];
                [self.delegate getHRAuditFinishWithPage:hrFinishedPage];
            }
            [self.hrListHeader actionBtAppearShift:NO];
        }else{
            if (!_jdFinishDataSource) {
                [BaseHelper showProgressLoading];
                [self.delegate getJDAuditFinishWithPage:JDFinishedPage];
            }
            [self.jdListHeader actionBtAppearShift:NO];
        }
    }
    if (bt == dealWaitBt && _appearType == AuditListAppearTypeFinishDeal) {   //  列表切换到未处理状态
        
        [dealWaitBt setBackgroundImage:[UIImage imageNamed:@"listActionBt"] forState:UIControlStateNormal];
        [dealWaitBt setTitleColor:ListActionBtSelectColor forState:UIControlStateNormal];
        
        [dealFinishBt setBackgroundImage:nil forState:UIControlStateNormal];
        [dealFinishBt setTitleColor:ListActionBtUnselectColor forState:UIControlStateNormal];
        
        _appearType = AuditListAppearTypeWaitDeal;
        [tableView reloadData];
        submitBt.hidden = NO;
        [tableView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.bottom.equalTo(self).mas_offset(-VerPxFit(80));
        }];
        
        headerView.height = HRAuditListHeaderHWithActionBt;
        
        if (_listType == ListTypeHR) {
            if (!_hrWaitDataSource) {
                [BaseHelper showProgressLoading];
                [self.delegate getHRWaitauditWithPage:hrWaitPage];
            }
            [self.hrListHeader actionBtAppearShift:YES];
            self.hrListHeader.height = HRAuditListHeaderHWithActionBt;
        }else{
            if (!_jdWaitDataSource) {
                [BaseHelper showProgressLoading];
                [self.delegate getJDWaitauditWithPage:JDWaitPage];
            }
            [self.jdListHeader actionBtAppearShift:YES];
            self.jdListHeader.height = HRAuditListHeaderHWithActionBt;
        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (_listType == ListTypeHR) {
        if (_appearType == AuditListAppearTypeWaitDeal) {
            return _hrWaitDataSource?_hrWaitDataSource.count:0;
        }else{
            return _hrFinishedDataSource?_hrFinishedDataSource.count:0;
        }
    }else{
        if (_appearType == AuditListAppearTypeWaitDeal) {
            return _jdWaitDataSource?_jdWaitDataSource.count:0;
        }else{
            return _jdFinishDataSource?_jdFinishDataSource.count:0;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (_listType == ListTypeHR) {
        static NSString * CellIdentifier = @"CellIdentifierHR";
        HRTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            cell = [[HRTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        NSArray * array;
        if (_appearType == AuditListAppearTypeWaitDeal) {
            array =  _hrWaitDataSource;
        }else{
            array =  _hrFinishedDataSource;
        }
        
        HRAuditeModel * model = array[indexPath.row];
        [cell setModel:model withAppearType:_appearType];
        return cell;
    }else{
        static NSString * CellIdentifier = @"CellIdentifierJD";
        JDTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (!cell) {
            cell = [[JDTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.delegate = self;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        NSArray * array;
        if (_appearType == AuditListAppearTypeWaitDeal) {
            array =  _jdWaitDataSource;
        }else{
            array =  _jdFinishDataSource;
        }
        JDAuditeModel * model = array[indexPath.row];
        [cell setModel:model withType:_appearType];
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_listType == ListTypeHR) {
        return 44;
    }
    
    NSArray * array;
    if (_appearType == AuditListAppearTypeWaitDeal) {
        array =  _jdWaitDataSource;
    }else{
        array =  _jdFinishDataSource;
    }
    JDAuditeModel * model = array[indexPath.row];
    if(model.noteIsShow){
        return cellHWithNote;
    }else{
        return cellH;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -- JDTableViewCellDelegate
- (void)refreshCellForNote:(JDAuditeModel *)model{
    if (_jdAuditeModelShowNote != model) {
        _jdAuditeModelShowNote.noteIsShow = NO;
    }
    _jdAuditeModelShowNote = model;
    [tableView reloadData];
}

#pragma mark -- AuditListHeaderDelegate

- (void)hrFailAllSelected:(BOOL)isSelected{
    if(isSelected){
        for (HRAuditeModel * model in _hrWaitDataSource) {
            model.auditString = @"Reject";
        }
    }else{
        for (HRAuditeModel * model in _hrWaitDataSource) {
            model.auditString = nil;
        }
    }
    [tableView reloadData];
}

- (void)hrPassAllSelected:(BOOL)isSelected{
    if(isSelected){
        for (HRAuditeModel * model in _hrWaitDataSource) {
            model.auditString = @"Accept";
        }
    }else{
        for (HRAuditeModel * model in _hrWaitDataSource) {
            model.auditString = nil;
        }
    }
    [tableView reloadData];
}

#pragma mark -- AuditJDListHeaderDelegate

- (void)jdFailAllSelected:(BOOL)isSelected{
    if(isSelected){
        for (JDAuditeModel * model in _jdWaitDataSource) {
            model.auditString = @"Reject";
        }
    }else{
        for (JDAuditeModel * model in _jdWaitDataSource) {
            model.auditString = nil;
        }
    }
    [tableView reloadData];
}

- (void)jdPassAllSelected:(BOOL)isSelected{
    if(isSelected){
        for (JDAuditeModel * model in _jdWaitDataSource) {
            model.auditString = @"Accept";
        }
    }else{
        for (JDAuditeModel * model in _jdWaitDataSource) {
            model.auditString = nil;
        }
    }
    [tableView reloadData];
}


@end

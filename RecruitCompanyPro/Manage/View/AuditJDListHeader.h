//
//  AuditJDListHeader.h
//  RecruitCompanyPro
//
//  Created by Michael on 2018/1/11.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>

#define JDAuditListHeaderH  VerPxFit(50)
#define JDAuditListHeaderHWithActionBt  VerPxFit(70)

@protocol AuditJDListHeaderDelegate
// jd 审核不通过一键全选
- (void)jdFailAllSelected:(BOOL)isSelected;
// jd 审核通过一键全选
- (void)jdPassAllSelected:(BOOL)isSelected;
@end

@interface AuditJDListHeader : UIView

@property (nonatomic , weak)  id<AuditJDListHeaderDelegate>delegate;

- (void)actionBtAppearShift:(BOOL)appearType;

@end

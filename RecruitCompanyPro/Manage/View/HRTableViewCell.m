//
//  HRTableViewCell.m
//  RecruitCompanyPro
//
//  Created by Michael on 2018/1/10.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "HRTableViewCell.h"

@interface HRTableViewCell(){
    
    UILabel * jobNumLb;     // 工号
    UILabel * nameLb;       // 姓名
    UILabel * idCardLb;     // 身份证号
    UILabel * positionLb;    // 职位名称
    
    UIButton * passBt;  // 通过按钮
    UIButton * failBt;   // 不通过按钮
    UIButton * resultBt;    // 结果按钮
}

@end

@implementation HRTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(nullable NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self createSubView];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)createSubView{
    @weakify(self)
    jobNumLb = [self lb];
    [jobNumLb mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.and.top.and.bottom.equalTo(self);
        make.width.mas_equalTo(HorPxFit(110));
    }];
    
    nameLb = [self lb];
    [nameLb mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.and.bottom.equalTo(self);
        make.left.mas_equalTo(jobNumLb.mas_right);
        make.width.mas_equalTo(HorPxFit(110));
    }];
    
    idCardLb = [self lb];
    [idCardLb mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.and.bottom.equalTo(self);
        make.left.mas_equalTo(nameLb.mas_right);
        make.width.mas_equalTo(HorPxFit(170));
    }];
    
    positionLb = [self lb];
    [positionLb mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.and.bottom.equalTo(self);
        make.left.mas_equalTo(idCardLb.mas_right);
        make.width.mas_equalTo(HorPxFit(120));
    }];
    
    failBt = [UIButton buttonWithType:UIButtonTypeCustom];
    failBt.hidden = YES;
    [failBt setImage:[UIImage imageNamed:@"failBt"] forState:UIControlStateNormal];
    [failBt addTarget:self action:@selector(failAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:failBt];
    [failBt mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.mas_equalTo(positionLb.mas_right).mas_offset(HorPxFit(5));
        make.top.and.bottom.equalTo(self);
        make.width.mas_equalTo(HorPxFit(70));
    }];
    
    passBt = [UIButton buttonWithType:UIButtonTypeCustom];
    passBt.hidden = YES;
    [passBt setImage:[UIImage imageNamed:@"passBt"] forState:UIControlStateNormal];
    [passBt addTarget:self action:@selector(passAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:passBt];
    [passBt mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.mas_equalTo(failBt.mas_right);
        make.top.and.bottom.equalTo(self);
        make.width.mas_equalTo(HorPxFit(70));
    }];
    
    resultBt = [UIButton buttonWithType:UIButtonTypeCustom];
    resultBt.hidden = YES;
    [self addSubview:resultBt];
    [resultBt mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.centerX.mas_equalTo(failBt.mas_right);
        make.top.and.bottom.equalTo(self);
        make.width.mas_equalTo(HorPxFit(70));
    }];
    
    UIImageView * imgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"listCellSep"]];
    [self addSubview:imgV];
    [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.and.right.and.bottom.equalTo(self);
        make.height.mas_equalTo(1);
    }];
}

- (void)failAction{
    [passBt setImage:[UIImage imageNamed:@"passBt"] forState:UIControlStateNormal];
    [failBt setImage:[UIImage imageNamed:@"failBtHigh"] forState:UIControlStateNormal];
    _hrAuditeModel.auditString = @"Reject";
}

- (void)passAction{
    [failBt setImage:[UIImage imageNamed:@"failBt"] forState:UIControlStateNormal];
    [passBt setImage:[UIImage imageNamed:@"passBtHigh"] forState:UIControlStateNormal];
    _hrAuditeModel.auditString = @"Accept";
}

- (UILabel *)lb{
    UILabel * lb = [[UILabel alloc] init];
    lb.textAlignment = NSTextAlignmentCenter;
    lb.font = [UIFont systemFontOfSize:13];
    lb.textColor = [UIColor whiteColor];
    lb.backgroundColor = [UIColor clearColor];
    [self addSubview:lb];
    return lb;
}

- (void)setModel:(HRAuditeModel *)model withAppearType:(AuditListAppearType)type{
    if (type == AuditListAppearTypeWaitDeal) {
        if (passBt.hidden) {
            passBt.hidden = NO;
            failBt.hidden = NO;
            resultBt.hidden = YES;
        }
    }else{
        if (resultBt.hidden) {
            resultBt.hidden = NO;
            passBt.hidden = YES;
            failBt.hidden = YES;
        }
    }
    self.hrAuditeModel = model;
    jobNumLb.text = _hrAuditeModel.jobNumber;
    nameLb.text = _hrAuditeModel.username;
    positionLb.text = _hrAuditeModel.position;
    idCardLb.text = _hrAuditeModel.idCardNumber;
    if ([_hrAuditeModel.dealResult isEqualToString:@"Disabled"]) {
        [resultBt setImage:[UIImage imageNamed:@"failBt"] forState:UIControlStateNormal];
    }else{
        [resultBt setImage:[UIImage imageNamed:@"passBt"] forState:UIControlStateNormal];
    }
    
    if ([_hrAuditeModel.auditString isEqualToString:@"Accept"]) {
        [failBt setImage:[UIImage imageNamed:@"failBt"] forState:UIControlStateNormal];
        [passBt setImage:[UIImage imageNamed:@"passBtHigh"] forState:UIControlStateNormal];
    }else if ([_hrAuditeModel.auditString isEqualToString:@"Reject"]){
        [passBt setImage:[UIImage imageNamed:@"passBt"] forState:UIControlStateNormal];
        [failBt setImage:[UIImage imageNamed:@"failBtHigh"] forState:UIControlStateNormal];
    }else{
        [passBt setImage:[UIImage imageNamed:@"passBt"] forState:UIControlStateNormal];
        [failBt setImage:[UIImage imageNamed:@"failBt"] forState:UIControlStateNormal];
    }
}

@end

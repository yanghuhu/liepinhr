//
//  RewardListCell.m
//  RecruitCompanyPro
//
//  Created by Michael on 2018/1/29.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "RewardListCell.h"

@interface RewardListCell(){
    
    UILabel * dateLb;    //  时间lb
    UILabel * serialNumberLb;    // 订单号
    UILabel * moneyLb;      // 金额
    UILabel * noteLb;   //  备注
}

@end

@implementation RewardListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(nullable NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        self.backgroundColor = [UIColor clearColor];
        [self createSubViews];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)createSubViews{
    
    dateLb = [self lb];
    dateLb.text = @"2018-10-10";
    [dateLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.top.and.bottom.equalTo(self);
        make.width.mas_equalTo(HorPxFit(150));
    }];
    
    serialNumberLb = [self lb];
    serialNumberLb.text = @"8988";
    [serialNumberLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(dateLb.mas_right);
        make.top.and.bottom.equalTo(self);
        make.width.mas_equalTo(HorPxFit(250));
    }];
    
    moneyLb = [self lb];
    moneyLb.text = @"9999";
    [moneyLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(serialNumberLb.mas_right);
        make.top.and.bottom.equalTo(self);
        make.width.mas_equalTo(HorPxFit(90));
    }];
    
    noteLb = [self lb];
    noteLb.numberOfLines = 0;
    noteLb.text = @"afsfafasfaf";
    [noteLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(moneyLb.mas_right);
        make.top.and.bottom.equalTo(self);
        make.right.equalTo(self);
    }];
    
    UIImageView * imgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"listCellSep"]];
    [self addSubview:imgV];
    [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.bottom.equalTo(self);
        make.height.mas_equalTo(1);
    }];
}


- (void)setRewardModel:(RewardModel *)rewardModel{
    _rewardModel = rewardModel;
    serialNumberLb.text = _rewardModel.serialNumber;
    moneyLb.text = [NSString stringWithFormat:@"%d",rewardModel.money];
    noteLb.text = rewardModel.desc;
    dateLb.text = [BaseHelper stringWithTimeIntevl:rewardModel.createTime format:kDateFormatTypeYYYYMMDDHHMM];
}

- (UILabel *)lb{
    UILabel * lb = [[UILabel alloc] init];
    lb.textAlignment = NSTextAlignmentCenter;
    lb.font = [UIFont systemFontOfSize:14];
    lb.textColor = [UIColor whiteColor];
    lb.backgroundColor = [UIColor clearColor];
    [self addSubview:lb];
    return lb;
}
@end

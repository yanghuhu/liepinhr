//
//  AuditJDListHeader.m
//  RecruitCompanyPro
//
//  Created by Michael on 2018/1/11.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "AuditJDListHeader.h"

@interface AuditJDListHeader(){

    UIButton * passAllBt;  // 通过一键全选按钮
    UIButton * failAllBt;   //  不通过一键全选按钮
}
@end

@implementation AuditJDListHeader

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self createSubViews];
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    @weakify(self)
    UILabel * positionLb = [self lb:@"职位"];
    [positionLb mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.and.top.equalTo(self);
        make.height.mas_equalTo(JDAuditListHeaderH);
        make.width.mas_equalTo(HorPxFit(110));
    }];
    
    UILabel * levelLb = [self lb:@"级别"];
    [levelLb mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.equalTo(self);
        make.left.mas_equalTo(positionLb.mas_right);
        make.width.mas_equalTo(HorPxFit(70));
        make.height.mas_equalTo(JDAuditListHeaderH);
    }];
    
    UILabel * numLb = [self lb:@"数量"];
    [numLb mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.equalTo(self);
        make.left.mas_equalTo(levelLb.mas_right);
        make.width.mas_equalTo(HorPxFit(40));
        make.height.mas_equalTo(JDAuditListHeaderH);
    }];
    
    UILabel * priceLb = [self lb:@"单价(元)"];
    [priceLb mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.equalTo(self);
        make.left.mas_equalTo(numLb.mas_right);
        make.width.mas_equalTo(HorPxFit(60));
        make.height.mas_equalTo(JDAuditListHeaderH);
    }];
    
    UILabel * HRLb = [self lb:@"HR"];
    [HRLb mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.equalTo(self);
        make.left.mas_equalTo(priceLb.mas_right);
        make.width.mas_equalTo(HorPxFit(90));
        make.height.mas_equalTo(JDAuditListHeaderH);
    }];
    
    UILabel * totalLb = [self lb:@"总计"];
    [totalLb mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.equalTo(self);
        make.left.mas_equalTo(HRLb.mas_right);
        make.width.mas_equalTo(HorPxFit(80));
        make.height.mas_equalTo(JDAuditListHeaderH);
    }];
    
    UILabel * noteLb = [self lb:@"备注"];
    [noteLb mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.equalTo(self);
        make.left.mas_equalTo(totalLb.mas_right);
        make.width.mas_equalTo(HorPxFit(50));
        make.height.mas_equalTo(JDAuditListHeaderH);
    }];
    
    UILabel * dealLb = [self lb:@"处理"];
    [dealLb mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.equalTo(self);
        make.left.mas_equalTo(noteLb.mas_right);
        make.right.equalTo(self);
        make.height.mas_equalTo(JDAuditListHeaderH);
    }];
    
    passAllBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [passAllBt addTarget:self action:@selector(passAllBtAction) forControlEvents:UIControlEventTouchUpInside];
    passAllBt.titleLabel.font = [UIFont systemFontOfSize:12];
    passAllBt.selected = NO;
    [passAllBt setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [passAllBt setTitle:@"通过全选" forState:UIControlStateNormal];
    [self addSubview:passAllBt];
    [passAllBt mas_updateConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.mas_equalTo(dealLb.mas_bottom);
        make.right.equalTo(self);
        make.height.mas_equalTo(JDAuditListHeaderHWithActionBt -JDAuditListHeaderH);
        make.width.mas_equalTo(HorPxFit(70));
    }];
    
    failAllBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [failAllBt addTarget:self action:@selector(failAllBtAction) forControlEvents:UIControlEventTouchUpInside];
    [failAllBt setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    failAllBt.selected = NO;
    [failAllBt setTitle:@"不通过全选" forState:UIControlStateNormal];
    failAllBt.titleLabel.font = [UIFont systemFontOfSize:12];
    [self addSubview:failAllBt];
    [failAllBt mas_updateConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(passAllBt.mas_left);
        make.top.mas_equalTo(dealLb.mas_bottom);
        make.height.mas_equalTo(JDAuditListHeaderHWithActionBt -JDAuditListHeaderH );
        make.width.mas_equalTo(HorPxFit(70));
    }];
}

- (void)actionBtAppearShift:(BOOL)appearType{
    if (appearType) {
        failAllBt.hidden = NO;
        passAllBt.hidden = NO;
    }else{
        failAllBt.hidden = YES;
        passAllBt.hidden = YES;
    }
}

- (void)failAllBtAction{
    failAllBt.selected = !failAllBt.selected;
    if (failAllBt.selected) {
        [failAllBt setTitleColor:COLOR(195, 4, 0, 1) forState:UIControlStateNormal];
        [passAllBt setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        passAllBt.selected = NO;
    }else{
        [failAllBt setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    }
    [self.delegate jdFailAllSelected:failAllBt.selected];
}

- (void)passAllBtAction{
    passAllBt.selected = !passAllBt.selected;
    if (passAllBt.selected) {
        [passAllBt setTitleColor:COLOR(40, 255, 32, 1) forState:UIControlStateNormal];
        [failAllBt setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        failAllBt.selected = NO;
    }else{
        [passAllBt setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    }
    [self.delegate jdPassAllSelected:passAllBt.selected];
}

- (UILabel *)lb:(NSString *)text{
    UILabel * lb = [[UILabel alloc] init];
    lb.textAlignment = NSTextAlignmentCenter;
    lb.font = [UIFont systemFontOfSize:15];
    lb.textColor = [UIColor whiteColor];
    lb.text = text;
    lb.backgroundColor = [UIColor clearColor];
    [self addSubview:lb];
    return lb;
}

@end


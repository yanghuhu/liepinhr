//
//  RewardListTitleView.m
//  RecruitCompanyPro
//
//  Created by Michael on 2018/1/29.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "RewardListTitleView.h"

@interface RewardListTitleView()

@end

@implementation RewardListTitleView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    
    UILabel * dateLb = [self lb:@"日期"];
    [dateLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.top.and.bottom.equalTo(self);
        make.width.mas_equalTo(HorPxFit(150));
    }];
    
    UILabel * getLb = [self lb:@"订单号"];
    [getLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(dateLb.mas_right);
        make.top.and.bottom.equalTo(self);
        make.width.mas_equalTo(HorPxFit(250));
    }];
    
    UILabel * outLb = [self lb:@"金额(元)"];
    [outLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(getLb.mas_right);
        make.top.and.bottom.equalTo(self);
        make.width.mas_equalTo(HorPxFit(90));
    }];
    
    UILabel * notelb = [self lb:@"备注"];
    [notelb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(outLb.mas_right);
        make.top.and.bottom.equalTo(self);
        make.right.equalTo(self);
    }];
}

- (UILabel *)lb:(NSString *)text{
    UILabel * lb = [[UILabel alloc] init];
    lb.textAlignment = NSTextAlignmentCenter;
    lb.font = [UIFont systemFontOfSize:15];
    lb.textColor = [UIColor whiteColor];
    lb.text = text;
    lb.backgroundColor = [UIColor clearColor];
    [self addSubview:lb];
    return lb;
}

@end

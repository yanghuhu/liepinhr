//
//  HRTableViewCell.h
//  RecruitCompanyPro
//
//  Created by Michael on 2018/1/10.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HRAuditeModel.h"

@interface HRTableViewCell : UITableViewCell

@property (nonatomic , strong) HRAuditeModel * hrAuditeModel;

- (void)setModel:(HRAuditeModel *)model withAppearType:(AuditListAppearType)type;

@end

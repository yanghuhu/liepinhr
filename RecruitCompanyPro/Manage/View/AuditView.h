//
//  AuditView.h
//  RecruitCompanyPro
//
//  Created by Michael on 2018/1/26.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JDAuditeModel.h"
#import "HRAuditeModel.h"

typedef enum {
    ListTypeHR,  //  hr 审核列表
    ListTypeJD     // 职位审核列表
}ListType;


@protocol AuditViewDelegate
// 获取职位待审核数据
- (void)getJDWaitauditWithPage:(NSInteger)page;
// 获取职位已审核数据
- (void)getJDAuditFinishWithPage:(NSInteger)page;
// 获取hr待审核数据
- (void)getHRWaitauditWithPage:(NSInteger)page;
// 获取hr已审核数据
- (void)getHRAuditFinishWithPage:(NSInteger)page;
// 提交hr待审核数据
- (void)hrAudtionActionWithData:(NSArray *)array;
// 提交职位待审核数据
- (void)jdAudtionActionWithData:(NSArray *)array;

@end

@interface AuditView : UIView

@property (nonatomic , weak) id<AuditViewDelegate>delegate;
@property (nonatomic , assign) ListType  listType;  // 列表类型

// jd 待审核数据展示
- (void)refreshJDWaitauditFinish:(BOOL)isSuccess withArray:(NSArray<JDAuditeModel*>*)array;
// jd 已审核数据展示
- (void)refreshJDAuditFinishFinish:(BOOL)isSuccess withArray:(NSArray<JDAuditeModel*>*)array;
// hr 待审核数据展示
- (void)refreshHRWaitauditFinish:(BOOL)isSuccess withArray:(NSArray<HRAuditeModel*>*)array;
// hr 已审核数据展示
- (void)refreshHRAuditFinishFinish:(BOOL)isSuccess withArray:(NSArray<HRAuditeModel*>*)array;

// hr 待审核数据处理完成
- (void)hrAuditActionFinishWithData:(NSArray * )array;
// 职位 待审核数据处理完成
- (void)jdAuditActionFinishWithData:(NSArray * )array;

@end

//
//  RewardView.h
//  RecruitCompanyPro
//
//  Created by Michael on 2018/1/26.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RewardViewDelegate
//  根据选择条件获取明细数据
- (void)getRewardDataWithStartTime:(NSDate *)startDate  endTiem:(NSDate *)endDate  dataType:(NSString *)type withPage:(NSInteger)page;

@end

@interface RewardView : UIView

@property (nonatomic , weak) id<RewardViewDelegate>delegate;
@property (nonatomic , strong) NSMutableArray * dataSource;

- (void)updateBalanceAppear:(NSInteger)balance;

@end

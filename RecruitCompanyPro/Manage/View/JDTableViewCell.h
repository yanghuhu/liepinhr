//
//  JDTableViewCell.h
//  RecruitCompanyPro
//
//  Created by Michael on 2018/1/11.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JDAuditeModel.h"

#define cellH  44
#define cellHWithNote  54

@class JDTableViewCell;

@protocol  JDTableViewCellDelegate
// note 显隐切换刷新cell
- (void)refreshCellForNote:(JDAuditeModel *)model;
@end

@interface JDTableViewCell : UITableViewCell

@property (nonatomic , assign) id<JDTableViewCellDelegate>delegate;
@property (nonatomic , strong) JDAuditeModel * jdAuditeModel;

- (void)setModel:(JDAuditeModel *)model withType:(AuditListAppearType)appearType;

@end

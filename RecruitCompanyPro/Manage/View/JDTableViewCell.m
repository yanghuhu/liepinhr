//
//  JDTableViewCell.m
//  RecruitCompanyPro
//
//  Created by Michael on 2018/1/11.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "JDTableViewCell.h"

@interface JDTableViewCell(){
    UILabel *  positionLb;    //   职位
    UILabel * levelLb;         //  等级
    UILabel * numberLb;        // 数量
    UILabel * priceLb;         // 赏金
    UILabel * hrLb;           //  hr
    UILabel * countLb;       // 总金额
    UIButton  * noteBt;      //  备注显隐按钮
    UILabel * noteLb;         //  备注
    
    UIButton * passBt;       //  通过按钮
    UIButton * failBt;       // 不通过按钮
    UIButton * resultBt;     //  结果按钮
}
@end

@implementation JDTableViewCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(nullable NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self createSubView];
    }
    return self;
}

- (void)createSubView{
    positionLb = [self lb];
    [positionLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.top.equalTo(self);
        make.height.mas_equalTo(cellH);
        make.width.mas_equalTo(HorPxFit(110));
    }];
    
    levelLb = [self lb];
    [levelLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.left.mas_equalTo(positionLb.mas_right);
        make.width.mas_equalTo(HorPxFit(70));
        make.height.mas_equalTo(cellH);
    }];
    
    numberLb = [self lb];
    [numberLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.left.mas_equalTo(levelLb.mas_right);
        make.width.mas_equalTo(HorPxFit(40));
        make.height.mas_equalTo(cellH);
    }];
    
    priceLb = [self lb];
    [priceLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.left.mas_equalTo(numberLb.mas_right);
        make.width.mas_equalTo(HorPxFit(60));
        make.height.mas_equalTo(cellH);
    }];
    
    hrLb = [self lb];
    [hrLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.left.mas_equalTo(priceLb.mas_right);
        make.width.mas_equalTo(HorPxFit(90));
        make.height.mas_equalTo(cellH);
    }];
    
    countLb = [self lb];
    countLb.text = [NSString stringWithFormat:@"%d",5000*5];
    [countLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.left.mas_equalTo(hrLb.mas_right);
        make.width.mas_equalTo(HorPxFit(80));
        make.height.mas_equalTo(cellH);
    }];
    
    noteBt = [UIButton buttonWithType:UIButtonTypeCustom];
    noteBt.titleLabel.font = [UIFont systemFontOfSize:13];
    [noteBt setTitle:@"无" forState:UIControlStateNormal];
    [noteBt addTarget:self action:@selector(noteAppearShift) forControlEvents:UIControlEventTouchUpInside];
    [noteBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self addSubview:noteBt];
    [noteBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self);
        make.left.mas_equalTo(countLb.mas_right);
        make.width.mas_equalTo(HorPxFit(50));
        make.height.mas_equalTo(cellH);
    }];
    
    failBt = [UIButton buttonWithType:UIButtonTypeCustom];
    failBt.hidden = YES;
    [failBt setImage:[UIImage imageNamed:@"failBt"] forState:UIControlStateNormal];
    [failBt addTarget:self action:@selector(failAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:failBt];
    [failBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(noteBt.mas_right).mas_offset(HorPxFit(5));
        make.top.equalTo(self);
        make.width.mas_equalTo(HorPxFit(70));
        make.height.mas_equalTo(cellH);
    }];
    
    passBt = [UIButton buttonWithType:UIButtonTypeCustom];
    passBt.hidden = YES;
    [passBt setImage:[UIImage imageNamed:@"passBt"] forState:UIControlStateNormal];
    [passBt addTarget:self action:@selector(passAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:passBt];
    [passBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(failBt.mas_right);
        make.top.equalTo(self);
        make.width.mas_equalTo(HorPxFit(70));
        make.height.mas_equalTo(cellH);
    }];
    
    resultBt = [UIButton buttonWithType:UIButtonTypeCustom];
    resultBt.hidden = YES;
    [self addSubview:resultBt];
    [resultBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(failBt.mas_right);
        make.top.equalTo(self);
        make.width.mas_equalTo(HorPxFit(70));
        make.height.mas_equalTo(cellH);
    }];
    
    noteLb = [self lb];
    noteLb.textAlignment = NSTextAlignmentLeft;
    noteLb.textColor = [UIColor redColor];
    noteLb.hidden = YES;
    [self addSubview:noteLb];
    [noteLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(self).mas_offset(HorPxFit(10));
        make.bottom.equalTo(self);
        make.height.mas_equalTo(17);
    }];
    
    UIImageView * imgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"listCellSep"]];
    [self addSubview:imgV];
    [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.bottom.equalTo(self);
        make.height.mas_equalTo(1);
    }];
}

- (void)failAction{
    [passBt setImage:[UIImage imageNamed:@"passBt"] forState:UIControlStateNormal];
    [failBt setImage:[UIImage imageNamed:@"failBtHigh"] forState:UIControlStateNormal];
    _jdAuditeModel.auditString = @"Reject";
}

- (void)passAction{
    [failBt setImage:[UIImage imageNamed:@"failBt"] forState:UIControlStateNormal];
    [passBt setImage:[UIImage imageNamed:@"passBtHigh"] forState:UIControlStateNormal];
    _jdAuditeModel.auditString = @"Accept";
}

- (void)noteAppearShift{
    //  当没有note的时候
    if (!self.jdAuditeModel.hrComment || self.jdAuditeModel.hrComment.length == 0) {
        noteLb.hidden = YES;
        [noteBt setTitle:@"无" forState:UIControlStateNormal];
        return;
    }
    
    if (self.jdAuditeModel.noteIsShow) {
        self.jdAuditeModel.noteIsShow = NO;
        noteLb.hidden = YES;
        [noteBt setImage:[UIImage imageNamed:@"arrowDown"] forState:UIControlStateNormal];
    }else{
        noteLb.hidden = NO;
        [noteBt setImage:[UIImage imageNamed:@"arrowUp"] forState:UIControlStateNormal];
        self.jdAuditeModel.noteIsShow = YES;
    }
    [self.delegate refreshCellForNote:_jdAuditeModel];
}

- (UILabel *)lb{
    UILabel * lb = [[UILabel alloc] init];
    lb.numberOfLines = 0;
    lb.textAlignment = NSTextAlignmentCenter;
    lb.font = [UIFont systemFontOfSize:13];
    lb.textColor = [UIColor whiteColor];
    lb.backgroundColor = [UIColor clearColor];
    [self addSubview:lb];
    return lb;
}

- (void)setModel:(JDAuditeModel *)model withType:(AuditListAppearType)appearType{
    self.jdAuditeModel = model;
    positionLb.text = model.jobDetailTitle;
    levelLb.text = model.jobDetailLevel;
    numberLb.text = [NSString stringWithFormat:@"%ld",model.jobDetailRecruitingNumber];
    priceLb.text = [NSString stringWithFormat:@"%.1f",model.jobDetailPrice];
    hrLb.text = model.jobDetailCreatorUsername;
    countLb.text = [NSString stringWithFormat:@"%.1f",model.total];
    noteLb.text = [NSString stringWithFormat:@"备注:%@",model.hrComment];
    if (model.hrComment && model.hrComment.length != 0) {
        if (model.noteIsShow) {
            [noteBt setImage:[UIImage imageNamed:@"arrowUp"] forState:UIControlStateNormal];
            noteLb.hidden = NO;
        }else{
            [noteBt setImage:[UIImage imageNamed:@"arrowDown"] forState:UIControlStateNormal];
            noteLb.hidden = YES;
        }
        [noteBt setTitle:nil forState:UIControlStateNormal];
    }else{
        [noteBt setImage:nil forState:UIControlStateNormal];
        [noteBt setTitle:@"无" forState:UIControlStateNormal];
    }
    
    if ([_jdAuditeModel.auditString isEqualToString:@"Accept"]) {
        [failBt setImage:[UIImage imageNamed:@"failBt"] forState:UIControlStateNormal];
        [passBt setImage:[UIImage imageNamed:@"passBtHigh"] forState:UIControlStateNormal];
    }else if ([_jdAuditeModel.auditString isEqualToString:@"Reject"]){
        [passBt setImage:[UIImage imageNamed:@"passBt"] forState:UIControlStateNormal];
        [failBt setImage:[UIImage imageNamed:@"failBtHigh"] forState:UIControlStateNormal];
    }else{
        [passBt setImage:[UIImage imageNamed:@"passBt"] forState:UIControlStateNormal];
        [failBt setImage:[UIImage imageNamed:@"failBt"] forState:UIControlStateNormal];
    }
    
    if (appearType == AuditListAppearTypeWaitDeal) {
        failBt.hidden = NO;
        passBt.hidden = NO;
        resultBt.hidden = YES;
    }else{
        failBt.hidden = YES;
        passBt.hidden = YES;
        resultBt.hidden = NO;
        if ([_jdAuditeModel.auditResult isEqualToString:@"Reject"]) {
            [resultBt setImage:[UIImage imageNamed:@"failBt"] forState:UIControlStateNormal];
        }else{
            [resultBt setImage:[UIImage imageNamed:@"passBt"] forState:UIControlStateNormal];
        }
    }
}

@end

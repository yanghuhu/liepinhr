//
//  RewardHeaderView.m
//  RecruitCompanyPro
//
//  Created by Michael on 2018/1/25.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "RewardHeaderView.h"
#import "AppDelegate.h"

#define TypeImgVTag  150
#define StartPickerTag 1000
#define EndPickerTag 1001

@interface RewardHeaderView(){
    
    UILabel * balanceLb;   // 总金额
    UIButton * startTimeBt;   // 起始时间按钮
    UIButton * endTimeBt;     // 终止时间按钮
    UIButton * incomeBt;    // 进账
    UIButton * expenseBt;    // 出账
    UIButton * allTypeBt;    // 全部
}
@property (nonatomic , strong) UIDatePicker * datePicker;  // 时间选择picker
@property (nonatomic , strong) UIView * datePickerContentV;   // 时间选择器容器view
@property (nonatomic , strong) NSString * dataType;     //时间
@end

@implementation RewardHeaderView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
  
    UIImage * img = [UIImage imageNamed:@"blanceBk"];
    CGFloat height = img.size.height * (CGRectGetWidth(self.frame)-2*HorPxFit(40)) / img.size.width;
    UIImageView * blanceBk = [[UIImageView alloc] initWithFrame:CGRectMake(HorPxFit(40), 0, img.size.width, height)];
    blanceBk.image = img;
    [self addSubview:blanceBk];
    
    UIImageView * yuImgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"blanceIcon"]];
    [blanceBk addSubview:yuImgV];
    [yuImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(blanceBk).mas_offset(HorPxFit(20));
        make.centerY.equalTo(blanceBk.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(45), HorPxFit(45)));
    }];
    
//    UILabel * titleLb = [[UILabel alloc] init];
//    titleLb.text = @"余额:";
//    titleLb.textColor = [UIColor whiteColor];
//    titleLb.font = [UIFont systemFontOfSize:16];
//    [blanceBk addSubview:titleLb];
//    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(yuImgV.mas_right).mas_offset(HorPxFit(20));
//        make.centerY.mas_equalTo(yuImgV.mas_centerY);
//        make.size.mas_equalTo(CGSizeMake(80, 30));
//    }];
    
    balanceLb = [[UILabel alloc] init];
    balanceLb.font = [UIFont systemFontOfSize:17];
    balanceLb.textColor = [UIColor whiteColor];
    [blanceBk addSubview:balanceLb];
    [balanceLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(yuImgV.mas_right).mas_offset(HorPxFit(20));
        make.centerY.equalTo(yuImgV.mas_centerY);
        make.right.mas_equalTo(blanceBk);
//        make.width.mas_equalTo(120);
        make.height.mas_equalTo(VerPxFit(30));
    }];
    
    UILabel * startTime = [[UILabel alloc] init];
    startTime.font = [UIFont systemFontOfSize:16];
    startTime.textColor = [UIColor whiteColor];
    startTime.text = @"起始时间:";
    [self addSubview:startTime];
    [startTime mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).mas_offset(HorPxFit(40));
        make.top.mas_equalTo(blanceBk.mas_bottom).mas_offset(VerPxFit(40));
        make.size.mas_equalTo(CGSizeMake(75, 30));
    }];
    
    startTimeBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [startTimeBt addTarget:self action:@selector(startTimeChoose) forControlEvents:UIControlEventTouchUpInside];
    startTimeBt.titleLabel.font = [UIFont systemFontOfSize:15];
    [self addSubview:startTimeBt];
    [startTimeBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(startTime.mas_right).mas_offset(HorPxFit(10));
        make.centerY.mas_equalTo(startTime.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(140), VerPxFit(24)));
    }];
    UIImageView * bottomLine_start = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"AccountBottom"]];
    [self addSubview:bottomLine_start];
    [bottomLine_start mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(startTimeBt);
        make.top.mas_equalTo(startTimeBt.mas_bottom);
        make.height.mas_equalTo(1);
    }];
    
    UILabel * endTime = [[UILabel alloc] init];
    endTime.font = [UIFont systemFontOfSize:16];
    endTime.text = @"截止时间:";
    endTime.textColor = [UIColor whiteColor];
    [self addSubview:endTime];
    [endTime mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(startTimeBt.mas_right).mas_offset(HorPxFit(60));
        make.centerY.mas_equalTo(startTimeBt.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(75, 30));
    }];
    
    endTimeBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [endTimeBt addTarget:self action:@selector(endTimeChoose) forControlEvents:UIControlEventTouchUpInside];
    endTimeBt.titleLabel.font = [UIFont systemFontOfSize:15];
    [self addSubview:endTimeBt];
    [endTimeBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(endTime.mas_right).mas_offset(HorPxFit(10));
        make.centerY.mas_equalTo(startTime.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(140), VerPxFit(24)));
    }];
    
    UIImageView * bottomLine_end = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"AccountBottom"]];
    [self addSubview:bottomLine_end];
    [bottomLine_end mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(endTimeBt);
        make.top.mas_equalTo(endTimeBt.mas_bottom);
        make.height.mas_equalTo(1);
    }];
    
    UILabel * typeChooseLb = [[UILabel alloc] init];
    typeChooseLb.font = [UIFont systemFontOfSize:16];
    typeChooseLb.textColor = [UIColor whiteColor];
    typeChooseLb.text = @"类别选择:";
    [self addSubview:typeChooseLb];
    [typeChooseLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(self).mas_offset(HorPxFit(40));
        make.top.mas_equalTo(startTime.mas_bottom).mas_offset(VerPxFit(15));
        make.size.mas_equalTo(CGSizeMake(75, 30));
    }];
    
    UIView * jinzhangView = [self moneyTypeView:@"进账"];
    [jinzhangView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(typeChooseLb.mas_right).mas_offset(HorPxFit(20));
        make.centerY.mas_equalTo(typeChooseLb.mas_centerY);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(VerPxFit(35));
    }];
    
    UIView * chuzhangView = [self moneyTypeView:@"支出"];
    [chuzhangView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(jinzhangView.mas_right).mas_offset(HorPxFit(15));
        make.centerY.mas_equalTo(typeChooseLb.mas_centerY);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(VerPxFit(35));
    }];
  
    UIView * quanbuView = [self moneyTypeView:@"全部"];
    [quanbuView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(chuzhangView.mas_right).mas_offset(HorPxFit(15));
        make.centerY.mas_equalTo(typeChooseLb.mas_centerY);
        make.width.mas_equalTo(80);
        make.height.mas_equalTo(VerPxFit(35));
    }];
    
    UIButton * sureBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [sureBt addTarget:self action:@selector(sureAction) forControlEvents:UIControlEventTouchUpInside];
    [sureBt setTitle:@"查询" forState:UIControlStateNormal];
    sureBt.titleLabel.font = [UIFont systemFontOfSize:15];
    [sureBt setBackgroundImage:[UIImage imageNamed:@"submitBtbk"] forState:UIControlStateNormal];
    [sureBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self addSubview:sureBt];
    [sureBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX);
        make.top.mas_equalTo(typeChooseLb.mas_bottom).mas_offset(VerPxFit(10));
        make.size.mas_equalTo(CGSizeMake(HorPxFit(280), VerPxFit(68)));
    }];
}

- (UIView *)moneyTypeView:(NSString *)title{
    UIView * contentV = [[UIView alloc] init];
    [self addSubview:contentV];
    
    UIImageView * imgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"circle"]];
    imgV.tag = TypeImgVTag;
    [contentV addSubview:imgV];
    [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(contentV);
        make.centerY.mas_equalTo(contentV.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(20), HorPxFit(20)));
    }];
    
    UILabel * titleLb = [[UILabel alloc] init];
    titleLb.font = [UIFont systemFontOfSize:15];
    titleLb.text = title;
    titleLb.textColor = [UIColor whiteColor];
    [contentV addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(contentV);
        make.top.and.bottom.equalTo(contentV);
        make.width.mas_equalTo(50);
    }];
    
    UIButton * bt = [UIButton buttonWithType:UIButtonTypeCustom];
    [bt addTarget:self action:@selector(dataTypeChoose:) forControlEvents:UIControlEventTouchUpInside];
    [contentV addSubview:bt];
    [bt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.bottom.and.top.equalTo(contentV);
    }];
    
    if ([title isEqualToString:@"进账"]) {
        incomeBt = bt;
    }else if([title isEqualToString:@"支出"]){
        expenseBt = bt;
    }else{
        allTypeBt = bt;
    }
    return contentV;
}


- (void)setBalance:(NSInteger)balance{
    NSString * balanceStr = [NSString stringWithFormat:@"余额   %ld 元",balance];
    balanceLb.text = balanceStr;
}

- (void)sureAction{
    [self.delegate getRewardDataWithStartTime:self.startDate endDate:self.endDate withType:self.dataType];
}

- (void)startTimeChoose{
    AppDelegate * appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    UIWindow * window = appdelegate.window;
    [window addSubview:self.datePickerContentV];
    self.datePicker.date = [NSDate date];
    if (self.endDate) {
        self.datePicker.maximumDate = self.endDate;
        self.datePicker.date = self.endDate;
        self.datePicker.minimumDate = nil;
    }else{
        self.datePicker.maximumDate = nil;
        self.datePicker.minimumDate = nil;
    }
    self.datePickerContentV.tag = StartPickerTag;
}

- (void)endTimeChoose{
    AppDelegate * appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    UIWindow * window = appdelegate.window;
    [window addSubview:self.datePickerContentV];
    self.datePicker.date = [NSDate date];
    if (self.startDate) {
        self.datePicker.minimumDate = self.startDate;
        self.datePicker.date = self.startDate;
        self.datePicker.maximumDate = nil;
    }else{
        self.datePicker.maximumDate = nil;
        self.datePicker.minimumDate = nil;
    }
    self.datePickerContentV.tag = EndPickerTag;
}

- (UIView *)datePickerContentV{
    if (!_datePickerContentV) {
        _datePickerContentV= [[UIView alloc] initWithFrame:CGRectMake(0, ScreenHeight-265, ScreenWidth, 261)];
        _datePickerContentV.backgroundColor = COLOR(240, 240, 240, 1);
        self.datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 261-216, ScreenWidth, 216)];
        _datePicker.backgroundColor = COLOR(230, 230, 230, 1);
        _datePicker.datePickerMode = UIDatePickerModeDate;
        _datePicker.minimumDate = [NSDate date];
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];//设置为中文 默认为英文
        _datePicker.locale = locale;
        [_datePickerContentV addSubview:_datePicker];
        
        UIButton * sureBt = [UIButton buttonWithType:UIButtonTypeCustom];
        [sureBt setTitle:@"确定" forState:UIControlStateNormal];
        [sureBt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        sureBt.titleLabel.font = [UIFont systemFontOfSize:14];
        [sureBt addTarget:self action:@selector(datePickerSure) forControlEvents:UIControlEventTouchUpInside];
        sureBt.frame = CGRectMake(ScreenWidth-70, 5, 60, 30);
        [_datePickerContentV addSubview:sureBt];
        
        UIButton * cancelBt = [UIButton buttonWithType:UIButtonTypeCustom];
        [cancelBt setTitle:@"取消" forState:UIControlStateNormal];
        [cancelBt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        cancelBt.titleLabel.font = [UIFont systemFontOfSize:14];
        [cancelBt addTarget:self action:@selector(datePickerCancel) forControlEvents:UIControlEventTouchUpInside];
        cancelBt.frame = CGRectMake(5, 5, 60, 30);
        [_datePickerContentV addSubview:cancelBt];
    }
    return _datePickerContentV;
}

-(void)datePickerSure{
    NSInteger tag = self.datePickerContentV.tag;
    if (tag == StartPickerTag) {
        self.startDate = self.datePicker.date;
        NSString * time = [BaseHelper stringFromDate:self.startDate format:kDateFormatTypeYYYYMMDD];
        [startTimeBt setTitle:time forState:UIControlStateNormal];
    }else{
        self.endDate = self.datePicker.date;
        NSString * time = [BaseHelper stringFromDate:self.endDate format:kDateFormatTypeYYYYMMDD];
        [endTimeBt setTitle:time forState:UIControlStateNormal];
    }
    [self datePickerCancel];
}

- (void)datePickerCancel{
    [self.datePickerContentV removeFromSuperview];
}

- (void)dataTypeChoose:(UIButton *)bt{
    if (bt == incomeBt) {
        
        UIImageView * imgV_in = [[bt superview]viewWithTag:TypeImgVTag];
        imgV_in.image = [UIImage imageNamed:@"circle_sel"];
        
        UIImageView * imgV_out = [[expenseBt superview]viewWithTag:TypeImgVTag];
        imgV_out.image = [UIImage imageNamed:@"circle"];
        
        UIImageView * imgV_all = [[allTypeBt superview]viewWithTag:TypeImgVTag];
        imgV_all.image = [UIImage imageNamed:@"circle"];
        
        self.dataType = @"RECHARGE";
    }else if(bt == expenseBt){
        
        UIImageView * imgV_in = [[bt superview]viewWithTag:TypeImgVTag];
        imgV_in.image = [UIImage imageNamed:@"circle_sel"];
        
        UIImageView * imgV_out = [[incomeBt superview]viewWithTag:TypeImgVTag];
        imgV_out.image = [UIImage imageNamed:@"circle"];
        
        UIImageView * imgV_all = [[allTypeBt superview]viewWithTag:TypeImgVTag];
        imgV_all.image = [UIImage imageNamed:@"circle"];
        
        self.dataType = @"ORDER_PAYMENT";
    }else{
        UIImageView * imgV_all = [[bt superview]viewWithTag:TypeImgVTag];
        imgV_all.image = [UIImage imageNamed:@"circle_sel"];
        
        UIImageView * imgV_out = [[expenseBt superview]viewWithTag:TypeImgVTag];
        imgV_out.image = [UIImage imageNamed:@"circle"];
        
        UIImageView * imgV_in = [[incomeBt superview]viewWithTag:TypeImgVTag];
        imgV_in.image = [UIImage imageNamed:@"circle"];
        
        self.dataType = nil;
    }
}

- (UILabel *)lb:(NSString *)text{
    UILabel * lb = [[UILabel alloc] init];
    lb.textAlignment = NSTextAlignmentCenter;
    if (text) {
        lb.text = text;
    }
    lb.font = [UIFont boldSystemFontOfSize:20];
    lb.textColor = [UIColor whiteColor];
    [self addSubview:lb];
    return lb;
}
@end

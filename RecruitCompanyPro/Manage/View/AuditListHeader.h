//
//  AuditListHeader.h
//  RecruitCompanyPro
//
//  Created by Michael on 2018/1/10.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>

#define HRAuditListHeaderH  VerPxFit(50)
#define HRAuditListHeaderHWithActionBt  VerPxFit(70)

@protocol AuditListHeaderDelegate
@optional
// hr审核不通过一键全选
- (void)hrFailAllSelected:(BOOL)isSelected;
// hr审核通过一键全选
- (void)hrPassAllSelected:(BOOL)isSelected;
@end

@interface AuditListHeader : UIView

@property (nonatomic , weak) id<AuditListHeaderDelegate>delegate;

- (void)actionBtAppearShift:(BOOL)appearType;

@end

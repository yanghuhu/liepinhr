//
//  HomeContainViewController.m
//  RecruitCompanyPro
//
//  Created by Michael on 2018/1/25.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "HomeContainViewController.h"
#import "AuditListHeader.h"
#import "HRAuditeModel.h"
#import "HRTableViewCell.h"
#import "AuditJDListHeader.h"
#import "UIView+YYAdd.h"
#import "JDAuditeModel.h"
#import "JDTableViewCell.h"
#import "ManageModule.h"
#import "AppDelegate.h"
#import "LoginViewController.h"
#import "BaseNavigationController.h"
#import "NSArray+YYAdd.h"
#import "AuditView.h"
#import "RewardView.h"
#import "UIImage+YYAdd.h"
#import "UserCenterViewController.h"
#import "BaseNavigationController.h"

typedef enum {
    ContainItemTypeHRAudit,   //  容器
    ContainItemTypeJDAudit,
    ContainItemTypeReward
}ContainItemType;

#define SelectColor COLOR(40, 225, 210, 1)
#define UNSelectColor [UIColor whiteColor]

@interface HomeContainViewController ()<AuditViewDelegate,RewardViewDelegate>{

    UIButton * hrAuditBt;
    UIButton * jdAuditBt;
    UIButton * rewardBt;
    UIImageView * itemSepLine;

    UIButton * dealWaitBt;
    UIButton * dealFinishBt;
}

@property (nonatomic , strong)  AuditView * auditView;        //  审核view
@property (nonatomic , strong)  RewardView * rewardView;        // 赏金view
@property (nonatomic , assign) ContainItemType  containItemType;     //  当前 view 的type

@end

@implementation HomeContainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBar.hidden = YES;
    self.view.backgroundColor =  COLOR(18, 20, 26, 1);
    
    self.containItemType = ContainItemTypeJDAudit;
    [self createSubViews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (_containItemType == ContainItemTypeJDAudit) {
        self.auditView.listType = ListTypeJD;
        [self.view addSubview:_auditView];
    }else if (_containItemType == ContainItemTypeHRAudit){
        self.auditView.listType = ListTypeHR;
        [self.view addSubview:_auditView];
    }else if (_containItemType == ContainItemTypeReward){
        [self.view addSubview:_rewardView];
    }
}

- (void)createSubViews{
    UIView * leftView = [[UIView alloc] init];
    [self.view addSubview:leftView];
    @weakify(self)
    [leftView mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.and.left.and.bottom.equalTo(self.view);
        make.width.mas_equalTo(HorPxFit(245));
    }];
    UIImageView * leftBkImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"leftBk"]];
    [leftView addSubview:leftBkImg];
    [leftBkImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.top.and.right.and.bottom.equalTo(leftView);
    }];
    
    UIButton * userHomeBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [userHomeBt addTarget:self action:@selector(toUserHomeVCAction) forControlEvents:UIControlEventTouchUpInside];
    [userHomeBt setImage:[[UIImage imageNamed:@"UserHome"] imageByTintColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    userHomeBt.titleLabel.font = [UIFont systemFontOfSize:20];
    [userHomeBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    userHomeBt.frame = CGRectMake(HorPxFit(15), VerPxFit(40), 100, 40);
    [self.view addSubview:userHomeBt];
    
    [userHomeBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(HorPxFit(40));
        make.top.mas_equalTo(VerPxFit(40));
        make.size.mas_equalTo(CGSizeMake(60, 40));
    }];
    
    jdAuditBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [jdAuditBt addTarget:self action:@selector(JobAuditAction) forControlEvents:UIControlEventTouchUpInside];
    [jdAuditBt setTitleColor:SelectColor forState:UIControlStateNormal];
    jdAuditBt.titleLabel.font = [UIFont systemFontOfSize:19];
    [jdAuditBt setTitle:@"职位审批" forState:UIControlStateNormal];
    [leftView addSubview:jdAuditBt];
    [jdAuditBt mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(leftView);
        make.height.mas_equalTo(45);
        make.centerY.mas_equalTo(leftView.mas_centerY);
    }];
    
    itemSepLine = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"segmentBk"]];
    [leftView addSubview:itemSepLine];
    [itemSepLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(jdAuditBt.mas_bottom);
        make.centerX.mas_equalTo(leftView.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(133), VerPxFit(5)));
    }];
    
    hrAuditBt = [UIButton buttonWithType:UIButtonTypeCustom];
    hrAuditBt.titleLabel.font = [UIFont systemFontOfSize:19];
    [hrAuditBt addTarget:self action:@selector(HRAuditAction) forControlEvents:UIControlEventTouchUpInside];
    [hrAuditBt setTitleColor:UNSelectColor forState:UIControlStateNormal];
    [hrAuditBt setTitle:@"HR审批" forState:UIControlStateNormal];
    [leftView addSubview:hrAuditBt];
    [hrAuditBt mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(leftView);
        make.bottom.mas_equalTo(jdAuditBt.mas_top).mas_offset(-VerPxFit(10));
        make.height.mas_equalTo(45);
    }];
    
    rewardBt = [UIButton buttonWithType:UIButtonTypeCustom];
    rewardBt.titleLabel.font = [UIFont systemFontOfSize:19];
    [rewardBt addTarget:self action:@selector(rewardAction) forControlEvents:UIControlEventTouchUpInside];
    [rewardBt setTitleColor:UNSelectColor forState:UIControlStateNormal];
    [rewardBt setTitle:@"赏金库" forState:UIControlStateNormal];
    [leftView addSubview:rewardBt];
    [rewardBt mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(leftView);
        make.top.mas_equalTo(jdAuditBt.mas_bottom).mas_offset(VerPxFit(10));
        make.height.mas_equalTo(45);
    }];
}

- (void)toUserHomeVCAction{
    UserCenterViewController * vc = [[UserCenterViewController alloc] init];
    BaseNavigationController * nav = [[BaseNavigationController alloc] initWithRootViewController:vc];
    nav.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewController:nav animated:YES completion:^{}];
}

- (AuditView *)auditView{
    if (!_auditView) {
        _auditView = [[AuditView alloc] initWithFrame:CGRectMake(HorPxFit(245), 0, CGRectGetWidth(self.view.frame)-HorPxFit(245), CGRectGetHeight(self.view.frame))];
        _auditView.delegate = self;
        _auditView.backgroundColor = COLOR(18, 20, 26, 1);
    }
    return _auditView;
}

- (RewardView *)rewardView{
    if (!_rewardView) {
        _rewardView = [[RewardView alloc] initWithFrame:CGRectMake(HorPxFit(245), 0, CGRectGetWidth(self.view.frame)-HorPxFit(245), CGRectGetHeight(self.view.frame))];
        _rewardView.delegate = self;
        _rewardView.backgroundColor = COLOR(18, 20, 26, 1);
    }
    return _rewardView;
}

//  切换到HR审核页面
- (void)HRAuditAction{
    
    if (_containItemType == ContainItemTypeHRAudit) {
        return;
    }
    if ([self.rewardView superview]) {
        [self.rewardView removeFromSuperview];
    }
    if (![self.auditView superview]) {
        [self.view addSubview:self.auditView];
    }
    self.containItemType = ContainItemTypeHRAudit;
    _auditView.listType = ListTypeHR;
    [jdAuditBt setTitleColor:UNSelectColor forState:UIControlStateNormal];
    [hrAuditBt setTitleColor:SelectColor forState:UIControlStateNormal];
    [rewardBt setTitleColor:UNSelectColor forState:UIControlStateNormal];
    
    itemSepLine.top = CGRectGetMaxY(hrAuditBt.frame);
}
//  切换到职位审核页面
- (void)JobAuditAction{
    if (_containItemType == ContainItemTypeJDAudit) {
        return;
    }
    self.containItemType = ContainItemTypeJDAudit;
    
    if ([self.rewardView superview]) {
        [self.rewardView removeFromSuperview];
    }
    if (![self.auditView superview]) {
        [self.view addSubview:self.auditView];
    }
    self.auditView.listType = ListTypeJD;
    [jdAuditBt setTitleColor:SelectColor forState:UIControlStateNormal];
    [hrAuditBt setTitleColor:UNSelectColor forState:UIControlStateNormal];
    [rewardBt setTitleColor:UNSelectColor forState:UIControlStateNormal];
    
    itemSepLine.top = CGRectGetMaxY(jdAuditBt.frame);
}
//  切换到赏金页面
- (void)rewardAction{
    if (_containItemType == ContainItemTypeReward) {
        return;
    }
    self.containItemType = ContainItemTypeReward;
    [jdAuditBt setTitleColor:UNSelectColor forState:UIControlStateNormal];
    [hrAuditBt setTitleColor:UNSelectColor forState:UIControlStateNormal];
    [rewardBt setTitleColor:SelectColor forState:UIControlStateNormal];
    
    if ([self.auditView superview]) {
        [self.auditView removeFromSuperview];
    }
    [self.view addSubview:self.rewardView];
    itemSepLine.top = CGRectGetMaxY(rewardBt.frame);
    if(!_rewardView.dataSource){
        [self getCompanyBalance];
    }
}

#pragma mark -- AuditViewDelegate
- (void)getJDWaitauditWithPage:(NSInteger)page{
    [ManageModule getJobAuditWaitWithPage:page success:^(NSArray *array){
        [_auditView refreshJDWaitauditFinish:YES withArray:array];
    } failure:^(NSString *error, ResponseType responseType) {
        [_auditView refreshJDWaitauditFinish:NO withArray:nil];
        if (error) {
            [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        }
    }];
}

- (void)getJDAuditFinishWithPage:(NSInteger)page{
    
    [ManageModule getJobAuditFinishedWithPage:page success:^(NSArray *array){
        [_auditView refreshJDAuditFinishFinish:YES withArray:array];
    } failure:^(NSString *error, ResponseType responseType) {
        [_auditView refreshJDAuditFinishFinish:NO withArray:nil];
        if (error) {
            [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        }
    }];
}

- (void)getHRWaitauditWithPage:(NSInteger)page{
    [ManageModule getHRAuditWaitWithPage:page success:^(NSArray *array){
        [_auditView refreshHRWaitauditFinish:YES withArray:array];
    } failure:^(NSString *error, ResponseType responseType) {
        [_auditView refreshHRWaitauditFinish:NO withArray:nil];
        if (error) {
            [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        }
    }];
}

- (void)getHRAuditFinishWithPage:(NSInteger)page{
    [ManageModule getHRAuditFinishedWithPage:page success:^(NSArray *array){
        [_auditView refreshHRAuditFinishFinish:YES withArray:array];
    } failure:^(NSString *error, ResponseType responseType) {
        [_auditView refreshHRAuditFinishFinish:NO withArray:nil];
        if (error) {
            [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        }
    }];
}

- (void)hrAudtionActionWithData:(NSArray *)array{
    NSMutableArray * tempArray = [NSMutableArray arrayWithCapacity:array.count];
    for (HRAuditeModel * model in array) {
        if (model.auditString) {
            NSDictionary * dic = @{@"result":model.auditString,@"id":@(model.id_)};
            [tempArray addObject:dic];
        }
    }
    NSDictionary * dic = @{@"hrAuditItems":tempArray};
    [BaseHelper showProgressLoadingInView:self.view];
    [ManageModule HRAuditWithArray:dic success:^{
        [BaseHelper hideProgressHudInView:self.view];
        [_auditView hrAuditActionFinishWithData:tempArray];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        if (error) {
            [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        }
    }];
}

- (void)jdAudtionActionWithData:(NSArray *)array{
    NSMutableArray * tempArray = [NSMutableArray arrayWithCapacity:array.count];
    for (JDAuditeModel * model in array) {
        if (model.auditString) {
            NSDictionary * dic = @{@"auditResult":model.auditString,@"id":@(model.id_),@"comment":@""};
            [tempArray addObject:dic];
        }
    }
    NSDictionary * dic = @{@"detailAuditItems":tempArray };
    [BaseHelper showProgressLoadingInView:self.view];
    [ManageModule jobAuditWithArray:dic success:^{
        [BaseHelper hideProgressHudInView:self.view];
        [_auditView jdAuditActionFinishWithData:tempArray];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        if (error) {
            [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        }
    }];
}

#pragma mark -- RewardViewDelegate
- (void)getRewardDataWithStartTime:(NSDate *)startDate endTiem:(NSDate *)endDate dataType:(NSString *)type withPage:(NSInteger)page{
    
    [BaseHelper showProgressLoadingInView:self.view];
    [ManageModule getCompanyRewardDetailWithStartTime:startDate?[startDate timeIntervalSince1970]*1000:0 endTime:endDate?[endDate timeIntervalSince1970]*1000:0 page:page changeType:type success:^(NSArray * dataArray){
        [BaseHelper hideProgressHudInView:self.view];
        _rewardView.dataSource = [NSMutableArray arrayWithArray:dataArray];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        if (error) {
            [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        }
    }];
}

- (void)getCompanyBalance{
    [ManageModule getCompanyBalanceSuccess:^(NSNumber * number){
        NSInteger balance = 0;
        if (number) {
            balance = [number longLongValue];
        }
        [_rewardView updateBalanceAppear:balance];
    } failure:^(NSString *error, ResponseType responseType) {
        
    }];
}

@end


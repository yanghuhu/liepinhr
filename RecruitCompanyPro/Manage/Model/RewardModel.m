//
//  RewardModel.m
//  RecruitCompanyPro
//
//  Created by Michael on 2018/2/6.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "RewardModel.h"

@implementation RewardModel

+ (NSDictionary *)modelCustomPropertyMapper {
    
    return @{@"id_":@"id",@"money":@"balance",@"desc":@"detail"};
}


- (NSTimeInterval)createTime{
    return _createTime/1000;
}

- (NSInteger)money{
    return _money/100;
}

@end

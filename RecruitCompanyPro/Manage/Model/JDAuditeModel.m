//
//  JDAuditeModel.m
//  RecruitCompanyPro
//
//  Created by Michael on 2018/1/11.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "JDAuditeModel.h"

@implementation JDAuditeModel

+ (NSDictionary *)modelCustomPropertyMapper {
    return @{@"id_":@"id"};
}

- (CGFloat)total{
    return  _jobDetailRecruitingNumber * _jobDetailPrice / 100;
}

- (CGFloat)jobDetailPrice{
    return _jobDetailPrice / 100;
}

@end

//
//  JDAuditeModel.h
//  RecruitCompanyPro
//
//  Created by Michael on 2018/1/11.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JDAuditeModel : NSObject

@property (nonatomic , assign) NSInteger id_;       //   id
@property (nonatomic , strong) NSString * jobDetailTitle;     //职位标题
@property (nonatomic , strong) NSString * jobDetailLevel;      // 职位级别
@property (nonatomic , assign) NSInteger jobDetailRecruitingNumber;    // 职位招聘人数
@property (nonatomic , assign) CGFloat jobDetailPrice;          // 职位赏金
@property (nonatomic , strong) NSString * jobDetailCreatorUsername;   // hr 姓名
@property (nonatomic , assign) CGFloat  total;          // 职位赏金和
@property (nonatomic , strong) NSString * hrComment;    // 备注
@property (nonatomic , strong) NSString * auditResult;   // 处理结果

@property (nonatomic , assign) BOOL noteIsShow;    //  是否显示备注
@property (nonatomic , strong) NSString * auditString;  // 审核操作结果

@end

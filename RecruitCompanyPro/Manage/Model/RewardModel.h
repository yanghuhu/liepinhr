//
//  RewardModel.h
//  RecruitCompanyPro
//
//  Created by Michael on 2018/2/6.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RewardModel : NSObject

@property (nonatomic , assign) NSInteger id_;   //  id
@property (nonatomic , assign) NSTimeInterval  createTime;   // 产生时间
@property (nonatomic , assign) NSInteger money;         //  金额
@property (nonatomic , strong) NSString * serialNumber;    // 账单号
@property (nonatomic , strong) NSString * desc;         //  账单描述

@end

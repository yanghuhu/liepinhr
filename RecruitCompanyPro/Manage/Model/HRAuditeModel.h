//
//  HRAuditeModel.h
//  RecruitCompanyPro
//
//  Created by Michael on 2018/1/10.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HRAuditeModel : NSObject

@property (nonatomic , assign) NSInteger id_;     //  id
@property (nonatomic , strong) NSString * jobNumber;       //  工号
@property (nonatomic , strong) NSString * idCardNumber;    // 身份证号
@property (nonatomic , strong) NSString * username;        // 用户名
@property (nonatomic , strong) NSString * position;        // 职位
@property (nonatomic , strong) NSString * dealResult;      // 处理结果

@property (nonatomic , strong) NSString * auditString;  // 审核操作结果

@end

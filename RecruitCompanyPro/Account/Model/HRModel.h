//
//  HRModel.h
//  HSBCTempPro
//
//  Created by Michael on 2017/11/13.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CompanyModel.h"

@interface HRModel : NSObject

@property (nonatomic , assign) NSInteger id_;        // id
@property (nonatomic , strong) NSString * nickName;      // 姓名
@property (nonatomic , strong) NSString * headPic;    // 头像
@property (nonatomic , strong) NSString * cellphone;     // 联系方式
@property (nonatomic , strong) NSString * type;      //  'HR', 'HEADHUNTER', 'HR_MANAGER', 'PLATFORM_MARKECTER'

@property (nonatomic , strong) NSString * state;    // 'Using', 'Auditing', 'Disabled'
@property (nonatomic , assign) CGFloat  score;   // 评价分数
@property (nonatomic , assign) NSInteger evaluateCount;  //总评价次数

@property (nonatomic , strong) NSString * netToken;  //  网络请求token

@property (nonatomic , strong) CompanyModel * company;


@end

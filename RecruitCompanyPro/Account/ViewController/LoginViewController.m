//
//  LoginViewController.m
//  RecruitCompanyPro
//
//  Created by Michael on 2017/11/29.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "LoginViewController.h"
#import "LoginContentView.h"
#import "RegisterContentView.h"
#import "AppDelegate.h"
#import "JobCenterViewController.h"
#import "BaseNavigationController.h"
#import "RecruitmentModule.h"
#import "UIView+YYAdd.h"
#import "AccountModule.h"
#import "HomeContainViewController.h"
#import "HRModel.h"
#import "RateView.h"
#import "PayPopView.h"
#import "FindPwdView.h"
#import "ResetPwdView.h"
#import "RecruitmentModule.h"

@interface LoginViewController ()<LoginContentViewDelegate,RegisterContentViewDelegate,FindPwdViewDelegate>{
}

@property (nonatomic , strong) LoginContentView * loginView;    // 登录容器view
@property (nonatomic , strong) RegisterContentView * registeView;   //  注册容器view
@property (nonatomic , strong) FindPwdView * findPwdView;           //  找回密码容器view
@property (nonatomic , strong) NSMutableArray * companyList;        //  企业列表
@property (nonatomic , assign) NSInteger page;     // 企业列表页码
@property (nonatomic,  strong) ResetPwdView *resetPwdView;          //找回密码二级页面

@property (nonatomic , strong) NSString * finePwdRole;  // 找回你们role

@property (nonatomic , strong) NSString * loginImageToken;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = RGB16(0x00eaff);
    self.navigationController.navigationBar.hidden = YES;
    
    UIImageView * bkImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"loginBk"]];
    [self.view addSubview:bkImg];
    [bkImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.top.and.bottom.equalTo(self.view);
    }];
    [self.view addSubview:self.loginView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (LoginContentView *)loginView{
    if (!_loginView) {
        CGFloat contentW = HorPxFit(600);
        CGFloat contentH = VerPxFit(500);
        _loginView = [[LoginContentView alloc] initWithFrame:CGRectMake(0, 0, contentW, contentH)];
        _loginView.delegate = self;
        _loginView.layer.cornerRadius = 20;
        _loginView.backgroundColor = [UIColor whiteColor];
        _loginView.center = self.view.center;
    }
    return _loginView;
}

- (RegisterContentView *)registeView{
    if (!_registeView) {
        CGFloat contentW = HorPxFit(600);
        CGFloat contentH = VerPxFit(500);
        _registeView = [[RegisterContentView alloc] initWithFrame:CGRectMake(0, contentH, contentW, contentH)];
        _registeView.delegate = self;
        _registeView.layer.cornerRadius = 20;
        _registeView.alpha = 0;
        _registeView.backgroundColor = [UIColor whiteColor];
        _registeView.center = self.view.center;
    }
    return _registeView;
}

- (FindPwdView *)findPwdView{
    if (!_findPwdView) {
        CGFloat contentW = HorPxFit(600);
        CGFloat contentH = VerPxFit(350);
        _findPwdView = [[FindPwdView alloc] initWithFrame:CGRectMake(0, contentH, contentW, contentH)];
        _findPwdView.delegate = self;
        _findPwdView.layer.cornerRadius = 20;
        _findPwdView.alpha = 0;
        _findPwdView.backgroundColor = [UIColor whiteColor];
        _findPwdView.center = self.view.center;
    }
    return _findPwdView;
}

- (ResetPwdView *)resetPwdView{
    if (!_resetPwdView) {
        CGFloat contentW = HorPxFit(600);
        CGFloat contentH = VerPxFit(350);
        _resetPwdView = [[ResetPwdView alloc] initWithFrame:CGRectMake(0, contentH, contentW, contentH)];
        _resetPwdView.delegate = self;
        _resetPwdView.layer.cornerRadius = 20;
        _resetPwdView.alpha = 0;
        _resetPwdView.backgroundColor = [UIColor whiteColor];
        _resetPwdView.center = self.view.center;
    }
    return _resetPwdView;
}

- (void)getCompanyList{

    [RecruitmentModule getCompanyListWithPageNum:_page success:^(NSArray *companys){
        [BaseHelper hideProgressHudInView:self.view];
        if (_page == 0) {
            [_companyList removeAllObjects];
        }
        if (companys && companys.count != 0) {
            [_companyList addObjectsFromArray:companys];
        }
        [_registeView updateCompanyList:_companyList];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}

#pragma mark -- LoginContentViewDelegate
- (void)loginWithPhone:(NSString *)phone password:(NSString *)password verifyCode:(NSString *)verifyCode imageToken:(NSString *)token{
    
    @weakify(self)
    [BaseHelper showProgressLoadingInView:self.view];
    
    if (_loginView.loginType == LoginTypeAccount) {
        [AccountModule loginWithPhone:phone password:password  verifyCode:verifyCode verifyImgToken:token role:self.loginView.loginRole success:^(HRModel *model){
            @strongify(self)
            [BaseHelper hideProgressHudInView:self.view];
            [self shiftWindowRoot];
            if (_loginView.isAutoLogin) {
                if ([[HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel.type isEqualToString:@"HR"]) {
                    if ([[HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel.state isEqualToString:@"Auditing"]) {
                        return;
                    }
                    if ([[HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel.state isEqualToString:@"Disabled"]) {
                        return;
                    }
                }
                NSDictionary * info = @{@"cellPhone":phone,@"id":@(model.id_),@"type":model.type,@"netToken":[HSBCGlobalInstance sharedHSBCGlobalInstance].netWorkToken};
                [BaseHelper saveUserToDataDefault:info];
            }
        } failure:^(NSString *error, ResponseType responseType) {
            @strongify(self)
            [BaseHelper hideProgressHudInView:self.view];
            [self netFailWihtError:error andStatusCode:responseType];
        }];
    }else{
        [AccountModule loginWithSmsPhone:phone verifyCode:verifyCode role:self.loginView.loginRole  success:^(HRModel *model){
            @strongify(self)
            [BaseHelper hideProgressHudInView:self.view];
            [self shiftWindowRoot];
            if (_loginView.isAutoLogin) {
                if ([[HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel.type isEqualToString:@"HR"]) {
                    if ([[HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel.state isEqualToString:@"Auditing"]) {
                        return;
                    }
                    if ([[HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel.state isEqualToString:@"Disabled"]) {
                        return;
                    }
                }
                NSDictionary * info = @{@"cellPhone":phone,@"id":@(model.id_),@"type":model.type,@"netToken":[HSBCGlobalInstance sharedHSBCGlobalInstance].netWorkToken};
                [BaseHelper saveUserToDataDefault:info];
            }
        } failure:^(NSString *error, ResponseType responseType) {
            @strongify(self)
            [BaseHelper hideProgressHudInView:self.view];
            [self netFailWihtError:error andStatusCode:responseType];
        }];
    }
}

- (void)registerAction{
    if (!self.companyList) {
        _page = 0;
        self.companyList = [NSMutableArray array];
        [BaseHelper showProgressLoadingInView:self.view];
        [self getCompanyList];
    }
    
    [self.view addSubview:self.registeView];
    [UIView animateWithDuration:0.4 animations:^{
        _registeView.center = self.view.center;
        _loginView.bottom = _registeView.top;
        _loginView.alpha = 0;
        _registeView.alpha = 1;
    } completion:^(BOOL finished) {
        [_loginView removeFromSuperview];
    }];
}

- (void)forgetPassword{
    [self.view addSubview:self.findPwdView];
    
    [UIView animateWithDuration:0.4 animations:^{
        self.findPwdView.center = self.view.center;
        _loginView.bottom = self.findPwdView.top;
        _loginView.alpha = 0;
        self.findPwdView.alpha = 1;
    } completion:^(BOOL finished) {
        [_loginView removeFromSuperview];
    }];
}

- (void)showResetPwdView{
    [self.view addSubview:self.resetPwdView];
    @weakify(self)
    [UIView animateWithDuration:0.4 animations:^{
        @strongify(self)
        self.resetPwdView.center = self.view.center;
        _findPwdView.bottom = self.resetPwdView.top;
        _findPwdView.alpha = 0;
        self.resetPwdView.alpha = 1;
    } completion:^(BOOL finished) {
        [_findPwdView removeFromSuperview];
    }];
}

- (void)findPwdBackToLogin{
    [self.view addSubview:_loginView];
    [UIView animateWithDuration:0.4 animations:^{
        _loginView.center = self.view.center;
        _findPwdView.top = _loginView.bottom;
        _loginView.alpha = 1;
        _findPwdView.alpha = 0;
    } completion:^(BOOL finished) {
        [_findPwdView removeFromSuperview];
    }];
}

- (void)shiftWindowRoot{
    
    UIViewController * vc;
    if ([[HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel.type isEqualToString:@"HR"]) {
        if ([[HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel.state isEqualToString:@"Auditing"]) {
            [BaseHelper showProgressHud:@"您的注册申请已提交，敬请企业审批" showLoading:NO canHide:YES];
            [self backToLogin];
            return;
        }
        if ([[HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel.state isEqualToString:@"Disabled"]) {
            [BaseHelper showProgressHud:@"账号已注销!" showLoading:NO canHide:YES];
            return;
        }
        vc = [[JobCenterViewController alloc] init];
    }else{
        vc = [[HomeContainViewController alloc] init];
    }
    if (!vc) {
        return;
    }
    BaseNavigationController * nav = [[BaseNavigationController alloc] initWithRootViewController:vc];
    AppDelegate * appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    appdelegate.window.rootViewController = nav;
}

#pragma mark -- RegisterContentViewDelegate
- (void)registerWithCompanyId:(NSInteger)companyId jobNum:(NSString *)jobNum cardNum:(NSString *)cardNum cellphone:(NSString *)phone password:(NSString *)password verifyCode:(NSString *)verifyCode{
    @weakify(self)
    [BaseHelper showProgressLoadingInView:self.view];
    [AccountModule registerWithCompanyId:companyId jobNum:jobNum cardNum:cardNum phone:phone password:password verifyCode:verifyCode success:^(HRModel * hrModel){
        @strongify(self)
        [BaseHelper hideProgressHudInView:self.view];
        [self shiftWindowRoot];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}

- (void)backToLogin{
    [self.view addSubview:_loginView];
    [UIView animateWithDuration:0.4 animations:^{
        _loginView.center = self.view.center;
        _registeView.top = _loginView.bottom;
        _loginView.alpha = 1;
        _registeView.alpha = 0;
    } completion:^(BOOL finished) {
        [_registeView removeFromSuperview];
    }];
}

- (void)getVerifyCode:(NSString *)phone{
    [BaseHelper showProgressLoadingInView:self.view];
    [RecruitmentModule getSMSVerifyCodeWithPhone:phone type:@"Register" Success:^{
        [BaseHelper hideProgressHudInView:self.view];
        [_registeView getVerifyCodeSuccess];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}

#pragma mark -- FindPwdViewDelegate
- (void)finePwdWithPhone:(NSString *)phone verifyCode:(NSString *)verifyCode withRole:(NSString *)role{
    @weakify(self)
    self.finePwdRole = role;
    [BaseHelper showProgressLoadingInView:self.view];
    [AccountModule resetPasswordWithRole:role phone:phone verifyCode:verifyCode success:^(NSString *token){
        @strongify(self)
        [BaseHelper hideProgressHudInView:self.view];
        [self showResetPwdView];
        
    } failure:^(NSString *error, ResponseType responseType) {
        @strongify(self)
        [BaseHelper hideProgressHudInView:self.view];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}

- (void)resetPwdBackTofind{
    [self.view addSubview:_findPwdView];
    [UIView animateWithDuration:0.4 animations:^{
        _findPwdView.center = self.view.center;
        _resetPwdView.top = _findPwdView.bottom;
        _findPwdView.alpha = 1;
        _resetPwdView.alpha = 0;
    } completion:^(BOOL finished) {
        [_resetPwdView removeFromSuperview];
    }];
}

- (void)resetPwdBackToLogin{
    [self.view addSubview:_loginView];
    [UIView animateWithDuration:0.4 animations:^{
        _loginView.center = self.view.center;
        _resetPwdView.top = _loginView.bottom;
        _loginView.alpha = 1;
        _resetPwdView.alpha = 0;
    } completion:^(BOOL finished) {
        [_resetPwdView removeFromSuperview];
    }];
}

- (void)resetPwdWithNewPwd:(NSString *)newPwd{
    @weakify(self);
    NSString *token = [[HSBCGlobalInstance sharedHSBCGlobalInstance] netWorkToken];
    [AccountModule nextResetPasswordWithNewPwd:newPwd token:token role:self.finePwdRole success:^{
        @strongify(self)
        [BaseHelper hideProgressHudInView:self.view];
        [BaseHelper showProgressHud:@"密码修改成功!" showLoading:NO canHide:YES];
        [self resetPwdBackToLogin];
        
    } failure:^(NSString *error, ResponseType responseType) {
        @strongify(self)
        [BaseHelper hideProgressHudInView:self.view];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}

- (void)getVerifyCodeForGetPwd:(NSString *)phone{
    [BaseHelper showProgressLoadingInView:self.view];
    [RecruitmentModule getSMSVerifyCodeWithPhone:phone type:@"Forget" Success:^{
        [BaseHelper hideProgressHudInView:self.view];
        [_findPwdView getVerifyCodeSuccess];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}

@end

//
//  LoginAccountScrollView.m
//  RecruitCompanyPro
//
//  Created by Michael on 2017/11/29.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "LoginAccountScrollView.h"
#import "FlyTextFile.h"
#import "RecruitmentModule.h"
#import "UIImageView+YYWebImage.h"

@interface LoginAccountScrollView()<UITextFieldDelegate,FlyTextFileDelegate>{
    
    UIView * accountContentView;  //  账号登录容器view
    UIView *smsContentView;       //  短信登录容器view
    
    NSString * phoneAc;        // 账号登录手机号
    NSString * pwdAC;           // 账号登录密码
    NSString * vercodeAc;        // 账号登录验证码
    
    NSString * smsSms;          // 短信登录 短信验证码
    NSString * phoneSms;        //短信登录 手机号
    
    FlyTextFile * phoneTfAc;      // 账号登录手机号tf
    FlyTextFile * pwdTfAC;          // 账号登录密码tf
    FlyTextFile * vercodeTfAc;      // 账号登录验证码tf
    UIImageView * verCodeImgAc;     // 账号登录验证码imgview
    
    FlyTextFile * phoneTfSms;       //短信登录 手机号tf
    FlyTextFile * smsTfSms;         // 短信登录 短信验证码
    UIButton * getSmsButton;     // 获取短信验证码按钮
    
    UIButton * pwdFlagV;      // 密码显隐藏按钮
}

@property (nonatomic , strong) NSString * verifyCodeImageUrl;

@end

@implementation LoginAccountScrollView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    accountContentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame))];
    accountContentView.backgroundColor = [UIColor clearColor];
    [self addSubview:accountContentView];
    
    smsContentView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(accountContentView.frame), 0, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame))];
    smsContentView.backgroundColor = [UIColor clearColor];
    [self addSubview:smsContentView];
    
    //  手机号  密码，  验证码
    CGFloat horPadding = HorPxFit(120);

    phoneTfAc = [self tf:@"手机号"];
    phoneTfAc.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [accountContentView addSubview:phoneTfAc];
    [phoneTfAc mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(accountContentView).mas_offset(horPadding);
        make.top.equalTo(accountContentView).mas_offset(InputVerPadding);
        make.right.equalTo(accountContentView).mas_offset(-horPadding);
        make.height.mas_equalTo(CellH);
    }];

    pwdTfAC = [self tf:@"密码"];
    pwdTfAC.secureTextEntry = YES;
    pwdTfAC.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [accountContentView addSubview:pwdTfAC];
    [pwdTfAC mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(phoneTfAc);
        make.top.mas_equalTo(phoneTfAc.mas_bottom).mas_offset(InputVerPadding);
        make.right.equalTo(accountContentView).mas_offset(-horPadding);
        make.height.mas_equalTo(CellH);
    }];

    pwdFlagV = [ UIButton buttonWithType:UIButtonTypeCustom];
    [pwdFlagV addTarget:self action:@selector(pwdSecureAction) forControlEvents:UIControlEventTouchUpInside];
    [pwdFlagV setImage:[UIImage imageNamed:@"eyeClose"] forState:UIControlStateNormal];
    [accountContentView addSubview:pwdFlagV];
    CGFloat h  = CellH;
    CGFloat w = h*160/120;
    [pwdFlagV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(pwdTfAC);
        make.centerY.mas_equalTo(pwdTfAC.mas_centerY).mas_offset(VerPxFit(10));
        make.size.mas_equalTo(CGSizeMake(w, h));
    }];
    
    verCodeImgAc = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"verfyCode"]];
    verCodeImgAc.userInteractionEnabled = YES;
    [verCodeImgAc setImageURL:[NSURL URLWithString:self.verifyCodeImageUrl]];
    [accountContentView addSubview:verCodeImgAc];
    [verCodeImgAc mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(accountContentView).mas_offset(-horPadding);
        make.top.mas_equalTo(pwdTfAC.mas_bottom).mas_offset(InputVerPadding+CellH/3);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(80), CellH/3*2));
    }];

    UITapGestureRecognizer * tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(verifyCodeRefresh)];
    [verCodeImgAc addGestureRecognizer:tap];
    
    vercodeTfAc = [self tf:@"验证码"];
    [accountContentView addSubview:vercodeTfAc];
    [vercodeTfAc mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(pwdTfAC);
        make.top.mas_equalTo(pwdTfAC.mas_bottom).mas_offset(InputVerPadding);
        make.right.mas_equalTo(verCodeImgAc.mas_left).mas_offset(-HorPxFit(10));
        make.height.mas_equalTo(CellH);
    }];
    
// 短信登录
    phoneTfSms = [self tf:@"手机号"];
    phoneTfSms.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [smsContentView addSubview:phoneTfSms];
    [phoneTfSms mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(smsContentView).mas_offset(horPadding);
        make.top.equalTo(smsContentView).mas_offset(InputVerPadding);
        make.right.equalTo(smsContentView).mas_offset(-horPadding);
        make.height.mas_equalTo(CellH);
    }];
    
    smsTfSms = [self tf:@"短信验证码"];
    [smsContentView addSubview:smsTfSms];
    [smsTfSms mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(smsContentView).mas_offset(horPadding);
        make.top.mas_equalTo(phoneTfSms.mas_bottom).mas_offset(InputVerPadding);
        make.right.equalTo(smsContentView).mas_offset(-(HorPxFit(120)+horPadding));
        make.height.mas_equalTo(CellH);
    }];
    
    getSmsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [getSmsButton addTarget:self action:@selector(getVerifyCode) forControlEvents:UIControlEventTouchUpInside];
    [getSmsButton setTitle:@"获取验证码" forState:UIControlStateNormal];
    getSmsButton.titleLabel.font = [UIFont systemFontOfSize:15];
    [getSmsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [getSmsButton setBackgroundImage:[UIImage imageNamed:@"codeBt"] forState:UIControlStateNormal];
    [smsContentView addSubview:getSmsButton];
    [getSmsButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(smsContentView).mas_offset(-horPadding);
        make.bottom.equalTo(smsTfSms);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(120), VerPxFit(40)));
    }];
}

- (void)verifyCodeRefresh{
    self.loginImageToken = [BaseHelper getRandom32bit];
    [verCodeImgAc setImageURL:[NSURL URLWithString:self.verifyCodeImageUrl]];
}

- (NSString *)verifyCodeImageUrl{
    if (!self.loginImageToken) {
        self.loginImageToken = [BaseHelper getRandom32bit];
    }
    NSString * url = [NSString stringWithFormat:@"%@auth/defaultKaptcha?token=%@",SeverUrl,self.loginImageToken];
    return url;
}

- (void)getVerifyCode{
    if (!phoneTfSms.text ||  ![BaseHelper isValidateMobile:phoneTfSms.text]) {
        [BaseHelper showProgressHud:@"请输入有效手机号" showLoading:NO canHide:YES];
        return;
    }
    [BaseHelper showProgressLoadingInView:self];
    [RecruitmentModule getSMSVerifyCodeWithPhone:phoneTfSms.text type:@"Login" Success:^{
        [BaseHelper hideProgressHudInView:self];
        [self getVerifyCodeSuccess];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self];
        if(error){
            [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        }
    }];
}

- (void)getVerifyCodeSuccess{
    [BaseHelper showProgressHud:@"发送成功" showLoading:NO canHide:YES];
    
    __block int time = 60;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(time<=0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                getSmsButton.enabled = YES;
                [getSmsButton setTitle:@"获取验证码" forState:(UIControlStateNormal)];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                getSmsButton.enabled = NO;
                [getSmsButton setTitle:[NSString stringWithFormat:@"%ds后重发", time] forState:(UIControlStateDisabled)];
            });
            time--;
        }
    });
    dispatch_resume(_timer);
}

- (void)pwdSecureAction{
    if (pwdTfAC.secureTextEntry) {
        pwdTfAC.secureTextEntry = NO;
        [pwdFlagV setImage:[UIImage imageNamed:@"eyeOpen"] forState:UIControlStateNormal];
    }else{
        [pwdFlagV setImage:[UIImage imageNamed:@"eyeClose"] forState:UIControlStateNormal];
        pwdTfAC.secureTextEntry = YES;
    }
}

- (NSString *)phone{
    if (_loginType == LoginTypeAccount) {
        return phoneTfAc.text;
    }else if (_loginType == LoginTypeSMS){
        return phoneTfSms.text;
    }else{
        
    }
    return @"";
}

- (NSString *)password{
    if (_loginType == LoginTypeAccount) {
        return pwdTfAC.text;
    }else if (_loginType == LoginTypeSMS){
    }else{
        
    }
    return @"";
}

- (NSString *)verifyCode{
    return vercodeTfAc.text;
}

- (NSString *)sms{
    if (_loginType == LoginTypeAccount) {
        
    }else if (_loginType == LoginTypeSMS){
        return smsTfSms.text;
    }else{
        
    }
    return @"";
}

- (FlyTextFile *)tf:(NSString *)placeHolder{
    FlyTextFile * tf = [[FlyTextFile alloc] init];
    tf.titleLbH = 20;
    [tf createSubViews];
    tf.delegate = self;
    tf.font = [UIFont systemFontOfSize:15];
    tf.Titlefont = [UIFont systemFontOfSize:13];
    tf.placeholder = placeHolder;
    return tf;
}

#pragma mark -- FlyTextFileDelegate
- (BOOL)textFieldShouldReturn:(FlyTextFile *)textField{
    if (textField ==  phoneTfAc) {
        [pwdTfAC becomeFirstResponder];
    }else if (textField == pwdTfAC){
        [vercodeTfAc becomeFirstResponder];
    }else if(textField == phoneTfSms){
        [smsTfSms becomeFirstResponder];
    }else if(textField == vercodeTfAc || textField == smsTfSms){
        [textField resignFirstResponder];
    }
    return YES;
}

- (BOOL)textField:(FlyTextFile *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString * validString = @"0123456789";
    if (textField == phoneTfAc || textField == phoneTfSms) {
        if (textField.text.length==11 && ![string isEqualToString:@""]) {
            return NO;
        }
        if ([validString rangeOfString:string].location == NSNotFound ) {
            if ([string isEqualToString:@""]) {
                return YES;
            }else{
                return NO;
            }
        }
    }
    return YES;
}
@end

//
//  RegisterContentView.m
//  RecruitCompanyPro
//
//  Created by Michael on 2017/11/29.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "RegisterContentView.h"
#import "HSTextField.h"
#import "JobCenterViewController.h"
#import "BaseNavigationController.h"
#import "AppDelegate.h"
#import "FlyTextFile.h"
#import "UIImage+YYAdd.h"
#import "LMJDropdownMenu.h"
#import "UIView+YYAdd.h"

#define TFBaseTag 100

@interface RegisterContentView()<FlyTextFileDelegate,LMJDropdownMenuDelegate>{
 
    UIButton * verifyCodeBt;     // 获取验证码按钮
    LMJDropdownMenu * companyDropTf;   //企业列表
    FlyTextFile * phoneTf;              // 手机号tf
    FlyTextFile * verifyCodeTf;         // 验证码tf
    FlyTextFile * passwordTf;           // 密码tf
    FlyTextFile * jobNumTf;             //  工号tf
    FlyTextFile * identityCardTf;        //  身份证号tf
}

@property (nonatomic , strong)  NSArray * companyList;   // 企业列表
@property (nonatomic , strong) CompanyModel * companySelected;    // 当前选中的企业
@end

@implementation RegisterContentView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)didMoveToSuperview{
    [super didMoveToSuperview];
    [self removeAllSubviews];
    if (self.superview) {
        [self createSubViews];
    }
}

- (void)createSubViews{
    @weakify(self)
    UIImageView * bkImgv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"loginAccountBk"]];
    [self addSubview:bkImgv];
    [bkImgv mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self).mas_offset(-HorPxFit(50));
        make.right.equalTo(self).mas_offset(HorPxFit(50));
        make.top.equalTo(self).mas_offset(-VerPxFit(33));
        make.bottom.equalTo(self).mas_offset(VerPxFit(50));
    }];
    
    UIButton * bkBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [bkBt addTarget:self action:@selector(bkAction) forControlEvents:UIControlEventTouchUpInside];
    [bkBt setImage:[[UIImage imageNamed:@"back"] imageByTintColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    [self addSubview:bkBt];
    [bkBt mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self).mas_offset(HorPxFit(10));
        make.top.equalTo(self).mas_offset(VerPxFit(10));
        make.size.mas_equalTo(CGSizeMake(HorPxFit(40), VerPxFit(40)));
    }];
    
    UILabel * titleLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, HorPxFit(100), VerPxFit(50))];
    titleLb.text = @"注册";
    titleLb.font = [UIFont systemFontOfSize:18];
    titleLb.textColor = [UIColor whiteColor];
    titleLb.textAlignment = NSTextAlignmentCenter;
    [self addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.equalTo(self).mas_offset(10);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(80), VerPxFit(46)));
        make.centerX.mas_equalTo(self.mas_centerX);
    }];

    CGFloat cellH = VerPxFit(40);
    CGFloat verPadding = VerPxFit(20);
    CGFloat horPadding = HorPxFit(120);

    CGFloat verfyCodeW = HorPxFit(120);
    
    companyDropTf = [[LMJDropdownMenu alloc] initWithFrame:CGRectMake(horPadding, verPadding*3, CGRectGetWidth(self.frame)-2*horPadding, cellH)];
    [companyDropTf.mainBtn setTitle:@"请选择企业" forState:UIControlStateNormal];
    companyDropTf.mainBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    companyDropTf.delegate  = self;
    companyDropTf.backgroundColor = [UIColor clearColor];
    [self addSubview:companyDropTf];
    
    CGFloat imgH = VerPxFit(15);
    CGFloat imgW = 30*imgH/35;

    UIImageView * flagImgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"listFlag"]];
    flagImgV.frame = CGRectMake(CGRectGetWidth(self.frame)-horPadding-imgW, verPadding*3+imgH, imgW, imgH);
    flagImgV.centerY = companyDropTf.centerY;
    [self addSubview:flagImgV];
    
    UIImageView * line = [[UIImageView alloc] init];
    line.image = [UIImage imageNamed:@"AccountBottom"];
    [self addSubview:line];
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(companyDropTf.mas_bottom).mas_offset(-VerPxFit(5));
        make.left.and.right.equalTo(companyDropTf);
        make.height.mas_equalTo(2);
    }];
    
    jobNumTf = [self tf:@"工号"];
    jobNumTf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [self addSubview:jobNumTf];
    [jobNumTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(companyDropTf);
        make.top.mas_equalTo(companyDropTf.mas_bottom).mas_offset(verPadding);
        make.right.equalTo(companyDropTf);
        make.height.mas_equalTo(cellH);
    }];
  
    identityCardTf = [self tf:@"身份证号"];
    identityCardTf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [self addSubview:identityCardTf];
    [identityCardTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(jobNumTf);
        make.top.mas_equalTo(jobNumTf.mas_bottom).mas_offset(verPadding);
        make.right.equalTo(jobNumTf);
        make.height.mas_equalTo(cellH);
    }];
    
    phoneTf = [self tf:@"手机号"];
    phoneTf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    [self addSubview:phoneTf];
    [phoneTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(identityCardTf);
        make.top.mas_equalTo(identityCardTf.mas_bottom).mas_offset(verPadding);
        make.right.equalTo(identityCardTf);
        make.height.mas_equalTo(cellH);
    }];
    
    verifyCodeBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [verifyCodeBt addTarget:self action:@selector(getVerifyCode) forControlEvents:UIControlEventTouchUpInside];
    [verifyCodeBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [verifyCodeBt setTitle:@"获取验证码" forState:UIControlStateNormal];
    verifyCodeBt.titleLabel.font = [UIFont systemFontOfSize:15];
    [verifyCodeBt setBackgroundImage:[UIImage imageNamed:@"codeBt"] forState:UIControlStateNormal];
    verifyCodeBt.layer.cornerRadius = 5;
    [self addSubview:verifyCodeBt];
    [verifyCodeBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(companyDropTf);
        make.top.mas_equalTo(phoneTf.mas_bottom).mas_offset(verPadding);
        make.width.mas_equalTo(verfyCodeW);
        make.height.mas_equalTo(cellH);
    }];
    
    verifyCodeTf = [self tf:@"验证码"];
    [self addSubview:verifyCodeTf];
    [verifyCodeTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(verifyCodeBt.mas_left).mas_offset(-HorPxFit(10));
        make.top.mas_equalTo(phoneTf.mas_bottom).mas_offset(verPadding);
        make.left.equalTo(companyDropTf);
        make.height.mas_equalTo(cellH);
    }];

    passwordTf = [self tf:@"密码"];
    passwordTf.secureTextEntry = YES;
    [self addSubview:passwordTf];
    [passwordTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(companyDropTf);
        make.right.equalTo(companyDropTf);
        make.top.mas_equalTo(verifyCodeTf.mas_bottom).mas_offset(verPadding);
        make.height.mas_equalTo(cellH);
    }];
    
    UIButton * sureBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [sureBt addTarget:self action:@selector(sureAction) forControlEvents:UIControlEventTouchUpInside];
    [sureBt setTitle:@"确定" forState:UIControlStateNormal];
    sureBt.titleLabel.font = [UIFont systemFontOfSize:16];
    [sureBt setBackgroundImage:[UIImage imageNamed:@"registerBk"] forState:UIControlStateNormal];
    [sureBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    sureBt.layer.cornerRadius = 5;
    [self addSubview:sureBt];

    [sureBt mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.bottom.equalTo(self).mas_offset(-VerPxFit(30));
        make.centerX.equalTo(self.mas_centerX);
        make.width.mas_equalTo(HorPxFit(280));
        make.height.mas_equalTo(VerPxFit(40));
    }];
}
     
- (void)getVerifyCode{
    if ([BaseHelper isValidateMobile:phoneTf.text]) {
        [self.delegate getVerifyCode:phoneTf.text];
    }else{
        [BaseHelper showProgressHud:@"请输入有效手机号" showLoading:NO canHide:YES];
    }
}

- (void)sureAction{
    [self resignFirstResponder];
    if (!_companySelected) {
        [BaseHelper showProgressHud:@"请选择企业" showLoading:NO canHide:YES];
        return;
    }
    if (!jobNumTf.text || jobNumTf.text.length == 0) {
        [BaseHelper showProgressHud:@"请输入工号" showLoading:NO canHide:YES];
        return;
    }
    if (!phoneTf.text  || phoneTf.text.length ==0 || ![BaseHelper isValidateMobile:phoneTf.text]) {
        [BaseHelper showProgressHud:@"请输入有效手机号码" showLoading:NO canHide:YES];
        return;
    }
    if (!verifyCodeTf.text  || verifyCodeTf.text.length ==0) {
        [BaseHelper showProgressHud:@"请输入验证码" showLoading:NO canHide:YES];
        return;
    }
    if (!passwordTf.text || passwordTf.text.length == 0) {
        [BaseHelper showProgressHud:@"请输入登录密码" showLoading:NO canHide:YES];
        return;
    }
    [self.delegate registerWithCompanyId:_companySelected.id_ jobNum:jobNumTf.text cardNum:identityCardTf.text cellphone:phoneTf.text password:passwordTf.text verifyCode:verifyCodeTf.text];
}

- (void)getVerifyCodeSuccess{
    [BaseHelper showProgressHud:@"发送成功" showLoading:NO canHide:YES];
    __block int time = 60;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(time<=0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                verifyCodeBt.enabled = YES;
                [verifyCodeBt setTitle:@"获取验证码" forState:(UIControlStateNormal)];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                verifyCodeBt.enabled = NO;
                [verifyCodeBt setTitle:[NSString stringWithFormat:@"%ds后重发", time] forState:(UIControlStateDisabled)];
            });
            time--;
        }
    });
    dispatch_resume(_timer);
}

- (void)shiftLinceState:(UIButton *)bt{
    bt.selected = !bt.selected;
}

- (void)bkAction{
    [self.delegate backToLogin];
}

- (void)updateCompanyList:(NSArray <CompanyModel *>*)companyList{
    self.companyList = companyList;
    NSMutableArray * titlesArray = [NSMutableArray array];
    for (CompanyModel * model in companyList) {
        [titlesArray addObject:model.name];
    }
    [companyDropTf setMenuTitles:titlesArray rowHeight:VerPxFit(40)];
}

- (FlyTextFile *)tf:(NSString *)placeHolder{
    FlyTextFile * tf = [[FlyTextFile alloc] init];
    tf.titleLbH = 16;
    [tf createSubViews];
    tf.delegate = self;
    tf.font = [UIFont systemFontOfSize:15];
    tf.Titlefont = [UIFont systemFontOfSize:13];
    tf.placeholder = placeHolder;
    return tf;
}

#pragma mark -- UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField.tag == 107) {
        [textField resignFirstResponder];
        return YES;
    }
    NSInteger tag = textField.tag;
    UITextField * tf = [self viewWithTag:tag+1];
    if ([tf isKindOfClass:[UITextField class]]) {
        [tf becomeFirstResponder];
    }
    return YES;
}

- (BOOL)textField:(FlyTextFile *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString * validString = @"0123456789";
    if (textField == identityCardTf) {
        if ((textField.text.length==18) && ![string isEqualToString:@""]) {
            return NO;
        }
        if ([validString rangeOfString:string].location == NSNotFound ) {
            if ([string isEqualToString:@""]) {
                return YES;
            }else{
                return NO;
            }
        }
    }else if (textField == phoneTf){
        if ((textField.text.length==11) && ![string isEqualToString:@""]) {
            return NO;
        }
        if ([validString rangeOfString:string].location == NSNotFound ) {
            if ([string isEqualToString:@""]) {
                return YES;
            }else{
                return NO;
            }
        }
    }
    
    return YES;
    
}

#pragma mark -- LMJDropdownMenuDelegate
- (void)dropdownMenu:(LMJDropdownMenu *)menu selectedCellNumber:(NSInteger)number{
    [menu hideDropDown];
    CompanyModel * model = _companyList[number];
    self.companySelected = model;
}

@end

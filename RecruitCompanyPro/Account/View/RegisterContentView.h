//
//  RegisterContentView.h
//  RecruitCompanyPro
//
//  Created by Michael on 2017/11/29.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CompanyModel.h"

@protocol RegisterContentViewDelegate
//注册
- (void)registerWithCompanyId:(NSInteger)companyId jobNum:(NSString *)jobNum cardNum:(NSString *)cardNum cellphone:(NSString *)phone password:(NSString *)password verifyCode:(NSString *)verifyCode;
//返回到登录页
- (void)backToLogin;
// 获取验证码
- (void)getVerifyCode:(NSString *)phone;
@end

@interface RegisterContentView : UIView

@property (nonatomic , weak) id<RegisterContentViewDelegate>delegate;

// 更新企业列表
- (void)updateCompanyList:(NSArray <CompanyModel *>*)companyList;
// 获取验证码成功
- (void)getVerifyCodeSuccess;
@end

//
//  LoginContentView.m
//  RecruitCompanyPro
//
//  Created by Michael on 2017/11/29.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "LoginContentView.h"

#define AutoLoginImgVTag  1100
#define HRLoginImgVTag    1200
#define HRManageLoginImgVTag    1300

#define LoginTypeSelColor [UIColor whiteColor]
#define LoginTypeUnselColor  COLOR(220, 220, 220, 1)

@interface LoginContentView(){
    UIButton * smsLoginBt;      // 短信登录按钮
    UIButton * accountLoginBt;    //账号登录按钮
    LoginAccountScrollView * accountSV;   //登录容器view
    
    UIImageView * leftFlagImgV;     //左边登录title下高亮标识
    UIImageView * rightFlagImgV;     // 右边登录title下高亮标识
    
    UIButton * autoLoginBt;      // 自动登录按钮
    UIButton * hrloginTypeBt;       // hr登录按钮
    UIButton * hrManageloginTypeBt;  // hr 管理员登录按钮
}

@end

@implementation LoginContentView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    
    UIImageView * bkImgv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"loginAccountBk"]];
    [self addSubview:bkImgv];
    [bkImgv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).mas_offset(-HorPxFit(50));
        make.right.equalTo(self).mas_offset(HorPxFit(50));
        make.top.equalTo(self).mas_offset(-VerPxFit(33));
        make.bottom.equalTo(self).mas_offset(VerPxFit(50));
    }];
    
    CGFloat contentW = HorPxFit(600);
    
    CGFloat btH = VerPxFit(70);
    CGFloat btW = contentW/2.0;
    
    accountLoginBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [accountLoginBt addTarget:self action:@selector(loginTypeShift:) forControlEvents:UIControlEventTouchUpInside];
    [accountLoginBt setTitle:@"密码登录" forState:UIControlStateNormal];
    accountLoginBt.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [accountLoginBt setTitleColor:LoginTypeSelColor forState:UIControlStateNormal];
    [self addSubview:accountLoginBt];
    [accountLoginBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self);
        make.top.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(btW, btH));
    }];
    
    smsLoginBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [smsLoginBt addTarget:self action:@selector(loginTypeShift:) forControlEvents:UIControlEventTouchUpInside];
    [smsLoginBt setTitle:@"短信登录" forState:UIControlStateNormal];
    smsLoginBt.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [smsLoginBt setTitleColor:LoginTypeUnselColor forState:UIControlStateNormal];
    [self addSubview:smsLoginBt];
    [smsLoginBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.and.top.equalTo(self);
        make.size.mas_equalTo(CGSizeMake(btW, btH));
    }];
    
    leftFlagImgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"loginItemBottom"]];
    [self addSubview:leftFlagImgV];
    [leftFlagImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(128, 4));
        make.top.equalTo(self).mas_offset(btH);
        make.centerX.mas_equalTo(accountLoginBt.mas_centerX);
    }];
    
    rightFlagImgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"loginItemBottom"]];
    rightFlagImgV.hidden = YES;
    [self addSubview:rightFlagImgV];
    [rightFlagImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(128, 4));
        make.top.equalTo(self).mas_offset(btH);
        make.centerX.mas_equalTo(smsLoginBt.mas_centerX);
    }];
    
    CGFloat btHorPadding = HorPxFit(150);
    CGFloat suerBtH = VerPxFit(80);
    
    accountSV = [[LoginAccountScrollView alloc] initWithFrame:CGRectMake(0, btH+VerPxFit(15), contentW, CellH*3+InputVerPadding*4)];
    accountSV.scrollEnabled = NO;
    [self addSubview:accountSV];
    
    accountSV.contentSize = CGSizeMake(CGRectGetWidth(accountSV.frame) * 2, CGRectGetHeight(accountSV.frame));
    
    UIButton * sureBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [sureBt setBackgroundImage:[UIImage imageNamed:@"loginbtBk"] forState:UIControlStateNormal];
    sureBt.layer.cornerRadius = 6;
    sureBt.titleLabel.font = [UIFont systemFontOfSize:15];
    [sureBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sureBt addTarget:self action:@selector(suerAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:sureBt];
    [sureBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(accountSV.mas_bottom).mas_offset(VerPxFit(45));
        make.left.equalTo(self).mas_offset(btHorPadding);
        make.right.equalTo(self).mas_offset(-btHorPadding);
        make.height.mas_equalTo(suerBtH);
    }];

    UILabel * lb = [[UILabel alloc] init];
    lb.textColor = [UIColor whiteColor];
    lb.textAlignment = NSTextAlignmentCenter;
    lb.font = [UIFont systemFontOfSize:16];
    lb.text = @"确定";
    [self addSubview:lb];
    [lb mas_updateConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).mas_offset(btHorPadding);
        make.right.equalTo(self).mas_offset(-btHorPadding);
        make.height.mas_equalTo(30);
        make.top.equalTo(sureBt).mas_offset(VerPxFit(20));
    }];
    
    UIView * hrloginTypeView = [self loginTypeViewWithType:0];
    [self addSubview:hrloginTypeView];
    [hrloginTypeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(sureBt).mas_offset(HorPxFit(10));
        make.top.mas_equalTo(accountSV.mas_bottom).mas_offset(VerPxFit(10));
        make.size.mas_equalTo(CGSizeMake(140, 25));
    }];
    
    UIView * hrManageTypeView = [self loginTypeViewWithType:1];
    [self addSubview:hrManageTypeView];
    [hrManageTypeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(hrloginTypeView.mas_right).mas_offset(HorPxFit(20));
        make.centerY.mas_equalTo(hrloginTypeView.mas_centerY);
        make.size.mas_equalTo(hrloginTypeView);
    }];
    
    UIImageView * autologinImgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"FuXuan_sel"]];
    autologinImgV.tag = AutoLoginImgVTag;
    [self addSubview:autologinImgV];
    [autologinImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(sureBt);
        make.top.mas_equalTo(sureBt.mas_bottom).mas_offset(VerPxFit(10));
        make.size.mas_equalTo(CGSizeMake(HorPxFit(18), HorPxFit(18)));
    }];
    
    UILabel * autoLogin = [[UILabel alloc] init];
    autoLogin.font = [UIFont systemFontOfSize:15];
    autoLogin.text = @"自动登录";
    autoLogin.textColor = [UIColor whiteColor];
    [self addSubview:autoLogin];
    [autoLogin mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(autologinImgV.mas_right);
        make.centerY.mas_equalTo(autologinImgV.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(80, 30));
    }];
    
    autoLoginBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [autoLoginBt addTarget:self action:@selector(autologinShift:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:autoLoginBt];
    [autoLoginBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(autologinImgV);
        make.right.equalTo(autoLogin);
        make.height.mas_equalTo(VerPxFit(40));
        make.centerY.equalTo(autologinImgV.mas_centerY);
    }];
    id obj = [DataDefault objectForKey:DataDefaultAutologinKey isPrivate:NO];
    BOOL isAutoLogin = NO;
    if (!obj) {
        [DataDefault setObject:@(YES) forKey:DataDefaultAutologinKey isPrivate:NO];
        isAutoLogin = YES;
    }else{
        isAutoLogin = [obj boolValue];
    }
    if (isAutoLogin) {
        autoLoginBt.selected = YES;
        autologinImgV.image = [UIImage imageNamed:@"FuXuan_sel"];
    }else{
        autoLoginBt.selected = NO;
        autologinImgV.image = [UIImage imageNamed:@"FuXuan_Unsel"];
    }
    
    UIButton * registerBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [registerBt setTitle:@"注册" forState:UIControlStateNormal];
    [registerBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [registerBt addTarget:self action:@selector(registerAction) forControlEvents:UIControlEventTouchUpInside];
    registerBt.titleLabel.font = [UIFont systemFontOfSize:15];
    [self addSubview:registerBt];
    [registerBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(sureBt.mas_centerX);
        make.centerY.mas_equalTo(autologinImgV.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(90), VerPxFit(40)));
    }];
    
    UIButton * forgetPwdBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [forgetPwdBt setTitle:@"忘记密码" forState:UIControlStateNormal];
    [forgetPwdBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [forgetPwdBt addTarget:self action:@selector(forgetPwdAction) forControlEvents:UIControlEventTouchUpInside];
    forgetPwdBt.titleLabel.font = [UIFont systemFontOfSize:15];
    [self addSubview:forgetPwdBt];
    [forgetPwdBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(sureBt);
        make.centerY.mas_equalTo(autologinImgV.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(90), VerPxFit(40)));
    }];
    
    self.loginRole = @"HR";
}

- (UIView *)loginTypeViewWithType:(int)type{
    
    UIView * contentView = [[UIView alloc] init];
    UIImageView * imgV = [[UIImageView alloc] init];
    imgV.image = [UIImage imageNamed:@"FuXuan_Unsel"];
    [contentView addSubview:imgV];
    [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(contentView);
        make.centerY.mas_equalTo(contentView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(18), HorPxFit(18)));
    }];
    UILabel * lb = [[UILabel alloc] init];
    lb.font = [UIFont systemFontOfSize:15];
    lb.textColor = [UIColor whiteColor];
    [contentView addSubview:lb];
    [lb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(imgV.mas_right);
        make.top.and.bottom.equalTo(contentView);
        make.width.mas_equalTo(100);
    }];
    UIButton * bt = [UIButton buttonWithType:UIButtonTypeCustom];
    [bt addTarget:self action:@selector(loginTypeChoose:) forControlEvents:UIControlEventTouchUpInside];
    [contentView addSubview:bt];
    [bt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.top.and.bottom.and.right.equalTo(contentView);
    }];
    if (type == 0) {
        imgV.tag = HRLoginImgVTag;
        lb.text = @"HR登录";
        hrloginTypeBt = bt;
        imgV.image = [UIImage imageNamed:@"FuXuan_sel"];
    }else{
        imgV.tag = HRManageLoginImgVTag;
        lb.text = @"管理员登录";
        hrManageloginTypeBt = bt;
    }
    return contentView;
}

- (void)loginTypeChoose:(UIButton *)bt{
 
    if (bt == hrloginTypeBt) {
        UIImageView * hrImgV = [self viewWithTag:HRLoginImgVTag];
        if (hrImgV) {
            hrImgV.image = [UIImage imageNamed:@"FuXuan_sel"];
        }
        UIImageView * manageImgV = [self viewWithTag:HRManageLoginImgVTag];
        if (manageImgV) {
            manageImgV.image = [UIImage imageNamed:@"FuXuan_Unsel"];
        }
        self.loginRole = @"HR";
    }else{
        UIImageView * hrImgV = [self viewWithTag:HRLoginImgVTag];
        if (hrImgV) {
            hrImgV.image = [UIImage imageNamed:@"FuXuan_Unsel"];
        }
        UIImageView * manageImgV = [self viewWithTag:HRManageLoginImgVTag];
        if (manageImgV) {
            manageImgV.image = [UIImage imageNamed:@"FuXuan_sel"];
        }
        self.loginRole = @"HR_MANAGER";
    }
}

- (BOOL)isAutoLogin{
    return  autoLoginBt.selected;
}

- (void)autologinShift:(UIButton *)bt{
    UIImageView * autologinImgV = [self viewWithTag:AutoLoginImgVTag];
    bt.selected = !bt.selected;
    if (bt.selected) {
        autologinImgV.image = [UIImage imageNamed:@"FuXuan_sel"];
    }else{
        autologinImgV.image = [UIImage imageNamed:@"FuXuan_Unsel"];
    }
    [DataDefault setObject:@(bt.selected) forKey:DataDefaultAutologinKey isPrivate:NO];
}

- (void)suerAction{
    [self resignFirstResponder];
    
    if (!accountSV.phone || accountSV.phone.length == 0) {
        [BaseHelper showProgressHud:@"请输入手机号" showLoading:NO canHide:YES];
        return;
    }
    
    if(_loginType == LoginTypeSMS){
        if (!accountSV.sms || accountSV.sms.length == 0) {
            [BaseHelper showProgressHud:@"请输入验证码" showLoading:NO canHide:YES];
            return;
        }
        [self.delegate loginWithPhone:accountSV.phone password:nil verifyCode:accountSV.sms imageToken:nil];
        
    }else{
        if (!accountSV.verifyCode || accountSV.verifyCode.length == 0) {
            [BaseHelper showProgressHud:@"请输入验证码" showLoading:NO canHide:YES];
            return;
        }
        if (!accountSV.password || accountSV.password.length == 0) {
            [BaseHelper showProgressHud:@"请输入登录密码" showLoading:NO canHide:YES];
            return;
        }
        [self.delegate loginWithPhone:accountSV.phone password:accountSV.password verifyCode:accountSV.verifyCode imageToken:accountSV.loginImageToken];
    }
}

- (void)registerAction{
    [self.delegate registerAction];
}

- (void)forgetPwdAction{
    [self.delegate forgetPassword];
}

- (void)loginTypeShift:(UIButton *)bt{
    
    if (bt == accountLoginBt) {
        if (_loginType == LoginTypeAccount) {
            return;
        }
        [accountLoginBt setTitleColor:LoginTypeSelColor forState:UIControlStateNormal];
        [smsLoginBt setTitleColor:LoginTypeUnselColor forState:UIControlStateNormal];
        self.loginType = LoginTypeAccount;
        accountSV.loginType = LoginTypeAccount;
        [accountSV setContentOffset:CGPointMake(0, 0) animated:NO];
        leftFlagImgV.hidden = NO;
        rightFlagImgV.hidden = YES;
    }else{
        if (_loginType == LoginTypeSMS) {
            return;
        }
        leftFlagImgV.hidden = YES;
        rightFlagImgV.hidden = NO;
        [smsLoginBt setTitleColor:LoginTypeSelColor forState:UIControlStateNormal];
        [accountLoginBt setTitleColor:LoginTypeUnselColor forState:UIControlStateNormal];
        self.loginType = LoginTypeSMS;
        accountSV.loginType = LoginTypeSMS;
        [accountSV setContentOffset:CGPointMake(CGRectGetWidth(accountSV.frame), 0) animated:NO];
    }
}


@end

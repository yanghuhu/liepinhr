//
//  ResetPwdView.h
//  LiePinSellPro
//
//  Created by Deve on 2018/3/1.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ResetPwdViewDelegate
//pws 新密码
- (void)resetPwdWithNewPwd:(NSString *)newPwd;
//  返回上一层
- (void)resetPwdBackTofind;
@end

@interface ResetPwdView : UIView

@property (nonatomic , weak) id<ResetPwdViewDelegate>delegate;
@property (nonatomic,copy) NSString *token;

@end

//
//  FindPwdView.m
//  RecruitCompanyPro
//
//  Created by Michael on 2018/2/6.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "FindPwdView.h"
#import "UIImage+YYAdd.h"
#import "FlyTextFile.h"

#define HRLoginImgVTag    1200
#define HRManageLoginImgVTag    1300

@interface FindPwdView()<UITextFieldDelegate,FlyTextFileDelegate>{
    
    FlyTextFile * phoneTf;         //  手机号 tf
    FlyTextFile * verifyCodeTf;    // 验证码tf
    UIButton * verifyCodeBt;       //  获取验证码按钮
    
    UIButton * hrloginActionBt;       // hr登录按钮
    UIButton * hrManageActionTypeBt;  // hr 管理员登录按钮
}
@property (nonatomic , strong)     NSString * actionRole;  //  当前操作的角色
@end

@implementation FindPwdView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.actionRole = @"HR";
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    
    @weakify(self)
    UIImageView * bkImgv = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"loginAccountBk"]];
    [self addSubview:bkImgv];
    [bkImgv mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self).mas_offset(-HorPxFit(50));
        make.right.equalTo(self).mas_offset(HorPxFit(50));
        make.top.equalTo(self).mas_offset(-VerPxFit(33));
        make.bottom.equalTo(self).mas_offset(VerPxFit(50));
    }];
    
    UIButton * bkBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [bkBt addTarget:self action:@selector(bkAction) forControlEvents:UIControlEventTouchUpInside];
    [bkBt setImage:[[UIImage imageNamed:@"back"] imageByTintColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    [self addSubview:bkBt];
    [bkBt mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self).mas_offset(HorPxFit(10));
        make.top.equalTo(self).mas_offset(VerPxFit(10));
        make.size.mas_equalTo(CGSizeMake(HorPxFit(40), VerPxFit(40)));
    }];
    
    UILabel * titleLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, HorPxFit(100), VerPxFit(50))];
    titleLb.text = @"身份验证";
    titleLb.font = [UIFont systemFontOfSize:18];
    titleLb.textColor = [UIColor whiteColor];
    titleLb.textAlignment = NSTextAlignmentCenter;
    [self addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.top.equalTo(self).mas_offset(10);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(80), VerPxFit(46)));
        make.centerX.mas_equalTo(self.mas_centerX);
    }];
    
    CGFloat cellH = VerPxFit(40);
    CGFloat verPadding = VerPxFit(20);
    CGFloat horPadding = HorPxFit(120);
    CGFloat verfyCodeW = HorPxFit(120);

    phoneTf = [self tf:@"手机号"];
    phoneTf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    phoneTf.tag = 100;
    [self addSubview:phoneTf];
    [phoneTf mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.left.equalTo(self).mas_offset(horPadding);
        make.top.mas_equalTo(titleLb.mas_bottom).mas_offset(VerPxFit(30));
        make.right.equalTo(self).mas_offset(-horPadding);
        make.height.mas_equalTo(cellH);
    }];
    
    verifyCodeBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [verifyCodeBt addTarget:self action:@selector(getVerifyCode) forControlEvents:UIControlEventTouchUpInside];
    [verifyCodeBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    verifyCodeBt.titleLabel.font  = [UIFont systemFontOfSize:15];
    [verifyCodeBt setTitle:@"获取验证码" forState:UIControlStateNormal];
    [verifyCodeBt setBackgroundImage:[UIImage imageNamed:@"codeBt"] forState:UIControlStateNormal];
    verifyCodeBt.layer.cornerRadius = 5;
    [self addSubview:verifyCodeBt];
    [verifyCodeBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(phoneTf);
        make.top.mas_equalTo(phoneTf.mas_bottom).mas_offset(verPadding);
        make.width.mas_equalTo(verfyCodeW);
        make.height.mas_equalTo(cellH);
    }];
    
    verifyCodeTf = [self tf:@"验证码"];
    verifyCodeTf.tag = 101;
    [self addSubview:verifyCodeTf];
    [verifyCodeTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(verifyCodeBt.mas_left).mas_offset(-HorPxFit(10));
        make.top.mas_equalTo(phoneTf.mas_bottom).mas_offset(verPadding);
        make.left.equalTo(phoneTf);
        make.height.mas_equalTo(cellH);
    }];
    
    UIView * hrloginTypeView = [self loginTypeViewWithType:0];
    [self addSubview:hrloginTypeView];
    [hrloginTypeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(verifyCodeTf).mas_offset(HorPxFit(10));
        make.top.mas_equalTo(verifyCodeTf.mas_bottom).mas_offset(VerPxFit(20));
        make.size.mas_equalTo(CGSizeMake(140, 25));
    }];
    
    UIView * hrManageTypeView = [self loginTypeViewWithType:1];
    [self addSubview:hrManageTypeView];
    [hrManageTypeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(hrloginTypeView.mas_right).mas_offset(HorPxFit(20));
        make.centerY.mas_equalTo(hrloginTypeView.mas_centerY);
        make.size.mas_equalTo(hrloginTypeView);
    }];

    UIButton * sureBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [sureBt addTarget:self action:@selector(sureAction) forControlEvents:UIControlEventTouchUpInside];
    [sureBt setTitle:@"下一步" forState:UIControlStateNormal];
    sureBt.titleLabel.font = [UIFont systemFontOfSize:18];
    [sureBt setBackgroundImage:[UIImage imageNamed:@"registerBk"] forState:UIControlStateNormal];
    [sureBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    sureBt.layer.cornerRadius = 5;
    [self addSubview:sureBt];
    
    [sureBt mas_makeConstraints:^(MASConstraintMaker *make) {
        @strongify(self)
        make.bottom.equalTo(self).mas_offset(-VerPxFit(20));
        make.centerX.equalTo(self.mas_centerX);
        make.width.mas_equalTo(HorPxFit(280));
        make.height.mas_equalTo(VerPxFit(40));
    }];
}

- (UIView *)loginTypeViewWithType:(int)type{
    
    UIView * contentView = [[UIView alloc] init];
    UIImageView * imgV = [[UIImageView alloc] init];
    imgV.image = [UIImage imageNamed:@"FuXuan_Unsel"];
    [contentView addSubview:imgV];
    [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(contentView);
        make.centerY.mas_equalTo(contentView.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(18), HorPxFit(18)));
    }];
    UILabel * lb = [[UILabel alloc] init];
    lb.font = [UIFont systemFontOfSize:15];
    lb.textColor = [UIColor whiteColor];
    [contentView addSubview:lb];
    [lb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(imgV.mas_right);
        make.top.and.bottom.equalTo(contentView);
        make.width.mas_equalTo(100);
    }];
    UIButton * bt = [UIButton buttonWithType:UIButtonTypeCustom];
    [bt addTarget:self action:@selector(yanzhengTypeChoose:) forControlEvents:UIControlEventTouchUpInside];
    [contentView addSubview:bt];
    [bt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.top.and.bottom.and.right.equalTo(contentView);
    }];
    if (type == 0) {
        imgV.tag = HRLoginImgVTag;
        lb.text = @"企业HR";
        imgV.image = [UIImage imageNamed:@"FuXuan_sel"];
        hrloginActionBt = bt;
    }else{
        imgV.tag = HRManageLoginImgVTag;
        lb.text = @"企业管理员";
        hrManageActionTypeBt = bt;
    }
    return contentView;
}

- (void)yanzhengTypeChoose:(UIButton *)bt{
    if (bt == hrloginActionBt) {
        UIImageView * hrImgV = [self viewWithTag:HRLoginImgVTag];
        if (hrImgV) {
            hrImgV.image = [UIImage imageNamed:@"FuXuan_sel"];
        }
        UIImageView * manageImgV = [self viewWithTag:HRManageLoginImgVTag];
        if (manageImgV) {
            manageImgV.image = [UIImage imageNamed:@"FuXuan_Unsel"];
        }
        self.actionRole = @"HR";
    }else{
        UIImageView * hrImgV = [self viewWithTag:HRLoginImgVTag];
        if (hrImgV) {
            hrImgV.image = [UIImage imageNamed:@"FuXuan_Unsel"];
        }
        UIImageView * manageImgV = [self viewWithTag:HRManageLoginImgVTag];
        if (manageImgV) {
            manageImgV.image = [UIImage imageNamed:@"FuXuan_sel"];
        }
        self.actionRole = @"HR_MANAGER";
    }
}

- (FlyTextFile *)tf:(NSString *)placeHolder{
    FlyTextFile * tf = [[FlyTextFile alloc] init];
    tf.titleLbH = 16;
    [tf createSubViews];
    tf.delegate = self;
    tf.font = [UIFont systemFontOfSize:15];
    tf.Titlefont = [UIFont systemFontOfSize:13];
    tf.placeholder = placeHolder;
    
    return tf;
}

- (void)getVerifyCode{
    if (!phoneTf.text ||  ![BaseHelper isValidateMobile:phoneTf.text]) {
        [BaseHelper showProgressHud:@"请输入有效手机号" showLoading:NO canHide:YES];
        return;
    }
    [self.delegate getVerifyCodeForGetPwd:phoneTf.text];
}

- (void)getVerifyCodeSuccess{
    [BaseHelper showProgressHud:@"发送成功" showLoading:NO canHide:YES];
    __block int time = 60;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(time<=0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                verifyCodeBt.enabled = YES;
                [verifyCodeBt setTitle:@"获取验证码" forState:(UIControlStateNormal)];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                verifyCodeBt.enabled = NO;
                [verifyCodeBt setTitle:[NSString stringWithFormat:@"%ds后重发", time] forState:(UIControlStateDisabled)];
            });
            time--;
        }
    });
    dispatch_resume(_timer);
}

- (void)sureAction{
    [self resignFirstResponder];
    if (!phoneTf.text ||  ![BaseHelper isValidateMobile:phoneTf.text]) {
        [BaseHelper showProgressHud:@"请输入有效手机号" showLoading:NO canHide:YES];
        return;
    }
    if (!verifyCodeTf.text || verifyCodeTf.text.length == 0) {
        [BaseHelper showProgressHud:@"请输入验证码" showLoading:NO canHide:YES];
        return;
    }

    [self extracted];
}

- (void)extracted {
    [self.delegate finePwdWithPhone:phoneTf.text verifyCode:verifyCodeTf.text withRole:self.actionRole];
}

- (void)bkAction{
    [self.delegate findPwdBackToLogin];
}

#pragma mark -- UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (textField.tag == 102) {
        [textField resignFirstResponder];
        return YES;
    }
    NSInteger tag = textField.tag;
    UITextField * tf = [self viewWithTag:tag+1];
    if ([tf isKindOfClass:[UITextField class]]) {
        [tf becomeFirstResponder];
    }
    return YES;
}

- (BOOL)textField:(FlyTextFile *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString * validString = @"0123456789";
    if (textField == phoneTf){
        if ((textField.text.length==11) && ![string isEqualToString:@""]) {
            return NO;
        }
        if ([validString rangeOfString:string].location == NSNotFound ) {
            if ([string isEqualToString:@""]) {
                return YES;
            }else{
                return NO;
            }
        }
    }
    
    return YES;

}
@end

//
//  FindPwdView.h
//  RecruitCompanyPro
//
//  Created by Michael on 2018/2/6.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol FindPwdViewDelegate
//  找回密码提交数据  phone:手机号    verifyCode:验证码   
- (void)finePwdWithPhone:(NSString *)phone verifyCode:(NSString *)verifyCode withRole:(NSString *)role;
//  返回登录页
- (void)findPwdBackToLogin;
// 获取验证码
- (void)getVerifyCodeForGetPwd:(NSString *)phone;
@end

@interface FindPwdView : UIView

@property (nonatomic , weak) id<FindPwdViewDelegate>delegate;

// 获取验证码成功
- (void)getVerifyCodeSuccess;
@end

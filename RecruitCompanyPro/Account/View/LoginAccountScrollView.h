//
//  LoginAccountScrollView.h
//  RecruitCompanyPro
//
//  Created by Michael on 2017/11/29.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>

#define  CellH HorPxFit(50)
#define  InputVerPadding VerPxFit(20)

typedef enum {
    LoginTypeAccount, // 账号登录
    LoginTypeSMS     // 短信登录
}LoginType;





@interface LoginAccountScrollView : UIScrollView

@property (nonatomic , assign) LoginType loginType;  //登录方式
@property (nonatomic , strong) NSString * phone;  // 登录手机号
@property (nonatomic , strong) NSString * password;  // 登录密码
@property (nonatomic , strong) NSString * verifyCode; // 账号登录验证码
@property (nonatomic , strong) NSString * loginImageToken;    // 图片验证码token

@property (nonatomic , strong) NSString * sms;     // 短信验证码

@end

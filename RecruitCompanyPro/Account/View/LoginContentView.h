//
//  LoginContentView.h
//  RecruitCompanyPro
//
//  Created by Michael on 2017/11/29.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginAccountScrollView.h"

@protocol LoginContentViewDelegate
// 登录
- (void)loginWithPhone:(NSString *)phone password:(NSString *)password verifyCode:(NSString *)verifyCode imageToken:(NSString *)token;
// 注册
- (void)registerAction;
//找回密码
- (void)forgetPassword;
@end

@interface LoginContentView : UIView

@property (nonatomic , weak) id<LoginContentViewDelegate>delegate;
@property (nonatomic , assign) BOOL isAutoLogin;    //是否自动登录
@property (nonatomic , strong) NSString * loginRole;  // 登录角色  hr/管理员
@property (nonatomic , assign) LoginType loginType;  // 登录类型   账号登录/短信登录

@end


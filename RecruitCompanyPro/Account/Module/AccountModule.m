//
//  AccountModule.m
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/11.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "AccountModule.h"
#import "HRModel.h"

@implementation AccountModule

+ (void)registerWithCompanyId:(NSInteger)companyId
                       jobNum:(NSString *)jobNum
                      cardNum:(NSString *)cardNum
                        phone:(NSString *)phone
                     password:(NSString *)password
                   verifyCode:(NSString *)verificationCode
                      success:(RequestSuccessBlock)succBlock
                      failure:(RequestFailureBlock)failBlock{
    NSDictionary * para = @{@"companyId" : @(companyId),
                            @"cellphone":phone,
                            @"verificationCode":verificationCode,
                            @"idCardNumber":cardNum,
                            @"password":password,
                            @"jobNumber":jobNum
                            };
    NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithDictionary:para];
    
    [NetworkHandle postRequestForApi:@"auth/hrRegister" mockObj:nil params:dic handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        NSString *token = response_[@"token"];
        if (token) {
            [HSBCGlobalInstance sharedHSBCGlobalInstance].netWorkToken = token;
        }
        NSDictionary * user = response_[@"user"];
        if (user) {
            HRModel * model = [HRModel modelWithDictionary:user];
            [HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel = model;
            return @[@YES,model];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)resetPasswordWithRole:(NSString *)role
                          phone:(NSString *)phone
                     verifyCode:(NSString *)verificationCode
                        success:(RequestSuccessBlock)succBlock
                        failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * para = @{@"platformType":@"HR",
                            @"cellphone":phone,
                            @"verificationCode":verificationCode,
                            };
    
    [NetworkHandle postRequestForApi:@"auth/setPassword" mockObj:nil params:para handle:^NSArray *(NSDictionary *response) {
        NSDictionary *dict = (NSDictionary *)response;
        NSString *token = dict[@"token"];
        if (token) {
            [HSBCGlobalInstance sharedHSBCGlobalInstance].netWorkToken = token;
            return @[@YES,token];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)nextResetPasswordWithNewPwd:(NSString *)newPwd token:(NSString *)token role:(NSString *)role success:(RequestSuccessBlock)succBlock failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * para = @{@"password":newPwd,
                            @"platformType":role,
                            @"token":token,
                            };
    [NetworkHandle postRequestForApi:@"auth/setPasswordStep2" mockObj:nil params:para handle:^NSArray *(id response) {
        
        return @[@YES];
        
    } success:^(id response){
        if (succBlock) {
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)loginWithPhone:(NSString *)phone
              password:(NSString *)pwd_
            verifyCode:(NSString *)code
        verifyImgToken:(NSString *)token
                  role:(NSString *)role
               success:(RequestSuccessBlock)succBlock
               failure:(RequestFailureBlock)failBlock{
 
    NSDictionary * para = @{@"cellphone":phone,
                            @"verificationCode":code,
                            @"password":pwd_,
                            @"token":token,
                            @"plateformType":role
                            };
    
    [NetworkHandle postRequestForApi:@"auth/login" mockObj:nil params:para handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        NSString *token = response_[@"token"];
        if (token) {
            [HSBCGlobalInstance sharedHSBCGlobalInstance].netWorkToken = token;
        }
        NSDictionary * user = response_[@"user"];
        if (user) {
            HRModel * model = [HRModel modelWithDictionary:user];
            [HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel = model;
            return @[@YES,model];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)loginWithSmsPhone:(NSString *)phone
               verifyCode:(NSString *)verifyCode
                     role:(NSString *)role
                  success:(RequestSuccessBlock)succBlock
                  failure:(RequestFailureBlock)failBlock{

    NSDictionary * para = @{@"cellphone":phone,
                            @"verificationCode":verifyCode,
                            @"plateformType":role
                            };
    [NetworkHandle postRequestForApi:@"auth/loginByCode" mockObj:nil params:para handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        NSString *token = response_[@"token"];
        if (token) {
            [HSBCGlobalInstance sharedHSBCGlobalInstance].netWorkToken = token;
        }
        NSDictionary * user = response_[@"user"];
        if (user) {
            HRModel * model = [HRModel modelWithDictionary:user];
            [HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel = model;
            return @[@YES,model];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)getUsrLocalImageCodeWithToken:(NSString *)token
                              Success:(RequestSuccessBlock)succBlock
                              failure:(RequestFailureBlock)failBlock{
    NSDictionary * para = @{@"token":token};
    [NetworkHandle getRequestForApi:@"auth/loginByCode" mockObj:nil params:para handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        NSString *token = response_[@"token"];
        if (token) {
            return @[@YES,token];
        }else{
            return @[@YES];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
    
    
}


@end

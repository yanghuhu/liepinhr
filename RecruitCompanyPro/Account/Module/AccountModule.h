//
//  AccountModule.h
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/11.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseModule.h"

@interface AccountModule : BaseModule
/*
 * hr 注册
 *@param
 *      companyId  企业id
 *      phone  hr 电话
 *      password 登录密码
 *      verificationCode   验证码
 */
+ (void)registerWithCompanyId:(NSInteger)companyId
                       jobNum:(NSString *)jobNum
                      cardNum:(NSString *)cardNum
                        phone:(NSString *)phone
                     password:(NSString *)password
                   verifyCode:(NSString *)verificationCode
                      success:(RequestSuccessBlock)succBlock
                      failure:(RequestFailureBlock)failBlock;

/*
 * 找回密码
 * @param
 *   pwd_  新密码
 *   phone   手机号
 *   verificationCode   验证码
 */
+ (void)resetPasswordWithRole:(NSString *)role
                          phone:(NSString *)phone
                     verifyCode:(NSString *)verificationCode
                        success:(RequestSuccessBlock)succBlock
                        failure:(RequestFailureBlock)failBlock;

/**
 *找回密码下一步
 *@param
 *     pwd_ 新密码
 *     newPwd  确认新密码
 *     token   token
 */
+ (void)nextResetPasswordWithNewPwd:(NSString *)newPwd
                              token:(NSString *)token
                               role:(NSString *)role
                            success:(RequestSuccessBlock)succBlock
                            failure:(RequestFailureBlock)failBlock;

/*
 * 用户登录
 *  @param
 *         phone  手机号
 *         pwd_  登录密码
 *         code  验证码
 */
+ (void)loginWithPhone:(NSString *)phone
                   password:(NSString *)pwd_
            verifyCode:(NSString *)code
        verifyImgToken:(NSString *)token
                  role:(NSString *)role
               success:(RequestSuccessBlock)succBlock
               failure:(RequestFailureBlock)failBlock;


/*
 * 短信登录
 */
+ (void)loginWithSmsPhone:(NSString *)phone
               verifyCode:(NSString *)verifyCode
                     role:(NSString *)role
                  success:(RequestSuccessBlock)succBlock
                  failure:(RequestFailureBlock)failBlock;

/*
 *  获取登录图片验证码
 */
+ (void)getUsrLocalImageCodeWithToken:(NSString *)token
                              Success:(RequestSuccessBlock)succBlock
                            failure:(RequestFailureBlock)failBlock;
@end

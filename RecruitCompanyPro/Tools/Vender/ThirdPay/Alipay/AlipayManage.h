//
//  AlipayManage.h
//  HSBCTempPro
//
//  Created by Michael on 2018/3/12.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AlipayManage : NSObject


@property (nonatomic , strong) NSString * orderId;
@property (nonatomic , assign) CGFloat price;

@property (nonatomic , strong) NSString * orderInfoEncrypted;  // 订单信息，支付前，先判断是否已有订单信息，如没有在用orderId和price生成

@property (nonatomic , strong) UIViewController * viewcontroller;

- (void)doPayFinish:(PayFinishBlock)block;

@end

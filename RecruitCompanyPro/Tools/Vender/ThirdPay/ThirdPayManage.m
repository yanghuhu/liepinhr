//
//  ThirdPayManage.m
//  HSBCTempPro
//
//  Created by Michael on 2018/3/12.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "ThirdPayManage.h"

@implementation ThirdPayManage

- (AlipayManage *)alipayManage{
    if (!_alipayManage) {
        _alipayManage = [[AlipayManage alloc] init];
    }
    return _alipayManage;
}

- (void)payWithType:(ThirdPayType)type OrderId:(NSString *)orderId  price:(CGFloat)price finishBlock:(PayFinishBlock)block{
    if (type == ThirdPayTypeAlipay) {
        self.alipayManage.price = price;
        self.alipayManage.orderId = orderId;
        self.alipayManage.orderInfoEncrypted  = nil;  // 这一步不能少
        [self.alipayManage doPayFinish:block];
    }else if (type == ThirdPayTypeWechat){
        [BaseHelper showProgressHud:@"暂不支持该支付方式" showLoading:NO canHide:YES];
    }else if (type == ThirdPayTypeBank){
        [BaseHelper showProgressHud:@"暂不支持该支付方式" showLoading:NO canHide:YES];
    }
}

- (void)payWithType:(ThirdPayType)type OrderInfo:(NSString *)orderInfo finishBlock:(PayFinishBlock)block{
    if (type == ThirdPayTypeAlipay) {
        self.alipayManage.orderInfoEncrypted = orderInfo;
        [self.alipayManage doPayFinish:block];
    }else if (type == ThirdPayTypeWechat){
        [BaseHelper showProgressHud:@"暂不支持该支付方式" showLoading:NO canHide:YES];
    }else if (type == ThirdPayTypeBank){
        [BaseHelper showProgressHud:@"暂不支持该支付方式" showLoading:NO canHide:YES];
    }
}

@end

//
//  CandidateModel.h
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/14.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CandidateModel : NSObject

@property (nonatomic , strong)  NSString * candidateExpectedSalary; //候选人期望月薪 ,
@property (nonatomic , strong) NSString * candidateGraduateSchool;   // 候选人毕业学校
@property (nonatomic , assign) NSInteger candidateId;  // 候选人信息ID
@property (nonatomic , strong) NSString * candidateName;  //候选人名字 ,
@property (nonatomic , strong) NSString * candidatePosition;   //候选人公司职位
@property (nonatomic , strong)  NSString * candidateWorkYears;  // 候选人工作年限
@property (nonatomic , assign) NSInteger id_;   //候选人订单ID
@property (nonatomic , strong) NSString * candidateAvatar;  //  头像
@property (nonatomic , strong) NSString * state;  //状态  ['Recommended', 'Accepted', 'Rejected', 'Wait_Appointment', 'Wait_Interview', 'Interview_Failed', 'Waint_Offer', 'Wait_Register', 'Wait_Positive', 'Positive']

@property (nonatomic , strong) NSString * status;  // 状态 = ['ON', 'OFF']

@property (nonatomic , assign) BOOL hrStateFlag;   //  hr状态提醒标识
@property (nonatomic , assign) BOOL hhStateFlag;     //  猎头状态提醒标识

@end

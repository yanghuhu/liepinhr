//
//  JobOrderDetailModel.h
//  HSBCTempPro
//
//  Created by Michael on 2017/12/10.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TalentModel.h"
#import "CompanyOrderModel.h"
typedef enum {
    OrderStepRecommend,
    OrderStepInterView,
    OrderStepEntry,
    OrderStepPositive,
}OrderStep;

@class OfferModel;
@class HeadHunter;

@interface JobOrderDetailModel : NSObject

@property (nonatomic , strong) TalentModel * talentModel;
@property (nonatomic , strong) CompanyOrderModel * orderModel;
@property (nonatomic , assign) NSString * interviewResult; // 面试结果
@property (nonatomic , strong) NSString * interviewResultDesc; //  面试结果描述
@property (nonatomic , assign)  NSTimeInterval  interviewTime;//  面试时间
@property (nonatomic , strong) OfferModel * offer;
@property (nonatomic , assign) NSTimeInterval payTime;   // 支付时间 ,
@property (nonatomic , assign) NSTimeInterval positiveTime;  // 转正时间
@property (nonatomic , strong)  NSString * rejectReason;   // 简历拒绝原因
@property (nonatomic , assign)  NSTimeInterval  reportDutyTime;   //  入职时间 ,
@property (nonatomic , strong)  NSString * state;    // 状态   'Recommended', 'Accepted', 'Rejected', 'Wait_Appointment', 'Wait_Interview', 'Interview_Failed', 'Waint_Offer', 'Wait_Register', 'No_Register', 'Wait_Positive', 'No_Positive', 'Positive']
@property (nonatomic , assign) BOOL hrEvaluateFlag;  //  评价标识  是否已评价
@property (nonatomic , strong) NSString * status;  //  订单闭合状态  'ON', 'OFF'
@property (nonatomic , assign) NSTimeInterval appointmentTime; // 预约面试时间
@property (nonatomic , assign) BOOL applyCommission; // 申请佣金
@property (nonatomic , assign) NSInteger itemPayFee;  //  支付金额
@property (nonatomic , assign) OrderStep orderStep;   // 订单步骤

@property (nonatomic , assign) NSInteger id_;
@end


@interface OfferModel : NSObject

@property (nonatomic , assign) NSInteger  contractPeriod;  // 合同年限
@property (nonatomic , assign) NSTimeInterval createTime;  // 创建时间
@property (nonatomic , strong) NSString * department; //  部门
@property (nonatomic , strong)  NSString * description_; //   说明
@property (nonatomic , strong) NSString * leader;   //    直接上级
@property (nonatomic , strong) NSString * position;    ///   职位
@property (nonatomic , assign)  NSInteger probationPeriod;    //     试用期
@property (nonatomic , assign) NSInteger  probationSalary;    //    试用期工资
@property (nonatomic , assign) NSTimeInterval reportDutyTime;    //  入职时间
@property (nonatomic , assign) NSInteger  salary;    //    工资
@property (nonatomic , strong)  NSString * workplace;     //  工作地点
@property (nonatomic , assign) NSInteger id_;
@end



//
//  ListPostionModel.h
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/13.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ListPostionModel : NSObject

@property (nonatomic , assign) NSInteger id_;
@property (nonatomic , strong)  NSString * title;   // 标题
@property (nonatomic , strong)  NSString *  state; // [ 'SUBMIT', 'PUBLISH', 'COMPLETED']
@property (nonatomic , assign) NSInteger processedNumber;  //已处理候选人数
@property (nonatomic , assign) NSInteger processingNumber; // 处理中候选人数
@property (nonatomic , assign)  NSInteger recruitedNumber;   // 已招聘人数
@property (nonatomic , assign)  NSInteger recruitingNumber;   // 职位招聘人数
@property (nonatomic , assign) NSInteger waitingProcessNumber;   // 待处理候选人数
@property (nonatomic , assign) NSTimeInterval publishTime;   // 发布时间
@property (nonatomic , assign)  NSTimeInterval createTime;   // 创建时间

@end

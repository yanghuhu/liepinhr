//
//  JobOrderDetailModel.m
//  HSBCTempPro
//
//  Created by Michael on 2017/12/10.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "JobOrderDetailModel.h"

@implementation JobOrderDetailModel
+ (NSDictionary *)modelCustomPropertyMapper {
    
    return @{@"talentModel":@"candidate",
             @"orderModel":@"jobTaskOrder",
             @"id_":@"id"
             };
}
// 订单返回时间为毫米
- (NSTimeInterval)positiveTime{
    return _positiveTime/1000;
}
// 订单返回时间为毫米
- (NSTimeInterval)reportDutyTime{
    return _reportDutyTime/1000;
}
// 订单返回时间为毫米
- (NSTimeInterval)appointmentTime{
    return _appointmentTime/1000;
}

- (OrderStep)orderStep{
    if ([_state isEqualToString:PositionCandidateOrderStateRecommended] || [_state isEqualToString:PositionCandidateOrderStateRejected]) {
        _orderStep = OrderStepRecommend;
    }else if ([_state isEqualToString:PositionCandidateOrderStateAccepted] ||
              [_state isEqualToString:PositionCandidateOrderStateWait_Appointment]||
              [_state isEqualToString:PositionCandidateOrderStateWait_Interview] ||
              [_state isEqualToString:PositionCandidateOrderStateInterview_Failed]){
        _orderStep = OrderStepInterView;
    }else if([_state isEqualToString:PositionCandidateOrderStateWaint_Offer] || [_state isEqualToString:PositionCandidateOrderStateWait_Register] || [_state isEqualToString:PositionCandidateOrderStateNot_Register]){
        _orderStep = OrderStepEntry;
    }else{
        _orderStep = OrderStepPositive;
    }
    return _orderStep;
}

@end

@implementation OfferModel
+ (NSDictionary *)modelCustomPropertyMapper {
    
    return @{@"id_":@"id",
             @"description_":@"description"
             };
}

- (NSTimeInterval)createTime{
    return _createTime/1000;
}

- (NSTimeInterval)reportDutyTime{
    return _reportDutyTime/1000;
}
@end



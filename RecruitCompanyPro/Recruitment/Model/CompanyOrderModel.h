//
//  CompanyOrderModel.h
//  HSBCTempPro
//
//  Created by Michael on 2017/12/5.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JobModel.h"
@class HeadHunter;

@interface CompanyOrderModel : NSObject

@property (nonatomic , assign) NSInteger id_;
@property (nonatomic , strong) JobModel *jobModel;  //  职位信息
@property (nonatomic , assign) NSTimeInterval createTime;   //订单生成时间
@property (nonatomic , strong) HeadHunter * headHunter;    //  猎头

@end

@interface HeadHunter : NSObject

@property (nonatomic , strong)  NSString * cellphone;    // 联系方式
@property (nonatomic , assign) NSInteger id_;

@end

//
//  CompanyModel.h
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/11.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CompanyModel : NSObject

@property (nonatomic , assign) NSInteger id_;
@property (nonatomic , strong) NSString * name;  // 公司名称
@property (nonatomic , strong) NSString * address;
@property (nonatomic , strong)  NSString * businessLicense; // 营业执照 ,
@property (nonatomic , strong) NSString * contactNumber; // 联系电话
@property (nonatomic , strong) NSString * contacts;   // 联系人
@property (nonatomic , strong)  NSString * percentageServiceFee;  // 服务费百分比
@property (nonatomic , strong) NSString * settleType;   // 结算方式 = ['COMPANY_SETTLE', 'ORDER_SETTLE'],
@property (nonatomic , strong) NSString * taxNumber;   // 税号

@end

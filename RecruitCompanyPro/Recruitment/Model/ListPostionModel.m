//
//  ListPostionModel.m
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/13.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "ListPostionModel.h"

@implementation ListPostionModel
+ (NSDictionary *)modelCustomPropertyMapper {
    
    return @{@"id_":@"id"};
}

//  server  返回时间单位为毫秒
- (NSTimeInterval)publishTime{
    return _publishTime/1000;
}
//  server  返回时间单位为毫秒
- (NSTimeInterval)createTime{
    return _createTime/1000;
}

@end

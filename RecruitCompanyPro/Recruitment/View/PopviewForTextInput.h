//
//  PopviewForTextInput.h
//  RecruitCompanyPro
//
//  Created by Michael on 2017/11/30.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    InputTypeDuty,    //职位描述
    InputTypeRequire,    // 职位要求
}InputType;

@protocol PopviewForTextInputDelegate

// 职位描述添加完成
- (void)jobdutyAddFinish:(NSString *)duty;
// 职位职责添加完成
- (void)jobRequireAddFinish:(NSString *)duty;

@end

@interface PopviewForTextInput : UIView

@property (nonatomic , assign) InputType inputType;   // 输入类型
@property (nonatomic , weak) id<PopviewForTextInputDelegate>delegate;


- (void)showWithType:(InputType)inputType withSupView:(UIView *)view;

@end

//
//  PopviewForTextInput.m
//  RecruitCompanyPro
//
//  Created by Michael on 2017/11/30.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "PopviewForTextInput.h"

@interface PopviewForTextInput()<UITextViewDelegate>{
    
    UITextView * textView;     // 输入框
    UIControl * bkControl;     // 蒙板控制器
}
@end

@implementation PopviewForTextInput

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.cornerRadius = 2;
        self.backgroundColor = [UIColor lightGrayColor];
        [self createSubViews];
    }
    return self;
}

- (UIButton *)btWithTitle:(NSString *)title sel:(SEL)select{
    UIButton * bt = [UIButton buttonWithType:UIButtonTypeCustom];
    [bt addTarget:self action:select forControlEvents:UIControlEventTouchUpInside];
    [bt setTitle:title forState:UIControlStateNormal];
    [bt setBackgroundColor:ThemeColor];
    [bt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    bt.layer.cornerRadius = 5;
    return bt;
}

- (void)createSubViews{
    bkControl = [[UIControl alloc] init];
    bkControl.backgroundColor = [UIColor blackColor];
    bkControl.alpha = 0.2;
    [bkControl addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    
    CGFloat padding = HorPxFit(40);
    CGFloat btW = (CGRectGetWidth(self.frame) - padding *3) / 2.0;
    
    UIButton * cancelBt = [self btWithTitle:@"取消" sel:@selector(cancelAction)];
    [self addSubview:cancelBt];
    [cancelBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).mas_offset(padding);
        make.bottom.equalTo(self).mas_offset(-VerPxFit(30));
        make.height.mas_equalTo(VerPxFit(40));
        make.width.mas_equalTo(btW);
    }];
    UIButton * sureBt = [self btWithTitle:@"确定" sel:@selector(suerAction)];
    [self addSubview:sureBt];
    [sureBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(cancelBt.mas_right).mas_offset(HorPxFit(40));
        make.bottom.equalTo(self).mas_offset(-VerPxFit(30));
        make.height.mas_equalTo(VerPxFit(40));
        make.width.mas_equalTo(btW);
    }];
    
    textView = [[UITextView alloc] init];
    textView.layer.borderColor = [UIColor grayColor].CGColor;
    textView.layer.borderWidth = 1;
    textView.returnKeyType = UIReturnKeyDone;
    textView.layer.cornerRadius = 4;
    [self addSubview:textView];
    [textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self).mas_offset(padding);
        make.right.equalTo(self).mas_offset(-padding);
        make.bottom.mas_equalTo(sureBt.mas_top).mas_offset(-padding);
    }];
}

- (void)suerAction{
    if (_inputType == InputTypeDuty) {
        [self.delegate jobdutyAddFinish:textView.text];
    }else if(_inputType == InputTypeRequire){
        [self.delegate jobRequireAddFinish:textView.text];
    }
    [self cancelAction];
}

- (void)cancelAction{
    [bkControl removeFromSuperview];
    [self removeFromSuperview];
}

- (void)showWithType:(InputType)inputType withSupView:(UIView *)view{
    self.inputType = inputType;
    bkControl.frame = view.bounds;
    textView.text = @"";
    self.center = view.center;
    [view addSubview:bkControl];
    [view addSubview:self];
}

#pragma mark -- UITextViewDelegate
- (BOOL)textViewShouldEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    return YES;
}

@end

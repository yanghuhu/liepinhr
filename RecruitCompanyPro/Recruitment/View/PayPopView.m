//
//  PayPopView.m
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/13.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "PayPopView.h"
#import "UIView+YYAdd.h"
#import "UIImage+YYAdd.h"

@interface PayPopView(){
    
    UILabel * titleLb;       // 标题 lb
    UITextField * noteTf;    //  备注tf
    UIButton * companyPayBt;     // 支付按钮
    UILabel * moneyLb;    // 金额lb
    UILabel * payTitleLb;
    UIView * companyPayAccessyView;
}

@property (nonatomic , strong) UIButton * payTypeBtSelect;    // 已选中的支付按钮
@property (nonatomic , strong) NSString * payType;           // 支付方式
@end

@implementation PayPopView
- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    
    UIImage * img = [UIImage imageNamed:@"popBack"];
    UIImageView * bkImg  = [[UIImageView alloc] initWithImage:[img stretchableImageWithLeftCapWidth:img.size.width/2.0 topCapHeight:img.size.height/2.0]];
    [self addSubview:bkImg];
    [bkImg mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.top.and.bottom.equalTo(self);
    }];
    
    CGFloat selfW = HorPxFit(img.size.width);
    CGFloat selfH = selfW * img.size.height / img.size.width;
    self.frame = CGRectMake(0, 0, selfW, selfH);
    
    UIButton * cancelBt = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBt.frame = CGRectMake(selfW -HorPxFit(85), VerPxFit(28), 40, 40);
    [cancelBt setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
    [cancelBt addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:cancelBt];
    
    titleLb = [[UILabel alloc] init];
    titleLb.text =@"支付";
    titleLb.font = [UIFont systemFontOfSize:17];
    titleLb.textColor = [UIColor whiteColor];
    titleLb.textAlignment = NSTextAlignmentCenter;
    [self addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX);
        make.top.equalTo(self).mas_offset(VerPxFit(35));
        make.size.mas_equalTo(CGSizeMake(190, 30));
    }];
    
    UIImageView * titleBottomLine = [[UIImageView alloc] init];
    titleBottomLine.backgroundColor = [UIColor grayColor];
    [self addSubview:titleBottomLine];
    [titleBottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(titleLb);
        make.width.mas_equalTo(40);
        make.top.mas_equalTo(titleLb.mas_bottom).mas_offset(VerPxFit(0));
        make.height.mas_equalTo(1);
    }];
    
    UILabel * moneyTitleLb = [[UILabel alloc] init];
    moneyTitleLb.text = @"金额";
    moneyTitleLb.textColor = [UIColor whiteColor];
    moneyTitleLb.font = [UIFont systemFontOfSize:15];
    [self addSubview:moneyTitleLb];
    [moneyTitleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).mas_offset(HorPxFit(80));
        make.top.mas_equalTo(titleBottomLine.mas_bottom).mas_offset(VerPxFit(25));
        make.size.mas_equalTo(CGSizeMake(HorPxFit(50), VerPxFit(20)));
    }];

    moneyLb = [[UILabel alloc] init];
    moneyLb.text = @"1000元";
    moneyLb.font = [UIFont systemFontOfSize:22];
    moneyLb.textAlignment = NSTextAlignmentCenter;
    moneyLb.textColor = [UIColor whiteColor];
    [self addSubview:moneyLb];
    [moneyLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(moneyTitleLb.mas_centerY);
        make.centerX.mas_equalTo(titleLb.mas_centerX);
        make.height.mas_equalTo(VerPxFit(20));
        make.width.mas_equalTo(HorPxFit(100));
    }];

    UIImageView * moneyIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"potionPayMoney"]];
    [self addSubview:moneyIcon];
    [moneyIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(moneyLb.mas_left).mas_offset(-HorPxFit(10));
        make.centerY.mas_equalTo(moneyTitleLb.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(20), HorPxFit(20)));
    }];
    
//    UIImageView * payTypeIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"quickPayIcon"]];
//    [self addSubview:payTypeIcon];
//    [payTypeIcon mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(moneyIcon);
//        make.top.mas_equalTo(moneyLb.mas_bottom).mas_offset(VerPxFit(15));
//        make.size.mas_equalTo(CGSizeMake(HorPxFit(30), HorPxFit(30)));
//    }];

    
    payTitleLb = [[UILabel alloc] init];
    payTitleLb.text = @"支付方式";
    payTitleLb.textColor = [UIColor whiteColor];
    payTitleLb.font = [UIFont systemFontOfSize:15];
    [self addSubview:payTitleLb];
    [payTitleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(moneyTitleLb);
        make.top.mas_equalTo(moneyTitleLb.mas_bottom).mas_offset(VerPxFit(30));
        make.size.mas_equalTo(CGSizeMake(HorPxFit(100), VerPxFit(20)));
    }];
    
  
//    if ([[HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel.company.settleType isEqualToString:@"COMPANY_SETTLE"]) {
        [self createViewForCompanyPay];
    selfH += VerPxFit(35)*2+VerPxFit(10);
    self.height =  selfH;
//    }else{
//        [self craeteViewForOrderPay];
//    }
    
    
    UIButton * payBt = [UIButton buttonWithType:UIButtonTypeCustom];
    payBt.frame = CGRectMake(0, selfH - 93, HorPxFit(200), 35);
    payBt.centerX = self.centerX;
    payBt.titleLabel.font = [UIFont systemFontOfSize:15];
    [payBt setTitle:@"支付" forState:UIControlStateNormal];
    [payBt setBackgroundImage:[UIImage imageNamed:@"btBk"] forState:UIControlStateNormal];
    [payBt addTarget:self action:@selector(payAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:payBt];
}

- (void)createViewForCompanyPay{
    
    NSArray * payIconName = @[@"wechatPay",@"zhifubaoPay",@"bankPay",@"QIYE"];
    CGFloat horPadding = HorPxFit(35);
    CGFloat itemW = (CGRectGetWidth(self.frame)-2*HorPxFit(170)- 2*horPadding) / 3.0;
    CGFloat curX = HorPxFit(170);
    for (int i=0; i<payIconName.count; i++) {
        if (i == 3) {
            UIView * view = [[UIView alloc] init];
            [self addSubview:view];
            [view mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(self).mas_offset(HorPxFit(170));
                make.top.mas_equalTo(payTitleLb.mas_bottom).mas_offset(VerPxFit(35)+VerPxFit(10));
                make.size.mas_equalTo(VerPxFit(30)*2);
                make.right.equalTo(self).mas_offset(-HorPxFit(170));
            }];
            
            UIImageView * flag = [[UIImageView alloc] init];
            flag.tag = 100;
            flag.image = [UIImage imageNamed:@"payUnSele"];
            [view addSubview:flag];
            [flag mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(view);
                make.left.equalTo(view);
                make.size.mas_equalTo(CGSizeMake(20,20));
            }];
            
            UIImageView * iconImgv = [[UIImageView alloc] init];
            iconImgv.image = [UIImage imageNamed:@"compayPayIcon"];
            iconImgv.contentMode = UIViewContentModeScaleAspectFill;
            [view addSubview:iconImgv];
            [iconImgv mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerY.mas_equalTo(flag.mas_centerY);
                make.left.mas_equalTo(flag.mas_right).mas_offset(HorPxFit(20));
                make.size.mas_equalTo(CGSizeMake(HorPxFit(73), VerPxFit(20)));
            }];
         
            companyPayBt = [UIButton buttonWithType:UIButtonTypeCustom];
            companyPayBt.tag = 200+3;
            [companyPayBt addTarget:self action:@selector(payTypeChoose:) forControlEvents:UIControlEventTouchUpInside];
            [view addSubview:companyPayBt];
            [companyPayBt mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.and.top.equalTo(view);
                make.right.equalTo(iconImgv);
                make.height.equalTo(iconImgv);
            }];
            
            companyPayAccessyView = [[UIView alloc] init];
            companyPayAccessyView.hidden = YES;
            [view addSubview:companyPayAccessyView];
            [companyPayAccessyView mas_makeConstraints:^(MASConstraintMaker *make) {
               make.left.mas_equalTo(flag.mas_right).mas_offset(HorPxFit(10));
                make.right.equalTo(view);
                make.top.mas_equalTo(iconImgv.mas_bottom).mas_offset(VerPxFit(14));
                make.height.mas_equalTo(VerPxFit(30));
            }];
            
            noteTf = [[UITextField alloc] init];
            noteTf.font = [UIFont systemFontOfSize:15];
            noteTf.borderStyle = UITextBorderStyleNone;
            noteTf.textColor = [UIColor whiteColor];
            
            noteTf.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"备注(选填)" attributes:@{NSForegroundColorAttributeName: COLOR(200, 200, 200, 1)}];
            UIView * leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 1)];
            noteTf.leftView = leftView;
            noteTf.leftViewMode = UITextFieldViewModeAlways;
            [companyPayAccessyView addSubview:noteTf];
            [noteTf mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.and.right.and.top.and.bottom.equalTo(companyPayAccessyView);
            }];
            
            UIImageView * imgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"payTitleBottom"]];
            [companyPayAccessyView addSubview:imgV];
            [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.and.right.equalTo(noteTf);
                make.bottom.equalTo(companyPayAccessyView);
                make.height.mas_equalTo(1);
            }];
            break;
        }
        
        UIView * view = [[UIView alloc] init];
        [self addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).mas_offset(curX);
            make.centerY.mas_equalTo(payTitleLb.mas_centerY);
//            make.top.mas_equalTo(payTitleLb.mas_bottom).mas_offset(VerPxFit(15));
            make.size.mas_equalTo(CGSizeMake(itemW, VerPxFit(35)));
        }];
        
        UIImageView * flag = [[UIImageView alloc] init];
        flag.tag = 100;
        flag.image = [UIImage imageNamed:@"payUnSele"];
        [view addSubview:flag];
        [flag mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(view);
            make.centerY.mas_equalTo(view.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(17,17));
        }];
        
        UIImageView * iconImgv = [[UIImageView alloc] init];
        iconImgv.image = [UIImage imageNamed:payIconName[i]];
        iconImgv.contentMode = UIViewContentModeScaleAspectFill;
        [view addSubview:iconImgv];
        [iconImgv mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(flag.mas_right).mas_offset(HorPxFit(20));
            make.top.equalTo(view);
            make.right.equalTo(view);
            make.height.equalTo(view);
        }];
        
        UIButton * bt = [UIButton buttonWithType:UIButtonTypeCustom];
        bt.tag = 200+i;
        [bt addTarget:self action:@selector(payTypeChoose:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:bt];
        [bt mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.and.right.and.top.and.bottom.equalTo(view);
        }];
        
        curX += (itemW+horPadding);
    }
}

- (void)craeteViewForOrderPay{
    
    NSArray * payIconName = @[@"wechatPay",@"zhifubaoPay",@"bankPay"];
    CGFloat horPadding = HorPxFit(35);
    CGFloat itemW = (CGRectGetWidth(self.frame)-2*HorPxFit(180)- 2*horPadding) / 3;
    CGFloat curX = HorPxFit(80)+HorPxFit(40);
    for (int i=0; i<payIconName.count; i++) {
        UIView * view = [[UIView alloc] init];
        [self addSubview:view];
        [view mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self).mas_offset(curX);
            make.top.mas_equalTo(payTitleLb.mas_bottom).mas_offset(VerPxFit(10));
            make.size.mas_equalTo(CGSizeMake(itemW, VerPxFit(40)));
        }];
        UIImageView * flag = [[UIImageView alloc] init];
        flag.tag = 100;
        flag.image = [UIImage imageNamed:@"payUnSele"];
        [view addSubview:flag];
        [flag mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(view);
            make.centerY.mas_equalTo(view.mas_centerY);
            make.size.mas_equalTo(CGSizeMake(20,20));
        }];
        
        UIImageView * iconImgv = [[UIImageView alloc] init];
        iconImgv.image = [UIImage imageNamed:payIconName[i]];
        iconImgv.contentMode = UIViewContentModeScaleAspectFill;
        [view addSubview:iconImgv];
        [iconImgv mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(flag.mas_right).mas_offset(HorPxFit(20));
            make.top.equalTo(view);
            make.right.equalTo(view);
            make.height.equalTo(view);
        }];
        
        UIButton * bt = [UIButton buttonWithType:UIButtonTypeCustom];
        bt.tag = 200+i;
        [bt addTarget:self action:@selector(payTypeChoose:) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:bt];
        [bt mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.and.right.and.top.and.bottom.equalTo(view);
        }];
        
        curX += (itemW+horPadding);
    }
}

- (void)showSupView:(UIView *)view withPrice:(CGFloat)price{
    self.center = view.center;
    [view addSubview:self];
    NSString * string =  [NSString stringWithFormat:@"%.2f元",price/100];
    
    CGSize size = [BaseHelper getSizeWithString:string font:[UIFont fontWithName:@"Arial-ItalicMT" size:20] contentWidth:MAXFLOAT contentHight:30];
    moneyLb.attributedText = [BaseHelper setSourceString:string targetString:@"元" forFont:[UIFont fontWithName:@"Arial-ItalicMT" size:10] color:[UIColor whiteColor] customFont:[UIFont fontWithName:@"Arial-ItalicMT" size:20] customColor:[UIColor whiteColor]];
    [moneyLb mas_updateConstraints:^(MASConstraintMaker *make) {
        make.width.mas_equalTo(size.width);
    }];
}

- (void)payTypeChoose:(UIButton *)bt{
    if (_payTypeBtSelect == bt) {
        return;
    }
    if(_payTypeBtSelect){
        UIImageView * imgV = [[_payTypeBtSelect superview] viewWithTag:100];
        if (imgV) {
            imgV.image = [UIImage imageNamed:@"payUnSele"];
        }
        if (_payTypeBtSelect == companyPayBt) {
            companyPayAccessyView.hidden = YES;
        }
    }
    
    UIImageView * imgV_ = [[bt superview] viewWithTag:100];
    imgV_.image =  [UIImage imageNamed:@"paySele"];
    self.payTypeBtSelect = bt;
    
    if (_payTypeBtSelect == companyPayBt) {
        noteTf.text = @"";
        companyPayAccessyView.hidden = NO;
    }
    
    switch (bt.tag) {
        case 200:{
            self.payType = @"WechatPay";
            break;
        }
        case 201:{
            self.payType = @"AliPay";
            break;
        }
        case 202:{
            self.payType = @"Bank";
            break;
        }
        case 203:{
            self.payType = @"CompanyPay";
            break;
        }
        default:
            break;
    }
}

- (void)payAction{
    [self.delegate payActionWihtType:self.payType withNote:noteTf.text];
}

- (void)cancelAction{
    [self removeFromSuperview];
}

@end

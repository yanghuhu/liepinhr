//
//  PostionDesAndResView.h
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/13.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PostionDesAndResViewDelegate
- (void)PostionDesAndResViewHightChange:(CGFloat)hight;
@end

@interface PostionDesAndResView : UIView

@property (nonatomic , strong) NSMutableArray * descArray; // 职位描述数组
@property (nonatomic , strong) NSMutableArray * respArray;  // 职位职责数组

@property (nonatomic , assign) id<PostionDesAndResViewDelegate>delegate;


- (CGFloat)initInfoWithDesc:(NSArray *)descArray respArray:(NSArray *)respArray;

@end

//
//  PostionCandidate.h
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/14.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CandidateModel.h"

@protocol PostionCandidateDelegate

// 选中候选人
- (void)candidateSelect:(CandidateModel *)model;
// 刷新候选人列表
- (void)candidateRefresh;
// 候选人列表加载更多
- (void)candidateGetMore;
@end

@interface PostionCandidate : UIView

@property (nonatomic , weak) id<PostionCandidateDelegate>delegate;

//刷新候选人列表展示
- (void)candidateSVUpdate:(NSArray *)candidateArray;
//移除特定候选人红点
- (void)removeItemViewHongDian:(CandidateModel *)candidateModel;

@end


@interface CandidateItemView:UIView


@end

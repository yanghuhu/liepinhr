//
//  DescAddView.m
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/14.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "DescAddView.h"
#import "UIImage+YYAdd.h"
#import "UIView+YYAdd.h"

@interface DescAddView()<UITextViewDelegate>{
    UITextView * textView;    //  输入框
    UIControl * bkControl;    //  蒙板控制器
}
@end

@implementation DescAddView

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    
    bkControl = [[UIControl alloc] init];
    
    UIImage * img = [UIImage imageNamed:@"popBack"];
    UIImageView * bkImg  = [[UIImageView alloc] initWithImage:[img stretchableImageWithLeftCapWidth:img.size.width/2.0 topCapHeight:img.size.height/2.0]];
    [self addSubview:bkImg];
    
    CGFloat selfW = HorPxFit(img.size.width);
    CGFloat selfH = selfW * img.size.height / img.size.width;
    self.frame = CGRectMake(0, 0, selfW, selfH);
    
    UIButton * cancelBt = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBt.frame =  CGRectMake(selfW -HorPxFit(80), VerPxFit(22), 40, 40);
    [cancelBt setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
    [cancelBt addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:cancelBt];
    
    UILabel * titleLb = [[UILabel alloc] init];
    if(_infoAddType  == InfoAddTypeDesc){
        titleLb.text = @"添加岗位描述信息";
    }else{
        titleLb.text = @"添加岗位职责信息";
    }
    titleLb.font = [UIFont systemFontOfSize:16];
    titleLb.textAlignment = NSTextAlignmentCenter;
    titleLb.textColor = [UIColor whiteColor];
    [self addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX);
        make.top.equalTo(self).mas_offset(VerPxFit(35));
        make.size.mas_equalTo(CGSizeMake(200, 30));
    }];
    UIButton * suerBt = [UIButton buttonWithType:UIButtonTypeCustom];
    suerBt.frame = CGRectMake(0, selfH - 85, 200, 35);
    suerBt.centerX = self.centerX;
    [suerBt setTitle:@"确定" forState:UIControlStateNormal];
    [suerBt setBackgroundImage:[UIImage imageNamed:@"btBk"] forState:UIControlStateNormal];
    [suerBt addTarget:self action:@selector(sureAction) forControlEvents:UIControlEventTouchUpInside];
    suerBt.titleLabel.font = [UIFont systemFontOfSize:15];
    [self addSubview:suerBt];
    
    textView = [[UITextView alloc] init];
    textView.delegate = self;
    textView.layer.cornerRadius = 8;
    textView.font = [UIFont systemFontOfSize:14];
    textView.returnKeyType = UIReturnKeyDone;
    [self addSubview:textView];
    [textView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).mas_offset(HorPxFit(80));
        make.right.equalTo(self).mas_offset(-HorPxFit(80));
        make.top.mas_equalTo(titleLb.mas_bottom).mas_offset(VerPxFit(10));
        make.bottom.mas_equalTo(suerBt.mas_top).mas_offset(-VerPxFit(10));
    }];
}

- (void)sureAction{
    if (!textView.text || textView.text.length == 0) {
        [BaseHelper showProgressHud:@"请输入内容" showLoading:NO canHide:YES];
        return;
    }
    [self.delegate addSureAction:textView.text withType:_infoAddType];
    [self cancelAction];
}

- (void)cancelAction{
    [bkControl removeAllSubviews];
    [self removeFromSuperview];
}

- (void)showSupView:(UIView *)view withType:(InfoAddType)type{
    _infoAddType =  type;
    self.center = view.center;
    [view addSubview:self];
}

#pragma mark -- UITextViewDelegate
- (void)textViewDidEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
}

@end

//
//  PostionDesAndResView.m
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/13.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "PostionDesAndResView.h"
#import "UIView+YYAdd.h"
#import "DescAddView.h"
#import "AppDelegate.h"

#define DescBaseTag 200
#define RespBaseTag  300

#define  TextFont [UIFont systemFontOfSize:14]
#define  CellH   40

@interface PostionDesAndResView()<UITableViewDelegate,UITableViewDataSource,DescAddViewDelegate>{
    UIView * descContentV;  // 职位描述容器view
    UIView * respContentV;    //  职位职责容器view
    UITableView * descTableView;        //  职位描述tbView
    UITableView * respTableView;        //  职位职责tbView
    
    CGFloat cellContentW;    //  tbView的cell 宽
    
    UILabel * respTitleLb;   // 职责titlelb
    UILabel * descTitleLb;    //  职位描述titlelb
    
    UIButton * descAddBt;  //  职位描述添加按钮
}

@property (nonatomic , strong) DescAddView * descAddView;

@end

@implementation PostionDesAndResView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    descTitleLb = [[UILabel alloc] init];
    descTitleLb.text = @"岗位要求";
    descTitleLb.font = [UIFont systemFontOfSize:15];
    descTitleLb.textColor = [UIColor whiteColor];
    [self addSubview:descTitleLb];
    [descTitleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self);
        make.height.mas_equalTo(30);
        make.width.mas_equalTo(200);
        make.top.mas_equalTo(0);
    }];
    
    descAddBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [descAddBt setTitle:@"+ 添加要求" forState:UIControlStateNormal];
    descAddBt.titleLabel.font = [UIFont systemFontOfSize:14];
    [descAddBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    descAddBt.backgroundColor = [UIColor clearColor];
    [descAddBt addTarget:self action:@selector(addRespAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:descAddBt];
    [descAddBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self);
        make.top.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(100, 30));
    }];
    
     descContentV = [[UIView alloc] init];
    descContentV.backgroundColor = [UIColor clearColor];
    [self addSubview:descContentV];
    [descContentV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(self);
        make.top.mas_equalTo(descTitleLb.mas_bottom);
        make.height.mas_equalTo(0);
    }];
    
    UIImage * bkImg = [UIImage imageNamed:@"descBk"];
    UIImageView * descBk = [[UIImageView alloc] initWithImage:[bkImg stretchableImageWithLeftCapWidth:bkImg.size.width/2.0 topCapHeight:bkImg.size.height/2.0]];
    [descContentV addSubview:descBk];
    [descBk mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.top.bottom.equalTo(descContentV);
    }];
    cellContentW = ScreenWidth-HorPxFit(10)-HorPxFit(50)-90-HorPxFit(140)-HorPxFit(75)*2 - 40 - 15 - HorPxFit(20)*4;
    
    descTableView = [[UITableView alloc] init];
    descTableView.delegate = self;
    descTableView.dataSource = self;
    descTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    descTableView.backgroundColor = [UIColor clearColor];
    [descContentV addSubview:descTableView];
    [descTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.top.and.bottom.equalTo(descContentV);
    }];
    
    respTitleLb = [[UILabel alloc] init];
    respTitleLb.textColor = [UIColor whiteColor];
    respTitleLb.text = @"职位描述";
    respTitleLb.font = [UIFont systemFontOfSize:15];
    [self addSubview:respTitleLb];
    [respTitleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self);
        make.top.mas_equalTo(descContentV.mas_bottom).mas_offset(VerPxFit(30));
        make.size.mas_equalTo(CGSizeMake(200, 30));
    }];
    
    UIButton * respAddBt = [UIButton buttonWithType:UIButtonTypeCustom];
    respAddBt.backgroundColor = [UIColor clearColor];
    [respAddBt addTarget:self action:@selector(descAddAction) forControlEvents:UIControlEventTouchUpInside];
    [respAddBt setTitle:@"+ 添加描述" forState:UIControlStateNormal];
    [respAddBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    respAddBt.titleLabel.font = [UIFont systemFontOfSize:14];
    [self addSubview:respAddBt];
    [respAddBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self);
        make.top.and.height.equalTo(respTitleLb);
        make.width.equalTo(descAddBt);
    }];
    
    respContentV = [[UIView alloc] init];
    respContentV.backgroundColor = [UIColor clearColor];
    [self addSubview:respContentV];
    [respContentV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(self);
        make.top.mas_equalTo(respAddBt.mas_bottom);
        make.height.mas_equalTo(0);
    }];
    UIImageView * respBk = [[UIImageView alloc] initWithImage:[bkImg stretchableImageWithLeftCapWidth:bkImg.size.width/2.0 topCapHeight:bkImg.size.height/2.0]];
    [respContentV addSubview:respBk];
    [respBk mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.top.bottom.equalTo(respContentV);
    }];
    
    respTableView = [[UITableView alloc] init];
    respTableView.backgroundColor = [UIColor clearColor];
    respTableView.delegate = self;
    respTableView.dataSource = self;
    respTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [respContentV addSubview:respTableView];
    [respTableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.top.and.bottom.equalTo(respContentV);
    }];
}

- (CGFloat)initInfoWithDesc:(NSArray *)descArray respArray:(NSArray *)respArray{
  
    self.descArray = [NSMutableArray arrayWithArray:descArray];
    self.respArray =  [NSMutableArray arrayWithArray:respArray];
    [descTableView reloadData];
    [respTableView reloadData];
    CGFloat descTbH = [self getHightForDesc];
    [descContentV mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(descTbH);
    }];
    CGFloat respH =  [self getHightForResp];
    [respContentV mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(respH);
    }];
    
    CGFloat selfH = descTbH+respH+60+VerPxFit(40);
    return selfH;
}

- (void)descAddAction{
    [self.descAddView showSupView:self.superview.superview.superview withType:InfoAddTypeDesc];
}

- (void)addRespAction{
    [self.descAddView showSupView:self.superview.superview.superview withType:InfoAddTypeResp];
}

- (void)itemDeleteAction:(UIButton *)bt{
    UITableViewCell * cell = (UITableViewCell *)[bt superview];
    if(bt.tag > 250){
        NSIndexPath * indexPath = [respTableView indexPathForCell:cell];
        [_respArray removeObjectAtIndex:indexPath.row];
    }else{
        NSIndexPath * indexPath = [descTableView indexPathForCell:cell];
        [_descArray removeObjectAtIndex:indexPath.row];
    }
    
    CGFloat selfH = [self initInfoWithDesc:_descArray respArray:_respArray];
    [self.delegate PostionDesAndResViewHightChange:selfH];
}

- (CGFloat)getHightForDesc{
    CGFloat hNeed = 0;
    for (NSString * text in _descArray) {
        hNeed += [self getCellHightWithText:text];
    }
    return hNeed;
}

- (CGFloat)getHightForResp{
    CGFloat hNeed = 0;
    for (NSString * text in _respArray) {
        hNeed += [self getCellHightWithText:text];
    }
    return hNeed;
}

- (CGFloat)getCellHightWithText:(NSString *)text{
    CGSize size = [BaseHelper getSizeWithString:text font:TextFont contentWidth:cellContentW contentHight:MAXFLOAT];
    if (size.height < 20) {
        return CellH;
    }
    return size.height+VerPxFit(20);
}

- (DescAddView *)descAddView{
    _descAddView = [[DescAddView alloc] init];
    _descAddView.delegate = self;
    return _descAddView;
}

#pragma mark --- UITableViewDelegate UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == descTableView) {
        return _descArray.count;
    }else{
        return _respArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * CellIdentifier = @"CellIdentifier";
    UITableViewCell  * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (!cell) {
        CGFloat horPadding = HorPxFit(20);
        CGFloat buttonW = 40;
        CGFloat titleW = 20;
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UILabel * titleLb = [[UILabel alloc] init];
        titleLb.backgroundColor = [UIColor clearColor];
        titleLb.textColor = [UIColor whiteColor];
        titleLb.font = [UIFont systemFontOfSize:14];
        [cell addSubview:titleLb];
        [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(horPadding);
            make.top.equalTo(cell);
            make.size.mas_equalTo(CGSizeMake(titleW, CellH));
        }];
        
        UIButton * deleteBt = [UIButton buttonWithType:UIButtonTypeCustom];
        [deleteBt addTarget:self action:@selector(itemDeleteAction:) forControlEvents:UIControlEventTouchUpInside];
        [deleteBt setImage:[UIImage imageNamed:@"descDele"] forState:UIControlStateNormal];
        deleteBt.titleLabel.font = [UIFont boldSystemFontOfSize:30];
        [deleteBt setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        deleteBt.frame = CGRectMake(CGRectGetWidth(tableView.frame)-buttonW-horPadding,0, buttonW,CellH);
        [cell addSubview:deleteBt];
        
        UILabel * contentLb = [[UILabel alloc] init];
        contentLb.backgroundColor = [UIColor clearColor];
        contentLb.textColor = [UIColor whiteColor];
        contentLb.numberOfLines = 0;
        contentLb.font = TextFont;
        [cell addSubview:contentLb];
        [contentLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.and.bottom.equalTo(cell);
            make.left.mas_equalTo(titleLb.mas_right).mas_offset(horPadding);
            make.right.mas_equalTo(deleteBt.mas_left).mas_offset(-horPadding);
        }];
        
        if (tableView == respTableView) {
            titleLb.tag = RespBaseTag+0;
            deleteBt.tag = RespBaseTag+1;
            contentLb.tag = RespBaseTag+2;
        }else{
            titleLb.tag = DescBaseTag+0;
            deleteBt.tag = DescBaseTag+1;
            contentLb.tag = DescBaseTag+2;
        }
    }
    
    NSString * text;
    if (tableView == respTableView) {
        text = _respArray[indexPath.row];
    }else{
        text = _descArray[indexPath.row];
    }
    UILabel * titleLb;
    UILabel * contentLb;
    UIButton * deleBt;
    
    NSArray * subs = [cell subviews];
    for(UIView * view in subs){
        if ([view isKindOfClass:[UIButton class]]) {
            deleBt = (UIButton *)view;
            break;
        }
    }
    if (tableView == respTableView) {
        titleLb = [cell viewWithTag:RespBaseTag];
        contentLb = [cell viewWithTag:(RespBaseTag+2)];
    }else{
        titleLb = [cell viewWithTag:DescBaseTag];
        contentLb = [cell viewWithTag:(DescBaseTag+2)];
    }
    titleLb.text =[NSString stringWithFormat:@"%ld.",indexPath.row+1];
    contentLb.text = text;

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString * text = nil;
    if (tableView == respTableView) {
        text = _respArray[indexPath.row];
    }else{
        text = _descArray[indexPath.row];
    }
    return [self getCellHightWithText:text];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark -- DescAddViewDelegate
- (void)addSureAction:(NSString *)text withType:(InfoAddType)type{
    if (type == InfoAddTypeResp) {
        [self.descArray addObject:text];
    }else if (type == InfoAddTypeDesc){
        [self.respArray addObject:text];
    }
    
   CGFloat selfH =  [self initInfoWithDesc:_descArray respArray:_respArray];
    [self.delegate PostionDesAndResViewHightChange:selfH];
}

@end

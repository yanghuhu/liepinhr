//
//  PostionCandidate.m
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/14.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "PostionCandidate.h"
#import "UIImage+YYAdd.h"
#import "UIImageView+YYWebImage.h"
#import "UIScrollView+PSRefresh.h"

#define hongdianFlagTag 1000
#define CandidateItemVBaseTag 500

@class CandidateItemView;
@protocol CandidateItemViewDelegate

- (void)itemChoosed:(CandidateModel *)model;
@end

@interface CandidateItemView(){
    
    UIImageView * avatarImgV; // 候选人头像imgV
    UILabel * namelb;      //  候选人姓名
}

@property (nonatomic , assign) id<CandidateItemViewDelegate>delegate;
@property (nonatomic , strong) CandidateModel * model;

@end

@implementation CandidateItemView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor redColor];
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    self.backgroundColor = [UIColor clearColor];
    CGFloat itemH = CGRectGetWidth(self.frame);
    avatarImgV = [[UIImageView alloc] initWithFrame:CGRectMake(0, VerPxFit(12),itemH,itemH)];
    avatarImgV.backgroundColor = [UIColor redColor];
    avatarImgV.layer.cornerRadius = itemH/2.0;
    avatarImgV.clipsToBounds = YES;
    avatarImgV.image = [BaseHelper avatarPlaceHolderWithGender:1];
    [self addSubview:avatarImgV];
    
    CGFloat lbY = CGRectGetHeight(self.frame) - 26;
    namelb = [[UILabel alloc] initWithFrame:CGRectMake(0, lbY, CGRectGetWidth(self.frame), 26)];
    namelb.textAlignment = NSTextAlignmentCenter;
    namelb.textColor = [UIColor whiteColor];
    namelb.font = [UIFont systemFontOfSize:13];
    [self addSubview:namelb];
    
    UIButton * bt = [UIButton buttonWithType:UIButtonTypeCustom];
    [bt addTarget:self action:@selector(itemChoosed) forControlEvents:UIControlEventTouchUpInside];
    bt.frame = self.bounds;
    [self addSubview:bt];
}

- (void)itemChoosed{
    [self.delegate itemChoosed:_model];
}

- (void)setModel:(CandidateModel *)model{
    _model = model;
    namelb.text = model.candidateName;
    
    @weakify(avatarImgV);
    [avatarImgV setImageWithURL:[NSURL URLWithString:model.candidateAvatar] placeholder:[BaseHelper avatarPlaceHolderWithGender:1l] options:YYWebImageOptionShowNetworkActivity completion:^(UIImage * _Nullable image, NSURL * _Nonnull url, YYWebImageFromType from, YYWebImageStage stage, NSError * _Nullable error) {
        if ([model.status isEqualToString:@"OFF"]) {
            @strongify(avatarImgV);
            if(image){
                avatarImgV.image = [BaseHelper grayImage:image];
            }
        }
    }];

    if (model.hrStateFlag) {
        UIView * flagView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, HorPxFit(9), HorPxFit(9))];
        flagView.tag = hongdianFlagTag;
        flagView.layer.cornerRadius = HorPxFit(9)/2.0;
        flagView.backgroundColor = [UIColor redColor];
        flagView.center = CGPointMake(CGRectGetMaxX(avatarImgV.frame)-CGRectGetWidth(avatarImgV.frame)/13, avatarImgV.center.y - avatarImgV.frame.size.height/5*2);
        [self addSubview:flagView];
    }
}

@end

@interface PostionCandidate()<UIScrollViewDelegate,CandidateItemViewDelegate>{
    
    UIScrollView * scrollView;   //  候选人容器view
    CGFloat itemW;       // 单个候选人宽度
}

@property (nonatomic , strong) NSArray *candidateArray;  // 候选人数组

@end

@implementation PostionCandidate

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor redColor];
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    self.backgroundColor = [UIColor clearColor];
    UIImage * img = [UIImage imageNamed:@"candidateBorder"];
    UIImageView * contentBk = [[UIImageView alloc] initWithImage:img];
    [self addSubview:contentBk];
    [contentBk mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).mas_offset(HorPxFit(0));
        make.top.equalTo(self).mas_offset(-VerPxFit(0));
        make.right.equalTo(self).mas_offset(HorPxFit(0));
        make.bottom.equalTo(self).mas_offset(VerPxFit(0));
    }];
    
    scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(5, 0, CGRectGetWidth(self.frame)-10, CGRectGetHeight(self.frame))];
    scrollView.backgroundColor = [UIColor clearColor];
    scrollView.showsVerticalScrollIndicator = NO;
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.delegate = self;
    [self addSubview:scrollView];
    
    @weakify(self)
    [scrollView addRefreshHeaderWithClosure:^{
        @strongify(self)
        [self changeDataSourceWithState:TableViewDataSourceChangeForRefresh];
    }];
    [scrollView addRefreshFooterWithClosure:^{
        @strongify(self)
        [self changeDataSourceWithState:TableViewDataSourceChangeForGetMore];
    }];
}


- (void)changeDataSourceWithState:(TableViewDataSourceChange) changeType{
    if (changeType == TableViewDataSourceChangeForGetMore) {
        [self.delegate candidateGetMore];
    }else{
        [self.delegate candidateRefresh];
    }
}

- (void)candidateSVUpdate:(NSArray <CandidateModel *>*)candidateArray{
    [scrollView endRefreshing];
    NSMutableArray * temp = [NSMutableArray array];
    // 第一位候选人从view中间显示，故前面添加占位数据
    [temp addObject:@"placeholder"];
    [temp addObject:@"placeholder"];
    [temp addObject:@"placeholder"];
    [temp addObjectsFromArray:candidateArray];
    [temp addObject:@"placeholder"];
    [temp addObject:@"placeholder"];
    [temp addObject:@"placeholder"];
    self.candidateArray = temp;

    CGFloat horPadding = HorPxFit(25);
    itemW = (CGRectGetWidth(self.frame) - 8*horPadding) / 7;
    
    CGFloat curX = horPadding;
    for (int i =0; i<_candidateArray.count; i++) {
        id obj = _candidateArray[i];
        if ([obj isKindOfClass:[NSString class]]) {
            curX += (itemW +horPadding);
            continue;
        }
        CandidateItemView * itemV = [[CandidateItemView alloc] initWithFrame:CGRectMake(curX, 0, itemW, CGRectGetHeight(self.frame))];
        itemV.model = obj;
        itemV.tag = CandidateItemVBaseTag+i;
        itemV.delegate = self;
        [scrollView addSubview:itemV];
        curX += (itemW +horPadding);
    }
    scrollView.contentSize = CGSizeMake(curX, CGRectGetHeight(self.frame));
    if (candidateArray.count > 0) {
        [self.delegate candidateSelect:candidateArray[0]];
    }
}

- (void)removeItemViewHongDian:(CandidateModel *)candidateModel{
    NSInteger modelIndex = -1;
    for (int i=0; i<_candidateArray.count; i++) {
        id obj = _candidateArray[i];
        if ([obj isKindOfClass:[NSString class]]) {
            continue;
        }
        CandidateModel * model = (CandidateModel *)obj;
        if (model.id_ == candidateModel.id_) {
            modelIndex = i;
            break;
        }
    }
    if (modelIndex != -1) {
        CandidateItemView * itemView = (CandidateItemView *)[self viewWithTag:modelIndex+CandidateItemVBaseTag];
        UIView * view = [itemView viewWithTag:hongdianFlagTag];
        [view removeFromSuperview];
    }
}

#pragma mark -- CandidateItemViewDelegate

- (void)itemChoosed:(CandidateModel *)model{
    NSInteger index = [_candidateArray indexOfObject:model];
    NSInteger nums = index+1  - 4;
    CGFloat uOffset = nums*(itemW + HorPxFit(25));
    [scrollView setContentOffset:CGPointMake(uOffset, 0) animated:YES];
    
    if ([model isKindOfClass:[CandidateModel class]]) {
        [self.delegate candidateSelect:model];
    }
}

#pragma mark -- UIScrollViewDelegate
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
   
    NSInteger singleW = itemW + 2*HorPxFit(25);
    
    NSInteger nums = scrollView.contentOffset.x /  singleW;
    CGFloat else_ = scrollView.contentOffset.x - nums * singleW;
    
    NSInteger index = 0;
    if (else_ > (singleW / 2.0)) {
        index =  nums+4+1;
    }else{
        index =  nums+3+1;
    }
    
    CandidateModel *model = _candidateArray[index];
    [self itemChoosed:model];
}


@end

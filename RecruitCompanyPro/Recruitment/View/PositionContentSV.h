//
//  PositionContentSV.h
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/12.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JobModel.h"

@protocol PositionContentSVDelegate
@optional
//职位发布
- (void)positionPayAction;
// 职位匹配
- (void)positionMapAction;
 // 职位提交
- (void)positonSubmit:(NSDictionary *)baseInfo descArray:(NSArray *)descArray respArray:(NSArray *)respArray;

@end

@interface PositionContentSV : UIScrollView

@property (nonatomic , weak) id<PositionContentSVDelegate>delegate_;
@property (nonatomic , strong) NSMutableDictionary * resultInfoDic; // 职位信息字典
@property (nonatomic , strong) NSArray * positionDescArray;     //  职位描述数组
@property (nonatomic , strong) NSArray * positionResponArray;       // 职位职责数组

// 跟新职位信息展示
- (void)updateInfoAppear:(JobModel *)model;
- (void)initDesc:(NSArray *)descArray  respArray:(NSArray *)respArray;

@end

//
//  PositionStateView.m
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/14.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "PositionStateView.h"
#import "AppDelegate.h"
#import "QRCodeCreateManage.h"
#import "UIView+YYAdd.h"
#import "RateView.h"

#define StatueTextColor COLOR(66, 163, 252, 1)
#define StepBallImgVTag  201
#define OfftfbaseTag 200
#define NotPostionTag  100

#define TitleFont  [UIFont systemFontOfSize:14]
#define TextFont  [UIFont systemFontOfSize:13]
#define SubTextFont [UIFont systemFontOfSize:13]
#define  ButtonFont  [UIFont systemFontOfSize:15]

@interface PositionStateView()<UITextFieldDelegate,UITextViewDelegate,RateViewDelegate,UIAlertViewDelegate>{
    
    UIButton * recommendBt;     // 阶段 推荐按钮
    UIButton * interviewBt;     // 阶段  面试按钮
    UIButton * entryBt;         //  阶段  入职按钮
    UIButton * cleaingBt;       //  阶段   清算按钮
    
    // 四个阶段view间的三条连接线
    UIImageView * firStepLine;
    UIImageView * secStepLine;
    UIImageView * thirStepLine;
    
    UIScrollView * stateInfoView;        //各阶段信息容器view
    
    UIButton * resumeConformityBt;     //  简历筛选  合格按钮
    UIButton * resumeNotConformityBt;   //  简历筛选  不合格按钮
    UITextField * resumeReasonTv;      //  简历筛选  信息输入
    
    UIButton * interViewSuccessBt;      //  面试结果  面试通过按钮
    UIButton * interViewFailBt;         //  面试结果  面试不通过按钮
    UITextField * interviewReasonTv;     //  面试结果  信息输入
    
    UIButton * entryDateBt;         //  offer 入职时间picker按钮
    UITextField * offDescTV;         // offer   备注信息输入
    
    NSArray * offKeys;          // offer 相关的所有字段的key
    UIButton * paySubmitBt;       // 支付按钮
    
    UIButton * resumBt;      //  简历按钮
    UIButton * rateBt;       // 评价按钮
}

@property (nonatomic , strong) NSMutableDictionary * offerInfodic;
@property (nonatomic , strong) UIView * datePickerContentV;
@property (nonatomic , strong) UIDatePicker * datePicker;
@property (nonatomic , strong) RateView * rateView;

@end

@implementation PositionStateView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self createSubViews];        
    }
    return self;
}

- (void)createSubViews{

    UIImage * img = [UIImage imageNamed:@"stateInfoBk"];
    UIImageView * contentBk = [[UIImageView alloc] initWithImage:img];
    [self addSubview:contentBk];
    [contentBk mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).mas_offset(-HorPxFit(0));
        make.top.equalTo(self).mas_offset(-VerPxFit(82));
        make.right.equalTo(self).mas_offset(HorPxFit(0));
        make.bottom.equalTo(self).mas_offset(VerPxFit(0));
    }];
    
    CGFloat horPadding = HorPxFit(80);
    CGFloat stateBallW = HorPxFit(40);
    CGFloat stateBallPadding = HorPxFit(40);
    CGFloat topPadding = VerPxFit(12);
    
    UILabel * stepTitle = [[UILabel alloc] initWithFrame:CGRectMake(horPadding/3.0, topPadding, 150, 20)];
    stepTitle.text = @"进度状态";
    stepTitle.textColor = [UIColor whiteColor];
    stepTitle.font = [UIFont systemFontOfSize:14];
    [self addSubview:stepTitle];
    
   recommendBt =  [self ballViewFrame:CGRectMake(horPadding, CGRectGetMaxY(stepTitle.frame)+VerPxFit(10), stateBallW, stateBallW+VerPxFit(30)) ballImg:@"canDetailRecommend" title:@"推荐" tag:100];
   interviewBt =  [self ballViewFrame:CGRectMake(horPadding+stateBallW+stateBallPadding, CGRectGetMaxY(stepTitle.frame)+VerPxFit(10), stateBallW, stateBallW+VerPxFit(30)) ballImg:@"canDetailInterview" title:@"面试" tag:101];
    entryBt =  [self ballViewFrame:CGRectMake(horPadding+stateBallW*2+stateBallPadding*2, CGRectGetMaxY(stepTitle.frame)+VerPxFit(10), stateBallW, stateBallW+VerPxFit(30)) ballImg:@"canDetailEntry" title:@"入职" tag:102];
    cleaingBt = [self ballViewFrame:CGRectMake(horPadding+stateBallW*3+stateBallPadding*3, CGRectGetMaxY(stepTitle.frame)+VerPxFit(10), stateBallW, stateBallW+VerPxFit(30)) ballImg:@"canDetailPosition" title:@"结算" tag:103];
    
    firStepLine = [self imgV:CGRectMake(horPadding+stateBallW, CGRectGetMaxY(stepTitle.frame)+VerPxFit(10)+stateBallW/2.0-2, stateBallPadding, 2) imgName:@"stepFirLine"];
    secStepLine = [self imgV:CGRectMake(horPadding+stateBallW*2+stateBallPadding, CGRectGetMaxY(stepTitle.frame)+VerPxFit(10)+stateBallW/2.0-2, stateBallPadding, 2) imgName:@"stepSecLine"];
    thirStepLine = [self imgV:CGRectMake(horPadding+stateBallW*3+stateBallPadding*2, CGRectGetMaxY(stepTitle.frame)+VerPxFit(10)+stateBallW/2.0-2, stateBallPadding, 2) imgName:@"stepThirLine"];
    
    resumBt = [UIButton buttonWithType:UIButtonTypeCustom];
    resumBt.frame = CGRectMake(CGRectGetWidth(self.frame) - HorPxFit(80)-HorPxFit(20), topPadding, HorPxFit(80), stateBallW/3*2);
    [resumBt setBackgroundImage:[UIImage imageNamed:@"btBk"] forState:UIControlStateNormal];
    [resumBt setTitle:@"查看简历" forState:UIControlStateNormal];
    [resumBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    resumBt.titleLabel.font = [UIFont systemFontOfSize:14];
    [resumBt addTarget:self action:@selector(resumeCheckAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:resumBt];
    
    UILabel * stateDescTitle = [[UILabel alloc] initWithFrame:CGRectMake(horPadding/3.0, CGRectGetMaxY(stepTitle.frame)+VerPxFit(10)+VerPxFit(30)+stateBallW, 150, 25)];
    stateDescTitle.text = @"状态描述";
    stateDescTitle.textColor = [UIColor whiteColor];
    stateDescTitle.font = [UIFont systemFontOfSize:14];
    [self addSubview:stateDescTitle];
    
    stateInfoView = [[UIScrollView alloc] initWithFrame:CGRectMake(horPadding, CGRectGetMaxY(stateDescTitle.frame), CGRectGetWidth(self.frame)-2*horPadding, CGRectGetHeight(self.frame)-CGRectGetMaxY(stateDescTitle.frame)-5)];
    stateInfoView.backgroundColor = [UIColor clearColor];
    [self addSubview:stateInfoView];
}

- (UIButton *)ballViewFrame:(CGRect)frame ballImg:(NSString *)imgName title:(NSString *)title tag:(NSInteger)tag{
    UIView * view = [[UIView alloc] initWithFrame:frame];
    [self addSubview:view];
    UIImageView * imgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:imgName]];
    imgV.tag = StepBallImgVTag;
    imgV.frame = CGRectMake(0, 0, frame.size.width, frame.size.width);
    [view addSubview:imgV];
    UILabel * titleLb = [[UILabel alloc] initWithFrame:CGRectMake(0, frame.size.height - 30, frame.size.width, VerPxFit(25))];
    titleLb.text = title;
    titleLb.textColor = [UIColor whiteColor];
    titleLb.font = [UIFont systemFontOfSize:13];
    titleLb.textAlignment = NSTextAlignmentCenter;
    [view addSubview:titleLb];
    
    UIButton * bt = [UIButton buttonWithType:UIButtonTypeCustom];
    bt.frame = view.bounds;
    bt.tag = tag;
    [bt addTarget:self action:@selector(stepShift:) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:bt];
    
    return bt;
}

- (UIImageView *)imgV:(CGRect)frame imgName:(NSString *)name{
    UIImageView * imgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:name]];
    imgV.frame = frame;
    [self addSubview:imgV];
    return imgV;
}

- (void)stepShift:(UIButton *)bt{
    switch (bt.tag) {
        case 100:{
            [self recommendBackView];
            break;
        }
        case 101:{
            [self interViewBackView];
            break;
        }
        case 102:{
            [self entryViewBackView];
            break;
        }
        case 103:{
            [self cleaingViewBackView];
            break;
        }
        default:
            break;
    }
}

- (void)setModel:(JobOrderDetailModel *)model{
    _model  = model;
    if (_model.orderStep == OrderStepRecommend) {
        recommendBt.enabled = YES;
        interviewBt.enabled = NO;
        entryBt.enabled = NO;
        cleaingBt.enabled = NO;
        
        UIImageView * imgVR = [recommendBt.superview viewWithTag:StepBallImgVTag];
        imgVR.image = [UIImage imageNamed:@"canDetailRecommend"];
        
        UIImage * img = [UIImage imageNamed:@"candetailUnBall"];
        UIImageView * imgV = [interviewBt.superview viewWithTag:StepBallImgVTag];
        imgV.image = img;
        
        UIImageView * imgVSec = [entryBt.superview viewWithTag:StepBallImgVTag];
        imgVSec.image = img;
        
        UIImageView * imgVThir = [cleaingBt.superview viewWithTag:StepBallImgVTag];
        imgVThir.image = img;
        
        secStepLine.image = [UIImage imageNamed:@"stepLineUn"];
        thirStepLine.image = [UIImage imageNamed:@"stepLineUn"];
    }else if(_model.orderStep == OrderStepInterView){
        recommendBt.enabled = YES;
        interviewBt.enabled = YES;
        entryBt.enabled = NO;
        cleaingBt.enabled = NO;
        
        UIImageView * imgVR = [recommendBt.superview viewWithTag:StepBallImgVTag];
        imgVR.image = [UIImage imageNamed:@"canDetailRecommend"];
        
        UIImageView * imgVI = [interviewBt.superview viewWithTag:StepBallImgVTag];
        imgVI.image = [UIImage imageNamed:@"canDetailInterview"];
        
        secStepLine.image = [UIImage imageNamed:@"stepSecLine"];
        
        UIImage * img = [UIImage imageNamed:@"candetailUnBall"];

        UIImageView * imgVSec = [entryBt.superview viewWithTag:StepBallImgVTag];
        imgVSec.image = img;
        
        UIImageView * imgVThir = [cleaingBt.superview viewWithTag:StepBallImgVTag];
        imgVThir.image = img;
        
        thirStepLine.image = [UIImage imageNamed:@"stepLineUn"];
        
    }else if (_model.orderStep == OrderStepEntry){
        recommendBt.enabled = YES;
        interviewBt.enabled = YES;
        entryBt.enabled = YES;
        cleaingBt.enabled = NO;
        
        UIImageView * imgVR = [recommendBt.superview viewWithTag:StepBallImgVTag];
        imgVR.image = [UIImage imageNamed:@"canDetailRecommend"];
        secStepLine.image = [UIImage imageNamed:@"stepSecLine"];

        UIImageView * imgVI = [interviewBt.superview viewWithTag:StepBallImgVTag];
        imgVI.image = [UIImage imageNamed:@"canDetailInterview"];
        thirStepLine.image = [UIImage imageNamed:@"stepThirLine"];

        UIImageView * imgVThir = [entryBt.superview viewWithTag:StepBallImgVTag];
        imgVThir.image = [UIImage imageNamed:@"canDetailEntry"];

        UIImageView * imgVfor = [cleaingBt.superview viewWithTag:StepBallImgVTag];
        imgVfor.image = [UIImage imageNamed:@"candetailUnBall"];;
    }else if (_model.orderStep == OrderStepPositive){
        recommendBt.enabled = YES;
        interviewBt.enabled = YES;
        entryBt.enabled = YES;
        cleaingBt.enabled = YES;
        
        UIImageView * imgVR = [recommendBt.superview viewWithTag:StepBallImgVTag];
        imgVR.image = [UIImage imageNamed:@"canDetailRecommend"];
        secStepLine.image = [UIImage imageNamed:@"stepSecLine"];
        
        UIImageView * imgVI = [interviewBt.superview viewWithTag:StepBallImgVTag];
        imgVI.image = [UIImage imageNamed:@"canDetailInterview"];
        thirStepLine.image = [UIImage imageNamed:@"stepThirLine"];
        
        UIImageView * imgVThir = [entryBt.superview viewWithTag:StepBallImgVTag];
        imgVThir.image = [UIImage imageNamed:@"canDetailEntry"];

        UIImageView * imgVfor = [cleaingBt.superview viewWithTag:StepBallImgVTag];
        imgVfor.image = [UIImage imageNamed:@"canDetailPosition"];;
    }
    [stateInfoView removeAllSubviews];
    
    if ([_model.state isEqualToString:PositionCandidateOrderStateRecommended]) {
        [self resumeDealView];
    }else if ([_model.state isEqualToString:PositionCandidateOrderStateAccepted]){
        [self viewForResumeSuccess];
    }else if ([_model.state isEqualToString:PositionCandidateOrderStateRejected]){
        [self viewForResumeFaile];
    }else if ([_model.state isEqualToString:PositionCandidateOrderStateWait_Appointment]){
        [self viewForWaitAppointment];
    }else if ([_model.state isEqualToString:PositionCandidateOrderStateWait_Interview]){
        [self viewForWaitInterview];
    }else if ([_model.state isEqualToString:PositionCandidateOrderStateInterview_Failed]){
        [self viewForInterviewResult];
    }else if ([_model.state isEqualToString:PositionCandidateOrderStateWaint_Offer]){
        offKeys = @[@"contractPeriod",@"department",@"leader",@"position",@"probationPeriod",@"probationSalary",@"salary",@"workplace",@"reportDutyTime",@"description"];
        self.offerInfodic = [NSMutableDictionary dictionary];
        [self viewForPublishoffer];
    }else if ([_model.state isEqualToString:PositionCandidateOrderStateWait_Register]){
        [self viewForofferCheck];
    }else if ([_model.state isEqualToString:PositionCandidateOrderStateNot_Register]){
        [self viewForofferCheck];
    }else if ([_model.state isEqualToString:PositionCandidateOrderStateWait_Positive]){
        [self viewForWaitPostion];
    }else if([_model.state isEqualToString:PositionCandidateOrderStateNot_Position]){
        [self viewForNotPosition];
    }else if([_model.state isEqualToString:PositionCandidateOrderStatePositive]){
        [self viewForPosition];
    }
}

- (void)resumeCheckAction{
    [self.delegate resumCheck:self.model.talentModel];
}

- (void)recommendBackView{
    [stateInfoView removeAllSubviews];
    if ([_model.state isEqualToString:PositionCandidateOrderStateRejected]) {
        [self viewForResumeFaile];
    }else{
        [self viewForRecommendView];
    }
}

- (void)interViewBackView{
    [stateInfoView removeAllSubviews];
    if ([_model.state isEqualToString:PositionCandidateOrderStateWait_Appointment]) {
        [self viewForWaitAppointment];
    }else if ([_model.state isEqualToString:PositionCandidateOrderStateWait_Interview]){
        [self viewForWaitInterview];
    }else if ([_model.state isEqualToString:PositionCandidateOrderStateInterview_Failed]){
        [self viewForInterviewResult];
    }else if ([_model.state isEqualToString:PositionCandidateOrderStateAccepted]){
        [self viewForResumeSuccess];
    }
    else {
        [self viewForInterviewResult];
    }
}

- (void)entryViewBackView{
    [stateInfoView removeAllSubviews];

    if ([_model.state isEqualToString:PositionCandidateOrderStateWaint_Offer]) {
        [self viewForPublishoffer];
    }else if ([_model.state isEqualToString:PositionCandidateOrderStateWait_Register] || [_model.state isEqualToString:PositionCandidateOrderStateWait_Positive] || [_model.state isEqualToString:PositionCandidateOrderStatePositive] || [_model.state isEqualToString:PositionCandidateOrderStateNot_Register] || [_model.state isEqualToString:PositionCandidateOrderStateNot_Position]){
        [self viewForofferCheck];
    }
}

- (void)cleaingViewBackView{
    [stateInfoView removeAllSubviews];
    if ([_model.state isEqualToString:PositionCandidateOrderStateWait_Positive]){
        [self viewForWaitPostion];
    }else if([_model.state isEqualToString:PositionCandidateOrderStatePositive]){
        [self viewForPosition];
    }else if ([_model.state isEqualToString:PositionCandidateOrderStateNot_Position]){
        [self viewForNotPosition];
    }
}


- (void)resumeDealView{
    CGFloat horPadding = HorPxFit(20);
    CGFloat verPadding = VerPxFit(17);
    
    resumeConformityBt  = [UIButton buttonWithType:UIButtonTypeCustom];
    resumeConformityBt.frame = CGRectMake(0, verPadding, HorPxFit(110), VerPxFit(30));
    [resumeConformityBt setTitle:@"简历合格" forState:UIControlStateNormal];
    [resumeConformityBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    resumeConformityBt.layer.cornerRadius = VerPxFit(30)/2.0;
    
    resumeConformityBt.layer.borderColor = [UIColor lightGrayColor].CGColor;
    resumeConformityBt.titleLabel.font = TextFont;
    resumeConformityBt.layer.borderWidth = 1;
    [resumeConformityBt addTarget:self action:@selector(resumeFiltrateAction:) forControlEvents:UIControlEventTouchUpInside];
    [stateInfoView addSubview:resumeConformityBt];
    
    
    resumeNotConformityBt  = [UIButton buttonWithType:UIButtonTypeCustom];
    resumeNotConformityBt.frame = CGRectMake(CGRectGetMaxX(resumeConformityBt.frame)+HorPxFit(30), verPadding, HorPxFit(110), VerPxFit(30));
    [resumeNotConformityBt setTitle:@"简历不合格" forState:UIControlStateNormal];
    [resumeNotConformityBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    resumeNotConformityBt.titleLabel.font = TextFont;
    resumeNotConformityBt.layer.cornerRadius = VerPxFit(30)/2.0;
    resumeNotConformityBt.layer.borderColor = [UIColor lightGrayColor].CGColor;
    resumeNotConformityBt.layer.borderWidth = 1;
    [resumeNotConformityBt addTarget:self action:@selector(resumeFiltrateAction:) forControlEvents:UIControlEventTouchUpInside];
    [stateInfoView addSubview:resumeNotConformityBt];
    
    
    UILabel * resonLb = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(resumeNotConformityBt.frame)+VerPxFit(15), 150, 25)];
    resonLb.text = @"原因陈述";
    resonLb.font = TitleFont;
    resonLb.textColor = [UIColor whiteColor];
    [stateInfoView addSubview:resonLb];
    
    CGFloat tvY = CGRectGetMaxY(resonLb.frame);
    
    resumeReasonTv = [[UITextField alloc] initWithFrame:CGRectMake(0, tvY, CGRectGetWidth(stateInfoView.frame),  VerPxFit(30))];
    resumeReasonTv.font = TextFont;
    resumeReasonTv.textColor = [UIColor whiteColor];
    resumeReasonTv.leftViewMode = UITextFieldViewModeAlways;
    [stateInfoView addSubview:resumeReasonTv];
    UIView * leftView_resume = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 0)];
    resumeReasonTv.leftView = leftView_resume;
    
    UIImageView * bottonLine = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"AccountBottom"]];
    [stateInfoView addSubview:bottonLine];
    [bottonLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(resumeReasonTv);
        make.top.mas_equalTo(resumeReasonTv.mas_bottom);
        make.height.mas_equalTo(1);
    }];
    
    UIButton * sureBt = [UIButton buttonWithType:UIButtonTypeCustom];
    sureBt.tag = 100;
    sureBt.titleLabel.font = ButtonFont;
    [sureBt setTitle:@"确定" forState:UIControlStateNormal];
    [sureBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sureBt setBackgroundImage:[UIImage imageNamed:@"btBk"] forState:UIControlStateNormal];
    sureBt.frame = CGRectMake(horPadding, CGRectGetHeight(stateInfoView.frame)-VerPxFit(40), HorPxFit(210), VerPxFit(35));
    [sureBt addTarget:self action:@selector(resumeDealSubmit) forControlEvents:UIControlEventTouchUpInside];
    [stateInfoView addSubview:sureBt];
    stateInfoView.contentSize =  stateInfoView.size;
    sureBt.centerX = resumeReasonTv.centerX;
}

- (void)viewForResumeSuccess{
    CGFloat horPadding = HorPxFit(50);
    CGFloat verPadding = VerPxFit(10);

    UILabel * lb = [self lb];
    lb.frame = CGRectMake(0, verPadding, CGRectGetWidth(stateInfoView.frame)-2*horPadding, 25);
    lb.text = @"等待猎头发布约面时间";
    lb.font = TitleFont;
    lb.textColor = StatueTextColor;
    [stateInfoView addSubview:lb];
}

- (void)viewForResumeFaile{
    CGFloat horPadding = HorPxFit(20);
    CGFloat verPadding = VerPxFit(7);
    CGFloat lbH = VerPxFit(25);
    UILabel * resultTitleLb = [self lb];
    resultTitleLb.frame = CGRectMake(0, verPadding, 200, lbH);
    resultTitleLb.text = @"筛选结果";
    resultTitleLb.font = TitleFont;
    [stateInfoView addSubview:resultTitleLb];
    
    UILabel  * resultlb = [self lb];
    resultlb.frame = CGRectMake(horPadding*2, CGRectGetMaxY(resultTitleLb.frame)+verPadding, 300, lbH);
    resultlb.text = @"简历不合格";
    resultlb.font = TextFont;
    [stateInfoView addSubview:resultlb];
    
    UILabel * reasonTitleLb = [self lb];
    reasonTitleLb.frame = CGRectMake(0, CGRectGetMaxY(resultlb.frame)+verPadding, 200, lbH);
    reasonTitleLb.text = @"原因描述";
    reasonTitleLb.font = TitleFont;
    [stateInfoView addSubview:reasonTitleLb];
    
    UILabel * reasonLb = [self lb];
    reasonLb.frame = CGRectMake(horPadding*2, CGRectGetMaxY(reasonTitleLb.frame)+verPadding, CGRectGetWidth(stateInfoView.frame)-4*horPadding, lbH);
    reasonLb.font = TextFont;
    reasonLb.text = _model.rejectReason;
    [stateInfoView addSubview:reasonLb];
    
    if (!_model.hrEvaluateFlag) {
        rateBt = [UIButton buttonWithType:UIButtonTypeCustom];
        rateBt.frame = CGRectMake(horPadding*2, CGRectGetMaxY(reasonLb.frame)+verPadding*2, HorPxFit(180), VerPxFit(32));
        [rateBt addTarget:self action:@selector(showRateView) forControlEvents:UIControlEventTouchUpInside];
        [rateBt setTitle:@"评价" forState:UIControlStateNormal];
        rateBt.titleLabel.font = ButtonFont;
        [rateBt setBackgroundImage:[UIImage imageNamed:@"btBk"] forState:UIControlStateNormal];
        [stateInfoView addSubview:rateBt];
        
        stateInfoView.contentSize = CGSizeMake(CGRectGetWidth(stateInfoView.frame), CGRectGetMaxY(rateBt.frame));
    }
}

- (void)viewForWaitAppointment{
    
    CGFloat horPadding = HorPxFit(20);
    CGFloat verPadding = VerPxFit(10);
    CGFloat lbH = VerPxFit(25);
    UILabel * resultTitleLb = [self lb];
    resultTitleLb.frame = CGRectMake(0, verPadding, 200, lbH);
    resultTitleLb.text = @"预约面试时间";
    resultTitleLb.font = TitleFont;
    resultTitleLb.textColor = [UIColor whiteColor];
    [stateInfoView addSubview:resultTitleLb];
    
    UILabel  * resultlb = [self lb];
    resultlb.frame = CGRectMake(horPadding*2, CGRectGetMaxY(resultTitleLb.frame)+verPadding, 300, lbH);
    resultlb.text = [BaseHelper stringWithTimeIntevl:_model.appointmentTime format:kDateFormatTypeYYYYMMDDHHMM];
    resultlb.textColor = [UIColor whiteColor];
    resultlb.font = TextFont;
    [stateInfoView addSubview:resultlb];
    stateInfoView.contentSize  = CGSizeMake(CGRectGetWidth(stateInfoView.frame), CGRectGetMaxY(resultlb.frame));

}

- (void)viewForWaitInterview{
    CGFloat horPadding = HorPxFit(20);
    CGFloat verPadding = VerPxFit(17);
    
    interViewSuccessBt  = [UIButton buttonWithType:UIButtonTypeCustom];
    interViewSuccessBt.frame = CGRectMake(0, verPadding, HorPxFit(110), VerPxFit(30));
    [interViewSuccessBt setTitle:@"面试通过" forState:UIControlStateNormal];
    [interViewSuccessBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    interViewSuccessBt.titleLabel.font = [UIFont systemFontOfSize:14];
    interViewSuccessBt.layer.cornerRadius = VerPxFit(30)/2.0;
    interViewSuccessBt.layer.borderColor = [UIColor lightGrayColor].CGColor;
    interViewSuccessBt.layer.borderWidth = 1;
    interViewSuccessBt.titleLabel.font = TextFont;
    [interViewSuccessBt addTarget:self action:@selector(interviewResultFiltrate:) forControlEvents:UIControlEventTouchUpInside];
    [stateInfoView addSubview:interViewSuccessBt];
    
    interViewFailBt  = [UIButton buttonWithType:UIButtonTypeCustom];
    interViewFailBt.frame = CGRectMake(CGRectGetMaxX(interViewSuccessBt.frame)+HorPxFit(30), verPadding, HorPxFit(110), VerPxFit(30));
    interViewFailBt.titleLabel.font = [UIFont systemFontOfSize:14];
    [interViewFailBt setTitle:@"面试不通过" forState:UIControlStateNormal];
    [interViewFailBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    interViewFailBt.layer.cornerRadius = VerPxFit(30)/2.0;
    interViewFailBt.titleLabel.font = TextFont;
    interViewFailBt.layer.borderColor = [UIColor lightGrayColor].CGColor;
    interViewFailBt.layer.borderWidth = 1;
    [interViewFailBt addTarget:self action:@selector(interviewResultFiltrate:) forControlEvents:UIControlEventTouchUpInside];
    [stateInfoView addSubview:interViewFailBt];
    
    UILabel * resonLb = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(interViewFailBt.frame)+VerPxFit(15), 150, 25)];
    resonLb.text = @"原因陈述";
    resonLb.textColor = [UIColor whiteColor];
    resonLb.font =TitleFont;
    [stateInfoView addSubview:resonLb];

    CGFloat tvY = CGRectGetMaxY(resonLb.frame);
    interviewReasonTv = [[UITextField alloc] initWithFrame:CGRectMake(0, tvY, CGRectGetWidth(stateInfoView.frame), VerPxFit(30))];
    interviewReasonTv.textColor = [UIColor whiteColor];
    interviewReasonTv.font =TextFont;
    [stateInfoView addSubview:interviewReasonTv];

    interviewReasonTv.leftViewMode = UITextFieldViewModeAlways;
    UIView * leftView_resume = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 0)];
    interviewReasonTv.leftView = leftView_resume;
    
    
    UIImageView * bottonLine = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"AccountBottom"]];
    [stateInfoView addSubview:bottonLine];
    [bottonLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(interviewReasonTv);
        make.top.mas_equalTo(interviewReasonTv.mas_bottom);
        make.height.mas_equalTo(1);
    }];
    
    UIButton * sureBt = [UIButton buttonWithType:UIButtonTypeCustom];
    sureBt.tag = 100;
    [sureBt setTitle:@"确定" forState:UIControlStateNormal];
    [sureBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sureBt setBackgroundImage:[UIImage imageNamed:@"btBk"] forState:UIControlStateNormal];
    sureBt.titleLabel.font = ButtonFont;
    sureBt.frame = CGRectMake(horPadding, CGRectGetHeight(stateInfoView.frame)-VerPxFit(40), HorPxFit(210), VerPxFit(35));
    [sureBt addTarget:self action:@selector(interviewResultSubmit) forControlEvents:UIControlEventTouchUpInside];
    sureBt.centerX = interviewReasonTv.centerX;
    [stateInfoView addSubview:sureBt];
    stateInfoView.contentSize =  CGSizeMake(CGRectGetWidth(stateInfoView.frame), CGRectGetMaxY(sureBt.frame)+10);
}

- (void)interviewSubmitFinish{
    interViewFailBt.enabled = NO;
    interViewSuccessBt.enabled = NO;
    UIButton * bt = [stateInfoView viewWithTag:100];
    bt.enabled = NO;
}

- (void)viewForInterviewResult{
    CGFloat horPadding = HorPxFit(20);
    CGFloat verPadding = VerPxFit(7);
    CGFloat lbH = VerPxFit(25);

    UILabel * resultTitleLb = [self lb];
    resultTitleLb.frame = CGRectMake(0, verPadding, 200, lbH);
    resultTitleLb.text = @"面试结果:";
    resultTitleLb.font = TitleFont;
    resultTitleLb.textColor = [UIColor whiteColor];
    [stateInfoView addSubview:resultTitleLb];
    
    UILabel  * resultlb = [self lb];
    resultlb.frame = CGRectMake(horPadding*2, CGRectGetMaxY(resultTitleLb.frame)+verPadding, 300, lbH);
    resultlb.textColor = [UIColor whiteColor];
    resultlb.font = TextFont;
    if ([_model.state isEqualToString:PositionCandidateOrderStateInterview_Failed]) {
        resultlb.text = @"面试不通过";
    }else{
        resultlb.text = @"面试通过";
    }
    [stateInfoView addSubview:resultlb];
    
    UILabel * reasonTitleLb = [self lb];
    reasonTitleLb.frame = CGRectMake(0, CGRectGetMaxY(resultlb.frame)+verPadding, 200, lbH);
    reasonTitleLb.textColor = [UIColor whiteColor];
    reasonTitleLb.text = @"原因描述:";
    reasonTitleLb.font = TitleFont;
    [stateInfoView addSubview:reasonTitleLb];
    
    UILabel * reasonLb = [self lb];
    reasonLb.frame = CGRectMake(horPadding*2, CGRectGetMaxY(reasonTitleLb.frame)+verPadding, CGRectGetWidth(stateInfoView.frame)-4*horPadding, lbH);
    reasonLb.text = _model.interviewResultDesc;
    reasonLb.textColor = [UIColor whiteColor];
    reasonLb.font = TextFont;
    [stateInfoView addSubview:reasonLb];
    stateInfoView.contentSize = CGSizeMake(CGRectGetWidth(stateInfoView.frame), CGRectGetMaxY(reasonLb.frame)+10);
    
    if ([_model.state isEqualToString:PositionCandidateOrderStateInterview_Failed]) {
        if (!_model.hrEvaluateFlag) {
            rateBt = [UIButton buttonWithType:UIButtonTypeCustom];
            rateBt.frame = CGRectMake(horPadding*2, CGRectGetMaxY(reasonLb.frame)+verPadding*2, HorPxFit(180), VerPxFit(32));
            [rateBt addTarget:self action:@selector(showRateView) forControlEvents:UIControlEventTouchUpInside];
            [rateBt setTitle:@"评价" forState:UIControlStateNormal];
            rateBt.titleLabel.font = ButtonFont;
            [rateBt setBackgroundImage:[UIImage imageNamed:@"btBk"] forState:UIControlStateNormal];
            [stateInfoView addSubview:rateBt];
            
            stateInfoView.contentSize = CGSizeMake(CGRectGetWidth(stateInfoView.frame), CGRectGetMaxY(rateBt.frame));
        }
    }
}

- (void)viewForPublishoffer{
    
    CGFloat horPadding = HorPxFit(50);
    CGFloat verPadding = VerPxFit(10);
    CGFloat lbH = 35;
    
    UILabel * headerLb= [[UILabel alloc] initWithFrame:CGRectMake(0, verPadding, CGRectGetWidth(stateInfoView.frame), lbH)];
    headerLb.text = @"填写 Offer 信息";
    headerLb.textAlignment = NSTextAlignmentCenter;
    headerLb.font = TitleFont;
    headerLb.textColor =StatueTextColor;
    [stateInfoView addSubview:headerLb];
    
    NSArray * titles = @[@"合同年限",@"部门",@"直接上级 ",@"职位",@"试用期",@"试用期工资",@"工资",@"工作地点",@"入职时间",@"说明"];
    
    CGFloat curX = 0;
    CGFloat curY = CGRectGetMaxY(headerLb.frame)+ verPadding;
    for (int i =0; i<titles.count; i++) {
        UILabel * lb = [[UILabel alloc] initWithFrame:CGRectMake(curX, curY, 120, lbH)];
        NSString * title = titles[i];
        lb.text = title;
        lb.textColor = [UIColor whiteColor];
        lb.font = [UIFont systemFontOfSize:15];
        [stateInfoView addSubview:lb];
        curX += CGRectGetWidth(lb.frame);
        if (i == titles.count - 2) {
            entryDateBt = [UIButton buttonWithType:UIButtonTypeCustom];
            [entryDateBt addTarget:self  action:@selector(addOffentryDate) forControlEvents:UIControlEventTouchUpInside];
            entryDateBt.frame = CGRectMake(curX, curY, CGRectGetWidth(stateInfoView.frame)-curX-horPadding, lbH);
            entryDateBt.layer.borderColor = [UIColor lightGrayColor].CGColor;
            entryDateBt.layer.borderWidth = 1;
            entryDateBt.titleLabel.font = TextFont;
            [entryDateBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [stateInfoView addSubview:entryDateBt];
            curY += (lbH+verPadding);
        }else{
            UITextField * tf = [[UITextField alloc] initWithFrame:CGRectMake(curX, curY, CGRectGetWidth(stateInfoView.frame)-curX-horPadding, lbH)];
            tf.layer.borderColor = [UIColor lightGrayColor].CGColor;
            tf.layer.borderWidth = 1;
            tf.delegate = self;
            tf.textColor = [UIColor whiteColor];
            tf.tag = OfftfbaseTag + i;
            tf.font = TextFont;
            tf.placeholder = title;
            [tf setValue:[UIColor clearColor] forKeyPath:@"_placeholderLabel.textColor"];
            UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 0)];
            tf.leftView = view;
            tf.leftViewMode = UITextFieldViewModeAlways;
            
            UILabel * v =  [[UILabel alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth, 30)];
            v.backgroundColor = [UIColor lightGrayColor];
            v.textAlignment = NSTextAlignmentCenter;
            v.font = [UIFont systemFontOfSize:17];
            v.text = [NSString stringWithFormat:@"正在填写 %@:",title];
            tf.inputAccessoryView =v;
            if (i==0 || i==4|| i==5||i==6) {
                tf.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
            }
            [stateInfoView addSubview:tf];
            curY += (lbH+verPadding);
            if (i < titles.count-2) {
                tf.returnKeyType = UIReturnKeyNext;
            }else{
                tf.returnKeyType = UIReturnKeyDone;
            }
            
            UILabel * lb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 40, lbH)];
            lb.textColor = [UIColor whiteColor];
            lb.textAlignment = NSTextAlignmentCenter;
            lb.font = TextFont;
            if (i == 0) {
                lb.text = @"年";
                lb.width = 30;
                tf.rightView = lb;
                tf.placeholder = [NSString stringWithFormat:@"%@,%@",title,lb.text];
            }else if (i == 4){
                lb.text = @"月";
                tf.rightView = lb;
                lb.width = 30;
                tf.placeholder = [NSString stringWithFormat:@"%@,%@",title,lb.text];
            }else if(i == 5 || i == 6){
                lb.text = @"元/月";
                lb.width = 50;
                tf.rightView = lb;
                tf.placeholder = [NSString stringWithFormat:@"%@,%@",title,lb.text];
            }
            tf.rightViewMode = UITextFieldViewModeAlways;
        }
        curX = 0;
    }
    UIButton * submitBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [submitBt addTarget:self action:@selector(offerInfoSubmit) forControlEvents:UIControlEventTouchUpInside];
    submitBt.frame = CGRectMake((CGRectGetWidth(stateInfoView.frame)-HorPxFit(150))/2.0, curY, HorPxFit(180), VerPxFit(32));
    [submitBt setTitle:@"提交" forState:UIControlStateNormal];
    submitBt.titleLabel.font = ButtonFont;
    [submitBt setBackgroundImage:[UIImage imageNamed:@"btBk"] forState:UIControlStateNormal];
    [stateInfoView addSubview:submitBt];
    
    curY += (CGRectGetHeight(submitBt.frame)+verPadding);
    stateInfoView.contentSize = CGSizeMake(CGRectGetWidth(stateInfoView.frame), curY);
}

- (void)viewForofferCheck{
    CGFloat horPadding = HorPxFit(50);
    CGFloat verPadding = VerPxFit(7);
    CGFloat lbH = VerPxFit(25);
    
    UILabel * headerLb = [self lb];
    headerLb.frame = CGRectMake(0, verPadding, CGRectGetWidth(stateInfoView.frame), lbH);
    if ([_model.state isEqualToString:PositionCandidateOrderStateWait_Positive] || [_model.state isEqualToString:PositionCandidateOrderStatePositive] || [_model.state isEqualToString:PositionCandidateOrderStateNot_Position]) {
        NSString * positionTime = [BaseHelper stringWithTimeIntevl:_model.reportDutyTime format:kDateFormatTypeYYYYMMDD];
        headerLb.text = [NSString stringWithFormat:@"入职时间:  %@",positionTime];
    }else if([_model.state isEqualToString:PositionCandidateOrderStateWait_Register]){
        headerLb.text = @"Offer 已发送,待入职";
    }else if ([_model.state isEqualToString:PositionCandidateOrderStateNot_Register]){
        headerLb.text = @"Offer 已发送,未入职";
    }
    headerLb.font = TitleFont;
    headerLb.textColor = StatueTextColor;
    [stateInfoView addSubview:headerLb];
    
    UILabel * offerTitleLb = [self lb];
    offerTitleLb.frame = CGRectMake(0, CGRectGetMaxY(headerLb.frame)+verPadding, CGRectGetWidth(stateInfoView.frame), lbH);
    offerTitleLb.text = @"Offer 信息:";
    offerTitleLb.font = TitleFont;
    offerTitleLb.textColor = [UIColor whiteColor];
    [stateInfoView addSubview:offerTitleLb];
    
    UILabel * contactLb = [self lb];
    contactLb.frame = CGRectMake(horPadding, CGRectGetMaxY(offerTitleLb.frame)+verPadding, CGRectGetWidth(stateInfoView.frame), lbH);
    contactLb.text = [NSString stringWithFormat:@"合同年限:        %ld 年",_model.offer.contractPeriod ];
    [stateInfoView addSubview:contactLb];
    
    UILabel * departmentLb = [self lb];
    departmentLb.frame = CGRectMake(horPadding, CGRectGetMaxY(contactLb.frame)+verPadding, CGRectGetWidth(stateInfoView.frame), lbH);
    departmentLb.text = [NSString stringWithFormat:@"部门:                %@",_model.offer.department ];
    [stateInfoView addSubview:departmentLb];
    
    UILabel * leaderLb = [self lb];
    leaderLb.frame = CGRectMake(horPadding, CGRectGetMaxY(departmentLb.frame)+verPadding, CGRectGetWidth(stateInfoView.frame), lbH);
    leaderLb.text = [NSString stringWithFormat:@"直接上级:        %@",_model.offer.leader];
    [stateInfoView addSubview:leaderLb];
    
    UILabel * positionLb = [self lb];
    positionLb.frame = CGRectMake(horPadding, CGRectGetMaxY(leaderLb.frame)+verPadding, CGRectGetWidth(stateInfoView.frame), lbH);
    positionLb.text = [NSString stringWithFormat:@"职位:                %@",_model.offer.position];
    [stateInfoView addSubview:positionLb];
    
    UILabel * probationPeriodLb = [self lb];
    probationPeriodLb.frame = CGRectMake(horPadding, CGRectGetMaxY(positionLb.frame)+verPadding, CGRectGetWidth(stateInfoView.frame), lbH);
    probationPeriodLb.text = [NSString stringWithFormat:@"试用期:            %ld 月",_model.offer.probationPeriod];
    [stateInfoView addSubview:probationPeriodLb];
    
    UILabel * probationSalaryLb = [self lb];
    probationSalaryLb.frame = CGRectMake(horPadding, CGRectGetMaxY(probationPeriodLb.frame)+verPadding, CGRectGetWidth(stateInfoView.frame), lbH);
    probationSalaryLb.text = [NSString stringWithFormat:@"试用期工资:    %ld 元/月",_model.offer.probationSalary];
    [stateInfoView addSubview:probationSalaryLb];
    
    UILabel * salaryLb = [self lb];
    salaryLb.frame = CGRectMake(horPadding, CGRectGetMaxY(probationSalaryLb.frame)+verPadding, CGRectGetWidth(stateInfoView.frame), lbH);
    salaryLb.text = [NSString stringWithFormat:@"工资:                %ld 元/月",_model.offer.salary];
    [stateInfoView addSubview:salaryLb];
    
    UILabel * workplaceLb = [self lb];
    workplaceLb.frame = CGRectMake(horPadding, CGRectGetMaxY(salaryLb.frame)+verPadding, CGRectGetWidth(stateInfoView.frame), lbH);
    workplaceLb.text = [NSString stringWithFormat:@"工作地点:        %@",_model.offer.workplace];
    [stateInfoView addSubview:workplaceLb];
    
    UILabel * reportDutyTimeLb = [self lb];
    reportDutyTimeLb.frame = CGRectMake(horPadding, CGRectGetMaxY(workplaceLb.frame)+verPadding, CGRectGetWidth(stateInfoView.frame), lbH);
    NSString * reportDutyTime = [BaseHelper stringWithTimeIntevl:_model.offer.reportDutyTime format:kDateFormatTypeYYYYMMDD];
    reportDutyTimeLb.text = [NSString stringWithFormat:@"入职时间:        %@",reportDutyTime];
    [stateInfoView addSubview:reportDutyTimeLb];
    
    UILabel * descriptionLb = [self lb];
    descriptionLb.frame = CGRectMake(horPadding, CGRectGetMaxY(reportDutyTimeLb.frame)+verPadding, CGRectGetWidth(stateInfoView.frame), lbH);
    descriptionLb.text = [NSString stringWithFormat:@"说明:                %@",_model.offer.description_?_model.offer.description_:@"无"];
    [stateInfoView addSubview:descriptionLb];
    
    stateInfoView.contentSize = CGSizeMake(CGRectGetWidth(stateInfoView.frame), CGRectGetMaxY(descriptionLb.frame)+20);

    if([_model.state isEqualToString:PositionCandidateOrderStateWait_Register]){
       UIButton * notRegisterBt = [UIButton buttonWithType:UIButtonTypeCustom];
        notRegisterBt.frame = CGRectMake(horPadding*2, CGRectGetMaxY(descriptionLb.frame)+verPadding*2, HorPxFit(180), VerPxFit(32));
        [notRegisterBt addTarget:self action:@selector(notRegisterAction) forControlEvents:UIControlEventTouchUpInside];
        [notRegisterBt setTitle:@"未入职" forState:UIControlStateNormal];
        notRegisterBt.titleLabel.font = ButtonFont;
        [notRegisterBt setBackgroundImage:[UIImage imageNamed:@"btBk"] forState:UIControlStateNormal];
        [stateInfoView addSubview:notRegisterBt];
        stateInfoView.contentSize = CGSizeMake(CGRectGetWidth(stateInfoView.frame), CGRectGetMaxY(notRegisterBt.frame)+20);
    }
  
    if ([_model.state isEqualToString:PositionCandidateOrderStateNot_Register] && !_model.hrEvaluateFlag ) {
        rateBt = [UIButton buttonWithType:UIButtonTypeCustom];
        rateBt.frame = CGRectMake(horPadding*2, CGRectGetMaxY(descriptionLb.frame)+verPadding*2, HorPxFit(180), VerPxFit(32));
        [rateBt addTarget:self action:@selector(showRateView) forControlEvents:UIControlEventTouchUpInside];
        [rateBt setTitle:@"评价" forState:UIControlStateNormal];
        rateBt.titleLabel.font = ButtonFont;
        [rateBt setBackgroundImage:[UIImage imageNamed:@"btBk"] forState:UIControlStateNormal];
        [stateInfoView addSubview:rateBt];
        
        stateInfoView.contentSize = CGSizeMake(CGRectGetWidth(stateInfoView.frame), CGRectGetMaxY(rateBt.frame));
    }
    stateInfoView.contentOffset = CGPointMake(0, 0);
}

- (void)viewForRecommendView{
    CGFloat horPadding = HorPxFit(20);
    CGFloat verPadding = VerPxFit(7);
    
    CGFloat lbH = VerPxFit(25);
    UILabel * resultTitleLb = [self lb];
    resultTitleLb.frame = CGRectMake(0, verPadding, 150, lbH);
    resultTitleLb.text = @"筛选结果:";
    resultTitleLb.font = TitleFont;
    [stateInfoView addSubview:resultTitleLb];
    
    UILabel  * resultlb = [self lb];
    resultlb.frame = CGRectMake(horPadding*2, CGRectGetMaxY(resultTitleLb.frame)+verPadding, 300, lbH);
    resultlb.text = @"简历合格";
    resultlb.font = TextFont;
    resultlb.textColor = [UIColor whiteColor];
    [stateInfoView addSubview:resultlb];
    
    UILabel * reasonTitleLb = [self lb];
    reasonTitleLb.frame = CGRectMake(0, CGRectGetMaxY(resultlb.frame)+verPadding, 200, lbH);
    reasonTitleLb.text = @"原因描述:";
    reasonTitleLb.font = TitleFont;
    reasonTitleLb.textColor = [UIColor whiteColor];
    [stateInfoView addSubview:reasonTitleLb];
    
    UILabel * reasonLb = [self lb];
    reasonLb.frame = CGRectMake(horPadding*2, CGRectGetMaxY(reasonTitleLb.frame)+verPadding, CGRectGetWidth(stateInfoView.frame)-4*horPadding, lbH);
    reasonLb.text = _model.rejectReason;
    reasonLb.numberOfLines = 0;
    reasonLb.font = TextFont;
    reasonLb.textColor = [UIColor whiteColor];
    [stateInfoView addSubview:reasonLb];
    stateInfoView.contentSize  = CGSizeMake(CGRectGetWidth(stateInfoView.frame), CGRectGetMaxY(resultlb.frame));
}

- (void)viewForWaitPostion{
    CGFloat verPadding = VerPxFit(7);
    CGFloat lbH = VerPxFit(25);
    
    UILabel * headerLb = [self lb];
    headerLb.frame = CGRectMake(0, verPadding, 200, lbH);
    headerLb.text = @"已入职，待转正";
    headerLb.font =TitleFont;
    headerLb.textColor = StatueTextColor;
    [stateInfoView addSubview:headerLb];
    
    UILabel * resultTitleLb = [self lb];
    resultTitleLb.frame = CGRectMake(0, CGRectGetMaxY(headerLb.frame)+verPadding, 100, lbH);
    resultTitleLb.text = @"入职时间:";
    resultTitleLb.font = TitleFont;
    [stateInfoView addSubview:resultTitleLb];
    
    UILabel  * resultlb = [self lb];
    resultlb.frame = CGRectMake(CGRectGetMaxX(resultTitleLb.frame), CGRectGetMaxY(headerLb.frame)+verPadding, 300, lbH);
    resultlb.text = [BaseHelper stringWithTimeIntevl:_model.reportDutyTime format:kDateFormatTypeYYYYMMDD];
    resultlb.font =TextFont;
    [stateInfoView addSubview:resultlb];
    
    UILabel * positionTitleLb = [self lb];
    positionTitleLb.frame = CGRectMake(0, CGRectGetMaxY(resultTitleLb.frame)+verPadding, 100, lbH);
    positionTitleLb.text = @"待转正时间:";
    positionTitleLb.font = TitleFont;
    [stateInfoView addSubview:positionTitleLb];
    
    UILabel * positionTimeLb = [self lb];
    positionTimeLb.frame = CGRectMake(CGRectGetMaxX(positionTitleLb.frame), CGRectGetMinY(positionTitleLb.frame), 300, lbH);
    
    NSDate * reportDutyDate = [NSDate dateWithTimeIntervalSince1970:_model.reportDutyTime];
    NSDate * positionDate = [BaseHelper date:reportDutyDate addMonth:_model.offer.probationPeriod day:0 year:0];
    positionTimeLb.text = [BaseHelper stringFromDate:positionDate format:kDateFormatTypeYYYYMMDD];
    positionTimeLb.font =TextFont;
    [stateInfoView addSubview:positionTimeLb];
    
    stateInfoView.contentSize = CGSizeMake(CGRectGetWidth(stateInfoView.frame), CGRectGetMaxY(positionTimeLb.frame));
    
    CGRect notPositionBtFrame;
    
    if (_model.applyCommission) {
        UIButton * submitBt = [UIButton buttonWithType:UIButtonTypeCustom];
        [submitBt addTarget:self action:@selector(payForReward) forControlEvents:UIControlEventTouchUpInside];
        submitBt.frame = CGRectMake(0, CGRectGetMaxY(positionTitleLb.frame)+verPadding*3, HorPxFit(180), VerPxFit(32));
        if (_model.itemPayFee == 0) {
            [submitBt setTitle:@"支付佣金" forState:UIControlStateNormal];
        }else{
            [submitBt setTitle:@"佣金已支付" forState:UIControlStateNormal];
            submitBt.enabled = NO;
        }
        submitBt.titleLabel.font =ButtonFont;
        [submitBt setBackgroundImage:[UIImage imageNamed:@"btBk"] forState:UIControlStateNormal];
        [submitBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [stateInfoView addSubview:submitBt];
        stateInfoView.contentSize = CGSizeMake(CGRectGetWidth(stateInfoView.frame), CGRectGetMaxY(submitBt.frame));
        
        notPositionBtFrame = CGRectMake(CGRectGetMaxX(submitBt.frame)+VerPxFit(15), CGRectGetMaxY(positionTitleLb.frame)+verPadding*3, HorPxFit(180), VerPxFit(32));
    }else{
        notPositionBtFrame = CGRectMake(0, CGRectGetMaxY(positionTitleLb.frame)+verPadding*3, HorPxFit(180), VerPxFit(32));
    }
    
    UIButton * notRegisterBt = [UIButton buttonWithType:UIButtonTypeCustom];
    notRegisterBt.frame = notPositionBtFrame;
    [notRegisterBt addTarget:self action:@selector(notPositionAction) forControlEvents:UIControlEventTouchUpInside];
    notRegisterBt.titleLabel.font = ButtonFont;

    [notRegisterBt setTitle:@"不予转正" forState:UIControlStateNormal];
    [notRegisterBt setBackgroundImage:[UIImage imageNamed:@"btBk"] forState:UIControlStateNormal];
    [stateInfoView addSubview:notRegisterBt];
    stateInfoView.contentSize = CGSizeMake(CGRectGetWidth(stateInfoView.frame), CGRectGetMaxY(notRegisterBt.frame)+20);
    
    if (_model.applyCommission) {
        
        UILabel * alertLb = [[UILabel alloc] init];
        alertLb.text = @"* 候选人转正后，未对佣金做任何处理的订单将于5-7个工作日内自动完成支付";
        alertLb.font = [UIFont systemFontOfSize:13];
        alertLb.numberOfLines = 0;
        alertLb.textColor = [UIColor redColor];
        [stateInfoView addSubview:alertLb];
        [alertLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(stateInfoView);
            make.top.mas_equalTo(notRegisterBt.mas_bottom);
            make.right.equalTo(stateInfoView);
            make.height.mas_equalTo(40);
        }];
        stateInfoView.contentSize = CGSizeMake(CGRectGetWidth(stateInfoView.frame), CGRectGetMaxY(alertLb.frame)+20);
    }
}

- (void)viewForNotPosition{
    CGFloat verPadding = VerPxFit(7);
    
    CGFloat lbH = VerPxFit(25);
    
    UILabel * headerLb = [self lb];
    headerLb.frame = CGRectMake(0, verPadding, 200, lbH);
    headerLb.text = @"已入职，未转正";
    headerLb.textColor = StatueTextColor;
    headerLb.font =TitleFont;
    [stateInfoView addSubview:headerLb];
    
    CGRect rateBtFrame;
    if (!_model.hrEvaluateFlag) {
        rateBtFrame = CGRectMake(0, CGRectGetMaxY(headerLb.frame)+verPadding*3, HorPxFit(180), VerPxFit(32));

        rateBt = [UIButton buttonWithType:UIButtonTypeCustom];
        rateBt.frame = rateBtFrame;
        [rateBt addTarget:self action:@selector(showRateView) forControlEvents:UIControlEventTouchUpInside];
        [rateBt setTitle:@"评价" forState:UIControlStateNormal];
        rateBt.titleLabel.font = ButtonFont;
        [rateBt setBackgroundImage:[UIImage imageNamed:@"btBk"] forState:UIControlStateNormal];
        [stateInfoView addSubview:rateBt];
        
        stateInfoView.contentSize = CGSizeMake(CGRectGetWidth(stateInfoView.frame), CGRectGetMaxY(rateBt.frame));
    }
}

- (void)viewForPosition{
    CGFloat horPadding = HorPxFit(20);
    CGFloat verPadding = VerPxFit(7);
    
    CGFloat lbH = VerPxFit(25);

    UILabel * resultTitleLb = [self lb];
    resultTitleLb.frame = CGRectMake(0, verPadding, 200, lbH);
    resultTitleLb.text = @"转正时间:";
    resultTitleLb.font = TitleFont;
    [stateInfoView addSubview:resultTitleLb];
    
    UILabel  * resultlb = [self lb];
    resultlb.frame = CGRectMake(horPadding, CGRectGetMaxY(resultTitleLb.frame)+verPadding, 300, lbH);
    resultlb.text = [BaseHelper stringWithTimeIntevl:_model.positiveTime format:kDateFormatTypeYYYYMMDD];
    resultlb.font = TextFont;
    [stateInfoView addSubview:resultlb];
    
    if (!_model.hrEvaluateFlag) {
        rateBt = [UIButton buttonWithType:UIButtonTypeCustom];
        rateBt.frame = CGRectMake(CGRectGetMaxX(paySubmitBt.frame)+10, CGRectGetMaxY(resultlb.frame)+verPadding*3, HorPxFit(180), VerPxFit(32));
        [rateBt addTarget:self action:@selector(showRateView) forControlEvents:UIControlEventTouchUpInside];
        [rateBt setTitle:@"评价" forState:UIControlStateNormal];
        rateBt.titleLabel.font = ButtonFont;
        [rateBt setBackgroundImage:[UIImage imageNamed:@"btBk"] forState:UIControlStateNormal];
        [stateInfoView addSubview:rateBt];
        rateBt.titleLabel.font = ButtonFont;
        stateInfoView.contentSize = CGSizeMake(CGRectGetWidth(stateInfoView.frame), CGRectGetMaxY(rateBt.frame));
        
        if (_model.itemPayFee == 0) {
            paySubmitBt.hidden  = YES;
        }
    }
}

- (void)resumeFiltrateAction:(UIButton *)bt{
    if (bt == resumeConformityBt) {
        if (resumeNotConformityBt.selected) {
            resumeNotConformityBt.selected = NO;
            [resumeNotConformityBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            resumeNotConformityBt.layer.borderColor = [UIColor lightGrayColor].CGColor;

        }
    }else{
        if (resumeConformityBt.selected) {
            resumeConformityBt.selected = NO;
            [resumeConformityBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            resumeConformityBt.layer.borderColor = [UIColor lightGrayColor].CGColor;

        }
    }
    bt.selected = YES;
    [bt setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    bt.layer.borderColor = [UIColor redColor].CGColor;
}

- (void)interviewResultFiltrate:(UIButton *)bt{
    if (bt == interViewSuccessBt) {
        if (interViewFailBt.selected) {
            interViewFailBt.selected = NO;
            [interViewFailBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            interViewFailBt.layer.borderColor = [UIColor lightGrayColor].CGColor;
        }
    }else{
        if (interViewSuccessBt.selected) {
            interViewSuccessBt.selected = NO;
            [interViewSuccessBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            interViewSuccessBt.layer.borderColor = [UIColor lightGrayColor].CGColor;
        }
    }
    bt.selected = YES;
    [bt setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    bt.layer.borderColor = [UIColor redColor].CGColor;
}

- (void)resumeDealSubmit{
    if (!resumeConformityBt.selected  && !resumeNotConformityBt.selected) {
        [BaseHelper showProgressHud:@"请选择简历筛选结果" showLoading:NO canHide:YES];
        return;
    }
    if(!resumeNotConformityBt && (!resumeReasonTv.text || resumeReasonTv.text.length ==0)){
        [BaseHelper showProgressHud:@"请填写简历不合格原因" showLoading:NO canHide:YES];
        return;
    }
    
    NSString * result;
    if (resumeConformityBt.selected) {
        result = @"ACCEPT";
    }else{
        result = @"REJECT";
    }
    [self.delegate resumeDeal:result desc:resumeReasonTv.text];
}

- (void)interviewResultSubmit{
    NSString * result;
    if (interViewFailBt.selected) {
        result = @"FAIL";
    }
    if (interViewSuccessBt.selected) {
        result = @"PASS";
    }
    if (!result) {
        [BaseHelper showProgressHud:@"请选择面试结果" showLoading:NO canHide:YES];
        return;
    }
    if (interViewFailBt.selected && (!interviewReasonTv.text || interviewReasonTv.text.length == 0)) {
        [BaseHelper showProgressHud:@"请添加面试结果描述" showLoading:NO canHide:YES];
        return;
    }
    [self.delegate interviewResult:result desc:interviewReasonTv.text];
}

- (void)addOffentryDate{
    AppDelegate * appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    UIWindow * window = appdelegate.window;
    [window addSubview:self.datePickerContentV];
    self.datePicker.date = [NSDate date];
}

- (void)offerInfoSubmit{
    
    NSArray * alertTitle = @[@"请填写合同年限",@"请填写部门",@"请填写直接上级",@"请填写职位",@"请填写试用期",@"请填写试用期工资",@"请填写工资",@"请填写工作地点",@"请填写选择时间",@"请填写说明"];
    BOOL isAllHad = YES;
    for (int i=0;i<offKeys.count;i++) {
        NSString * key = offKeys[i];
        if ([key isEqualToString:@"description"]) {
            continue;
        }
        id obj = self.offerInfodic[key];
        if (!obj) {
            NSString * alert = alertTitle[i];
            [BaseHelper showProgressHud:alert showLoading:NO canHide:YES];
            isAllHad = NO;
            break;
        }
    }
    if (!isAllHad) {
        return;
    }
    NSString * hetong = _offerInfodic[@"contractPeriod"];
    if ([hetong intValue] == 0) {
        [BaseHelper showProgressHud:@"合同期限不可为0年" showLoading:NO canHide:YES];
        return;
    }
    NSString * shiyongqi = _offerInfodic[@"probationPeriod"];
    if ([shiyongqi intValue] == 0) {
        [BaseHelper showProgressHud:@"试用期不可为0月" showLoading:NO canHide:YES];
        return;
    }
    
    [self.delegate offerInfoSubmit:_offerInfodic];
}

- (void)offerSubmitSuccess:(NSString *)url{
    [stateInfoView removeAllSubviews];
    stateInfoView.contentSize = stateInfoView.frame.size;
    
    QRCodeCreateManage * mange = [[QRCodeCreateManage alloc] init];
    UIImage * img = [mange createWithString:url];
    UIImageView * imgV = [[UIImageView alloc] initWithImage:img];
    imgV.frame = CGRectMake(10, 10, 100, 100);
    [stateInfoView addSubview:imgV];
}

- (UILabel *)lb{
    UILabel  *lb = [[UILabel alloc] init];
    lb.font = TextFont;
    lb.textColor = [UIColor whiteColor];
    return lb;
}

- (void)payForReward{
    [self.delegate payForReward];
}

#pragma mark -- datePicker
- (UIView *)datePickerContentV{
    if (!_datePickerContentV) {
        _datePickerContentV= [[UIView alloc] initWithFrame:CGRectMake(0, ScreenHeight-265, ScreenWidth, 261)];
        _datePickerContentV.backgroundColor = COLOR(240, 240, 240, 1);
        self.datePicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 261-216, ScreenWidth, 216)];
        _datePicker.backgroundColor = COLOR(230, 230, 230, 1);
        _datePicker.datePickerMode = UIDatePickerModeDateAndTime;
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDate *currentDate = [NSDate date];
        NSDateComponents *comps = [[NSDateComponents alloc] init];
        [comps setHour:24];
        NSDate *minDate = [calendar dateByAddingComponents:comps toDate:currentDate options:0];    _datePicker.minimumDate = minDate;
        NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];//设置为中文 默认为英文
        _datePicker.locale = locale;
        [_datePickerContentV addSubview:_datePicker];
        
        UIButton * sureBt = [UIButton buttonWithType:UIButtonTypeCustom];
        [sureBt setTitle:@"确定" forState:UIControlStateNormal];
        [sureBt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        sureBt.titleLabel.font = [UIFont systemFontOfSize:14];
        [sureBt addTarget:self action:@selector(datePickerSure) forControlEvents:UIControlEventTouchUpInside];
        //        sureBt.backgroundColor = [UIColor redColor];
        sureBt.frame = CGRectMake(ScreenWidth-70, 5, 60, 30);
        [_datePickerContentV addSubview:sureBt];
        
        UIButton * cancelBt = [UIButton buttonWithType:UIButtonTypeCustom];
        [cancelBt setTitle:@"取消" forState:UIControlStateNormal];
        [cancelBt setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        //        cancelBt.backgroundColor = [UIColor redColor];
        cancelBt.titleLabel.font = [UIFont systemFontOfSize:14];
        [cancelBt addTarget:self action:@selector(datePickerCancel) forControlEvents:UIControlEventTouchUpInside];
        cancelBt.frame = CGRectMake(5, 5, 60, 30);
        [_datePickerContentV addSubview:cancelBt];
    }
    return _datePickerContentV;
}

- (void)datePickerSure{
    NSDate * date = [_datePicker date];
    [_offerInfodic setObject:@([date timeIntervalSince1970]*1000) forKey:@"reportDutyTime"];
    [entryDateBt setTitle:[BaseHelper stringFromDate:date format:kDateFormatTypeYYYYMMDDHHMM] forState:UIControlStateNormal];
    [self datePickerCancel];
}

- (void)datePickerCancel{
    [self.datePickerContentV removeFromSuperview];
}

#pragma mark -- UITextViewDelegate
- (void)textViewDidEndEditing:(UITextView *)textView{
    [textView resignFirstResponder];
    if (textView.text && textView.text.length != 0) {
        [_offerInfodic setObject:textView.text forKey:@"description"];
    }
}

- (void)showRateView{
    self.rateView = [[RateView alloc] initWithFrame:CGRectMake(0, 0, HorPxFit(500), VerPxFit(300))];;
    self.rateView.delegate  = self;
    self.rateView.model = _model;
    [self.rateView showWithSupView:nil];
}

#pragma mark -- UITextFieldDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField{
    NSInteger index = textField.tag - OfftfbaseTag;
    NSString * key = offKeys[index];
    if (!textField.text || textField.text.length ==0) {
        [self.offerInfodic removeObjectForKey:key];
    }else{
        [self.offerInfodic setObject:textField.text forKey:key];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    if(textField.tag > 199 && textField.tag != 207){
        UITextField * tf = [stateInfoView viewWithTag:textField.tag +1];
        [tf becomeFirstResponder];
    }else{
        [textField resignFirstResponder];
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    NSString * validStr = @"0123456789";
    if (textField.tag ==  OfftfbaseTag || textField.tag == OfftfbaseTag+4 || textField.tag == OfftfbaseTag+5 || textField.tag == OfftfbaseTag+6) {
        if ([validStr rangeOfString:string].location ==  NSNotFound && ![string isEqualToString:@""]) {
            return NO;
        }
    }
    UIView * accessoryView = textField.inputAccessoryView;
    if (accessoryView  && [accessoryView isKindOfClass:[UILabel class]]) {
        
        NSString * text = textField.text;
        if ([string isEqualToString:@""]) {
            if (text.length != 0) {
                text = [text substringToIndex:text.length-1];
            }
        }else{
            text = [NSString stringWithFormat:@"%@%@",text,string];
        }
        
        NSArray * placeholders = [textField.placeholder componentsSeparatedByString:@","];
        NSString * pFir = placeholders.count > 0?placeholders[0]:@"";
        NSString * pSec = placeholders.count > 1?placeholders[1]:@"";
        
        UILabel * lb = (UILabel *)accessoryView;
        
        lb.text = [NSString stringWithFormat:@"正在填写 %@:  %@ %@",pFir,text,[text isEqualToString:@""]?@"":pSec];
        
        if (range.location  < textField.text.length && textField.text.length != 0) {
            NSString * string_ = [text substringToIndex:range.location];
            lb.text = [NSString stringWithFormat:@"正在填写 %@: %@%@ %@",pFir,string_,string,[text isEqualToString:@""]?@"":pSec];
            return YES;
        }
    }
    return YES;
}

#pragma mark -- RateViewDelegate
- (void)rateFinish{
    rateBt.hidden = YES;
}

- (void)notRegisterAction{
    [self.delegate notRegisterAction];
}

- (void)notPositionAction{
    CGFloat percentageServiceFee =  [[HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel.company.percentageServiceFee floatValue];
    NSMutableString * string = [NSMutableString stringWithFormat:@"1.不予转正将收取%.0f%%的服务费.其他费用将退回.\n",percentageServiceFee*100];
    [string appendFormat:@"2.如需继续招聘请重发招聘信息."];
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil message:string delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    alert.tag = NotPostionTag;
    [alert show];
}

- (void)positionAction{
    [self.delegate positionAction];
}

#pragma mark -- UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex == 1) {
        [self.delegate notPositionAction];
    }
}

@end

//
//  PayVerifyView.h
//  RecruitCompanyPro
//
//  Created by Michael on 2018/2/9.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasePopView.h"

@protocol PayVerifyViewDelegte

 // 获取验证码
- (void)getVerifyCodeAction;
 // 支付成功
- (void)PayVerifyWithVerifyCode:(NSString *)verifyCode;
@end

@interface PayVerifyView : BasePopView

@property (nonatomic , weak) id<PayVerifyViewDelegte>delegate;

// 验证码发送成功
- (void)verifyCodeSendSuccess;

@end

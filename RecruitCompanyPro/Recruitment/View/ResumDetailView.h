//
//  ResumDetailView.h
//  HSBCTempPro
//
//  Created by Michael on 2017/11/16.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TalentModel.h"
#import "BasePopView.h"
#import "JobModel.h"

@interface ResumDetailView : BasePopView

@property (nonatomic , strong) TalentModel * talentModel;
@property (nonatomic , strong) JobModel * jobmodel;

- (void)initInfoWithTalent:(TalentModel *)model;
- (void)showWithSuperView:(UIView *)view;
@end

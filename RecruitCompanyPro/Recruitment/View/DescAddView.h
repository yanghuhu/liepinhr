//
//  DescAddView.h
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/14.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    InfoAddTypeDesc,
    InfoAddTypeResp,
}InfoAddType;

@protocol DescAddViewDelegate
@optional
- (void)addSureAction:(NSString *)text withType:(InfoAddType)type;
@end

@interface DescAddView : UIView
@property (nonatomic , assign) InfoAddType infoAddType;
@property (nonatomic , assign) id<DescAddViewDelegate>delegate;
- (void)showSupView:(UIView *)view withType:(InfoAddType)type;

@end

//
//  PositionStateView.h
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/14.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JobOrderDetailModel.h"
#import "TalentModel.h"

@protocol PositionStateViewDelegate
- (void)resumeDeal:(NSString *)result desc:(NSString *)desc;
- (void)offerInfoSubmit:(NSDictionary *)info;
- (void)interviewResult:(NSString *)result desc:(NSString *)desc;
- (void)payForReward;
- (void)resumCheck:(TalentModel *)model;
- (void)notRegisterAction;  //  未入职 关闭订单
- (void)notPositionAction;   //  未转正  关闭订单
- (void)positionAction;   //  转正  关闭订单
@end

@interface PositionStateView : UIView

@property (nonatomic , strong) JobOrderDetailModel * model;
@property (nonatomic , weak)  id<PositionStateViewDelegate>delegate;

// 职位offer提交成功返回二维码url
- (void)offerSubmitSuccess:(NSString *)url;
// 面试结果提交成功
- (void)interviewSubmitFinish;
@end

//
//  PositionCollectionCell.h
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/12.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListPostionModel.h"

@interface PositionCollectionCell : UICollectionViewCell

@property (nonatomic , assign) BOOL isCreateSubs;   // 是否已创建subviews
@property (nonatomic , strong)  ListPostionModel * model;   //  职位model

- (void)createSubViews;

@end

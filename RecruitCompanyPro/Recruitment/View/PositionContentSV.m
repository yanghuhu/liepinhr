//
//  PositionContentSV.m
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/12.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "PositionContentSV.h"
#import "TTTextField.h"
#import "LMJDropdownMenu.h"
#import "PostionDesAndResView.h"
#import "UIView+YYAdd.h"
#import "JobRequirementModel.h"
#import "JobResponsibilityModel.h"

@interface PositionContentSV()<TTTextFieldDelegate,LMJDropdownMenuDelegate,PostionDesAndResViewDelegate,UITextViewDelegate>{
 
    TTTextField * titleTf;       // 标题tf
    TTTextField * locationTf;   //  地点tf
    TTTextField * wageTf;        // 工资tf
    TTTextField * numberTf;         // 数量tf
    TTTextField * rewardTf;         // 赏金tf
    
    LMJDropdownMenu * hangyeDropdownMenu;   // 行业
    LMJDropdownMenu * levelDropdownMenu;       // 等级
    LMJDropdownMenu * positionDropdownMenu;     // 职位
    LMJDropdownMenu * educationDropdownMenu;    // 学历
    LMJDropdownMenu * jingyanDropdownMenu;     // 经验
    LMJDropdownMenu * xingzhiDropdownMenu;      //  性质
    
    UIControl * bkControl;          // 蒙板控制器
    UIButton * positionMapBt;       // 职位匹配按钮
    PostionDesAndResView * postionDesAndResView;   // 职位描述，职位职责容器view
    
    UITextField *noteTextField;   // 备注 tf
    
    UIImageView * mianyiImgV;
    UIButton * mianyiBt;
}
@property (nonatomic , strong) JobModel * jobModel;
@property (nonatomic , strong)  LMJDropdownMenu * curShowMenu;
@property (nonatomic , strong) TTTextField * curEditTf;

@end

@implementation PositionContentSV

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{

    self.resultInfoDic = [NSMutableDictionary dictionary];
    CGFloat contentSVW = ScreenWidth-HorPxFit(10)-HorPxFit(50)-90-HorPxFit(140)-HorPxFit(75)*2;
    CGFloat horPadding = HorPxFit(30);
    CGFloat verPadding = HorPxFit(10);
    CGFloat itemW = (contentSVW - horPadding*2)/3;
    CGFloat itemH = VerPxFit(45);
    
    bkControl = [[UIControl alloc] initWithFrame:CGRectMake(0, 0, contentSVW, ScreenHeight - VerPxFit(77)-VerPxFit(50) -  VerPxFit(30) - 1 - 60 +8-20)];
    [bkControl addTarget:self action: @selector(dismissPopmenu) forControlEvents:UIControlEventTouchUpInside];
    bkControl.backgroundColor = [UIColor clearColor];
    [self addSubview:bkControl];
    
    CGFloat curY = 0;
    titleTf = [self tf:@"标题" placeHolder:@"请填写标题" frame:CGRectMake(0, curY, itemW*2, itemH*1)];
    titleTf.tf.font = [UIFont systemFontOfSize:14];
    titleTf.titleFont =[UIFont systemFontOfSize:12];
    curY += (itemH+verPadding);

    //     @[@"标题",@"行业",@"职位",@"级别",@"学历",@"经验",@"性质",@"地点",@"薪资",@"人数",@"金额"];
    
    hangyeDropdownMenu = [self popList:@"行业" placeHolder:@"请选择行业" frame:CGRectMake(0,curY,itemW,itemH) withTitles:[HSBCGlobalInstance sharedHSBCGlobalInstance].hangyeArray];
    positionDropdownMenu = [self popList:@"职位" placeHolder:@"请选择职位" frame:CGRectMake(itemW+horPadding,curY,itemW,itemH) withTitles:[HSBCGlobalInstance sharedHSBCGlobalInstance].positionArray];
    levelDropdownMenu = [self popList:@"级别" placeHolder:@"请选择级别" frame:CGRectMake(itemW*2+horPadding*2,curY,itemW,itemH) withTitles:[HSBCGlobalInstance sharedHSBCGlobalInstance].levelArray];

    curY += (itemH+verPadding);
    
    educationDropdownMenu = [self popList:@"学历" placeHolder:@"请选择学历" frame:CGRectMake(0,curY,itemW,itemH) withTitles:[HSBCGlobalInstance sharedHSBCGlobalInstance].educationArray];
    jingyanDropdownMenu = [self popList:@"经验要求" placeHolder:@"请选择经验要求" frame:CGRectMake(itemW+horPadding,curY,itemW,itemH) withTitles:[HSBCGlobalInstance sharedHSBCGlobalInstance].jingyanArray];
    xingzhiDropdownMenu =  [self popList:@"工作性质" placeHolder:@"请选择工作性质" frame:CGRectMake(itemW*2+horPadding*2,curY,itemW,itemH) withTitles:[HSBCGlobalInstance sharedHSBCGlobalInstance].xingzhiArray];

    curY += (itemH+verPadding);
    
    numberTf = [self tf:@"人数" placeHolder:@"请填写人数" frame:CGRectMake(0,curY, itemW, itemH)];
    numberTf.keyboardType =  UIKeyboardTypeNumbersAndPunctuation;
    locationTf = [self tf:@"工作地点" placeHolder:@"请填写工作地点" frame:CGRectMake(itemW+horPadding,curY, itemW, itemH)];
    
    UILabel * unitLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 35, itemH)];
    unitLb.textColor = [UIColor whiteColor];
    unitLb.text = @"元/月";
    unitLb.font = [UIFont systemFontOfSize:12];
    wageTf = [self tf:@"薪资" placeHolder:@"薪资" frame:CGRectMake(itemW*2+horPadding*2,curY, itemW/3*1.8, itemH)];
    wageTf.keyboardType =  UIKeyboardTypeNumbersAndPunctuation;
    wageTf.tf.rightView = unitLb;
    wageTf.tf.rightViewMode = UITextFieldViewModeAlways;
    curY += (itemH+verPadding);
    
    UILabel * mianyilb = [[UILabel alloc] init];
    mianyilb.text = @"面议";
    mianyilb.textColor = [UIColor whiteColor];
    mianyilb.font = [UIFont systemFontOfSize:14];
    [self addSubview:mianyilb];
    [mianyilb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(xingzhiDropdownMenu);
        make.centerY.mas_equalTo(wageTf.mas_centerY).mas_offset(VerPxFit(10));;
        make.size.mas_equalTo(CGSizeMake(35, 25));
    }];
    
    mianyiImgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"circle"]];
    [self addSubview:mianyiImgV];
    [mianyiImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(mianyilb.mas_left).mas_offset(-HorPxFit(8));
        make.centerY.mas_equalTo(mianyilb.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(17), HorPxFit(17)));
    }];
    
    mianyiBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [mianyiBt addTarget:self action:@selector(mianyiAction:) forControlEvents:UIControlEventTouchUpInside];
//    mianyiBt.backgroundColor = [UIColor redColor];
    [self addSubview:mianyiBt];
    [mianyiBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(mianyiImgV.mas_left);
        make.right.mas_equalTo(mianyilb.mas_right);
        make.centerY.mas_equalTo(mianyiImgV.mas_centerY);
        make.height.mas_equalTo(mianyilb.mas_height);
    }];
    
    UILabel * unitLb_ = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 40, itemH)];
    unitLb_.textColor = [UIColor whiteColor];
    unitLb_.text = @"元/人";
    unitLb_.font = [UIFont systemFontOfSize:13];
    rewardTf = [self tf:@"赏金" placeHolder:@"请填写赏金" frame:CGRectMake(0,curY, itemW, itemH)];
    rewardTf.keyboardType =  UIKeyboardTypeNumbersAndPunctuation;
    rewardTf.tf.rightView = unitLb_;
    rewardTf.tf.rightViewMode = UITextFieldViewModeAlways;
    
    curY += (itemH+verPadding);
    
    positionMapBt = [UIButton buttonWithType:UIButtonTypeCustom];
    positionMapBt.frame = CGRectMake(itemW,curY+verPadding, HorPxFit(250), VerPxFit(35));
    [positionMapBt setTitle:@"职位匹配" forState:UIControlStateNormal];
    positionMapBt.titleLabel.font = [UIFont systemFontOfSize:14];
    [positionMapBt addTarget:self action:@selector(positionMapAction) forControlEvents:UIControlEventTouchUpInside];
    [positionMapBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [positionMapBt setBackgroundImage:[UIImage imageNamed:@"btBk"] forState:UIControlStateNormal];
    [self addSubview:positionMapBt];
}

- (void)updateInfoAppear:(JobModel *)model{
    self.jobModel = model;
    titleTf.text = model.title;
    locationTf.text = model.workArea;
    numberTf.text = [NSString stringWithFormat:@"%ld",model.recruitingNumber];
    rewardTf.text = [NSString stringWithFormat:@"%.2f",model.price/100];
    if ([model.salaryRange floatValue] == 0) {
        [self mianyiAction:mianyiBt];
    }else{
        wageTf.text = model.salaryRange;
    }
    
    [hangyeDropdownMenu.mainBtn setTitle:model.profession forState:UIControlStateNormal];
    [levelDropdownMenu.mainBtn setTitle:model.level forState:UIControlStateNormal];
    [positionDropdownMenu.mainBtn setTitle:model.position forState:UIControlStateNormal];
    [educationDropdownMenu.mainBtn setTitle:model.education forState:UIControlStateNormal];
    [jingyanDropdownMenu.mainBtn setTitle:model.workExperience forState:UIControlStateNormal];
    [xingzhiDropdownMenu.mainBtn setTitle:model.jobNature forState:UIControlStateNormal];
    
    [self initDesc:model.jobRequireArray respArray:model.jobResponsibitiyArray];
    
    noteTextField.text = model.additionalInfo;

    self.resultInfoDic =[NSMutableDictionary dictionaryWithDictionary:@{@"title":model.title,@"profession":model.profession,@"position":model.position,@"level":model.level,@"education":model.education,@"workExperience":model.workExperience,@"jobNature":model.jobNature,@"workArea":model.workArea,@"salaryRange":model.salaryRange,@"recruitingNumber":@(model.recruitingNumber),@"price":@(model.price),@"additionalInfo":model.additionalInfo?model.additionalInfo:@""}];
}

- (TTTextField *)tf:(NSString *)title placeHolder:(NSString *)placeHolder frame:(CGRect)frame{
    TTTextField * tf  = [[TTTextField alloc] initWithFrame:frame];
    tf.title = title;
    tf.text = placeHolder;
    tf.titleFont = [UIFont systemFontOfSize:12];
    tf.tf.font = [UIFont systemFontOfSize:14];
    tf.placeholder = placeHolder;
    tf.delegate = self;
    [self addSubview:tf];
    return  tf;
}

- (LMJDropdownMenu *)popList:(NSString *)title placeHolder:(NSString *)placeHolder frame:(CGRect)frame withTitles:(NSArray *)titles{
    
    UILabel * titleLb = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(frame), CGRectGetMinY(frame), CGRectGetWidth(frame), 17)];
    titleLb.font = [UIFont systemFontOfSize:12];
    titleLb.textColor = [UIColor whiteColor];
    titleLb.text = title;
    [self addSubview:titleLb];
    
    UIImageView * bottomline = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"AccountBottom"]];
    bottomline.frame = CGRectMake(CGRectGetMinX(frame), CGRectGetMaxY(frame)-1, CGRectGetWidth(frame), 1);
    [self addSubview:bottomline];
    
    UIImage * img = [UIImage imageNamed:@"listFlag"];
    UIImageView * imgV = [[UIImageView alloc] initWithImage:img];
    CGFloat imgH = CGRectGetHeight(frame)-CGRectGetHeight(titleLb.frame)-CGRectGetHeight(bottomline.frame)-12;
    CGFloat imgW = 30*imgH/35;
    imgV.frame = CGRectMake(CGRectGetMaxX(frame)-imgW, CGRectGetMaxY(titleLb.frame)+9, imgW, imgH);
    [self addSubview:imgV];
   
    LMJDropdownMenu * dropMenu = [[LMJDropdownMenu alloc] initWithFrame:CGRectMake(CGRectGetMinX(frame), CGRectGetMaxY(titleLb.frame), CGRectGetWidth(frame), CGRectGetHeight(frame)-CGRectGetHeight(titleLb.frame)-CGRectGetHeight(bottomline.frame))];
    [dropMenu setMenuTitles:titles rowHeight:CGRectGetHeight(dropMenu.frame)+8];
    [dropMenu.mainBtn setTitle:placeHolder forState:UIControlStateNormal];
    [dropMenu.mainBtn setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    dropMenu.mainBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    dropMenu.delegate = self;
    dropMenu.backgroundColor = [UIColor clearColor];
    [self addSubview:dropMenu];
    return dropMenu;
}

- (void)mianyiAction:(UIButton *)bt{
    bt.selected = !bt.selected;
    if (bt.selected) {
        mianyiImgV.image = [UIImage imageNamed:@"circle_sel"];
        [_resultInfoDic setObject:@"0" forKey:@"salaryRange"];
        wageTf.text = @"薪资";
    }else{
        mianyiImgV.image = [UIImage imageNamed:@"circle"];
        if (wageTf.isTfHadText) {
            [_resultInfoDic setObject:wageTf.text forKey:@"salaryRange"];
        }else{
            [_resultInfoDic removeObjectForKey:@"salaryRange"];
        }
    }
}

- (void)dismissPopmenu{
    if (_curShowMenu) {
        [_curShowMenu hideDropDown];
        _curShowMenu = nil;
    }
    if (_curEditTf) {
        [_curEditTf resignFirstResponder];
        _curEditTf = nil;
    }
}

- (void)positionMapAction{
    [self.delegate_ positionMapAction];
}

- (NSArray *)positionResponArray{
    return  postionDesAndResView.respArray;
}

- (NSArray *)positionDescArray{
    return postionDesAndResView.descArray;
}

- (void)publishAction{
    
    NSArray * titles = @[@"title",@"profession",@"position",@"level",@"education",@"workExperience",@"jobNature",@"workArea",@"salaryRange",@"recruitingNumber",@"price"];
    NSArray * alertArray = @[@"请填写标题",@"请选择行业",@"请求选择职位",@"请选择级别",@"请选择学历",@"请选择经验要求",@"请选择工作性质",@"请填写工作地点",@"请填写薪资",@"请填写人数",@"请填写赏金"];
    BOOL isAllHad = YES;
    for (int i=0;i<titles.count;i++) {
        NSString * title = titles[i];
        NSString * text = _resultInfoDic[title];
        if (!text) {
            NSString * alert = alertArray[i];
            [BaseHelper showProgressHud:alert showLoading:NO canHide:YES];
            isAllHad = NO;
            break;
        }
    }
    if (!isAllHad) {
        return;
    }
    
    NSArray * descArray = postionDesAndResView.descArray;
    if (!descArray || descArray.count == 0) {
        [BaseHelper showProgressHud:@"请添加岗位描述" showLoading:NO canHide:YES];
        return;
    }
    NSArray * respArray = postionDesAndResView.respArray;
    if (!respArray || respArray.count == 0) {
        [BaseHelper showProgressHud:@"请添加岗位职责" showLoading:NO canHide:YES];
        return;
    }
    [self.delegate_ positonSubmit:_resultInfoDic descArray:descArray respArray:respArray];
}

- (void)initDesc:(NSArray *)descArray  respArray:(NSArray *)respArray{
    if (!postionDesAndResView) {
        CGFloat contentSVW = ScreenWidth-HorPxFit(10)-HorPxFit(50)-90-HorPxFit(140)-HorPxFit(75)*2;
        
        postionDesAndResView = [[PostionDesAndResView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(positionMapBt.frame)+VerPxFit(20), contentSVW, 1000)];
        postionDesAndResView.delegate = self;
        [self addSubview:postionDesAndResView];
        
        UIView * noteContentView = [[UIView alloc] init];
        [self addSubview:noteContentView];
        [noteContentView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(postionDesAndResView);
            make.top.mas_equalTo(postionDesAndResView.mas_bottom).mas_offset(VerPxFit(30));
            make.right.equalTo(postionDesAndResView);
            make.height.mas_equalTo(35+VerPxFit(30));
        }];
        
        UILabel * noteTitleLb = [[UILabel alloc] init];
        noteTitleLb.text = @"备注";
        noteTitleLb.font = [UIFont systemFontOfSize:16];
        noteTitleLb.textColor = [UIColor whiteColor];
        [noteContentView addSubview:noteTitleLb];
        [noteTitleLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(noteContentView);
            make.top.equalTo(noteContentView);
            make.size.mas_equalTo(CGSizeMake(80, 30));
        }];
        
        noteTextField = [[UITextField alloc] init];
        noteTextField.textColor = [UIColor whiteColor];
        noteTextField.font = [UIFont systemFontOfSize:15];
        noteTextField.textColor = [UIColor whiteColor];
        noteTextField.delegate = self;
        [noteContentView addSubview:noteTextField];
        [noteTextField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(noteTitleLb.mas_bottom);
            make.left.and.right.equalTo(postionDesAndResView);
            make.bottom.mas_equalTo(noteContentView.mas_bottom);
        }];
        
        UIImageView * bottomLine_ = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"AccountBottom"]];
        [noteContentView addSubview:bottomLine_];
        [bottomLine_ mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.and.right.equalTo(noteContentView);
            make.bottom.mas_equalTo(noteContentView.mas_bottom);
            make.height.mas_equalTo(1);
        }];
        
        UIButton * publicBt = [UIButton buttonWithType:UIButtonTypeCustom];
        [publicBt addTarget:self action:@selector(publishAction) forControlEvents:UIControlEventTouchUpInside];
        [publicBt setBackgroundImage:[UIImage imageNamed:@"btBk"] forState:UIControlStateNormal];
        if (_jobModel && [_jobModel.state isEqualToString:@"AUDIT_FAILED"]) {
            [publicBt setTitle:@"发布职务" forState:UIControlStateNormal];
        }else{
            [publicBt setTitle:@"提交职务" forState:UIControlStateNormal];
        }
        
        publicBt.titleLabel.font = [UIFont systemFontOfSize:15];
        [publicBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self addSubview:publicBt];
        [publicBt mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.mas_equalTo(noteContentView.mas_bottom).mas_offset(VerPxFit(30));
            make.centerX.mas_equalTo(self.mas_centerX);
            make.size.mas_equalTo(CGSizeMake(HorPxFit(250), VerPxFit(35)));
        }];
    }
    NSMutableArray * descTextArrayTemp = [NSMutableArray arrayWithCapacity:descArray.count];
    NSMutableArray * respArrayTemp = [NSMutableArray arrayWithCapacity:respArray.count];
    for(JobRequirementModel * model in descArray){
        [descTextArrayTemp addObject:model.description_];
    }
    
    for (JobResponsibilityModel * mode in respArray) {
        [respArrayTemp addObject:mode.description_];
    }
    
    self.positionDescArray = descTextArrayTemp;
    self.positionResponArray = respArrayTemp;
    
    CGFloat hNeed = [postionDesAndResView initInfoWithDesc:descTextArrayTemp respArray:respArrayTemp];
    postionDesAndResView.height = hNeed;
    self.contentSize = CGSizeMake(CGRectGetWidth(self.frame), CGRectGetMaxY(postionDesAndResView.frame)+VerPxFit(30)*3+VerPxFit(40) + VerPxFit(30)+35);
}

#pragma mark -- TTTextFieldDelegate
- (void)textFieldDidBeginEditing:(TTTextField *)tf{
    if (_curEditTf) {
        [_curEditTf resignFirstResponder];
    }
    if (_curShowMenu) {
        [_curShowMenu hideDropDown];
    }
    self.curEditTf = tf;
}

- (void)ttTextFieldDidEndEditing:(TTTextField *)tf{
    if (tf == titleTf) {
        if (tf.isTfHadText) {
            [_resultInfoDic setObject:tf.text forKey:@"title"];
        }else{
            [_resultInfoDic removeObjectForKey:@"title"];
        }
    }else if (tf == locationTf){
        if (tf.isTfHadText) {
            [_resultInfoDic setObject:tf.text forKey:@"workArea"];
        }else{
            [_resultInfoDic removeObjectForKey:@"workArea"];
        }
    }else if (tf == wageTf){
        if (mianyiBt.selected) {
            [self mianyiAction:mianyiBt];
        }
        if (tf.isTfHadText) {
            [_resultInfoDic setObject:tf.text forKey:@"salaryRange"];
        }else{
            [_resultInfoDic removeObjectForKey:@"salaryRange"];
        }
    }else if (tf == numberTf){
        if (tf.isTfHadText) {
            [_resultInfoDic setObject:tf.text forKey:@"recruitingNumber"];
        }else{
            [_resultInfoDic removeObjectForKey:@"recruitingNumber"];
        }
    }else if (tf == rewardTf){
        if (tf.isTfHadText) {
            [_resultInfoDic setObject:@([tf.text floatValue]*100) forKey:@"price"];
        }else{
            [_resultInfoDic removeObjectForKey:@"price"];
        }
    }
}

- (BOOL)textField:(TTTextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if (textField == wageTf || textField == numberTf) {
        NSString * validString = @"0123456789";
        if ([validString rangeOfString:string].location == NSNotFound ) {
            if ([string isEqualToString:@""]) {
                return YES;
            }else{
                return NO;
            }
        }
    }else if (textField == rewardTf){
        NSString * validString = @"0123456789.";
        if ([validString rangeOfString:string].location == NSNotFound ) {
            if ([string isEqualToString:@""]) {
                return YES;
            }else{
                return NO;
            }
        }
    }
    return YES;
}

#pragma mark -- UITextViewDelegate
- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (textField == noteTextField){
        if (noteTextField.text && noteTextField.text.length != 0) {
            [_resultInfoDic setObject:noteTextField.text forKey:@"additionalInfo"];
        }else{
            [_resultInfoDic removeObjectForKey:@"additionalInfo"];
        }
    }
}

#pragma mark -- LMJDropdownMenuDelegate
- (void)dropdownMenuWillShow:(LMJDropdownMenu *)menu{
    if (_curShowMenu) {
        [_curShowMenu hideDropDown];
    }
    [bkControl removeFromSuperview];
    [self insertSubview:bkControl belowSubview:menu];
    self.curShowMenu = menu;
}

- (void)dropdownMenuWillHidden:(LMJDropdownMenu *)menu{
    self.curShowMenu = nil;
    [bkControl removeFromSuperview];
}

- (void)dropdownMenu:(LMJDropdownMenu *)menu selectedCellNumber:(NSInteger)number{
    if (!menu.textSelected) {
        return;
    }
    if (menu == hangyeDropdownMenu) {
        [_resultInfoDic setObject:menu.textSelected forKey:@"profession"];
    }else if (menu == positionDropdownMenu){
        [_resultInfoDic setObject:menu.textSelected forKey:@"position"];
    }else if (menu == levelDropdownMenu){
        [_resultInfoDic setObject:menu.textSelected forKey:@"level"];
    }else if (menu == educationDropdownMenu){
        [_resultInfoDic setObject:menu.textSelected forKey:@"education"];
    }else if (menu == jingyanDropdownMenu){
        [_resultInfoDic setObject:menu.textSelected forKey:@"workExperience"];
    }else if (menu == xingzhiDropdownMenu){
        [_resultInfoDic setObject:menu.textSelected forKey:@"jobNature"];
    }
}

- (BOOL)getPositionInfo{
    NSMutableDictionary * dic = [NSMutableDictionary dictionary];
    if(titleTf.text){
        [dic setObject:titleTf.text forKey:@"title"];
    }else{
        [BaseHelper showProgressHud:@"请填写标题" showLoading:NO canHide:YES];
    }
    return YES;
}

#pragma mark -- PostionDesAndResViewDelegate
- (void)PostionDesAndResViewHightChange:(CGFloat)hight{
    postionDesAndResView.height = hight;

    self.contentSize = CGSizeMake(CGRectGetWidth(self.frame), CGRectGetMaxY(postionDesAndResView.frame)+VerPxFit(30)*2+VerPxFit(40) + VerPxFit(50)+35);
}

@end

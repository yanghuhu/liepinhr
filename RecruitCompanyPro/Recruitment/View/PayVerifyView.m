//
//  PayVerifyView.m
//  RecruitCompanyPro
//
//  Created by Michael on 2018/2/9.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "PayVerifyView.h"
#import "UIImage+YYAdd.h"

@interface PayVerifyView()<UITextFieldDelegate>{
    UITextField * verifyCodeTf; //  验证码tf
    UIButton * getCodeBt;     //  获取验证码bt
}
@end

@implementation PayVerifyView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self createSubViews];
    }
    return self;
}

- (void)didMoveToSuperview{
    if ([self superview]) {
        [self getVerifyCode];
    }
}

- (void)createSubViews{
    
    UIImageView * bkImageV = [[UIImageView alloc] init];
    bkImageV.image = [UIImage imageNamed:@"popBack"];
    [self addSubview:bkImageV];
    [bkImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.bottom.and.top.equalTo(self);
    }];
    
    UIButton * bt = [UIButton buttonWithType:UIButtonTypeCustom];
    [bt addTarget:self action:@selector(dismissAction) forControlEvents:UIControlEventTouchUpInside];
    [bt setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
    [self addSubview:bt];
    [bt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).mas_offset(VerPxFit(25));
        make.right.equalTo(self).mas_offset(-HorPxFit(30));
        make.size.mas_equalTo(CGSizeMake(HorPxFit(50), HorPxFit(50)));
    }];
    
    UILabel * titleLb = [[UILabel alloc] init];
    titleLb.text = @"短信验证码";
    titleLb.font  = [UIFont systemFontOfSize:16];
    titleLb.textAlignment = NSTextAlignmentCenter;
    titleLb.textColor = [UIColor whiteColor];
    [self addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).mas_offset(HorPxFit(40));
        make.right.equalTo(self).mas_offset(-HorPxFit(40));
        make.top.equalTo(self).mas_offset(VerPxFit(40));
        make.height.mas_equalTo(30);
    }];
    
    getCodeBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [getCodeBt addTarget:self action:@selector(getVerifyCode) forControlEvents:UIControlEventTouchUpInside];
    [getCodeBt setTitle:@"获取验证码" forState:UIControlStateNormal];
    getCodeBt.titleLabel.font = [UIFont systemFontOfSize:14];
    [getCodeBt setBackgroundImage:[UIImage imageNamed:@"codeBt"] forState:UIControlStateNormal];
    [self addSubview:getCodeBt];
    [getCodeBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(self.mas_centerY).mas_offset(-VerPxFit(20));
        make.right.equalTo(self).mas_offset(-HorPxFit(80));
        make.size.mas_equalTo(CGSizeMake(HorPxFit(80), VerPxFit(35)));
    }];
    
    verifyCodeTf = [[UITextField alloc] init];
    verifyCodeTf.textColor = [UIColor whiteColor];
    verifyCodeTf.delegate = self;
    
    UIView * leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 0)];
    verifyCodeTf.leftView = leftView;
    verifyCodeTf.leftViewMode = UITextFieldViewModeAlways;
    verifyCodeTf.font = [UIFont systemFontOfSize:15];
    [self addSubview:verifyCodeTf];
    [verifyCodeTf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(getCodeBt.mas_centerY);
        make.right.mas_equalTo(getCodeBt.mas_left).mas_offset(-HorPxFit(20));
        make.left.equalTo(self).mas_offset(HorPxFit(90));
        make.height.mas_equalTo(VerPxFit(25));
    }];
    
    UIImageView * bottomLine = [[UIImageView alloc] init];
    bottomLine.image = [UIImage imageNamed:@"AccountBottom"];
    [self addSubview:bottomLine];
    [bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(verifyCodeTf);
        make.top.mas_equalTo(verifyCodeTf.mas_bottom);
        make.height.mas_equalTo(VerPxFit(2));
    }];
    
    UIButton * sureBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [sureBt addTarget:self action:@selector(suerAction) forControlEvents:UIControlEventTouchUpInside];
    [sureBt setBackgroundImage:[UIImage imageNamed:@"btBk"] forState:UIControlStateNormal];
    sureBt.titleLabel.font = [UIFont systemFontOfSize:15];
    [sureBt setTitle:@"支付" forState:UIControlStateNormal];
    [self addSubview:sureBt];
    [sureBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).mas_offset(HorPxFit(110));
        make.right.equalTo(self).mas_offset(-HorPxFit(110));
        make.bottom.equalTo(self).mas_offset(-VerPxFit(60));
        make.height.mas_equalTo(VerPxFit(34));
    }];
}

- (void)suerAction{
    NSString * verifyCode = verifyCodeTf.text;
    if (!verifyCode || verifyCode.length == 0 ) {
        [BaseHelper showProgressHud:@"请输入验证码" showLoading:NO canHide:YES];
        return;
    }
    [self.delegate PayVerifyWithVerifyCode:verifyCode];
}

- (void)getVerifyCode{
    [self.delegate getVerifyCodeAction];
}

- (void)verifyCodeSendSuccess{
    [BaseHelper showProgressHud:@"发送成功" showLoading:NO canHide:YES];
    __block int time = 60;
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(time<=0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                getCodeBt.enabled = YES;
                [getCodeBt setTitle:@"获取验证码" forState:(UIControlStateNormal)];
            });
        }else{
            dispatch_async(dispatch_get_main_queue(), ^{
                getCodeBt.enabled = NO;
                [getCodeBt setTitle:[NSString stringWithFormat:@"%ds后重发", time] forState:(UIControlStateDisabled)];
            });
            time--;
        }
    });
    dispatch_resume(_timer);
}

@end

//
//  PayPopView.h
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/13.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PayPopViewDelegate
- (void)payActionWihtType:(NSString *)type withNote:(NSString *)note;
@end

@interface PayPopView : UIView

@property (nonatomic , weak) id<PayPopViewDelegate>delegate;
- (void)showSupView:(UIView *)view withPrice:(CGFloat)price;
@end

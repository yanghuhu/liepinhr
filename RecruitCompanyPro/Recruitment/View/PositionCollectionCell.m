//
//  PositionCollectionCell.m
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/12.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "PositionCollectionCell.h"
#import "UIImage+YYAdd.h"

@interface PositionCollectionCell(){
    
    UILabel  * positionLb;  // 职位lb
    UILabel * timeLb;       // 时间lb
    UILabel * needLb;       // 需招聘人数lb
    UILabel * hadLb;        // 已招聘人数lb
    UILabel * waitLb;       // 待招聘人数lb
    UILabel * dealLb;       // 已处理lb
    UILabel * dealinglb;    // 正在处理lb
    
    UIImageView * bkImgV;       //背景imgV
    UIImageView  * verLineImgV;   //  十字分割线
    UIImageView * leftLine;
    UIImageView * rightLine;
    
    UIImageView * flagImgV;     // 状态标签imgView
    UILabel * flagTextLb;   //  职位状态描述
}

@end

@implementation PositionCollectionCell

- (void)createSubViews{
    _isCreateSubs = YES;
    bkImgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"positionBk"]];
    [self addSubview:bkImgV];
    [bkImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.left.and.bottom.equalTo(self);
    }];
    
    flagImgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"positionFlag"]];
    flagImgV.hidden = YES;
    [self addSubview:flagImgV];
    [flagImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).mas_offset(2);
        make.top.equalTo(self).mas_offset(1);
        make.size.mas_equalTo(CGSizeMake(HorPxFit(20), VerPxFit(50)));
    }];
    
    flagTextLb = [[UILabel alloc] init];
    flagTextLb.numberOfLines = 0;
    flagTextLb.textColor = [UIColor whiteColor];
    flagTextLb.font = [UIFont systemFontOfSize:10];
    [flagImgV addSubview:flagTextLb];
    [flagTextLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(flagImgV);
        make.left.equalTo(flagImgV).mas_offset(HorPxFit(5));
        make.right.equalTo(flagImgV).mas_offset(HorPxFit(-4));
        make.height.mas_equalTo(flagImgV).mas_offset(-VerPxFit(10));
    }];
    
    UIFont * titleFont = [UIFont boldSystemFontOfSize:16];
    UIFont  * textFont = [UIFont systemFontOfSize:13];
    UIFont * detailFont = [UIFont systemFontOfSize:10];
    positionLb = [self lb:titleFont];
    [positionLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.mas_equalTo(VerPxFit(8));
        make.centerX.mas_equalTo(self.mas_centerX);
        make.left.equalTo(self).offset(HorPxFit(10));
        make.right.equalTo(self).offset(-HorPxFit(10));
        make.height.mas_equalTo(VerPxFit(21));
    }];
    
    timeLb = [self lb:detailFont];
    [timeLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(positionLb);
        make.top.mas_equalTo(positionLb.mas_bottom).mas_offset(-3);
        make.height.mas_equalTo(VerPxFit(17));
    }];
    
    verLineImgV = [[UIImageView alloc] init];
    verLineImgV.image = [UIImage imageNamed:@"shiziCaise"];
    [self addSubview:verLineImgV];
    [verLineImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX);
        make.centerY.mas_equalTo(self.mas_centerY).mas_offset(VerPxFit(10));
        make.size.mas_equalTo(CGSizeMake(HorPxFit(114), VerPxFit(20)));
    }];
    
    UILabel * needTitle = [self lb:textFont];
    needTitle.textAlignment = NSTextAlignmentCenter;
    needTitle.text = @"需求";
    [needTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(verLineImgV.mas_centerX);
        make.left.mas_equalTo(verLineImgV.mas_left);
        make.bottom.mas_equalTo(verLineImgV.mas_centerY);
        make.height.mas_equalTo(VerPxFit(25));
    }];
    
    UILabel * hadTitle = [self lb:textFont];
    hadTitle.text = @"已招";
    hadTitle.textAlignment = NSTextAlignmentCenter;
    [hadTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(verLineImgV);
        make.left.mas_equalTo(verLineImgV.mas_centerX);
        make.bottom.mas_equalTo(verLineImgV.mas_centerY);
        make.height.mas_equalTo(VerPxFit(25));
    }];
    
    needLb = [self lb:textFont];
    needLb.textAlignment = NSTextAlignmentCenter;
    [needLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(needTitle);
        make.top.mas_equalTo(verLineImgV.mas_centerY);
        make.height.mas_equalTo(VerPxFit(25));
    }];
    
    hadLb = [self lb:textFont];
    hadLb.textAlignment = NSTextAlignmentCenter;
    [hadLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(hadTitle);
        make.top.mas_equalTo(verLineImgV.mas_centerY);
        make.height.mas_equalTo(VerPxFit(25));
    }];
 
    CGFloat dealW = 60;
    
    dealLb = [self lb:detailFont];
    [dealLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX);
        make.top.mas_equalTo(hadLb.mas_bottom).mas_offset(9);
        make.height.mas_equalTo(VerPxFit(17));
        make.width.mas_equalTo(dealW);
    }];
    
    leftLine = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sepLine"]];
    [self addSubview:leftLine];
    [leftLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(dealLb.mas_left).mas_offset(-2);
        make.centerY.mas_equalTo(dealLb.mas_centerY);
        make.width.mas_equalTo(1);
    }];
    
    waitLb = [self lb:detailFont];
    [waitLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(leftLine.mas_left);
        make.centerY.mas_equalTo(dealLb.mas_centerY);
        make.width.mas_equalTo(dealW);
    }];
  
    rightLine = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sepLine"]];
    [self addSubview:rightLine];
    [rightLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(dealLb.mas_right).mas_offset(2);
        make.centerY.mas_equalTo(dealLb.mas_centerY);
        make.width.mas_equalTo(1);
    }];
    
    dealinglb = [self lb:detailFont];
    [dealinglb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.mas_equalTo(rightLine.mas_right);
        make.centerY.mas_equalTo(dealLb.mas_centerY);
        make.width.mas_equalTo(dealW);
    }];
}

- (void)setModel:(ListPostionModel *)model{
    _model = model;
    positionLb.text = model.title;
    needLb.text = [NSString stringWithFormat:@"%ld",model.recruitingNumber];
    hadLb.text = [NSString stringWithFormat:@"%ld",model.recruitedNumber];
    dealLb.text = [NSString stringWithFormat:@"已处理%ld人",model.processedNumber];
    waitLb.text = [NSString stringWithFormat:@"待处理%ld人",model.waitingProcessNumber];
    dealinglb.text =  [NSString stringWithFormat:@"处理中%ld人",model.processingNumber];
    
    if (![model.state isEqualToString:@"SUBMIT"]) {
        timeLb.text = [BaseHelper stringWithTimeIntevl:model.publishTime format:kDateFormatTypeYYYYMMDD];
    }else{
        timeLb.text = [BaseHelper stringWithTimeIntevl:model.createTime format:kDateFormatTypeYYYYMMDD];
    }
    
    if ([model.state isEqualToString:@"SUBMIT"]) {
        flagImgV.hidden = NO;
        flagTextLb.text = @"未发布";
    }else if ([model.state isEqualToString:@"WAIT_AUDIT"]){
        flagImgV.hidden = NO;
        flagTextLb.text = @"待审核";
    }else if ([model.state isEqualToString:@"AUDIT_FAILED"]){
        flagImgV.hidden = NO;
        flagTextLb.text = @"驳回";
    }else{
        flagImgV.hidden = YES;
    }

    if ([model.state isEqualToString:@"COMPLETED"]) {
        positionLb.textColor = [UIColor lightGrayColor];
        needLb.textColor = [UIColor lightGrayColor];
        hadLb.textColor = [UIColor lightGrayColor];
        dealLb.textColor = [UIColor lightGrayColor];
        waitLb.textColor = [UIColor lightGrayColor];
        dealinglb.textColor = [UIColor lightGrayColor];
        verLineImgV.image = [UIImage imageNamed:@"shizaihuise"];
        bkImgV.image = [UIImage imageNamed:@"positionBkGray"];
        leftLine.image = [UIImage imageNamed:@"sepLineGray"];
        rightLine.image =  [UIImage imageNamed:@"sepLineGray"];
    }else{
        positionLb.textColor = [UIColor lightGrayColor];
        needLb.textColor = [UIColor whiteColor];
        hadLb.textColor = [UIColor whiteColor];
        dealLb.textColor = [UIColor whiteColor];
        waitLb.textColor = [UIColor whiteColor];
        dealinglb.textColor = [UIColor whiteColor];
        verLineImgV.image = [UIImage imageNamed:@"shiziCaise"];
        bkImgV.image = [UIImage imageNamed:@"positionBk"];
    }
}

- (UILabel *)lb:(UIFont *)font{
    UILabel * lb = [[UILabel alloc] init];
    lb.textColor = [UIColor whiteColor];
    lb.font = font;
    lb.textAlignment=  NSTextAlignmentCenter;
    [self addSubview:lb];
    return lb;
}

@end

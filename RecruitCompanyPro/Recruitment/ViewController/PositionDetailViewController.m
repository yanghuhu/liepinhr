//
//  PositionDetailViewController.m
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/14.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "PositionDetailViewController.h"
#import "UIImage+YYAdd.h"
#import "RecruitmentModule.h"
#import "PostionCandidate.h"
#import "JobOrderDetailModel.h"
#import "PositionStateView.h"
#import "ResumDetailView.h"
#import "PayVerifyView.h"

@interface PositionDetailViewController ()<PostionCandidateDelegate,PositionStateViewDelegate,PayVerifyViewDelegte>{
    
    UIView * contentV;
    UIScrollView * baseContetSV;
    
    PostionCandidate * postionCandidateSV;  // 职位候选人容器view
    PositionStateView * positionStateView;      // 候选人进度信息展示容器view
    NSInteger pageNum;          //  候选人页码
}

@property (nonatomic , strong) ResumDetailView * resumDetailView; // 简历详情view
@property (nonatomic , strong) NSMutableArray * candidateArray;      //  候选人数组
@property (nonatomic , strong) CandidateModel * candidateSelected;     // 当前选中的候选人
@property (nonatomic , strong) PayVerifyView * payVerifyView;       // 支付验证容器view
@end

@implementation PositionDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createSubView];
    [BaseHelper showProgressLoadingInView:self.view];
    pageNum = 0;
    [self getCandidatelist];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [IQKeyboardManager sharedManager].enable = NO;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [IQKeyboardManager sharedManager].enable = YES;
}

- (void)createSubView{
    baseContetSV = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    [self.view addSubview:baseContetSV];
    
    UIImageView * imgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"positionDetailBk"]];
    [self.view addSubview:imgV];
    [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.top.and.bottom.equalTo(self.view);
    }];
    
    UIButton * bkBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [bkBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    bkBt.frame = CGRectMake(HorPxFit(25), VerPxFit(25), 60, 45);
    [bkBt setImage:[[UIImage imageNamed:@"back"] imageByTintColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    [self.view addSubview:bkBt];
   
    contentV = [[UIView alloc] initWithFrame:CGRectMake(HorPxFit(120), VerPxFit(70), ScreenWidth - HorPxFit(120)*2, ScreenHeight - VerPxFit(40) - VerPxFit(70))];
    contentV.backgroundColor = [UIColor clearColor];
    [self.view addSubview:contentV];
    
    UIImage * img = [UIImage imageNamed:@"positionAddContentBk"];
    UIImageView * contentBk = [[UIImageView alloc] initWithImage:img];
    [contentV addSubview:contentBk];
    [contentBk mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(contentV).mas_offset(-HorPxFit(25));
        make.top.equalTo(contentV).mas_offset(-VerPxFit(10));
        make.right.equalTo(contentV).mas_offset(HorPxFit(24));
        make.bottom.equalTo(contentV).mas_offset(VerPxFit(17));
    }];
    
    UILabel * titlelb = [[UILabel alloc] initWithFrame:CGRectMake(HorPxFit(40), VerPxFit(26), 90, 35)];
    titlelb.textColor = [UIColor whiteColor];
    titlelb.text = @"职位进度";
    titlelb.font = [UIFont systemFontOfSize:17];
    titlelb.textAlignment = NSTextAlignmentCenter;
    [contentV addSubview:titlelb];
    
    UIImageView * flagImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"detailSegLine"]];
    flagImg.frame = CGRectMake(CGRectGetMinX(titlelb.frame), CGRectGetMaxY(titlelb.frame), CGRectGetWidth(titlelb.frame), 1);
    [contentV addSubview:flagImg];
    
    CGFloat horPadding = HorPxFit(70);
    UILabel * candidateTitle = [[UILabel alloc] initWithFrame:CGRectMake(horPadding, CGRectGetMaxY(flagImg.frame)+VerPxFit(10), 100, 30)];
    candidateTitle.text = @"候选人";
    candidateTitle.font = [UIFont systemFontOfSize:15];
    candidateTitle.textColor = [UIColor whiteColor];
    [contentV addSubview:candidateTitle];

    postionCandidateSV = [[PostionCandidate alloc] initWithFrame:CGRectMake(horPadding, CGRectGetMaxY(candidateTitle.frame), CGRectGetWidth(contentV.frame)- 2*horPadding, VerPxFit(100))];
    postionCandidateSV.delegate = self;
    [contentV addSubview:postionCandidateSV];
    [self addStateViewWithModel:nil];
}

- (void)addStateViewWithModel:(JobOrderDetailModel *)model{
    if (!positionStateView) {
        CGFloat horPadding = HorPxFit(70);
        CGFloat verPadding = VerPxFit(40);
        CGFloat stateY = postionCandidateSV.frame.size.height + postionCandidateSV.frame.origin.y;

        UILabel * candidateDetail = [[UILabel alloc] initWithFrame:CGRectMake(horPadding, stateY+VerPxFit(10), 150, 30)];
        candidateDetail.text = @"候选人进度信息";
        candidateDetail.font = [UIFont systemFontOfSize:15];
        candidateDetail.textColor = [UIColor whiteColor];
        [contentV addSubview:candidateDetail];
        
        stateY += (30+VerPxFit(10));
        
        positionStateView = [[PositionStateView alloc] initWithFrame:CGRectMake(horPadding,stateY , CGRectGetWidth(postionCandidateSV.frame), CGRectGetHeight(contentV.frame)-verPadding-stateY)];
        positionStateView.delegate = self;
        [contentV addSubview:positionStateView];
    }
    if (model) {
        positionStateView.hidden = NO;
        positionStateView.model = model;
    }else{
        positionStateView.hidden = YES;
    }
}

- (void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)changeDataSourceWithState:(TableViewDataSourceChange) changeType{
    if (changeType == TableViewDataSourceChangeForGetMore) {
        //  加载更多
        pageNum ++;
    }else{
        // 下拉刷新
        pageNum = 0;
    }
    [self getCandidatelist];
}

- (void)getCandidatelist{
    if (!_candidateArray) {
        _candidateArray = [NSMutableArray array];
    }
    [RecruitmentModule getPositionCandidateList:pageNum id_:_model.id_ success:^(NSArray * candidateList){
        [BaseHelper hideProgressHudInView:self.view];
        if (pageNum == 0) {
            [_candidateArray removeAllObjects];
        }
        if (candidateList && candidateList.count !=0) {
            [_candidateArray addObjectsFromArray:candidateList];
        }
        [postionCandidateSV candidateSVUpdate:_candidateArray];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}

- (ResumDetailView *)resumDetailView{
    if (!_resumDetailView) {
        CGFloat horpadding = HorPxFit(150);
        _resumDetailView = [[ResumDetailView alloc] initWithFrame:CGRectMake(0, 0, ScreenWidth-2*horpadding, 0)];
    }
    return _resumDetailView;
}

#pragma mark -- PostionCandidateDelegate
- (void)candidateSelect:(CandidateModel *)candidateModel{
    
    [BaseHelper showProgressLoadingInView:self.view];
    [RecruitmentModule getCandidateDetailWithId:candidateModel.id_ success:^(JobOrderDetailModel *model){
        [BaseHelper hideProgressHudInView:self.view];
        if (!positionStateView) {
            positionStateView = [[PositionStateView alloc] initWithFrame:CGRectMake(CGRectGetMinX(postionCandidateSV.frame), CGRectGetMaxY(postionCandidateSV.frame), ScreenWidth - 2*CGRectGetMinX(postionCandidateSV.frame), ScreenHeight-CGRectGetMaxY(postionCandidateSV.frame)-VerPxFit(60)*2)];
            positionStateView.delegate = self;
            [self.view addSubview:positionStateView];
        }
        self.candidateSelected = candidateModel;
        [self addStateViewWithModel:model];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
    
    if (candidateModel.hrStateFlag) {
        [RecruitmentModule orderReadedWithId:candidateModel.id_ success:^{
            candidateModel.hrStateFlag = NO;
            [postionCandidateSV removeItemViewHongDian:candidateModel];
        } failure:^(NSString *error, ResponseType responseType) {
        }];
    }
}

- (void)candidateRefresh{
    [self changeDataSourceWithState:TableViewDataSourceChangeForRefresh];
}

- (void)candidateGetMore{
    [self changeDataSourceWithState:TableViewDataSourceChangeForGetMore];
}

#pragma mark -- PositionStateViewDelegate
- (void)resumeDeal:(NSString *)result desc:(NSString *)desc{
    [BaseHelper showProgressLoadingInView:self.view];
    [RecruitmentModule resumeDealOrderId:_candidateSelected.id_ result:result desc:desc success:^{
        [BaseHelper hideProgressHudInView:self.view];
        [BaseHelper showProgressHud:@"提交成功" showLoading:NO canHide:YES];

        [self candidateSelect:_candidateSelected];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}

- (void)interviewResult:(NSString *)result desc:(NSString *)desc{
    [BaseHelper showProgressLoadingInView:self.view];
    [RecruitmentModule submintPositionInterViewWihtId:_candidateSelected.id_ result:result desc:desc success:^{
        [BaseHelper hideProgressHudInView:self.view];
        [BaseHelper showProgressHud:@"提交成功" showLoading:NO canHide:YES];
        [self candidateSelect:_candidateSelected];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}

- (void)payForReward{
    self.payVerifyView = [[PayVerifyView alloc] initWithFrame:CGRectMake(0, 0, HorPxFit(400), VerPxFit(300))];
    self.payVerifyView.delegate = self;
    [self.payVerifyView showWithSupView:self.view];
}

- (void)offerInfoSubmit:(NSDictionary *)info{
    NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithDictionary:info];
    [dic setObject:@(_candidateSelected.id_) forKey:@"id"];
    [BaseHelper showProgressLoadingInView:self.view];
    [RecruitmentModule submitOfferInfo:dic success:^(NSString * url){
        [BaseHelper hideProgressHudInView:self.view];
        [positionStateView offerSubmitSuccess:url];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}

- (void)resumCheck:(TalentModel *)model{
    
    [BaseHelper showProgressLoadingInView:self.view];
    @weakify(self);
    [RecruitmentModule getTalentDetailWithId:model.id_ success:^(TalentModel *model){
        [BaseHelper hideProgressHudInView:self.view];
        [weak_self.resumDetailView initInfoWithTalent:model];
        [weak_self.resumDetailView showWithSuperView:nil];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}

- (void)notRegisterAction{
    [BaseHelper showProgressLoadingInView:self.view];
    @weakify(self);
    [RecruitmentModule talentNotRegister:_candidateSelected.id_ success:^{
        [BaseHelper hideProgressHudInView:self.view];
        [weak_self candidateSelect:_candidateSelected];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        [weak_self netFailWihtError:error andStatusCode:responseType];
    }];
}

- (void)notPositionAction{
    [BaseHelper showProgressLoadingInView:self.view];
    @weakify(self);
    [RecruitmentModule talentNotPositionWithId:_candidateSelected.id_ success:^{
        [BaseHelper hideProgressHudInView:self.view];
        [weak_self candidateSelect:_candidateSelected];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        [weak_self netFailWihtError:error andStatusCode:responseType];
    }];
}

- (void)positionAction{
    [BaseHelper showProgressLoadingInView:self.view];
    @weakify(self);
    [RecruitmentModule talentPositionWithId:_candidateSelected.id_ success:^{
        [BaseHelper hideProgressHudInView:self.view];
        [weak_self candidateSelect:_candidateSelected];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        [weak_self netFailWihtError:error andStatusCode:responseType];
    }];
}

#pragma mark -- PayVerifyViewDelegte
- (void)getVerifyCodeAction{
    [BaseHelper showProgressLoadingInView:self.view];
    @weakify(self);
    [RecruitmentModule getSMSVerifyCodeWithPhone:[HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel.cellphone type:@"Pay_Confirm" Success:^{
        [BaseHelper hideProgressHudInView:self.view];
        [BaseHelper showProgressHud:@"验证码发送成功" showLoading:NO canHide:YES];
        [weak_self.payVerifyView verifyCodeSendSuccess];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        if (error) {
            [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        }
    }];
}

- (void)PayVerifyWithVerifyCode:(NSString *)verifyCode{
    [BaseHelper showProgressLoadingInView:self.view];
    @weakify(self);
    [RecruitmentModule payForReward:_candidateSelected.id_ verifyCode:verifyCode success:^{
        [BaseHelper hideProgressHudInView:self.view];
        [BaseHelper showProgressHud:@"支付成功" showLoading:NO canHide:YES];
        [weak_self.payVerifyView dismissAction];
        [weak_self candidateSelect:_candidateSelected];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}

@end

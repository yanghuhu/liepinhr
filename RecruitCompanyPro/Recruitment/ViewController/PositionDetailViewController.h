//
//  PositionDetailViewController.h
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/14.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ListPostionModel.h"
#import "BaseViewController.h"

@interface PositionDetailViewController : BaseViewController

@property (nonatomic , strong) ListPostionModel * model;

@end

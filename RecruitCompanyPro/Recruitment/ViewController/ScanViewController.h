//
//  ScanViewController.h
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/16.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@protocol ScanViewControllerDelegate
// 扫描时间处理成功
- (void)submitSuccess;
@end

@interface ScanViewController : BaseViewController

@property (nonatomic , weak) id<ScanViewControllerDelegate>delegate;

@end

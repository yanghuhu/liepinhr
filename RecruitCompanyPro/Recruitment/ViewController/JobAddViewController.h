//
//  JobAddViewController.h
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/12.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "ListPostionModel.h"

@protocol JobAddViewControllerDelegate
// 职位发布成功
- (void)jobPublishSuccess;
@end

@interface JobAddViewController : BaseViewController

@property (nonatomic , strong) ListPostionModel * listPositionModel;
@property (nonatomic , assign) id<JobAddViewControllerDelegate>delegate;

@end

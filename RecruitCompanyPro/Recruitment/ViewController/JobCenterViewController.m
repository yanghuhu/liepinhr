//
//  JobCenterViewController.m
//  RecruitCompanyPro
//
//  Created by Michael on 2017/11/30.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "JobCenterViewController.h"
#import "JobAddViewController.h"
#import "AppDelegate.h"
#import "LoginViewController.h"
#import "ListPostionModel.h"
#import "RecruitmentModule.h"
#import "PositionCollectionCell.h"
#import "PositionDetailViewController.h"
#import "ScanViewController.h"
#import "UserCenterViewController.h"
#import "UIImage+YYAdd.h"
#import "BaseNavigationController.h"

@interface JobCenterViewController ()<UICollectionViewDelegate,UICollectionViewDataSource,JobAddViewControllerDelegate,ScanViewControllerDelegate>{
    UICollectionView * collectionView;
    UIView * contentView;
}

@property (nonatomic , assign) NSInteger pageNum;        // 数据源页码
@property (nonatomic , strong) NSMutableArray * positionArray;   // 职位数据源
@end

@implementation JobCenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBar.hidden = YES;
    [self createSubViews];
    [self getDataSource];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)createSubViews{

    UIImageView * imgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"positionHomeBk"]];
    [self.view addSubview:imgV];
    [imgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.top.and.bottom.equalTo(self.view);
    }];

    UIButton * userHomeBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [userHomeBt addTarget:self action:@selector(toUserHomeVCAction) forControlEvents:UIControlEventTouchUpInside];
    [userHomeBt setImage:[[UIImage imageNamed:@"UserHome"] imageByTintColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    userHomeBt.titleLabel.font = [UIFont systemFontOfSize:20];
    [userHomeBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    userHomeBt.frame = CGRectMake(HorPxFit(15), VerPxFit(40), 100, 40);
    [self.view addSubview:userHomeBt];
    
    UIButton * scanBt = [UIButton buttonWithType:UIButtonTypeCustom];
    scanBt.frame = CGRectMake(ScreenWidth - HorPxFit(120)-HorPxFit(40), VerPxFit(30), HorPxFit(100), VerPxFit(66));
    [scanBt addTarget:self action:@selector(scanAction) forControlEvents:UIControlEventTouchUpInside];
    [scanBt setImage:[UIImage imageNamed:@"scan"] forState:UIControlStateNormal];
    [self.view addSubview:scanBt];
    
    contentView = [[UIView alloc] initWithFrame:CGRectMake(HorPxFit(80), VerPxFit(130), ScreenWidth-HorPxFit(80)*2, ScreenHeight-VerPxFit(190))];
    contentView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:contentView];
    
    UIImageView * contentBk = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"posiontHomeCotentBK"]];
    contentBk.frame = CGRectMake(-HorPxFit(20), -VerPxFit(10), CGRectGetWidth(contentView.frame)+2*HorPxFit(20), CGRectGetHeight(contentView.frame)+VerPxFit(10)+VerPxFit(26));
    [contentView addSubview:contentBk];
    
    CGFloat horPadding = HorPxFit(90);
    UILabel * titleLb = [[UILabel alloc] initWithFrame:CGRectMake(horPadding/2, VerPxFit(15), 200, 25)];
    titleLb.text = @"职位列表";
    titleLb.textColor = [UIColor whiteColor];
    titleLb.font = [UIFont boldSystemFontOfSize:18];
    [contentView addSubview:titleLb];
    
    UIImage * img = [UIImage imageNamed:@"positionAdd"];
    UIImageView * addImgV = [[UIImageView alloc] initWithImage:img];
    [contentView addSubview:addImgV];
    [addImgV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(contentView);
        make.top.mas_equalTo(titleLb.mas_bottom).mas_offset(VerPxFit(10));
        make.size.mas_equalTo(CGSizeMake(HorPxFit(img.size.width), VerPxFit(img.size.height)));
    }];
    
    UILabel * addTitle = [[UILabel alloc] init];
    addTitle.textAlignment = NSTextAlignmentCenter;
    addTitle.text = @"添加职位";
    addTitle.textColor = [UIColor whiteColor];
    addTitle.font = [UIFont systemFontOfSize:15];
    [contentView addSubview:addTitle];
    [addTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(addImgV.mas_centerY);
        make.right.mas_equalTo(addImgV.mas_left);
        make.size.mas_equalTo(CGSizeMake(70, 30));
    }];
    
    UIButton * addBt = [UIButton buttonWithType:UIButtonTypeCustom];
    addBt.backgroundColor = [UIColor clearColor];
    [addBt addTarget:self action:@selector(jobAddAction) forControlEvents:UIControlEventTouchUpInside];
    [contentView addSubview:addBt];
    [addBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.mas_equalTo(addImgV.mas_right);
        make.left.mas_equalTo(addTitle.mas_left);
        make.top.equalTo(addTitle);
        make.bottom.equalTo(addTitle);
    }];
    
    UIImageView * lineImgv = [[UIImageView alloc] init];
    lineImgv.image = [UIImage imageNamed:@"bkLine"];
    [contentView addSubview:lineImgv];
    [lineImgv mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(addBt.mas_centerY);
        make.left.equalTo(contentView);
        make.right.mas_equalTo(addTitle.mas_left);
        make.height.mas_equalTo(2);
    }];
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    collectionView = [[UICollectionView alloc] initWithFrame:contentView.bounds collectionViewLayout:layout];
    collectionView.showsVerticalScrollIndicator = NO;
    collectionView.showsHorizontalScrollIndicator = NO;
    collectionView.backgroundColor = [UIColor clearColor];
    collectionView.delegate = self;
    collectionView.dataSource = self;
    [collectionView registerClass:[PositionCollectionCell class] forCellWithReuseIdentifier:@"cellId"];
    [contentView addSubview:collectionView];
    @weakify(self)
    collectionView.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        [weak_self changeDataSourceWithState:TableViewDataSourceChangeForRefresh];
    }];
    collectionView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingBlock:^{
        [weak_self changeDataSourceWithState:TableViewDataSourceChangeForGetMore];
    }];
    [collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(contentView).mas_offset(horPadding);
        make.right.equalTo(contentView).mas_offset(-horPadding);
        make.top.mas_equalTo(lineImgv.mas_bottom).mas_offset(VerPxFit(20));
        make.bottom.equalTo(contentView).mas_offset(-HorPxFit(10));
    }];
}

- (void)changeDataSourceWithState:(TableViewDataSourceChange) changeType{
    if (changeType == TableViewDataSourceChangeForGetMore) {
        //  加载更多
        _pageNum ++;
    }else{
        // 下拉刷新
        _pageNum = 0;
    }
    [self getDataSource];
}

- (void)getDataSource{
    if (!_positionArray) {
        self.positionArray = [NSMutableArray array];
    }
    [BaseHelper showProgressLoadingInView:self.view];
    [RecruitmentModule getPositionList:_pageNum success:^(NSArray *array){
        [BaseHelper hideProgressHudInView:self.view];
        if (_pageNum == 0) {
            [_positionArray removeAllObjects];
        }
        if (array && array.count != 0) {
            [_positionArray addObjectsFromArray:array];
        }
        if (!array || array.count < 10) {
            [collectionView.mj_footer resetNoMoreData];
        }
        [collectionView reloadData];
        [collectionView.mj_header endRefreshing];
        [collectionView.mj_footer endRefreshing];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}

- (void)jobAddAction{
    JobAddViewController * vc = [[JobAddViewController alloc] init];
    vc.delegate = self;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)toUserHomeVCAction{
    UserCenterViewController * vc = [[UserCenterViewController alloc] init];
    BaseNavigationController * nav = [[BaseNavigationController alloc] initWithRootViewController:vc];
    nav.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewController:nav animated:YES completion:^{
    }];
}

- (void)scanAction{
    ScanViewController * vc = [[ScanViewController alloc] init];
    vc.delegate = self;
    BaseNavigationController * nav = [[BaseNavigationController alloc] initWithRootViewController:vc];
    nav.modalPresentationStyle = UIModalPresentationFormSheet;
    [self presentViewController:nav animated:YES completion:^{
    }];
}

#pragma mark collectionView代理方法
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _positionArray.count;
}

- (PositionCollectionCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PositionCollectionCell *cell = (PositionCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cellId" forIndexPath:indexPath];
    if (!cell.isCreateSubs) {
        [cell createSubViews];
    }
    ListPostionModel * model = _positionArray[indexPath.row];
    cell.model = model;
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(HorPxFit(195), 135);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10, 10, 10,10);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return (CGRectGetWidth(collectionView.frame) - HorPxFit(195)*3) / 4;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 25;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    ListPostionModel * model = _positionArray[indexPath.row];
    
    if ([model.state isEqualToString:@"SUBMIT"]) {
        JobAddViewController * vc = [[JobAddViewController alloc] init];
        vc.listPositionModel = model;
        [self.navigationController pushViewController:vc animated:YES];
    }else if ([model.state isEqualToString:@"WAIT_AUDIT"]){
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:nil message:@"企业管理员正在审核中" delegate:nil cancelButtonTitle:@"好的" otherButtonTitles:nil, nil];
        [alert show];
    }else if ([model.state isEqualToString:@"AUDIT_FAILED"]){
        JobAddViewController * vc = [[JobAddViewController alloc] init];
        vc.listPositionModel = model;
        [self.navigationController pushViewController:vc animated:YES];
    }else{
        PositionDetailViewController * vc = [[PositionDetailViewController alloc] init];
        vc.model = model;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark -- JobAddViewControllerDelegate
- (void)jobPublishSuccess{
    [self changeDataSourceWithState:TableViewDataSourceChangeForRefresh];
}

#pragma mark -  ScanViewControllerDelegate
- (void)submitSuccess{
    UIAlertView * alter = [[UIAlertView alloc] initWithTitle:nil message:@"入职信息提交成功" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
    [alter show];
}
@end

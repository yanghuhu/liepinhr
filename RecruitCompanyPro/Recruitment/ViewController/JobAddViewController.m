//
//  JobAddViewController.m
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/12.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "JobAddViewController.h"
#import "UIImage+YYAdd.h"
#import "PositionContentSV.h"
#import "RecruitmentModule.h"
#import "JobModel.h"
#import "PayPopView.h"
#import "DescAddView.h"
#import "ThirdPayManage.h"

@interface JobAddViewController ()<PositionContentSVDelegate,PayPopViewDelegate,DescAddViewDelegate>{
    UIView * contentV;
    PositionContentSV * contentSV;
}

@property (nonatomic , strong) PayPopView * payPopView;  //   职位发布页面
@property (nonatomic , strong) JobModel * jobModel;
@property (nonatomic , strong) ThirdPayManage * thirdPayManage;
@property (nonatomic , strong) NSString * payType;   // 支付方式
@property (nonatomic , strong) NSString * payNote;    // 支付备注
@end

@implementation JobAddViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createSubViews];
    if(_listPositionModel){
        [self getPositionDetail];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(payFinsih:) name:PayResultDictionaryNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)createSubViews{
    UIImageView * bkImgV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"positionAddBk"]];
    bkImgV.frame = self.view.bounds;
    [self.view addSubview:bkImgV];
    
    UIButton * bkBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [bkBt addTarget:self action:@selector(backAction) forControlEvents:UIControlEventTouchUpInside];
    bkBt.frame = CGRectMake(HorPxFit(25), VerPxFit(25), 60, 45);
    [bkBt setImage:[[UIImage imageNamed:@"back"] imageByTintColor:[UIColor whiteColor]] forState:UIControlStateNormal];
    [self.view addSubview:bkBt];
    
    contentV = [[UIView alloc] initWithFrame:CGRectMake(HorPxFit(75), VerPxFit(77), ScreenWidth-HorPxFit(75)*2, ScreenHeight - VerPxFit(77)-VerPxFit(50))];
    contentV.backgroundColor = [UIColor clearColor];
    [self.view addSubview:contentV];
    
    UIImage * img = [UIImage imageNamed:@"positionAddContentBk"];
    UIImageView * contentBk = [[UIImageView alloc] initWithImage:img];
    [contentV addSubview:contentBk];
    [contentBk mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(contentV).mas_offset(-HorPxFit(25));
        make.top.equalTo(contentV).mas_offset(-VerPxFit(10));
        make.right.equalTo(contentV).mas_offset(HorPxFit(24));
        make.bottom.equalTo(contentV).mas_offset(VerPxFit(17));
    }];
    
    UILabel * titleLb = [[UILabel alloc] initWithFrame:CGRectMake(HorPxFit(50), VerPxFit(30), 90, 30)];
    titleLb.text = @"职位添加";
    titleLb.textColor = [UIColor whiteColor];
    titleLb.textAlignment = NSTextAlignmentCenter;
    titleLb.font  = [UIFont systemFontOfSize:16];
    [contentV addSubview:titleLb];
    UIImageView * titleline = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"postionAddTitleLine"]];
    [contentV addSubview:titleline];
    [titleline mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(titleLb);
        make.top.mas_equalTo(titleLb.mas_bottom);
        make.height.mas_equalTo(1);
    }];
    
    UILabel * englishTitle = [[UILabel alloc] init];
    englishTitle.font = [UIFont systemFontOfSize:10];
    englishTitle.text = @"Job addition";
    englishTitle.textColor = [UIColor whiteColor];
    englishTitle.textAlignment = NSTextAlignmentCenter;
    [contentV addSubview:englishTitle];
    [englishTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(titleLb.mas_centerX);
        make.top.mas_equalTo(titleline.mas_bottom);
        make.width.and.height.equalTo(titleLb);
    }];

    contentSV = [[PositionContentSV alloc] init];
    contentSV.showsVerticalScrollIndicator = NO;
    contentSV.showsHorizontalScrollIndicator = NO;
    contentSV.delegate_ = self;
    [contentV addSubview:contentSV];
    [contentSV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(contentV).mas_offset(HorPxFit(10)+HorPxFit(50)+90);
        make.right.equalTo(contentV).mas_offset(-HorPxFit(140));
        make.top.mas_equalTo(englishTitle.mas_bottom).mas_offset(-8);
        make.bottom.mas_equalTo(contentV).mas_offset(-VerPxFit(20));
    }];
}

- (void)getPositionDetail{
    [BaseHelper showProgressLoadingInView:self.view];
    [RecruitmentModule getPostionDetailWithId:_listPositionModel.id_ Success:^(JobModel * model){
        [BaseHelper hideProgressHudInView:self.view];
        self.jobModel = model;
        [self jobInfoAppear];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        if (error) {
            [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        }
    }];
}

- (void)backAction{
    [self.navigationController popViewControllerAnimated:YES];
}

- (PayPopView *)payPopView{
    if (!_payPopView) {
        _payPopView = [[PayPopView alloc] init];
        _payPopView.delegate = self;
    }
    return _payPopView;
}

- (ThirdPayManage *)thirdPayManage{
    if (!_thirdPayManage) {
        _thirdPayManage = [[ThirdPayManage alloc] init];
    }
    return _thirdPayManage;
}

- (void)jobInfoAppear{
    
    UIButton * publishBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [publishBt setTitle:@"发布职位" forState:UIControlStateNormal];
    publishBt.backgroundColor = [UIColor redColor];
    [publishBt setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [publishBt setBackgroundImage:[UIImage imageNamed:@"btBk"] forState:UIControlStateNormal];
    publishBt.titleLabel.font = [UIFont systemFontOfSize:14];
    [publishBt addTarget:self action:@selector(publishEditPositionAction) forControlEvents:UIControlEventTouchUpInside];
    [contentV addSubview:publishBt];
    [publishBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(contentSV.mas_top).mas_offset(-VerPxFit(10));
        make.right.equalTo(contentV).mas_offset(-HorPxFit(40));
        make.size.mas_equalTo(CGSizeMake(HorPxFit(130), VerPxFit(30)));
    }];
    [contentSV updateInfoAppear:_jobModel];
}

- (void)publishEditPositionAction{
    NSArray * titles=@[@"title",@"profession",@"position",@"level",@"education",@"workExperience",@"jobNature",@"workArea",@"salaryRange",@"recruitingNumber",@"price",@"additionalInfo"];
    
    NSDictionary * resultNew = contentSV.resultInfoDic;
    BOOL isNeedSubmit = NO;
    for (int i=0; i<titles.count; i++) {
        NSString * key = titles[i];
        
        if ([key isEqualToString:@"price"]) {
            NSInteger priceNew = [resultNew[@"price"] integerValue];
            NSInteger priceOld = _jobModel.price;
            if (priceOld != priceNew) {
                isNeedSubmit = YES;
                break;
            }
        }else if ([key isEqualToString:@"recruitingNumber"]){
            NSInteger priceNew = [resultNew[@"recruitingNumber"] integerValue];
            NSInteger priceOld = _jobModel.recruitingNumber;
            if (priceOld != priceNew) {
                isNeedSubmit = YES;
                break;
            }
        }else{
            id valueNew = resultNew[key];
            SEL select = NSSelectorFromString(key);
            id valueOld = [_jobModel performSelector:select];
            
            NSString * new = (NSString *)valueNew;
            NSString * old = (NSString *)valueOld;
            if (![new isEqualToString:old]) {
                if ([key isEqualToString:@"additionalInfo"]) {
                    if ([new isEqualToString:@""] && !old) {
                        continue;
                    }else{
                        isNeedSubmit = YES;
                        break;
                    }
                }else{
                    isNeedSubmit = YES;
                    break;
                }
            }
        }
    }
    
    if (isNeedSubmit) {
        [BaseHelper showProgressHud:@"职位信息有更改，请提交后在发布" showLoading:NO canHide:YES];
        return;
    }
    
    NSArray * descArrayNew = contentSV.positionDescArray;
    NSArray * responderArrayNew = contentSV.positionResponArray;
    
    NSMutableArray *  descArrayOld = [NSMutableArray array];
    NSMutableArray *  respArrayOld = [NSMutableArray array];
    for (JobRequirementModel * require in  _jobModel.jobRequireArray) {
        [descArrayOld addObject:require.description_];
    }
    
    for (JobResponsibilityModel * responder in  _jobModel.jobResponsibitiyArray) {
        [respArrayOld addObject:responder.description_];
    }
    
    BOOL ishad = YES;
    for (NSString * oldDes in descArrayOld) {
        BOOL isThisHad = NO;
        for (NSString * newDes in descArrayNew) {
            if ([oldDes isEqualToString:newDes]) {
                isThisHad = YES;
                break;
            }
        }
        if (isThisHad == NO) {
            ishad = NO;
            break;
        }
    }
    if (!ishad) {
        [BaseHelper showProgressHud:@"职位信息有更改，请提交后在发布" showLoading:NO canHide:YES];
        return;
    }
    ishad = YES;
    for (NSString * oldRes in respArrayOld) {
        BOOL isThisHad = NO;
        for (NSString * newRes in responderArrayNew) {
            if ([oldRes isEqualToString:newRes]) {
                isThisHad = YES;
                break;
            }
        }
        if (isThisHad == NO) {
            ishad = NO;
            break;
        }
    }
    if (!ishad) {
        [BaseHelper showProgressHud:@"职位信息有更改，请提交后在发布" showLoading:NO canHide:YES];
        return;
    }    
    CGFloat price = [contentSV.resultInfoDic[@"price"] floatValue] * [contentSV.resultInfoDic[@"recruitingNumber"] intValue];
    [self.payPopView showSupView:self.view withPrice:price];
}

- (void)payFinsih:(NSNotification *)nofi{
    NSDictionary * resultDic = (NSDictionary *)[nofi object];
    NSString * statusStr = resultDic[@"resultStatus"];
    
    if ([statusStr isEqualToString:@"9000"]) {
        [BaseHelper showProgressHud:@"支付成功" showLoading:NO canHide:YES];
        [self.navigationController popViewControllerAnimated:YES];
    } else if ([statusStr isEqualToString:@"8000"]) {
        [BaseHelper showProgressHud:@"正在处理中" showLoading:NO canHide:YES];
    } else if ([statusStr isEqualToString:@"6001"]) {
        [BaseHelper showProgressHud:@"取消支付" showLoading:NO canHide:YES];
    } else if ([statusStr isEqualToString:@"6002"]) {
        [BaseHelper showProgressHud:@"网络故障支付失败" showLoading:NO canHide:YES];
    } else if ([statusStr isEqualToString:@"4000"]) {
        [BaseHelper showProgressHud:@"支付失败" showLoading:NO canHide:YES];
    }
}

#pragma mark -- PositionContentSVDelegate
- (void)positionMapAction{
    NSDictionary * baseInfo = contentSV.resultInfoDic;
    NSArray * titles = @[@"title",@"profession",@"position",@"level",@"education",@"workExperience",@"jobNature",@"workArea",@"salaryRange",@"recruitingNumber",@"price"];
    NSArray * alertArray = @[@"请填写标题",@"请选择行业",@"请求选择职位",@"请选择级别",@"请选择学历",@"请选择经验要求",@"请选择工作性质",@"请填写工作地点",@"请填写薪资",@"请填写人数",@"请填写赏金"];
    BOOL isAllHad = YES;
    for (int i=0;i<titles.count;i++) {
        NSString * title = titles[i];
        NSString * text = baseInfo[title];
        if (!text) {
            NSString * alert = alertArray[i];
            [BaseHelper showProgressHud:alert showLoading:NO canHide:YES];
            isAllHad = NO;
            break;
        }
    }
    if (!isAllHad) {
        return;
    }
    [BaseHelper showProgressLoadingInView:self.view];
    [RecruitmentModule postionMappingWithTagArray:[baseInfo allValues] success:^(NSDictionary *info){
        [BaseHelper hideProgressHudInView:self.view];
        NSArray *  descArray = info[@"require"];
        NSArray * respArray = info[@"responsibility"];
        [contentSV initDesc:descArray respArray:respArray];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        if(error){
         [self netFailWihtError:error andStatusCode:responseType];
        }
    }];
}

- (void)positonSubmit:(NSDictionary *)baseInfo descArray:(NSArray *)descArray respArray:(NSArray *)respArray{
    
    if (_listPositionModel) {
        [self updatePostionInfo:baseInfo descArray:descArray respArray:respArray];
        return;
    }
    [BaseHelper showProgressLoadingInView:self.view];
    [RecruitmentModule submitPositionWithBaseInfo:baseInfo descArray:descArray respArray:respArray success:^(JobModel * jobModel){
        [BaseHelper hideProgressHudInView:self.view];
        self.jobModel = jobModel;
        NSInteger price = [contentSV.resultInfoDic[@"price"] intValue] * [contentSV.resultInfoDic[@"recruitingNumber"] intValue];
        [self.payPopView showSupView:self.view withPrice:price];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}

- (void)updatePostionInfo:(NSDictionary *)baseInfo descArray:(NSArray *)descArray respArray:(NSArray *)respArray{
    NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithDictionary:baseInfo];
    [dic setObject:@(_listPositionModel.id_) forKey:@"id"];
    [BaseHelper showProgressLoadingInView:self.view];
    [RecruitmentModule updataPositionWithBaseInfo:dic descArray:descArray respArray:respArray success:^(JobModel * jobModel){
        [BaseHelper hideProgressHudInView:self.view];
        self.jobModel = jobModel;
        NSInteger price = [contentSV.resultInfoDic[@"price"] intValue] * [contentSV.resultInfoDic[@"recruitingNumber"] intValue];
        [self.payPopView showSupView:self.view withPrice:price];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}

#pragma mark -- PayPopViewDelegate

- (void)payActionWihtType:(NSString *)payType withNote:(NSString *)note{
    self.payType = payType;
    self.payNote = note;
    
    ThirdPayType  thirdPayType;
    if ([payType isEqualToString:@"WechatPay"]) {
        thirdPayType = ThirdPayTypeWechat;
    }else if([payType isEqualToString:@"AliPay"]){
        thirdPayType = ThirdPayTypeAlipay;
    }else if([payType isEqualToString:@"Bank"]){
        thirdPayType = ThirdPayTypeBank;
    }

    if([payType isEqualToString:@"AliPay"]){
        CGFloat amount = [contentSV.resultInfoDic[@"price"] floatValue] * [contentSV.resultInfoDic[@"recruitingNumber"] intValue];
        [BaseHelper showProgressLoadingInView:self.view];
        [RecruitmentModule payWithAmount:amount body:_jobModel.title outTradeNo:_jobModel.serialNumber subject:@"职位发布" success:^(NSString * orderString){
            [BaseHelper hideProgressHudInView:self.view];

            [self.thirdPayManage payWithType:thirdPayType OrderInfo:orderString finishBlock:^(NSDictionary *info) {
                [[NSNotificationCenter defaultCenter] postNotificationName:PayResultDictionaryNotification object:info];
            }];
        } failure:^(NSString *error, ResponseType responseType) {
            [BaseHelper hideProgressHudInView:self.view];
            [self netFailWihtError:error andStatusCode:responseType];
        }];
    }else if ([payType isEqualToString:@"CompanyPay"]){
        [self payFinishAction];
    }

    return;
    //  本地第三方支付
    if([payType isEqualToString:@"CompanyPay"]){
        [self payFinishAction];
        return;
    }
    if ([payType isEqualToString:@"WechatPay"]) {
        thirdPayType = ThirdPayTypeWechat;
    }else if([payType isEqualToString:@"AliPay"]){
        thirdPayType = ThirdPayTypeAlipay;
    }else if([payType isEqualToString:@"Bank"]){
        thirdPayType = ThirdPayTypeBank;
    }
    
    CGFloat price = [contentSV.resultInfoDic[@"price"] intValue] * [contentSV.resultInfoDic[@"recruitingNumber"] intValue] /  100;
    [self.thirdPayManage payWithType:thirdPayType OrderId:_jobModel.serialNumber price:price finishBlock:^(NSDictionary * payFinishInfo){
        NSLog(@"%@",payFinishInfo);
    }];
}


- (void)payFinishAction{
    [BaseHelper showProgressLoadingInView:self.view];
    [RecruitmentModule publicPositionWithId:_jobModel.id_ payType:self.payType note:self.payNote success:^(JobModel * jobmodel){
        [BaseHelper hideProgressHudInView:self.view];
        self.jobModel =jobmodel;
        [self.delegate jobPublishSuccess];
        [self.navigationController popViewControllerAnimated:YES];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHudInView:self.view];
        [self netFailWihtError:error andStatusCode:responseType];
    }];
}

@end

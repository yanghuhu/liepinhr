//
//  RecruitmentModule.h
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/11.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CompanyModel.h"
#import "BaseModule.h"

@interface RecruitmentModule : BaseModule

/*
 *  企业列表
 *  @param
 *      pageNum  当前页面
 *  succBlock
 *      NSArray  [CompanyModel]
 */
+ (void)getCompanyListWithPageNum:(NSInteger)pageNum
                         success:(RequestSuccessBlock)succBlock
                         failure:(RequestFailureBlock)failBlock;

/*
 * 发布职位
 *  @param
 *    education  学历
 *    jobNature  工作性质（全职，兼职）
 *     level  等级
 *      position  职位
 *      price   价格
 *      profession  行业
 *      recruitingNumber  招聘人数
 *      requirement [string]  岗位要求
 *      responsibilities [string]  岗位职责
 *      salaryRange  薪资范围
 *      title     招聘title
 *      workArea  工作地点
 *      workExperience  工作年限
 *
 */
+ (void)submitPositonWithEducation:(NSString *)education
                         jobNature:(NSString *)jobNature
                             level:(NSString *)level
                          position:(NSString *)position
                             price:(NSInteger)price
                        profession:(NSString *)profession
                  recruitingNumber:(NSInteger)recruitingNumber
                       requirement:(NSArray *)requirement
                  responsibilities:(NSArray *)responsibilities
                       salaryRange:(NSString *)salaryRange
                             title:(NSString *)title
                          workArea:(NSString *)workArea
                    workExperience:(NSString *)workExperience
                           success:(RequestSuccessBlock)succBlock
                           failure:(RequestFailureBlock)failBlock;

/*
 * 发布职位
 *  @param
 *    education  学历
 *    jobNature  工作性质（全职，兼职）
 *     level  等级
 *      position  职位
 *      price   价格
 *      profession  行业
 *      recruitingNumber  招聘人数
 *      requirement [string]  岗位要求
 *      responsibilities [string]  岗位职责
 *      salaryRange  薪资范围
 *      title     招聘title
 *      workArea  工作地点
 *      workExperience  工作年限
 *
 */
+ (void)updataPositionWithBaseInfo:(NSDictionary *)baseInfo
                         descArray:(NSArray *)descArray
                         respArray:(NSArray *)respArray
                           success:(RequestSuccessBlock)succBlock
                           failure:(RequestFailureBlock)failBlock;
    

/*
 *  关闭职位
 *  @param
 *      positionId  职位id
 */
+ (void)positionCloseWithId:(NSInteger)positionId
                    success:(RequestSuccessBlock)succBlock
                    failure:(RequestFailureBlock)failBlock;

/*
 *  获取职位列表
 *  @param
 *      page  页码
 */
+ (void)getPositionList:(NSInteger)page
                success:(RequestSuccessBlock)succBlock
                failure:(RequestFailureBlock)failBlock;

/*
 *  提交面试结果
 *  @param
 *      id_ 提交的面试订单id
 *      result  面试结果
 *      desc  面试描述
 */
+ (void)submintPositionInterViewWihtId:(NSInteger)id_
                              result:(NSString *)result
                              desc:(NSString *)desc
                           success:(RequestSuccessBlock)succBlock
                           failure:(RequestFailureBlock)failBlock;

/*
 *  职位匹配
 *  tags   【string】 职位信息数组
 *
 */
+ (void)postionMappingWithTagArray:(NSArray *)tags
                           success:(RequestSuccessBlock)succBlock
                           failure:(RequestFailureBlock)failBlock;

/*
 * 提交职位信息
 *  baseInfo   职位基本信息
 *
 *  descArray
 *          职位描述信息数组
 * respArray
 *          职位职责数组
 */
+ (void)submitPositionWithBaseInfo:(NSDictionary *)baseInfo
                         descArray:(NSArray *)descArray
                         respArray:(NSArray *)respArray
                           success:(RequestSuccessBlock)succBlock
                           failure:(RequestFailureBlock)failBlock;

/*
 * 发布职位
 *   id   职位id
 */
+ (void)publicPositionWithId:(NSInteger)id_
                    payType:(NSString *)payType
                        note:(NSString *)note
                     success:(RequestSuccessBlock)succBlock
                     failure:(RequestFailureBlock)failBlock;

/*
 * 获取职位候选人列表
 *   page 页码
 *   id  职位id
 */
+ (void)getPositionCandidateList:(NSInteger)page
                     id_:(NSInteger)id_
                success:(RequestSuccessBlock)succBlock
                failure:(RequestFailureBlock)failBlock;

/*
 * 获取候选人详情
 * orderId   候选人订单id
 */
+ (void)getCandidateDetailWithId:(NSInteger)orderId
                         success:(RequestSuccessBlock)succBlock
                         failure:(RequestFailureBlock)failBlock;

/*
 * 支付佣金
 *  id   订单id
 */
+ (void)payForReward:(NSInteger)id_
          verifyCode:(NSString *)verifyCode
             success:(RequestSuccessBlock)succBlock
             failure:(RequestFailureBlock)failBlock;

/*
 *  简历处理
 *   result : REJECT  ACCEPT
 *   desc:   信息描述
 */
+ (void)resumeDealOrderId:(NSInteger)id_
                   result:(NSString *)result
                    desc:(NSString *)desc
                 success:(RequestSuccessBlock)succBlock
                 failure:(RequestFailureBlock)failBlock;

/*
 *  提交offer 信息
 *
 */
+ (void)submitOfferInfo:(NSDictionary *)info
                  success:(RequestSuccessBlock)succBlock
                  failure:(RequestFailureBlock)failBlock;

/*
 * hr提交入职
 *      info 二维码信息
 */
+ (void)submitInfoForRntry:(NSString *)info
                   success:(RequestSuccessBlock)succBlock
                   failure:(RequestFailureBlock)failBlock;

/*
 *  查看简历
 *      id  tanlent Id
 */
+ (void)getTalentDetailWithId:(NSInteger)id_
                      success:(RequestSuccessBlock)succBlock
                      failure:(RequestFailureBlock)failBlock;

/*
 * 提交评分
 *
 *   rate  评分
 *   id  猎头id
 */

+ (void)submitRate:(CGFloat)rate
            withId:(NSInteger)id_
           success:(RequestSuccessBlock)succBlock
           failure:(RequestFailureBlock)failBlock;

/*
 *  候选人未入职
 *    id   订单id
 */
+ (void)talentNotRegister:(NSInteger)orderId
                  success:(RequestSuccessBlock)succBlock
                  failure:(RequestFailureBlock)failBlock;

/*
 *  候选人未转正
 *    id   订单id
 */
+ (void)talentNotPositionWithId:(NSInteger)orderId
                        success:(RequestSuccessBlock)succBlock
                        failure:(RequestFailureBlock)failBlock;

/*
 *  候选人转正
 *    id   订单id
 */
+ (void)talentPositionWithId:(NSInteger)orderId
                        success:(RequestSuccessBlock)succBlock
                        failure:(RequestFailureBlock)failBlock;

/*
 *  红点已读提交
 *      id  订单id
 */
+ (void)orderReadedWithId:(NSInteger)id_
                  success:(RequestSuccessBlock)succBlock
                  failure:(RequestFailureBlock)failBlock;


/*
 *  获取短信验证码
 */
+ (void)getSMSVerifyCodeWithPhone:(NSString *)phone
                             type:(NSString *)type
                          Success:(RequestSuccessBlock)succBlock
                          failure:(RequestFailureBlock)failBloc;


/*
 *  获取职位列表
 */
+ (void)getPostionDetailWithId:(NSInteger)positionId
                       Success:(RequestSuccessBlock)succBlock
                       failure:(RequestFailureBlock)failBlock;


/*
 *  支付接口
 *   param    amount  支付金额
 *             body   描述
 *              outTradeNo   订单描述
 *              subject    标题
 */
+ (void)payWithAmount:(CGFloat)amount
                 body:(NSString *)body
           outTradeNo:(NSString *)bianhao
              subject:(NSString *)title
              success:(RequestSuccessBlock)succBlock
              failure:(RequestFailureBlock)failBlock;

@end

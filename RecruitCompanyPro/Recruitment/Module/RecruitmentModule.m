//
//  RecruitmentModule.m
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/11.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "RecruitmentModule.h"
#import "HRModel.h"
#import "NSSafeMutableDictionary.h"
#import "JobModel.h"
#import "JobRequirementModel.h"
#import "JobResponsibilityModel.h"
#import "ListPostionModel.h"
#import "CandidateModel.h"
#import "JobOrderDetailModel.h"

@implementation RecruitmentModule

+ (void)getCompanyListWithPageNum:(NSInteger)pageNum
                         success:(RequestSuccessBlock)succBlock
                         failure:(RequestFailureBlock)failBlock{
    NSDictionary * para = @{@"page" : @(pageNum),
                            @"size":@(100)};
    NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithDictionary:para];
   
    [NetworkHandle getRequestForApi:@"company/companies" mockObj:nil params:dic handle:^NSArray *(NSDictionary *response) {
        NSArray * response_ = (NSArray *)response;
        if (response_) {
            NSArray * models = [NSArray modelArrayWithClass:[CompanyModel class] json:[response modelToJSONObject]];
            return @[@YES,models];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}


+ (void)registerWithCompanyId:(NSInteger)companyId
                        phone:(NSString *)phone
                     password:(NSString *)password
                   verifyCode:(NSString *)verificationCode
                      success:(RequestSuccessBlock)succBlock
                      failure:(RequestFailureBlock)failBlock{
    NSDictionary * para = @{@"companyId" : @(companyId),
                            @"phone":phone,
                            @"password":password,
                            @"verificationCode":verificationCode
                            };
    NSMutableDictionary * dic = [NSMutableDictionary dictionaryWithDictionary:para];
    
    [NetworkHandle postRequestForApi:@"auth/hrRegister" mockObj:nil params:dic handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        if (response_) {
            HRModel * model = [HRModel modelWithDictionary:response_];
            return @[@YES,model];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}



+ (void)submitPositonWithEducation:(NSString *)education
                         jobNature:(NSString *)jobNature
                             level:(NSString *)level
                          position:(NSString *)position
                             price:(NSInteger)price
                        profession:(NSString *)profession
                  recruitingNumber:(NSInteger)recruitingNumber
                       requirement:(NSArray *)requirement
                  responsibilities:(NSArray *)responsibilities
                       salaryRange:(NSString *)salaryRange
                             title:(NSString *)title
                          workArea:(NSString *)workArea
                    workExperience:(NSString *)workExperience
                            success:(RequestSuccessBlock)succBlock
                            failure:(RequestFailureBlock)failBlock{
    
    NSSafeMutableDictionary * dic = [NSSafeMutableDictionary dictionary];
    
    [dic setObject:education forKey:@"education"];
    [dic setObject:jobNature forKey:@"jobNature"];
    [dic setObject:level forKey:@"level"];[dic setObject:position forKey:@"position"];
    [dic setObject:@(price/100) forKey:@"price"];
    [dic setObject:profession forKey:@"profession"];
    [dic setObject:@(recruitingNumber) forKey:@"recruitingNumber"];
    [dic setObject:requirement forKey:@"requirement"];
    [dic setObject:responsibilities forKey:@"responsibilities"];
    [dic setObject:salaryRange forKey:@"salaryRange"];
    [dic setObject:title forKey:@"title"];
    [dic setObject:workArea forKey:@"workArea"];
    [dic setObject:workExperience forKey:@"workExperience"];
    
    [NetworkHandle postRequestForApi:@"jobDetail/create" mockObj:nil params:(NSDictionary *)dic handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        if (response_) {
            JobModel * model = [JobModel modelWithDictionary:response_];
            return @[@YES,model];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)positionCloseWithId:(NSInteger)positionId
                    success:(RequestSuccessBlock)succBlock
                    failure:(RequestFailureBlock)failBlock{
    NSSafeMutableDictionary * dic = [NSSafeMutableDictionary dictionary];
    [dic setObject:@(positionId) forKey:@"id"];
    [NetworkHandle postRequestForApi:@"jobDetail/close" mockObj:nil params:(NSDictionary *)dic handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        if (response_) {
            JobModel * model = [JobModel modelWithDictionary:response_];
            return @[@YES,model];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];

}

+ (void)getPositionList:(NSInteger)page
                success:(RequestSuccessBlock)succBlock
                failure:(RequestFailureBlock)failBlock{
    NSDictionary * dic = @{@"page":@(page), @"sort":@"createTime,desc"};

    [NetworkHandle getRequestForApi:@"jobDetail/getMyJobDetails" mockObj:nil params:dic handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        NSArray * content = response_[@"content"];
        if (content) {
            NSArray * models = [NSArray modelArrayWithClass:[ListPostionModel class] json:[content modelToJSONObject]];
            return @[@YES,models];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)submintPositionInterViewWihtId:(NSInteger)id_
                                result:(NSString *)result
                                  desc:(NSString *)desc
                               success:(RequestSuccessBlock)succBlock
                               failure:(RequestFailureBlock)failBlock{
    NSDictionary * dic = @{@"id":@(id_),@"result":result,@"reason":desc};
    [NetworkHandle postRequestForApi:@"jobDetailOrder/saveInterviewResult" mockObj:nil params:dic handle:^NSArray *(NSDictionary *response) {
        return @[@YES];
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}


+ (void)postionMappingWithTagArray:(NSArray *)tags
                           success:(RequestSuccessBlock)succBlock
                           failure:(RequestFailureBlock)failBlock{
    
    NSMutableString * string = [NSMutableString string];
    [string appendString:@"["];
    for (int i=0;i<tags.count;i++) {
        id obj = tags[i];
        if ([obj isKindOfClass:[NSString class]]) {
            [string appendFormat:@"%@",obj];
        }else{
            [string appendFormat:@"%@",[obj stringValue]];
        }
        if (i != tags.count-1) {
            [string appendString:@","];
        }else{
            [string appendString:@"]"];
        }
    }
    
//    NSString * string = [NSString stringWithFormat:@"[%@]",[tags componentsJoinedByString:@","]];
    NSDictionary * dic = @{@"tags":[BaseHelper encodeString:string]};
    [NetworkHandle getRequestForApi:@"jobDetailConfig/getJobDetailConfigs" mockObj:nil params:dic handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        if(!response_){
            return @[@NO];
        }
        NSArray * requirements = response_[@"requirements"];
        NSArray * responsibilities = response_[@"responsibilities"];
        
        NSMutableDictionary * result = [NSMutableDictionary dictionary];
        
        if (requirements) {
            NSArray * requirementArray = [NSArray modelArrayWithClass:[JobRequirementModel class] json:[requirements modelToJSONObject]];
            if (requirementArray) {
                [result setObject:requirementArray forKey:@"require"];
            }
        }
        if (responsibilities) {
            NSArray * responsibilitiesModels  = [NSArray modelArrayWithClass:[JobResponsibilityModel class] json:[responsibilities modelToJSONObject]];
            if (responsibilitiesModels) {
                [result setObject:responsibilitiesModels forKey:@"responsibility"];
            }
        }
        return @[@YES,result];
        
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)submitPositionWithBaseInfo:(NSDictionary *)baseInfo
                         descArray:(NSArray *)descArray
                         respArray:(NSArray *)respArray
                           success:(RequestSuccessBlock)succBlock
                           failure:(RequestFailureBlock)failBlock{
    
    NSMutableDictionary * para = [NSMutableDictionary dictionary];
    if (baseInfo) {
        [para addEntriesFromDictionary:baseInfo];
    }
    if (descArray) {
        [para setObject:descArray forKey:@"requirementList"];
    }
    if (respArray) {
        [para setObject:respArray forKey:@"responsibilityList"];
    }
 
    [NetworkHandle postRequestForApi:@"jobDetail/create" mockObj:nil params:para handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        if (response_) {
            JobModel * model = [JobModel modelWithDictionary:response_];
            return @[@YES,model];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
    
}

+ (void)updataPositionWithBaseInfo:(NSDictionary *)baseInfo
                         descArray:(NSArray *)descArray
                         respArray:(NSArray *)respArray
                           success:(RequestSuccessBlock)succBlock
                           failure:(RequestFailureBlock)failBlock{
    
    NSMutableDictionary * para = [NSMutableDictionary dictionary];
    if (baseInfo) {
        [para addEntriesFromDictionary:baseInfo];
    }
    if (descArray) {
        [para setObject:descArray forKey:@"requirementList"];
    }
    if (respArray) {
        [para setObject:respArray forKey:@"responsibilityList"];
    }
    
    [NetworkHandle postRequestForApi:@"jobDetail/modify" mockObj:nil params:para handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        if (response_) {
            JobModel * model = [JobModel modelWithDictionary:response_];
            return @[@YES,model];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)publicPositionWithId:(NSInteger)id_
                     payType:(NSString *)payType
                        note:(NSString *)note
                     success:(RequestSuccessBlock)succBlock
                     failure:(RequestFailureBlock)failBlock{
    NSDictionary * dic = @{@"id":@(id_),@"payType":payType,@"comment":note?note:@""};
    [NetworkHandle postRequestForApi:@"jobDetail/publish" mockObj:nil params:dic handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        if (response_) {
            JobModel * model = [JobModel modelWithDictionary:response_];
            return @[@YES,model];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)getPositionCandidateList:(NSInteger)page
                     id_:(NSInteger)id_
                success:(RequestSuccessBlock)succBlock
                failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * dic = @{@"jobDetailId":@(id_),@"page":@(page),@"size":@(10)};
    [NetworkHandle getRequestForApi:@"jobDetailOrder/candidates" mockObj:nil params:dic handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        NSArray * content = response_[@"content"];
        if (content) {
            NSArray * modelstArray = [NSArray modelArrayWithClass:[CandidateModel class] json:[content modelToJSONObject]];
            return @[@YES,modelstArray];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)getCandidateDetailWithId:(NSInteger)orderId
                         success:(RequestSuccessBlock)succBlock
                         failure:(RequestFailureBlock)failBlock{
    
    [NetworkHandle getRequestForApi:[NSString stringWithFormat:@"jobDetailOrder/getJobTaskOrderItem/%ld",orderId] mockObj:nil params:nil handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        if (response_) {
            JobOrderDetailModel * model = [JobOrderDetailModel modelWithDictionary:response_];
            return @[@YES,model];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)payForReward:(NSInteger)id_
          verifyCode:(NSString *)verifyCode
             success:(RequestSuccessBlock)succBlock
             failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * dic = @{@"id":@(id_),@"verificationCode":verifyCode};
    [NetworkHandle postRequestForApi:@"jobDetailOrder/confirmCommission" mockObj:nil params:dic handle:^NSArray *(NSDictionary *response) {
        return @[@YES];
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)resumeDealOrderId:(NSInteger)id_
                   result:(NSString *)result
                     desc:(NSString *)desc
                  success:(RequestSuccessBlock)succBlock
                  failure:(RequestFailureBlock)failBlock{
 
    NSDictionary * dic = @{@"id":@(id_),@"reason":desc,@"result":result};
    [NetworkHandle postRequestForApi:@"jobDetailOrder/acceptCandidate" mockObj:nil params:dic handle:^NSArray *(NSDictionary *response) {
        return @[@YES];
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)submitOfferInfo:(NSDictionary *)info
                success:(RequestSuccessBlock)succBlock
                failure:(RequestFailureBlock)failBlock{
    
    [NetworkHandle postRequestForApi:@"jobDetailOrder/saveOffer" mockObj:nil params:info handle:^NSArray *(NSDictionary *response) {
        NSDictionary * info = (NSDictionary *)response;
        NSString * url = info[@"url"];
        if (url) {
            return @[@YES,url];
        }else{
            return @[@YES];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)submitInfoForRntry:(NSString *)info
                   success:(RequestSuccessBlock)succBlock
                   failure:(RequestFailureBlock)failBlock{
    NSDictionary * dic = @{@"id":info};
    [NetworkHandle postRequestForApi:@"jobDetailOrder/candidateRegister" mockObj:nil params:dic handle:^NSArray *(NSDictionary *response) {
        return @[@YES];
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)getTalentDetailWithId:(NSInteger)talentId
                      success:(RequestSuccessBlock)succBlock
                      failure:(RequestFailureBlock)failBlock{
    
    NSString * api = [NSString stringWithFormat:@"human/resume/%ld",talentId];
    
//    mockData = [RecruitMock createWithFunctionName:NSStringFromSelector(_cmd)];
    [NetworkHandle getRequestForApi:api mockObj:nil params:nil handle:^NSArray *(NSDictionary *response) {
        NSDictionary * response_ = (NSDictionary *)response;
        if (response_) {
            TalentModel * model = [TalentModel modelWithDictionary:response_];
            return @[@YES,model];
        }else{
            return @[@NO];
        }
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:failBlock];
}

+ (void)submitRate:(CGFloat)rate
            withId:(NSInteger)id_
           success:(RequestSuccessBlock)succBlock
           failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * dic = @{@"id":@(id_),@"comment":@"",@"evaluateType":@"HR",@"score":@(rate)};
    [NetWorkHelper postRequestForApi:@"jobDetailOrder/evaluate" params:dic handle:^NSArray *(id response) {
        return @[@YES];
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:false];
}

+ (void)talentNotRegister:(NSInteger)orderId
                  success:(RequestSuccessBlock)succBlock
                  failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * dic = @{@"id":@(orderId)};
    [NetWorkHelper postRequestForApi:@"jobDetailOrder/candidateNoRegister" params:dic handle:^NSArray *(id response) {
        return @[@YES];
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:false];
}
+ (void)talentNotPositionWithId:(NSInteger)orderId
                        success:(RequestSuccessBlock)succBlock
                        failure:(RequestFailureBlock)failBlock{
 
    NSDictionary * dic = @{@"id":@(orderId)};
    [NetWorkHelper postRequestForApi:@"jobDetailOrder/candidateNoPositive" params:dic handle:^NSArray *(id response) {
        return @[@YES];
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:false];
}

+ (void)talentPositionWithId:(NSInteger)orderId
                        success:(RequestSuccessBlock)succBlock
                        failure:(RequestFailureBlock)failBlock{
    
    NSDictionary * dic = @{@"id":@(orderId)};
    [NetWorkHelper postRequestForApi:@"jobDetailOrder/candidatePositive" params:dic handle:^NSArray *(id response) {
        return @[@YES];
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:false];
}

+ (void)orderReadedWithId:(NSInteger)id_
                  success:(RequestSuccessBlock)succBlock
                  failure:(RequestFailureBlock)failBlock{
    NSDictionary * dic = @{@"id":@(id_),@"requestType":@"HR"};
    [NetWorkHelper postRequestForApi:@"jobDetailOrder/resetChangeStateFlag" params:dic handle:^NSArray *(id response) {
        return @[@YES];
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:false];
}

+ (void)getSMSVerifyCodeWithPhone:(NSString *)phone
                             type:(NSString *)type
                          Success:(RequestSuccessBlock)succBlock
                        failure:(RequestFailureBlock)failBloc{
    NSDictionary * dic = @{@"cellphone":phone,@"type":type,@"platformType":@"HR"};
    [NetWorkHelper postRequestForApi:@"auth/sendCode" params:dic handle:^NSArray *(id response) {
        return @[@YES];
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:false];
}

+ (void)getPostionDetailWithId:(NSInteger)positionId
                       Success:(RequestSuccessBlock)succBlock
                       failure:(RequestFailureBlock)failBlock{
    
    NSString * api = [NSString stringWithFormat:@"jobDetail/%ld",positionId];
    [NetWorkHelper getRequestForApi:api params:nil handle:^NSArray *(id response) {
        if (response) {
            JobModel * model = [JobModel modelWithDictionary:response];
            return @[@YES,model];
        }
        return @[@YES];
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:false];
}


+ (void)payWithAmount:(CGFloat)amount
                 body:(NSString *)body
           outTradeNo:(NSString *)bianhao
              subject:(NSString *)title
              success:(RequestSuccessBlock)succBlock
              failure:(RequestFailureBlock)failBlock{
    
    NSMutableDictionary * param = [NSMutableDictionary dictionary];
    [param setObject:@(amount) forKey:@"amount"];
    if (body) {
        [param setObject:body forKey:@"body"];
    }
    if (bianhao) {
        [param setObject:bianhao forKey:@"outTradeNo"];
    }
    if (title) {
        [param setObject:title forKey:@"subject"];
    }
    [param setObject:@"HR" forKey:@"platformType"];
    [NetWorkHelper postRequestForApi:@"aliPay/unifiedOrder" params:param handle:^NSArray *(id response) {
        NSDictionary * info = response;
        NSString * orderString = info[@"orderStr"];
        return @[@YES,orderString?orderString:@""];
    } success:^(id response) {
        if(succBlock){
            succBlock(response);
        }
    } failure:false];

}
@end

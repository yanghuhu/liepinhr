//
//  BaseViewController.m
//  MVVMStartUp
//
//  Created by Michael on 2017/10/25.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "BaseViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // 点击页面背景隐藏键盘
    [IQKeyboardManager sharedManager].shouldResignOnTouchOutside = YES;

    [self setNavigationBackItem];
    [self createViews];
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createViews {
    // write me
}

- (void)loadData {
    // write me
}

- (void)setNavigationBackItem{

    UIBarButtonItem *backItem = [[UIBarButtonItem alloc]initWithTitle:@""
                                                                style:UIBarButtonItemStylePlain
                                                               target:self
                                                               action:@selector(navigationBackAction)];
    self.navigationController.navigationBar.tintColor = RGB16(0xffffff);
    self.navigationController.navigationBar.backIndicatorImage = [UIImage imageNamed:@"back"];
    self.navigationController.navigationBar.backIndicatorTransitionMaskImage = [UIImage imageNamed:@"back"];
    self.navigationItem.backBarButtonItem = backItem;
}

- (void)navigationBackAction{
    UIViewController * vc = self.navigationController.viewControllers[0];
    if (self == vc) {
        [self dismissViewControllerAnimated:YES completion:^{
            nil;
        }];
    }else{
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)netFailWihtError:(NSString *)error andStatusCode:(NSInteger)statusCode{
    if (error) {
        [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        return;
    }
    [BaseHelper showProgressHud:[NSString stringWithFormat:@"网络错误码：%ld",statusCode] showLoading:NO canHide:YES];
}
@end

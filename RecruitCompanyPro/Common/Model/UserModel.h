//
//  UserModel.h
//  HSBCTempPro
//
//  Created by Michael on 2017/10/26.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserModel : NSObject

@property (nonatomic, assign) NSInteger  uid;   // 用户id
@property (nonatomic, strong) NSString * nickName;  //用户昵称
@property (nonatomic, strong) NSString * icon;     //用户头像
@property (nonatomic, assign) NSInteger gender;     //用户头像

@end

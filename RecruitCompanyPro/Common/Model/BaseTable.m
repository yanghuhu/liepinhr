//
//  BaseTable.m
//  JLGProject
//
//  Created by Michael on 2017/3/18.
//  Copyright © 2017年 yang. All rights reserved.
//

#import "BaseTable.h"
#import "DataDefault.h"

@implementation BaseTable


- (BOOL)cleanTable {
    return [self cleanTableImp:[DBHelper sharedDBHelper].imp];
}

- (BOOL)cleanTableImp:(DBHelperImp *)imp {
    if ([self respondsToSelector:@selector(createTableIfNotExist)]) {
        [self performSelector:@selector(createTableIfNotExist)];
    }
    return [imp deleteAllDataFromTable:self.tableName];
}

- (NSInteger)currentTableVersion {
    return [DataDefault integerForKey:[NSString stringWithFormat:@"%@TableVersion",self.tableName] isPrivate:YES];
}

- (void)setCurrentTableVersion:(NSInteger)version {
    [DataDefault setInteger:version forKey:[NSString stringWithFormat:@"%@TableVersion",self.tableName] isPrivate:YES];
}

- (void)createAndUpgradeTable
{
    if ([self respondsToSelector:@selector(createTableIfNotExist)]) {
        [self performSelector:@selector(createTableIfNotExist)];
    }
    
    if ([self respondsToSelector:@selector(updateTableAccordingToDbVersion)]) {
        [self performSelector:@selector(updateTableAccordingToDbVersion)];
    }
}

- (DBHelperImp *)getPublicDb
{
    DBHelperImp *publicImp = [[DBHelperImp alloc] initWithoutRole];
    return publicImp;
}

- (NSString *)whereContainWithDictionary:(NSDictionary *)dictionary{
    NSSafeMutableDictionary * dic = [NSSafeMutableDictionary dictionary];
    [dic addEntriesFromDictionary:dictionary];
    return  [[DBHelper sharedDBHelper].imp whereStrByAttrAndValue:dic];
}

@end

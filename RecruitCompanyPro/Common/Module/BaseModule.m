//
//  BaseModule.m
//  JLGProject
//
//  Created by yang on 2017/3/1.
//  Copyright © 2017年 yang. All rights reserved.
//

#import "BaseModule.h"

@implementation BaseModule

+ (NSDictionary *)reqestParam:(NSDictionary *)info{

    if (!info) {
        info = [NSDictionary dictionary];
    }
    
    NSMutableDictionary * dictionary = [NSMutableDictionary dictionaryWithDictionary:info];
    if ([HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel) {
        [dictionary setObject:[NSNumber numberWithInteger:[HSBCGlobalInstance sharedHSBCGlobalInstance].curUserModel.id_] forKey:@"uid"];
    }
    [dictionary setObject:@"IOS" forKey:@"device"];
    
    return dictionary;
}

@end

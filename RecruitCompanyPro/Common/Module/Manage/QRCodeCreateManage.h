//
//  QRCodeCreateManage.h
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/14.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreImage/CoreImage.h>

@interface QRCodeCreateManage : NSObject

// 使用string 信息生成二维码
- (UIImage *)createWithString:(NSString *)string;

@end

//
//  QRCodeScanManage.h
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/13.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@protocol QRCodeScanManageDelegate
// 二维码扫描成功
- (void)qrScanSuccess:(NSString *)qrInfo;
@end

@interface QRCodeScanManage : NSObject<AVCaptureMetadataOutputObjectsDelegate>

@property (nonatomic , strong) UIView * supView;
@property (nonatomic , weak) id<QRCodeScanManageDelegate>delegate;

- (void)initSet;
- (void)userFinish;

@end

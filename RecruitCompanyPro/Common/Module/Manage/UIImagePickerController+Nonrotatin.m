//
//  UIImagePickerController+Nonrotatin.m
//  LiePinSellPro
//
//  Created by Michael on 2018/3/2.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "UIImagePickerController+Nonrotatin.h"

@implementation UIImagePickerController (Nonrotating)

- (NSInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscape;
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden{
    return YES;
}
@end

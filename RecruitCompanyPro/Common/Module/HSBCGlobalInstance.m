//
//  HSBCGlobalInstance.m
//  HSBCDemo
//
//  Created by Michael on 2017/10/25.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "HSBCGlobalInstance.h"

@implementation HSBCGlobalInstance

DEFINE_SINGLETON_FOR_CLASS(HSBCGlobalInstance)

-(NSArray *)hangyeArray{
    if (!_hangyeArray) {
        _hangyeArray =  @[@"计算机"];
    }
    return _hangyeArray;
}

- (NSArray *)educationArray{
    if (!_educationArray) {
        _educationArray = @[@"大专及以上",
                            @"本科及以上",
                            @"研究生及以上"];
    }
    return _educationArray;
}
- (NSArray *)jingyanArray{
    if (!_jingyanArray) {
        _jingyanArray = @[@"1至3年",
                          @"3至5年",
                          @"5至7年"];
    }
    return _jingyanArray;
}
- (NSArray *)levelArray{
    if (!_levelArray) {
        _levelArray = @[@"初级",@"中级",@"高级"];
    }
    return _levelArray;
}
- (NSArray *)xingzhiArray{
    if (!_xingzhiArray) {
        _xingzhiArray = @[@"兼职",@"全职"];
    }
    return _xingzhiArray;
}

- (NSArray *)positionArray{
    if (!_positionArray) {
        _positionArray = @[@"Java软件开发",@"IOS软件开发",@"其他"];
    }
    return _positionArray;
}

@end

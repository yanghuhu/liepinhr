//
//  HSBCGlobalInstance.h
//  HSBCDemo
//
//  Created by Michael on 2017/10/25.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "Commondefine.h"
#import "HRModel.h"
#import "JobOrderDetailModel.h"

@interface HSBCGlobalInstance : NSObject

DECLARE_SINGLETON_FOR_CLASS(HSBCGlobalInstance)

@property (nonatomic,strong) HRModel * curUserModel;

@property (nonatomic , strong) NSString * netWorkToken;

@property (nonatomic , assign)  BOOL needMock;

@property (nonatomic , strong) NSArray * hangyeArray; // 行业
@property (nonatomic , strong) NSArray * positionArray;  // 职位
@property (nonatomic , strong) NSArray * educationArray;  // 学历
@property (nonatomic , strong) NSArray * jingyanArray;   // 经验
@property (nonatomic , strong) NSArray * levelArray;     // 级别
@property (nonatomic , strong) NSArray * xingzhiArray;   // 性质

@property (nonatomic , assign) BOOL isShowLibrary;  // 是否显示相册

@end

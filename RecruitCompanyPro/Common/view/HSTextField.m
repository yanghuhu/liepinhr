//
//  HSTextField.m
//  RecruitCompanyPro
//
//  Created by Michael on 2017/11/29.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "HSTextField.h"

@implementation HSTextField

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setStype];
    }
    return self;
}

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setStype];
    }
    return self;
}

- (void)setStype{
    self.borderStyle = UITextBorderStyleNone;
    self.layer.borderColor = COLOR(220, 220, 220, 1).CGColor;
    self.layer.borderWidth = 1;
    
    UIView * view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 1)];
    self.leftViewMode = UITextFieldViewModeAlways;
    self.leftView = view;
}

@end

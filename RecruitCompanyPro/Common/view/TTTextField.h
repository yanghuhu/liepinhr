//
//  TTTextField.h
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/12.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>

@class TTTextField;

@protocol TTTextFieldDelegate

- (void)textFieldDidBeginEditing:(TTTextField *)tf;
- (void)ttTextFieldDidEndEditing:(TTTextField *)tf;
- (BOOL)textField:(TTTextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
@end

@interface TTTextField : UIView
@property (nonatomic , strong)   UITextField * tf;
@property (nonatomic , strong) NSString * title;
@property (nonatomic , strong) NSString * placeholder;
@property (nonatomic , strong) NSString * text;
@property (nonatomic , assign) BOOL isTfHadText;
@property (nonatomic , strong) UIFont * titleFont;
@property(nonatomic) UIKeyboardType keyboardType;


@property (nonatomic , weak) id<TTTextFieldDelegate>delegate;
@end

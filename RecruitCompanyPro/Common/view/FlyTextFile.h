//
//  FlyTextFile.h
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/11.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <Foundation/Foundation.h>

@class FlyTextFile;

@protocol FlyTextFileDelegate
@optional

- (BOOL)textField:(FlyTextFile *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
- (BOOL)textFieldShouldReturn:(FlyTextFile*)textField;

@end

@interface FlyTextFile : UIView

@property (nonatomic , weak) id<FlyTextFileDelegate>delegate;

@property (nonatomic , strong) UIFont * Titlefont;   // title字体大小
@property (nonatomic , strong) UIFont * font;       // 字体样式
@property (nonatomic , strong) NSString * text;     //  内容

@property(nonatomic) UIReturnKeyType returnKeyType;
@property(nonatomic) UIKeyboardType keyboardType;

@property (nullable, nonatomic,copy)   NSString *placeholder;

@property(nonatomic,getter=isSecureTextEntry) BOOL secureTextEntry;
@property (nonatomic , assign) CGFloat titleLbH;
@property (nonatomic , assign) BOOL needDropDownList;

- (void)createSubViews;

@end

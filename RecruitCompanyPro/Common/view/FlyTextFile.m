//
//  FlyTextFile.m
//  RecruitCompanyPro
//
//  Created by Michael on 2017/12/11.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import "FlyTextFile.h"
#import "UIView+YYAdd.h"

@interface FlyTextFile()<UITextFieldDelegate>{
    
    UILabel * headerLb;
    UITextField * tf;
}
@end

@implementation FlyTextFile
- (void)createSubViews{
    CGFloat bottomLbh = 2;
    headerLb = [[UILabel alloc]init];
    headerLb.font = [UIFont systemFontOfSize:13];
    headerLb.textColor = [UIColor whiteColor];
    [self addSubview:headerLb];
    [headerLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self);
        make.right.equalTo(self);
        make.height.mas_equalTo(_titleLbH);
        make.top.equalTo(self).mas_offset(_titleLbH);
    }];
    
    tf = [[UITextField alloc] init];
    tf.backgroundColor = [UIColor clearColor];
    tf.font = _font;
    tf.textColor = [UIColor whiteColor];
    tf.delegate = self;
    tf.borderStyle = UITextBorderStyleNone;
    [self addSubview:tf];
    [tf mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self);
        make.right.equalTo(self);
        make.top.equalTo(self).mas_offset(_titleLbH);
        make.bottom.equalTo(self).mas_offset(-bottomLbh);
    }];
    
    UIImageView * bottomLine = [[UIImageView alloc] init];
    bottomLine.image = [UIImage imageNamed:@"AccountBottom"];
    [self addSubview:bottomLine];
    [bottomLine mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self);
        make.right.equalTo(self);
        make.bottom.equalTo(self);
        make.height.mas_equalTo(bottomLbh);
    }];
}

- (void)setSecureTextEntry:(BOOL)secureTextEntry{
    _secureTextEntry = secureTextEntry;
    tf.secureTextEntry = secureTextEntry;
}

- (void)setFont:(UIFont *)font{
    _font = font;
    headerLb.font = font;
    tf.font = font;
}

- (NSString *)text{
    return tf.text;
}

- (void)setText:(NSString *)text{
    tf.text = text;
    [self textFieldShouldBeginEditing:tf];
}

- (void)setNeedDropDownList:(BOOL)needDropDownList{
    _needDropDownList = needDropDownList;
}

- (void)setPlaceholder:(NSString *)placeholder{
    headerLb.text = placeholder;
}

- (void)setReturnKeyType:(UIReturnKeyType)returnKeyType{
    tf.returnKeyType = returnKeyType;
}

- (void)setKeyboardType:(UIKeyboardType)keyboardType{
    tf.keyboardType = keyboardType;
}

#pragma mark -- UITextFieldDelegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    [UIView animateWithDuration:0.2 animations:^{
        [headerLb mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self);
        }];
        headerLb.font = _Titlefont;
        [self layoutIfNeeded];
        headerLb.textColor = [UIColor lightGrayColor];
    } completion:^(BOOL finished) {
        
    }];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    if (!textField.text || textField.text.length == 0) {
       
        [UIView animateWithDuration:0.5 animations:^{
            headerLb.textColor = [UIColor whiteColor];
            [headerLb mas_updateConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self).mas_offset(20);
            }];
            headerLb.font = _font;
            [self layoutIfNeeded];
        } completion:^(BOOL finished) {
            NSLog(@"%@",NSStringFromCGRect(headerLb.frame));
        }];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;   // return NO to
{
    if (!self.delegate) {
        return YES;
    }
    return  [self.delegate textField:self shouldChangeCharactersInRange:range replacementString:string];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (!self.delegate) {
        return NO;
    }
    return  [self.delegate textFieldShouldReturn:self];
}

@end

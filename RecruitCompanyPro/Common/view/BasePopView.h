//
//  BasePopView.h
//  HSBCTempPro
//
//  Created by Michael on 2017/11/16.
//  Copyright © 2017年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BasePopView : UIView{
    UIControl * bkControl;    
}


- (UIView *)supView:(UIView *)view;
//显示   supView:父view
- (void)showWithSupView:(UIView *)supView;
// 隐藏
- (void)dismissAction;
// 添加右上角取消按钮
- (void)addTopRightDismissBt;

@end

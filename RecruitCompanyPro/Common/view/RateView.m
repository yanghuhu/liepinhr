//
//  RateView.m
//  HSBCTempPro
//
//  Created by Michael on 2018/1/12.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import "RateView.h"
#import "GradeView.h"
#import "RecruitmentModule.h"

@interface RateView(){
    
    GradeView * gradeView;
}
@end

@implementation RateView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
//        self.backgroundColor = COLOR(50, 53, 51, 1);
        [self createSubViews];
    }
    return self;
}

- (void)createSubViews{
    
    
    UIImageView * bkImageV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"popBack"]];
    [self addSubview:bkImageV];
    [bkImageV mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.and.top.and.bottom.equalTo(self);
    }];
    
    UIButton * closeBt = [UIButton buttonWithType:UIButtonTypeCustom];
    [closeBt addTarget:self action:@selector(dismissAction) forControlEvents:UIControlEventTouchUpInside];
    [closeBt setImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
    [self addSubview:closeBt];
    [closeBt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).mas_offset(VerPxFit(16));
        make.right.equalTo(self).mas_offset(-HorPxFit(30));
        make.size.mas_equalTo(CGSizeMake(HorPxFit(50), HorPxFit(50)));
    }];

    UILabel * titleLb = [[UILabel alloc] init];
    titleLb.text = @"订单已完成";
    titleLb.textAlignment = NSTextAlignmentCenter;
    titleLb.font = [UIFont boldSystemFontOfSize:17];
    titleLb.textColor = [UIColor whiteColor];
    [self addSubview:titleLb];
    [titleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX);
        make.top.equalTo(self).mas_offset(VerPxFit(40));
        make.left.and.right.equalTo(self);
    }];
    
    UIImageView * view = [[UIImageView alloc] init];
    view.backgroundColor = [UIColor grayColor];
    [self addSubview:view];
    [view mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(titleLb);
        make.width.mas_equalTo(90);
        make.top.mas_equalTo(titleLb.mas_bottom).mas_offset(VerPxFit(8));
        make.height.mas_equalTo(1);
    }];
    
    UILabel * subTitleLb = [[UILabel alloc] init];
    subTitleLb.text = @"请评价一下和您沟通的猎头吧";
    subTitleLb.textAlignment = NSTextAlignmentCenter;
    subTitleLb.font = [UIFont systemFontOfSize:15];
    subTitleLb.textColor = [UIColor whiteColor];
    [self addSubview:subTitleLb];
    [subTitleLb mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.and.right.equalTo(self);
        make.top.mas_equalTo(titleLb.mas_bottom).offset(VerPxFit(35));
        make.height.mas_equalTo(25);
    }];
    
    UIButton * bt = [UIButton buttonWithType:UIButtonTypeCustom];
    [bt addTarget:self action:@selector(sureAction) forControlEvents:UIControlEventTouchUpInside];
    [bt setBackgroundImage:[UIImage imageNamed:@"btBk"] forState:UIControlStateNormal];
    [bt setTitle:@"提交" forState:UIControlStateNormal];
    bt.titleLabel.font = [UIFont systemFontOfSize:15];
    [self addSubview:bt];
    [bt mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.mas_equalTo(self).mas_offset(-VerPxFit(60));
        make.height.mas_equalTo(PopBtHeight);
        make.width.mas_equalTo(PopBtWidth);
        make.centerX.mas_equalTo(self.mas_centerX);
    }];
    
    gradeView = [[GradeView alloc] initWithHeight:25 sourece:5];
    [self addSubview:gradeView];
    [gradeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.mas_equalTo(self.mas_centerX);
        make.bottom.mas_equalTo(bt.mas_top).offset(-VerPxFit(40));
        make.size.mas_equalTo(CGSizeMake(CGRectGetWidth(gradeView.frame), 30));
    }];
}

- (void)sureAction{
    
    [BaseHelper showProgressLoading];
    [RecruitmentModule submitRate:gradeView.source withId:_model.id_ success:^{
        [BaseHelper hideProgressHud];
        [BaseHelper showProgressHud:@"提交成功" showLoading:NO canHide:YES];
        [self.delegate rateFinish];
        [self dismissAction];
    } failure:^(NSString *error, ResponseType responseType) {
        [BaseHelper hideProgressHud];
        if (error) {
            [BaseHelper showProgressHud:error showLoading:NO canHide:YES];
        }
    }];
}

@end

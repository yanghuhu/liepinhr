//
//  RateView.h
//  HSBCTempPro
//
//  Created by Michael on 2018/1/12.
//  Copyright © 2018年 Michael. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BasePopView.h"
#import "JobOrderDetailModel.h"

@protocol RateViewDelegate
- (void)rateFinish;
@end

@interface RateView : BasePopView


@property (nonatomic , strong) JobOrderDetailModel * model;
@property (nonatomic , weak) id<RateViewDelegate>delegate;

@end

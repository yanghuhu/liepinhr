//
//  DBHelper.h
//  JLG_StartUp
//
//  Created by yang on 2017/2/10.
//  Copyright © 2017年 yang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DBHelperImp.h"
#import "FMDatabase.h"
#import "FMDatabaseQueue.h"

#define kDataBaseVersion                                1

@interface DBHelper : NSObject

@property (nonatomic, readonly) DBHelperImp *imp;

+ (DBHelper *)sharedDBHelper;

/**
 *  统一生成所有Table单例，包含生成表与升级表
 */
- (void)createAllTable;

/**
 *  删除所有表
 */
- (void)dropAllTable;

/**
 * 生成特定用户数据库
 */
- (id)initWithRole:(UserModel *)role;

/**
 *  生成无用户数据库
 */
- (id)initWithoutRole;


- (id)initWithDataBase:(FMDatabase *)db andDBQueue:(FMDatabaseQueue *)dbQueue;
/**
 *  清除数据库
 */
- (void)clearDBForRole:(UserModel *)role;



@end

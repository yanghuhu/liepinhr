//
//  DBHelper.m
//  JLG_StartUp
//
//  Created by yang on 2017/2/10.
//  Copyright © 2017年 yang. All rights reserved.
//

#import "DBHelper.h"
#import "DBHelperImp.h"
#import <objc/runtime.h>
#import "DataDefault.h"
#import "BaseTable.h"

//所有表名，用来统一初生和kill的表类名，记得有新的表往这里面加。

#define kAllTableName @[@"UserTable"]

@interface DBHelper()

@end

@implementation DBHelper

//static DBHelper* shareDBHelper;
//SYNTHESIZE_SINGLETON_FOR_CLASS(DBHelper)

//在单元测试里，这个单例会跑两次，http://stackoverflow.com/questions/21014843/ios-testing-dispatch-once-get-called-twice-first-in-app-second-in-test-probl
static DBHelper *jlgSharedDBHelper = nil;

+ (DBHelper *)sharedDBHelper
{
    @synchronized(self)
    {
        if (jlgSharedDBHelper == nil)
        {
            jlgSharedDBHelper = [[self alloc] init];
        }
    }
    return jlgSharedDBHelper;
}

+ (id)allocWithZone:(NSZone *)zone
{
    @synchronized(self)
    {
        if (jlgSharedDBHelper == nil)
        {
            jlgSharedDBHelper = [super allocWithZone:zone];
            return jlgSharedDBHelper;
        }
    }
    return nil;
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

- (void)invalide {
    //    _imp = nil;
}

- (id)initWithRole:(UserModel *)role {
    self = [super init];
    if (self) {
        _imp = [[DBHelperImp alloc] initWithRole:role];
    }
    return self;
}

- (id)initWithoutRole {
    self = [super init];
    if (self) {
        _imp = [[DBHelperImp alloc] initWithoutRole];
    }
    return self;
}

- (id)initWithDataBase:(FMDatabase *)db andDBQueue:(FMDatabaseQueue *)dbQueue {
    self = [super init];
    if (self) {
        _imp = [[DBHelperImp alloc] initWithDataBase:db andDBQueue:dbQueue];
    }
    return self;
}

- (void)clearDBForRole:(UserModel *)role {
    NSArray *documentPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,  NSUserDomainMask,YES);
    NSString *ourDocumentPath = [documentPaths objectAtIndex:0];
    
    NSString * uid = [NSString stringWithFormat:@"%ld", (long)role.uid];
    
    NSString *filename = [NSString stringWithFormat:@"JLG_1_%@",uid];
    NSString *filenameEnc = [NSString stringWithFormat:@"%@_enc", filename];
    NSString *fullPathEnc = [ourDocumentPath stringByAppendingFormat:@"/%@.db",filenameEnc];
    NSArray *arrKeys = [[[NSUserDefaults standardUserDefaults] dictionaryRepresentation] allKeys];
    NSString *tableKeyFiler = [DataDefault getRoleKeyWithRole:role key:@"TableVersion" isPrivate:YES];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF ENDSWITH %@",tableKeyFiler];
    NSArray *allTableKey =
    [arrKeys filteredArrayUsingPredicate:predicate];
    for (NSString *tableKey in allTableKey) {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:tableKey];
    }
    [[NSFileManager defaultManager] removeItemAtPath:fullPathEnc error:nil];
}



/**
 *  统一生成所有Table单例，包含生成表与升级表
 */
- (void)createAllTable {
    for (NSString *tableName in kAllTableName) {
        Class class = NSClassFromString(tableName);
        NSString *singletonSelStr = [NSString stringWithFormat:@"shared%@", tableName];
        SEL singletonSel = NSSelectorFromString(singletonSelStr);
        if (class && [class respondsToSelector:singletonSel]) {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Warc-performSelector-leaks"
            BaseTable *classObj = [class performSelector:singletonSel];
            if (classObj) {
                [classObj createAndUpgradeTable];
            }
#pragma clang diagnostic pop
        }else {
            DLog(@"error: no table named \"%@\"", tableName);
        }
    }
}

- (void)dropAllTable
{
    for (NSString *tableName in kAllTableName) {
        if (![self.imp dropTable:tableName]) {
            DLog(@"删除表(%@)失败",tableName);
        }
    }
}

@end
